(ns fonetica-port.views.c16
  "Chapter 16"
  (:require [fonetica-port.views.components.shared :as shared :refer [ipa femp red ipa-like]]
            [reitit.frontend.easy :as rfe]))

(defn ra1
  "Respostos activity 1"
  []
  [:section.ra1
   [:li "Escreva o símbolo do alofone descrito pelos seguintes traços fonéticos."
    (let [items [["oclusivo"
                  "velar"
                  "sonoro"
                  "[g]"]
                 ["oclusivo"
                  "bilabial"
                  "surdo"
                  "[p]"]
                 ["oclusivo"
                  "dental"
                  "sonoro"
                  "[d]"]
                 ["oclusivo"
                  "bilabial"
                  "sonoro"
                  "[b]"]
                 ["oclusivo"
                  "dental"
                  "surdo"
                  "[t]"]
                 ["oclusivo"
                  "bilabial"
                  "surdo"
                  "[k]"]]
          [litems ritems] (partition (* 0.5 (count items)) items)
          make-twocol (fn  [[la lb lc r]]
                        [:li
                         [:div.grid2
                          [:div.l [:div la] [:div lb] [:div lc]]
                          [:div.r.red.ipa-like.is-size-4 r]]])]
      [:ol.list.is-lower-alpha
       [:div.grid2
        (into [:div.l] (map make-twocol litems))
        (into [:div.r] (map make-twocol ritems))]])]])

(defn ra2
  "Respostos activity 2"
  []
  [:section.ra2
   [:li "Indique os traços fonéticos de cada alofone da lista."
    (let [items [
                 ["[k]"
                  "oclusivo"
                  "velar"
                  "surdo"]

                 ["[t]"
                  "oclusivo"
                  "dental"
                  "surdo"]

                 ["[p]"
                  "oclusivo"
                  "bilabial"
                  "surdo"]

                 ["[g]"
                  "oclusivo"
                  "velar"
                  "sonoro"]

                 ["[d]"
                  "oclusivo"
                  "dental"
                  "sonoro"]

                 ["[b]"
                  "oclusivo"
                  "bilabial"
                  "sonoro"]
                 ]
          [litems ritems] (partition (* 0.5 (count items)) items)
          make-twocol (fn  [[l ra rb rc]]
                        [:li
                         [:div.grid2
                          [:div.l.ipa-like.is-size-4 l]
                          [:div.r.red
                           [:div ra]
                           [:div rb]
                           [:div rc]]]])]
      [:ol.list.is-lower-alpha
       [:div.grid2
        (into [:div.l] (map make-twocol litems))
        (into [:div.r] (map make-twocol ritems))]])]])

(defn ra3
  "Respostos activity 3"
  []
  [:section.ra3
   [:li "Faça uma cadeia fonética, mudando um só traço de cada vez."
    [:div.red "As respostas podem variar; só é preciso tomar cuidado de mudar um só traço de cada vez."]]])

(defn ra4
  "Respostas activity 4"
  []
  [:section.ra4
   [:li "Para as seguintes configurações de VOT (início de vibração das cordas vocais), identifique os
sons e em que idioma são produzidos assim."
    (let [items [["C16-RA4a.jpg" [:span "Os sons oclusivos surdos " [ipa "[p t k]"] " do português."]]
                 ["C16-RA4b.jpg" [:span "Os sons oclusivos sonoros " [ipa "[b d g]"] " do inglês."]]
                 ["C16-RA4c.jpg" [:span "Os sons oclusivos surdos " [ipa "[p t k]"] " do inglês."]]
                 ["C16-RA4d.jpg" [:span "Os sons oclusivos sonoros " [ipa "[b d g]"] " do português."]]]]

      (into [:ol.list.is-lower-alpha]
            (for [[src txt] items]
              [:li [:div.grid2
                    [shared/image {:full-url (shared/file src)
                                   :alt (str "graph of " txt)}]

                    [:div.red txt]]])))]])
    
(defn ra5
  "Respostas activity 5"
  []
  [:section.ra5
   [:li "Transcreva as seguintes palavras com os fonemas " [ipa "/p b/"] ", lembrando-se da possível necessidade de uma vogal epentética."
    (let [items [["cabe" "[kÒbi]" "[kÒbš]"] 
                 ["branco" [:span "[bRë" [:sup "N"] "ku]"] [:span "[bRë" [:sup "N"] "ku]"]] 
                 ["abster" "[abistÓR]" [:span "[ab" [:sup "š"] "stÓ³]"]] 
                 ["FAB" "[fÒbi]" [:span "[fÒb" [:sup "š"] "]"]] 
                 ["abismo" "[abÔzmu]" "[AbÔZmu]"] 
                 ["oblíquo" "[oblÔkwu]" "[OblÔkwu]"] 
                 ["capturar" "[kapiturÒR]" [:span "[kap" [:sup "š"] "turÒ³]"]] 
                 ["pano" "[pënu]" "[pA1nu]"] 
                 ["apical" "[apikÒK]" "[ApikÒL]"] 
                 ["CEP" "[c×pi]" [:span "[c×p" [:sup "i"] "]"]] 
                 ["implicar" [:span "[Þ" [:sup "m"] "plikÒR]"] [:span "[Þ" [:sup "m"] "plikÒ³]"]] 
                 ["aprovar" "[apRovÒR]" "[ApRuvÒ³]"]]]
      (into [:ol.list.is-lower-alpha
             [:div.grid3 [:span.blank] [:span.is-underlined "Portugal"] [:span.is-underlined "Brasil"]]]
            (for [[a b c] items]
              [:li.list-item [:div.grid3
                    [:span a]
                    [:span.ipa.red b]
                    [:span.ipa.red c]]])))]])

(defn ra6
  "Respostas activity 6"
  []
  [:section.ra6
   [:li "Transcreva as seguintes palavras com os fonemas " [ipa "/t d/"] ", lembrando-se dos casos em que ocorre vogal epentética e daqueles em que ocorre palatalização."
    (let [items [
                 ["bate" "[bÒÎi]" "[bÒtš]"] 
                 ["temo" "[tçmu]" "[tÓmu]"] 
                 ["time" "[Îèmi]" [:span "[tÓm" [:sup "š"] "]"]] 
                 ["atlas" "[ÒtlAs]" "[ÒtlAS]"] 
                 ["ritmo" "[hÔÎimu]" [:span "[³Ôt" [:sup "š"] "mu]"]] 
                 ["reto" "[h×tu]" "[³×tu]"] 
                 ["adiar" "[aËiÒR]" "[AdjÒ³]"] 
                 ["drama" "[dRëmA]" "[dRA1mA]"] 
                 ["rude" "[hÖËi]" [:span "[³Öd" [:sup "š"] "]"]] 
                 ["verdade" "[verdÒËi]" [:span "[v‰rdÒd" [:sup "š"] "]"]] 
                 ["duplo" "[dÖplu]" "[dÖplu]"] 
                 ["advogar" "[aËivogÒR]" [:span "[Ad" [:sup "š"] "vugÒ³]"]]]]
      (into [:ol.list.is-lower-alpha
             [:div.grid3 [:span.blank] [:span.is-underlined "Portugal"] [:span.is-underlined "Brasil"]]]
            (for [[a b c] items]
              [:li.list-item [:div.grid3
                    [:span a]
                    [:span.ipa.red b]
                              [:span.ipa.red c]]])))]])

(defn ra7
  "Respostas activity 7"
  []
  [:section.ra7
   [:li "Transcreva as seguintes palavras com os fonemas " [ipa "/k g/"] ", lembrando-se da possível necessidade de uma vogal epentética."
    (let [items [
                 ["aqui" "[akÔ]" "[AkÔ]"] 
                 ["casa" "[kÒzA]" "[kÒzA]"]
                 ["pacto" [:span "[pÒk" [:span.ipa2 [:sup "Ê"]] "t" [:span.ipa2 "Í"] "]"] [:span "[pÒk" [:span.ipa2 [:sup "Ê"]] "t" [:span.ipa2 "Í"] "]"]] 
                 ["fique" "[fÔki]" [:span "[fÔk" [:sup "š"] "]"]] 
                 ["claro" "[klÒRu]" "[klÒRu]"] 
                 ["acredita" "[akReËÔtA]" "[Akr‰dÔtA]"] 
                 ["globo" "[glÕbu]" "[glÕbu]"] 
                 ["paradigma" "[paraËÔgimA]" [:span "[pArAËÔg" [:sup "š"] "mA]"]] 
                 ["aguardo" "[agwÒRdu]" "[AgwÒRdu]"] 
                 ["grande" [:span "[gRë" [:sup "ø"] "Ëi]"] [:span "[gRë" [:sup "ª"] "d" [:sup "š"] "]"]] 
                 ["gula" "[gÖlA]" "[gÖlA]"] 
                 ["sangue" [:span "[së" [:sup "N"] "gi]"] [:span "[së" [:sup "N"] "g" [:sup "š"] "]"]] ]]
      (into [:ol.list.is-lower-alpha
             [:div.grid3 [:span.blank] [:span.is-underlined "Portugal"] [:span.is-underlined "Brasil"]]]
            (for [[a b c] items]
              [:li.list-item [:div.grid3
                    [:span a]
                    [:span.ipa.red b]
                              [:span.ipa.red c]]])))]])

(defn render-respostas
  "Chapter 16 respostas"
  []
  [:main.c16.respostas.content
   [shared/fonetica-title "Capítulo 16 — Respostos a las atividades"]
   [:section.atividades
    [:h2 "ATIVIDADES"]
    [:ol.atividades.list
     [ra1]
     [ra2]
     [ra3]
     [ra4]
     [ra5]
     [ra6]
     [ra7]]]])

(defn a1
  "Activity 1"
  []
  [:section.a1
   [:li "Escreva o símbolo do alofone descrito pelos seguintes traços fonéticos."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c16-r) :same-session]
    (let [items [["oclusivo"
                  "velar"
                  "sonoro"
                  "[g]"]
                 ["oclusivo"
                  "bilabial"
                  "surdo"
                  "[p]"]
                 ["oclusivo"
                  "dental"
                  "sonoro"
                  "[d]"]
                 ["oclusivo"
                  "bilabial"
                  "sonoro"
                  "[b]"]
                 ["oclusivo"
                  "dental"
                  "surdo"
                  "[t]"]
                 ["oclusivo"
                  "bilabial"
                  "surdo"
                  "[k]"]]
          [litems ritems] (partition (* 0.5 (count items)) items)
          make-twocol (fn  [[la lb lc r]]
                        [:li
                         [:div.grid2
                          [:div.l [:div la] [:div lb] [:div lc]]
                          [:div.r.red.ipa-like.is-size-4 ]]])]
      [:ol.list.is-lower-alpha
       [:div.grid2
        (into [:div.l] (map make-twocol litems))
        (into [:div.r] (map make-twocol ritems))]])]])

(defn a2
  "Activity 2"
  []
  [:section.a2
   [:li "Indique os traços fonéticos de cada alofone da lista."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c16-r) :same-session]
    (let [items [
                 ["[k]"
                  "oclusivo"
                  "velar"
                  "surdo"]

                 ["[t]"
                  "oclusivo"
                  "dental"
                  "surdo"]

                 ["[p]"
                  "oclusivo"
                  "bilabial"
                  "surdo"]

                 ["[g]"
                  "oclusivo"
                  "velar"
                  "sonoro"]

                 ["[d]"
                  "oclusivo"
                  "dental"
                  "sonoro"]

                 ["[b]"
                  "oclusivo"
                  "bilabial"
                  "sonoro"]
                 ]
          [litems ritems] (partition (* 0.5 (count items)) items)
          make-twocol (fn  [[l ra rb rc]]
                        [:li
                         [:div.grid2
                          [:div.l.ipa-like.is-size-4 l]
                          [:div.r.red]]])]
      [:ol.list.is-lower-alpha
       [:div.grid2
        (into [:div.l] (map make-twocol litems))
        (into [:div.r] (map make-twocol ritems))]])]])

(defn a3
  "Activity 3"
  []
  [:section.a3
   [:li "Faça uma cadeia fonética, mudando um só traço de cada vez."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c16-r) :same-session]
    (let [items [ "[d] > [ ] > [ ] > [ ]"
                 "[p] > [ ] > [ ] > [ ]"
                 "[b] > [ ] > [ ] > [ ]"
                 "[g] > [ ] > [ ] > [ ]"]]
      (into [:ol.list.is-lower-alpha]
            (for [i items]
              [:li.list-item [:div.ipa i]])))]])

(defn a4
  "Activity 4"
  []
  [:section.a4
   [:li "Para as seguintes configurações de VOT (início de vibração das cordas vocais), identifique os
sons e em que idioma são produzidos assim."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c16-r) :same-session]
    (let [items [["C16-RA4a.jpg" "Os sons oclusivos surdos " [ipa "[p t k]"] " do português."]
                 ["C16-RA4b.jpg" "Os sons oclusivos sonoros " [ipa "[b d g]"] " do inglês."]
                 ["C16-RA4c.jpg" "Os sons oclusivos surdos " [ipa "[p t k]"] " do inglês."]
                 ["C16-RA4d.jpg" "Os sons oclusivos sonoros " [ipa "[b d g]"] " do português."]]]

      (into [:ol.list.is-lower-alpha]
            (for [[src txt] items]
              [:li [:div.grid2
                    [shared/image {:full-url (shared/file src)
                                   :alt "graph of problem"}]

                    [:div.blank]]])))]])

(defn a5
  "Activity 5"
  []
  [:section.a5
   [:li "Transcreva as seguintes palavras com os fonemas " [ipa "/p b/"] ", lembrando-se da possível necessidade de uma vogal epentética."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c16-r) :same-session]
    (let [items [["cabe" "[kÒbi]" "[kÒbš]"] 
                 ["branco" "[bRë" [:sup "N"] "ku]" "[bRë" [:sup "N"] "ku]"] 
                 ["abster" "[abistÓR]" "[ab" [:sup "š"] "stÓ³]"] 
                 ["FAB" "[fÒbi]" "[fÒb" [:sup "š"] "]"] 
                 ["abismo" "[abÔzmu]" "[AbÔZmu]"] 
                 ["oblíquo" "[oblÔkwu]" "[OblÔkwu]"] 
                 ["capturar" "[kapiturÒR]" "[kap" [:sup "š"] "turÒ³]"] 
                 ["pano" "[pënu]" "[pA1nu]"] 
                 ["apical" "[apikÒK]" "[ApikÒL]"] 
                 ["CEP" "[c×pi]" "[c×p" [:sup "i"] "]"] 
                 ["implicar" "[Þ" [:sup "m"] "plikÒR]" "[Þ" [:sup "m"] "plikÒ³]"] 
                 ["aprovar" "[apRovÒR]" "[ApRuvÒ³]"]]]
      (into [:ol.list.is-lower-alpha
             [:div.grid3 [:span.blank] [:span.is-underlined "Portugal"] [:span.is-underlined "Brasil"]]]
            (for [[a b c] items]
              [:li.list-item [:div.grid3
                              [:span a]
                              [:span.blank]
                              [:span.blank]]])))]])

(defn a6
  "Activity 6"
  []
  [:section.a6
   [:li "Transcreva as seguintes palavras com os fonemas " [ipa "/t d/"] ", lembrando-se dos casos em que ocorre vogal epentética e daqueles em que ocorre palatalização."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c16-r) :same-session]
    (let [items [
                 ["bate" "[bÒÎi]" "[bÒtš]"] 
                 ["temo" "[tçmu]" "[tÓmu]"] 
                 ["time" "[Îèmi]" "[tÓm" [:sup "š"] "]"] 
                 ["atlas" "[ÒtlAs]" "[ÒtlAS]"] 
                 ["ritmo" "[hÔÎimu]" "[³Ôt" [:sup "š"] "mu]"] 
                 ["reto" "[h×tu]" "[³×tu]"] 
                 ["adiar" "[aËiÒR]" "[AdjÒ³]"] 
                 ["drama" "[dRëmA]" "[dRA1mA]"] 
                 ["rude" "[hÖËi]" "[³Öd" [:sup "š"] "]"] 
                 ["verdade" "[verdÒËi]" "[v‰rdÒd" [:sup "š"] "]"] 
                 ["duplo" "[dÖplu]" "[dÖplu]"] 
                 ["advogar" "[aËivogÒR]" "[Ad" [:sup "š"] "vugÒ³]"]]]
      (into [:ol.list.is-lower-alpha
             [:div.grid3 [:span.blank] [:span.is-underlined "Portugal"] [:span.is-underlined "Brasil"]]]
            (for [[a b c] items]
              [:li.list-item [:div.grid3
                              [:span a]
                              [:span.blank]
                              [:span.blank]]])))]])

(defn a7
  "Activity 7"
  []
  [:section.a7
   [:li "Transcreva as seguintes palavras com os fonemas " [ipa "/k g/"] ", lembrando-se da possível necessidade de uma vogal epentética."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c16-r) :same-session]
    (let [items [
                 ["aqui" "[akÔ]" "[AkÔ]"] 
                 ["casa" "[kÒzA]" "[kÒzA]"] 
                 ["pacto" [:span "[pÒk" [:span.ipa2 [:sup "Ê"]] "t" [:span.ipa2 "Í"] "]"] [:span "[pÒk" [:span.ipa2 [:sup "Ê"]] "t" [:span.ipa2 "Í"] "]"]] 
                 ["fique" "[fÔki]" "[fÔk" [:sup "š"] "]"] 
                 ["claro" "[klÒRu]" "[klÒRu]"] 
                 ["acredita" "[akReËÔtA]" "[Akr‰dÔtA]"] 
                 ["globo" "[glÕbu]" "[glÕbu]"] 
                 ["paradigma" "[paraËÔgimA]" "[pArAËÔg" [:sup "š"] "mA]"] 
                 ["aguardo" "[agwÒRdu]" "[AgwÒRdu]"] 
                 ["grande" "[gRë" [:sup "ø"] "Ëi]" "[gRë" [:sup "ª"] "d" [:sup "š"] "]"] 
                 ["gula" "[gÖlA]" "[gÖlA]"] 
                 ["sangue" "[së" [:sup "N"] "gi]" "[së" [:sup "N"] "g" [:sup "š"] "]"] ]]
      (into [:ol.list.is-lower-alpha
             [:div.grid3 [:span.blank] [:span.is-underlined "Portugal"] [:span.is-underlined "Brasil"]]]
            (for [[a b c] items]
              [:li.list-item [:div.grid3
                              [:span a]
                              [:span.blank]
                              [:span.blank]]])))]])

(defn atividades
  "Chapter 16 atividades"
  []
  [:section.atividades.content
   [:h2 "ATIVIDADES"]
   [:ol.atividades
    [a1]
    [a2]
    [a3]
    [a4]
    [a5]
    [a6]
    [a7]]])

(defn recursos
  "Materials"
  []
  [:section.recursos
   [:h3 "RECURSOS ELETRÔNICOS"]
   (let [audio-items [
                      [:span "Pares mínimos de fonemas oclusivos."]
                      [:span "As palavras {caça} e {classe}."]
                      [:span "Vogais epentéticas em posição final de palavras estrangeiras."]
                      [:span "Vogais epentéticas em acrônimos."]
                      [:span "Vogais epentéticas em posição interior de palavra."]
                      [:span "A palatalização de " [ipa "/t d/"] " diante da vogal " [ipa "[i]"] "."]
                      [:span "A palatalização de " [ipa "/t d/"] " em inglês."]
                      [:span "A palavra {pá} de português e {paw} do inglês."]
                      [:span "A palavra {bê} de português e {bay} do inglês."]
                      [:span "Pares mínimos entre os fonemas oclusivos em posição inicial de palavra."]
                      [:span "A palavra {papai}."]
                      [:span "A palavra {apto}."]
                      [:span "Vogal epentética depois de " [ipa "/p/"] " final."]
                      [:span "O " [ipa "/p/"] " inglês em posição final."]
                      [:span "A palatalização do fonema " [ipa "/t/"] "."]
                      [:span "A palavra {tatu}."]
                      [:span "A palavra {ritmo}."]
                      [:span "Vogal epentética depois de " [ipa "/t/"] " final."]
                      [:span "A palavra {bate}."]
                      [:span "Variações na pronúncia de " [ipa "[Î]"] "."]
                      [:span "Variações na pronúncia do fonema " [ipa "/t/"] " do inglês."]
                      [:span "As palavras {atmosfera} e {natural} do português e {not you} e {natural} do inglês."]
                      [:span "As palavras {cara}, {foro} e {mero}."]
                      [:span "As palavras {quis} e {cubo}."]
                      [:span "A palavra {acabo}."]
                      [:span "A palavra {pacto}."]
                      [:span "Vogal epentética depois de " [ipa "/k/"] " final."]
                      [:span "O " [ipa "/k/"] " inglês em posição inicial e final."]
                      [:span "As palavras {boto} e {voto}."]
                      [:span "As palavras {sob} e {sub}."]
                      [:span "A palavra {bebê}."]
                      [:span "A palavra {abnega}."]
                      [:span "Vogal epentética depois de " [ipa "/b/"] " final."]
                      [:span "A pronúncia dialetal de {obrigado} no norte de Portugal."]
                      [:span "A palatalização do fonema " [ipa "/d/"] "."]
                      [:span "A palavra {dado}."]
                      [:span "A palavra {advir}."]
                      [:span "Vogal epentética depois de " [ipa "/d/"] " final."]
                      [:span "A palavra {dica}."]
                      [:span "Variações na pronúncia do fonema " [ipa "/d/"] " do inglês."]
                      [:span "A palavra {durante} do português e {during} e {did you} do inglês."]
                      [:span "As palavras {guisa} e {gula}."]
                      [:span "A palavra {gagá}."]
                      [:span "A palavra {dogma}."]
                      [:span "Vogal epentética depois de " [ipa "/g/"] " final."]]
         ex-items [[:span "Exercícios de pronúncia do fonema " [ipa "/p/"] "."]
                   [:span "Exercícios de pronúncia do fonema " [ipa "/t/"] "."]
                   [:span "Exercícios de pronúncia do fonema " [ipa "/k/"] "."]
                   [:span "Exercícios de pronúncia do fonema " [ipa "/b/"] "."]
                   [:span "Exercícios de pronúncia do fonema " [ipa "/d/"] "."]
                   [:span "Exercícios de pronúncia do fonema " [ipa "/g/"] "."]
                   [:span "Exercícios de pronúncia dos fonemas oclusivos surdos e sonoros."]]
         fonetica-line-item (fn [n i]
                              [:li.list-item [shared/media-icon :audio
                                    (shared/file (str "C16-M" (inc n) ".mp3"))] i])
         fonetica-line-item2 (fn [n i]
                               [:li.list-item [shared/media-icon :ex
                                     (shared/file (str "C16-M" (+ 46 n) ".mp3"))] i])]
     (into
      [:ol.list]
      (conj 
       (map-indexed fonetica-line-item2 ex-items)
       (map-indexed fonetica-line-item audio-items))))])

(defn render
  "Chapter sixteen view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 16 — Os fonemas oclusivos"
    "eResource"]
   [:div.content.chapter-16
    [recursos]
    [atividades]]
   [shared/footer]])
