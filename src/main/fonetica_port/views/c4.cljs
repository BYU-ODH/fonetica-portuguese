(ns fonetica-port.views.c4
  "Chapter 4"
  (:require [fonetica-port.views.components.shared :as shared :refer [ipa femp]]
            [reitit.frontend.easy :as rfe]))

(defn render-m1
  "Chapter 4 m1 Sumerian"
  []
  [:main.c4.recursos.content
   [shared/fonetica-title "Texto em cuneiforme suméria"]
   [:p "A frase abaixo vem de uma inscrição em alguns tijolos de um templo sumério que diz que Hamurabi (rei da Babilônia) era o rei para deus sobre a terra." [:sup "1"] " Pode-se ver que o nome é representado por distintos símbolos para cada sílaba."]
   [:img {:src (shared/file "C4-M1-sumerian-inscription.jpg")}]
   [:p.endnote {:style  {"marginTop" "2em"}} [:sup "1"] " L. W. King, " [:em "The Letter and Inscriptions of Hammurabi"]" (London: Luzac, 1898) vol. 1, p. 124, 183."]])

(defn render-m2
  "Chapter 4 m2 Egyptian"
  []
  [:main.c4.recursos.content
   [shared/fonetica-title "Texto em hieroglifos egípcios"]
   [:p "O seguinte exemplo textual" [:sup "1"] " contém a aplicação ao egípcio da inovação suméria dos dois tipos de determinativos. O primeiro tipo é o " [:strong "determinativo semântico"]", que serve para especificar o significado de um logograma anterior. No exemplo de abaixo, o logograma , que representa ‘leme/timão’, passa a significar ‘voz’ quando é seguido pelo determinativo semântico ‘homem’. Dessa maneira, certos símbolos compostos passaram a representar conceitos específicos diferentes."]
   [:img {:src (shared/file "C4-M2-egyptian-writing.jpg")}] 
   [:p.endnote {:style  {"margin-top" "2em"}} [:sup "1"] " W. V. Davies, " [:em "Egyptian Hieroglyphs"] " (London: British Museum, 1987), p. 43."]])

(defn render-m3
  "Chapter 4 m3 Chinese"
  []
  [:main.c4.recursos.content
   [shared/fonetica-title "Texto em caracteres chineses"]
   [:p "Como demonstra o exemplo textual abaixo," [:sup "1"] " a orientação da escrita chinesa é de cima para baixo em colunas lidas da direita para a esquerda. O texto, que vem das analectos de Confúcio, traduz-se como: “O mestre disse: ‘Na linguagem, só é preciso comunicar o significado’.” Cada caractere do texto é seguido do significado, transliteração e transcrição fonética em mandarim."]
   [:img {:src (shared/file "C4-M3-chinese-writing.jpg")}]
   [:p.endnote {:style {"margin-top" "2em"}} [:sup "1"] " James Legge, " [:em "The Chinese Classics"] " (Hong Kong: Hong Kong University Press, 1960) vol. 1 (" [:em "Confucian Analects"] "), p. 305."]])

(defn render-m4
  "Chapter 4 m4 Mediterranean"
  []
  [:main.c4.recursos.content
   [shared/fonetica-title "Texto en escritura silábica mediterránea"]
   [:p "O seguinte exemplo textual" [:sup "1"] " com transliteração e tradução, vem de uma estatueta de bronze descoberta em Sevilha:"]
   [:img {:src (shared/file "C4-M4-feniciotext.png")}]
   [:p "Esta inscrição fenício-ibérica data da primeira metade do século oito antes de Cristo. O texto completo louva a deusa Astarte por ouvir as orações dirigidas a ela."]
   [:p.endnote {:style {"margin-top" "2em"}} [:sup "1"] " Frank Moore Cross, Jr., “The Old Phoenician Inscription from Spain Dedicated to Hurrián
Astarte,” " [:em "Harvard Theological Review"] " 64 (1971): 189-90."]])

(defn render-m5
  "Chapter 4 m5 Japanese"
  []
  [:main.c4.recursos.content
   [shared/fonetica-title "Os silabários japoneses"]
   [:img {:src (shared/file "C4-M5-japanese.png")}]])

(defn render-m6
  "Chapter 4 m6 Arabic"
  []
  [:main.c4.recursos.content
   [shared/fonetica-title "El alfabeto árabe"]
   [:p "No árabe, diferentes alógrafos representam o mesmo grafema quando ocorre isoladamente ou em posição inicial, interior ou final de palavra. É preciso lembrar também que a escrita árabe é da direita para a esquerda."]

   (let [rows [["'alif" "ا" "ﺎ" "ﺎ" "ا" "[?]"]
               ["baa'" "ﺑ" "ﺒ" "ﺐ" "ب" "[b]"]
               ["taa'" "ﺗ" "ﺘ" "ﺖ" "ت" "[t]"]
               ["thaa'" "ﺛ" "ﺜ" "ﺚ" "ث" "[T]"]
               ["jiim" "ﺟ" "ﺠ" "ﺞ" "ج" "[Ë Z F g]"]
               ["Haa'" "ﺣ" "ﺤ" "ﺢ" "ح" "[• —]"]
               ["khaa'" "ﺧ" "ﺨ" "ﺦ" "خ" "[X x]"]
               ["daal" "د" "ﺪ" "ﺪ" "د" "[d]"]
               ["dhaal" "ذ" "ﺬ" "ﺬ" "ذ" "[D]"]
               ["raa'" "ر" "ﺮ" "ﺮ" "ر" [:span "[r r" [:sup "Â"]"]"]]
               ["zaay" "ز" "ﺯ" "ﺯ" "ز" "[z]"]
               ["siin" "ﺳ" "ﺴ" "ﺲ" "س" "[s]"]
               ["shiin" "ﺷ" "ﺸ" "ﺶ" "ش" "[S]"]
               ["Saad" "ﺻ" "ﺼ" "ﺺ" "ص" [:span "[s""" [:sup "Â"] "]"]]
               ["Daad" "ﺿ" "ﻀ" "ﺾ" "ض" [:span "[d" [:sup "Â"] " z" [:sup "Â"] "]"]]
               ["Taa'" "ﻃ" "ﻄ" "ﻂ" "ط" [:span "[t" [:sup "Â"] "]"]]
               ["dhaa'" "ﻇ" "ﻈ" "ﻆ" "ظ" [:span "[D" [:sup "Â"] "]"]]
               ["ain" "ﻋ" "ﻌ" "ﻊ" "ع" "[Â]"]
               ["ghain" "ﻏ" "ﻐ" "ﻎ" "غ" "[³ G]"]
               ["faa'" "ﻓ" "ﻔ" "ﻒ" "ف" "[f]"]
               ["qaaf" "ﻗ" "ﻘ" "ﻖ" "ق" "[q]"]
               ["kaaf" "ﻛ" "ﻜ" "ﻚ" "ك" "[k]"]
               ["laam" "ﻟ" "ﻠ" "ﻞ" "ل" "[l œ]"]
               ["miim" "ﻣ" "ﻤ" "ﻢ" "م" "[m]"]
               ["nuun" "ﻧ" "ﻨ" "ﻦ" "ن" "[n]"]
               ["haa'" "ﻫ" "ﻬ" "ﻪ" "ه" "[h]"]
               ["waaw" "و" "ﻮ" "ﻮ" "و" "[w]"]
               ["yaa'" "ﻳ" "ﻴ" "ﻲ" "ي" "[j]"]]]
     [:table.table.ftable.is-bordered
      [:thead
       [:tr
        [:td "Nome"]
        [:td "Posição inicial"]
        [:td "Posição medial"]
        [:td "Posição final"]
        [:td "Posição isolada"]
        [:td "Valor fonético"]]]
      (into [:tbody]
            (for [[a b c d e f] rows]
              [:tr [:td a] [:td b] [:td c] [:td d] [:td e] [:td.ipa f]]))])])

(defn render-m7
  "Chapter 4 m7 Phonecian"
  []
  [:main.c4.recursos.content
   [shared/fonetica-title "El alfabeto fenicio"]
   [:p "O alfabeto fenício consistia em 22 grafemas consonantais, que se apresentam abaixo com a devida correspondência fonológica."]
   [:img {:src (shared/file "C4-M7-alfabetofenicio.png")}]])

(defn render-m8
  "Chapter 4 m8 Hebrew"
  []
  (let [hebrew-top ["{" "z" "y" "x" "w" "u" "s" "r" "q" "o" "m" "l" "j" "i" "h" "g" "f" "e" "k" "c" "b" "a"]
        ipa-bottom ["t" "š" "r" "k" "t" "s" "p" "Â" "s" "n" "m" "l" "x" "y" "t" "h" "z" "w" "h" "d" "g" "b" "?"]
        hebrew-table
        [:table.table
         (into [:tr.hebrew] (for [h hebrew-top] [:td h]))
         (into [:tr.ipa] (for [i ipa-bottom] [:td i]))]
        hebrew-screenshot [:img {:src (shared/file "C4-M8_hebrew.png")} ]
        hebrew2-screenshot [:img {:src (shared/file "C4-M8_hebrew2.png")}]]
    
    [:main.c4.recursos.content
     [:section.m8-1
      [shared/fonetica-title "El alfabeto hebraico"]
      [:p "O alfabeto hebraico consistia em 22 grafemas consonantais, que se apresentam com uma correspondência fonológica."]
      ;;hebrew-table
      hebrew-screenshot]
     [:section.m8-2
      [shared/fonetica-title "Texto em alfabeto hebraico"]
      [:p "O seguinte exemplo textual, com sua transliteração e tradução, é de um poema de Todros Abuláfia, que viveu en Toledo.1 No poema, que data do século XIII, emprega-se o alfabeto consonantal hebraico para representar o espanhol."]
      hebrew2-screenshot]
     [:p.endnote {:style {"margin-top" "2em"}} [:sup "1"] "L. P. Harvey, " [:em "Hispano-Arabic Strophic Poetry: Studies by Samuel Miklos Stern"] "(Oxford: Clarendon Press, 1974) p. 146."]]))

(defn render-m9
  "Chapter 4 m9 Iberian"
  []
  [:main.c4.recursos.content
   [:section.m9-1
    [shared/fonetica-title "Texto grego"]
    [:p "A língua grega foi instrumento de transmissão da cultura grega por todo o mediterrâneo, que inclui a Península Ibérica. Muito do que se sabe da Ibéria por volta do tempo de Cristo deve- se ao geógrafo grego Estrabão, que escreveu sobre todo o mundo conhecido pelos gregos e romanos da época. O seguinte texto introduz seus comentários sobre a Ibéria:" [:sup "1"]]
    [:div.has-text-centered
     [:p.is-size-5 "Πρῶτον δὲ μέρος αὐτῆς ἐστι τὸ ἑσπέριον, ὡς ἔφαμεν, ἡ ᾽Ιβηρία."]
     [:p.is-size-6 "Prōton de meros autēs esti to Hesperion, hōs efamen, hē Ibēria."]
     [:p.is-size-7 "Como dizia, a primeira parte de Europa é a ocidental, a saber, Ibéria."]]]
   [:p.endnote {:style {"margin-top" "2em"}} [:sup "1"] "Strabo," [:em "The Geography of Strabo"] "(London: William Heinemann, 1969) vol. 2, p. 2-3"]])

(defn render-m10
  "Chapter 4 m10 Latin"
  []
  [:main.c4.recursos.content
   [:section.m10
    [shared/fonetica-title "Texto en latín en el afabeto romano"]
    [:p "O uso do Império Romano alcançou uma área geográfica incrivelmente extensa e o seu alfabeto foi adotado por um número admirável de idiomas. De Roma, o Império expandiu-se em todas as direções alcançando ao oeste as Ilhas Britânicas, ao leste Babilônia, ao norte o Rio Danúbio e ao sul a atual África do Norte."]
    [:p "A Península Ibérica, que adotou o latim e seu alfabeto no final do século III a. C., teve um papel importantíssimo no Império Romano, pois foi o berço de grandes filósofos, escritores e até imperadores. Sêneca, um dos maiores filósofos e escritores do Império, era natural de Córdoba.  Eis o que disse esse filósofo quanto à importância do silêncio para o estudo:" [:sup "1"]]
    [:div.has-text-centered
     [:p.is-size-5 "PEREAM SI EST TAM NECESSARIUM QUAM VIDETUR SILENTIUM IN STUDIA SEPOSITO"]
     [:p.is-size-6 "Morreria se o silêncio fosse tão essencial para o estudo em reclusão como parece."]]]
   [:p.endnote {:style {"margin-top" "2em"}} [:sup "1"] "L.Seneca," [:em " L. Annaei Senecae ad Lucilium Epistulae Morales"] " (Oxford: Oxford University Press, 1965) vol. 1, p. 147."]])

(defn render-m11
  "Chapter 4 m11 cyrillic"
  []
  [:main.c4.recursos.content
   [:section.m11
    [shared/fonetica-title "Texto em russo no alfabeto cirílico"]
    [:p "Um exemplo textual do russo, com sua transliteração e tradução, extraído do romance «Анна Каренина» (Anna Karenina), do escritor " [:strong "Лев Николаевич Толстой"] " (Lev NikolaevicÏ Tolstoj)." [:sup "1"]]
    [:div.has-text-centered
     [:p.is-size-5 [:strong "Все счастливые семьи похожи друг на друга, каждая несчастливая семья
несчастлива по-своему."]]
     [:p.is-size-6 "Vse scastlivye semji pokhoži drug na druga, každaya nescastlivaya semja nescastliva po-svoemu."
      [:br]
      "Todas as famílias felizes são iguais, toda família infeliz o é à sua própria maneira."]]]
   [:p.endnote {:style {"margin-top" "2em"}} [:sup "1"] "Лев Николаевич Толстой, " [:em "Анна Каренина"] " (New York: International University Press, 1900) p. 7."]])

(defn render
  "Chapter four view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 4 — Sistemas de escrita"
    "eResource"]
   [:div.content.chapter-4
    [:section.recursos
     [:h3 "RECURSOS ELETRÔNICOS"]
     [:ol 
      [:li [shared/media-icon :text (rfe/href :fonetica-port.routes/c4-m1) :same-session] "Texto em cuneiforme suméria."]
      [:li [shared/media-icon :text (rfe/href :fonetica-port.routes/c4-m2) :same-session] "Texto em hieroglifos egípcios."]
      [:li [shared/media-icon :text (rfe/href :fonetica-port.routes/c4-m3) :same-session] "Texto em caracteres chineses."]
      [:li [shared/media-icon :text (rfe/href :fonetica-port.routes/c4-m4) :same-session] "Texto em escrita silábica mediterrânea."]
      [:li [shared/media-icon :text (rfe/href :fonetica-port.routes/c4-m5) :same-session] "Os silabários japoneses."]
      [:li [shared/media-icon :text (rfe/href :fonetica-port.routes/c4-m6) :same-session] "O alfabeto árabe."]
      [:li [shared/media-icon :text (rfe/href :fonetica-port.routes/c4-m7) :same-session] "O alfabeto fenício."]
      [:li [shared/media-icon :text (rfe/href :fonetica-port.routes/c4-m8) :same-session] "Alfabeto e texto hebraicos."]
      [:li [shared/media-icon :text (rfe/href :fonetica-port.routes/c4-m9) :same-session] "Texto grego."]
      [:li [shared/media-icon :text (rfe/href :fonetica-port.routes/c4-m10) :same-session] "Texto em latim no alfabeto latino."]
      [:li [shared/media-icon :text (rfe/href :fonetica-port.routes/c4-m11) :same-session] "Texto em russo no alfabeto cirílico."]]]]
   
   [shared/footer]])
