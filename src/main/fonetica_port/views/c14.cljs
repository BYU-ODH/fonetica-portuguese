(ns fonetica-port.views.c14
  "Chapter 14"
  (:require [fonetica-port.views.components.shared :as shared :refer [ipa femp red]]
            [reitit.frontend.easy :as rfe]))

(defn ra1
  "Respostas Atividades 1"
  []
  [:section.ra1
   [:li "Identifique a solução fonética preferida para os encontros vocálicos nas seguintes palavras ou frases (fusão, ditongo/tritongo, sinérese/sinalefa, hiato). Depois, faça a transcrição fonética completa de cada item indicando se a transcrição segue a norma brasileira ou portuguesa."
    (let [items [[["ch" [:strong "á a"] "guado"] ["fusão alongada"] [" " [ipa "[SÒ:gwÒdu]"] ""]]
                 [["novel" [:strong "a o"] "culta"] ["elisão"] ["B " [ipa "[nov×lokÖKtA]"] " P " [ipa "[nuv×lOkÖLtA]"] ""]]
                 [["oit" [:strong "o a"] "tos"] ["ditongo crescente"] [" " [ipa "[ÕitwÒtus]"] " " [ipa "[ÕItwÒtuS]"] ""]]
                 [["olh" [:strong "e a"] "qui"] ["ditongo crescente"] [" " [ipa "[ØYjakÔ]"] ""]]
                 [["p" [:strong "á i"] "mensa"] ["ditongo decrescente"] [" " [ipa "[pÒImçsA]"] ""]]
                 [["ág" [:strong "io"]] ["ditongo crescente"] ["" [ipa "[ÒZju]"] ""]]
                 [["p" [:strong "oe"] "ma"] ["sinérese/hiato"] ["" [ipa "[po.Ó.mA]"] " " [ipa "[po`ÓmA]"] ""]]
                 [["r" [:strong "oí"] "do"] ["hiato natural/ditongo crescente"] ["B " [ipa "[ho.Ô.du]"] " P " [ipa "[³u.Ô.du]"]]]
                 [["l" [:strong "ê e"] "dições"] ["fusão alongada/ditongo decrescente"] ["B " [ipa "[lÓ:Ëiséãs]"] " P " [ipa "[lÓIdiséãS]"]]]
                 [["aul" [:strong "a i"] "ntensa"] ["elisão"] [ [:span.ipa "[ÒUlÞ" [:sup "ª"] "tçsA]"]]]
                 [["p" [:strong "ia"] "no"] ["hiato/ditongo crescente"] ["B " [ipa "[pi.á1.nu]"] " P " [ipa "[pjá1.nu]"]]]
                 [["av" [:strong "ó i"] "do sa"] ["ditongo decrescente"] ["B " [ipa "[avØIdØzA]"] " B " [ipa "[AvØIdØzA]"]]]
                 [["c" [:strong "oi"] "tado"] ["ditongo decrescente"] [ [ipa "[koItÒdu]"]]]
                 [["at" [:strong "ue"] "i"] ["hiato e ditongo decrescente/tritongo"] ["B " [ipa "[a.tu.ÓI]"] " P " [ipa "[A.twÓI]"]]]
                 [["p" [:strong "ó o"] "d" [:strong "io"] "so"] ["sinalefa/hiato—hiato/ditongo crescente"] ["B " [ipa "[pØ`oËi.Õzu]"] " P " [ipa "[pØ.odjÕ.zu]"]]]
                 [["aç" [:strong "aí"]] ["hiato natural"] ["B " [ipa "[asaÔ]"] " P " [ipa "[AsaÔ]"]]]
                 [["fas" [:strong "e i"] "nicial"] ["fusão simples"] ["B " [ipa "[fÒzinisiÒK]"] " P " [ipa "[fÒzinisjÒL]"]]]
                 [["d" [:strong "ue"] "lo"] ["hiato/ditongo crescente"] ["B " [ipa "[du.×.lu]"] " P " [ipa "[dw×.lu]"]]]
                 [["cont" [:strong "e a i"] "deia"] ["tritongo"] ["B " [ipa "[ké" [:sup "ø"] "ÎjaI.d×I.A]"] " P " [ipa "[ké" [:sup "ª"] ".tAI.dA1.jA]"]]]
                 [["qu" [:strong "e e"] "rro"] ["ditongo crescente/ fusão simples"] ["B " [ipa "[kjÓhu]"] " P " [ipa "[kÓ³u]"]]]
                 [["fal" [:strong "ou"]] ["ditongo decrescente"] ["B " [ipa "[falÕU]"] " P " [ipa "[fAlÕU]"]]]
                 [["f" [:strong "iei"] "s"] ["hiato/ditongo decrescente—tritongo"] ["B " [ipa "[fi.×Is]"] " P " [ipa "[fj×IS]"]]]
                 [["pag" [:strong "a h"] "oje"] ["sinalefa"] ["B " [ipa "[pÒgA`ÕZi]"] " P " [ipa "[pÒgA`ÕZš]"]]]
                 [["t" [:strong "eo"] "ria"] ["sinérese/ditongo crescente"] ["B " [ipa "[te`o.RÔ.A]"] " P " [ipa "[tju.RÔ.A]"]]]
                 [["s" [:strong "aí"] "mos"] ["hiato natural"] ["B " [ipa "[sa.Ô.mus]"] " P " [ipa "[sA.Ô.muS]"]]]
                 [["v" [:strong "i o"] "ntem"] ["hiato natural"] ["B " [ipa "[vÔ.é" [:sup "ª"] ".tçã]"] " P " [ipa "[vÔ.é" [:sup "ª"] ".tëã]"]]]]
          ra1-items (fn [[a b c]]
                      [:li.list-item
                       [:div.grid3
                        (into [:span] a)
                        (into [:span.red] b)
                        (into [:span.red] c)]])]
      (into [:ol.list.is-lower-alpha]
            (map ra1-items items)))]])

(defn ra2
  "Respostas Atividades 2"
  []
  [:section.ra2
   [:li "Indique a solução fonética preferida para as seguintes descrições de encontros vocálicos dentro da mesma palavra. Dê pelo menos dois exemplos de cada fenômeno, dando tanto a ortografia quanto a transcrição fonética."
    (let [headings ["Encontros vocálicos" "Fórmula" "Solução fonética" [:span "Exemplos" [red "*"]]]
          rows [["duas vogais homólogas"
                 "C14-RA2a.png"
                 "fusão com alongamento e mudança de amplitude e tom"]
                ["vogal alta átona seguida de outra vogal"
                 "C14-RA2b.jpg"
                 ["B: hiato (menos " [:em "kw gw"] ")"
                  "P: ditongo crescente"]]

                ["vogal não alta seguida de vogal alta átona"
                 "C14-RA2c.jpg"
                 "ditongo decrescente"]

                ["vogal alta átona seguida de vogal não alta seguida de vogal alta átona"
                 "C14-RA2d.jpg"
                 ["B: hiato mais ditongo decrescente (menos " [:em "kw gw"] ")"
                  "P: tritongo"]]

                ["duas vogais heterólogas não altas"
                 "C14-RA2e.png"
                 ["B: hiato (sinérese)"
                  "P: ditongo crescente por alçamento de vogais médias ou hiato"]]

                ["uma vogal alta tônica seguida de outra vogal"
                 "C14-RA2f.jpg"
                 "hiato nat"]]
          make-content (fn [cs]
                         (if (string? cs) [:td.red cs]
                             (into [:td.red]
                                   (for [c cs] [:p c]))))]
      [:table.table.is-bordered
       [:thead 
        (into [:tr]
              (for [h headings]
                [:th h]))]
       (into [:tbody]
             (for [[a b c] rows]
               [:tr
                [:td a]
                [:td [shared/image {:full-url (shared/file b)
                                    :alt (str "fullsize " c)}]]
                [make-content c]
                [:td.blank]]))])
    [red "* Os exemplos podem variar."]]])

(defn ra3
  "Respostas Atividades 3"
  []
  [:section.ra3
   [:li "Indique a solução fonética preferida para as seguintes descrições de encontros vocálicos entre duas palavras. Dê pelo menos dois exemplos de cada fenômeno, dando tanto a ortografia quanto a transcrição fonética."
    (let [headings ["Encontros vocálicos" "Fórmula" "Solução fonética" [:span "Exemplos" [red "*"]]]
          rows [[[:span "duas vogais homólogas (menos " [ipa "/a/"] ")"]
                 "C14-RA3a.png"
                 "fusão com alongamento e mudança de amplitude e tom"]

                [[:span "primeira palavra terminada em " [ipa "/a/"] " átona seguida de vogal átona"]
                 "C14-RA3b.png"
                 [:span "elisão da vogal " [ipa "[A]"]]]

                [[:span "primeira palavra terminada em " [ipa "/a/"] " átona seguida de vogal tônica"]
                 "C14-RA3c.png"
                 [:span "sinalefa com " [:span.ipa2 "[A`„]"]]]

                ["vogal alta átona seguida de outra vogal"
                 "C14-RA3d.jpg"
                 "ditongo crescente"]

                ["vogal não alta seguida de vogal alta átona"
                 "C14-RA3e.jpg"
                 "ditongo decrescente"]

                ["vogal alta átona seguida de vogal não alta seguida de vogal alta átona"
                 "C14-RA3f.jpg"
                 "tritongo"]

                ["duas vogais heterólogas foneticamente não altas"
                 "C14-RA3g.png" 
                 "hiato (ou sinalefa)"]

                ["uma vogal alta tônica seguida de outra vogal"
                 "C14-RA3h.jpg"
                 "hiato natural"]]
          make-content (fn [cs]
                         [:td.red cs])]
      [:table.table.is-bordered
       [:thead 
        (into [:tr]
              (for [h headings]
                [:th h]))]
       (into [:tbody]
             (for [[a b c] rows]
               [:tr
                [:td a]
                [:td [shared/image {:full-url (shared/file b)
                                    :alt (str "fullsize " c)}]]
                [make-content c]
                [:td.blank]]))])
    [red "* Os exemplos podem variar."]]])

(defn render-respostas
  "Chapter 14 respostas"
  []
  [:main.c14.respostas.content
   [shared/fonetica-title "Capítulo 14 — Encontros vocálicos"
    "Respostas para o eResource"]
   [:section.atividades
    [:h2 "ATIVIDADES"]
    [:ol.atividades.list
     [ra1]
     [ra2]
     [ra3]]]])

(defn a1
  "Atividades 1"
  []
  [:section.a1
   [:li "Identifique a solução fonética preferida para os encontros vocálicos nas seguintes palavras ou frases (fusão, ditongo/tritongo, sinérese/sinalefa, hiato). Depois, faça a transcrição fonética completa de cada item indicando se a transcrição segue a norma brasileira ou portuguesa."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c14-r) :same-session]
    (let [items [[["chá aguado "] ["fusão alongada"] [" [SÒ:gwÒdu]"]]
                 [["novela oculta"] ["elisão "] ["B [nov×lokÖKtA] P [nuv×lOkÖLtA]"]]
                 [["oito atos "] ["ditongo crescente"] [" [ÕitwÒtus] [ÕItwÒtuS]"]]
                 [["olhe aqui "] ["ditongo crescente"] [" [ØYjakÔ]"]]
                 [["pá imensa "] ["ditongo decrescente"] [" [pÒImçsA]"]]
                 [["ágio "] ["ditongo crescente "] ["[ÒZju]"]]
                 [["poema "] ["sinérese/hiato "] ["[po.Ó.mA] [po`ÓmA]"]]
                 [["roído "] ["hiato natural/ditongo crescente "] ["B [ho.Ô.du] P [³u.Ô.du]"]]
                 [["lê edições "] ["fusão alongada/ditongo decrescente "] ["B [lÓ:Ëiséãs] P [lÓIdiséãS]"]]
                 [["aula intensa "] ["elisão "] ["[ÒUlÞ" [:sup "ª"] "tçsA]"]]
                 [["piano "] ["hiato/ditongo crescente "] ["B [pi.á1.nu] P [pjá1.nu]"]]
                 [["avó ido sa"] ["ditongo decrescente "] ["B [avØIdØzA] B [AvØIdØzA]"]]
                 [["coitado "] ["ditongo decrescente "] ["[koItÒdu]"]]
                 [["atuei"] ["hiato e ditongo decrescente/tritongo "] ["B [a.tu.ÓI] P [A.twÓI]"]]
                 [["pó odioso"] ["sinalefa/hiato—hiato/ditongo crescente "] ["B [pØ`oËi.Õzu] P [pØ.odjÕ.zu]"]]
                 [["açaí"] ["hiato natural "] ["B [asaÔ] P [AsaÔ]"]]
                 [["fase inicial"] ["fusão simples "] ["B [fÒzinisiÒK] P [fÒzinisjÒL]"]]
                 [["duelo"] ["hiato/ditongo crescente"] ["B [du.×.lu] P [dw×.lu]"]]
                 [["conte a ideia"] ["tritongo"] ["B [ké" [:sup "ø"] "ÎjaI.d×I.A] P [ké" [:sup "ª"] ".tAI.dA1.jA]"]]
                 [["que erro"] ["ditongo crescente/ fusão simples "] ["B [kjÓhu] P [kÓ³u]"]]
                 [["falou"] ["ditongo decrescente "] ["B [falÕU] P [fAlÕU]"]]
                 [["fieis"] ["hiato/ditongo decrescente—tritongo "] ["B [fi.×Is] P [fj×IS]"]]
                 [["paga hoje"] ["sinalefa "] ["B [pÒgA`ÕZi] P [pÒgA`ÕZš]"]]
                 [["teoria"] ["sinérese/ditongo crescente "] ["B [te`o.RÔ.A] P [tju.RÔ.A]"]]
                 [["saímos"] ["hiato natural "] ["B [sa.Ô.mus] P [sA.Ô.muS]"]]
                 [["vi ontem"] ["hiato natural "] ["B [vÔ.é" [:sup "ª"] ".tçã] P [vÔ.é" [:sup "ª"] ".tëã]"]]]
          ra1-items (fn [[a b c]]
                      [:li (into [:span] a)])]
      (into [:ol.list.is-lower-alpha]
            (map ra1-items items)))]])

(defn a2
  "Atividades 2"
  []
  [:section.a2
   [:li "Indique a solução fonética preferida para as seguintes descrições de encontros vocálicos dentro da mesma palavra. Dê pelo menos dois exemplos de cada fenômeno, dando tanto a ortografia quanto a transcrição fonética."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c14-r) :same-session]
    (let [headings ["Encontros vocálicos" "Fórmula" "Solução fonética" [:span "Exemplos"]]
          rows [["duas vogais homólogas"
                 "C14-RA2a.png"
                 "fusão com alongamento e mudança de amplitude e tom"]
                ["vogal alta átona seguida de outra vogal"
                 "C14-RA2b.jpg"
                 ["B: hiato (menos " [:em "kw gw"] ")"
                  "P: ditongo crescente"]]

                ["vogal não alta seguida de vogal alta átona"
                 "C14-RA2c.jpg"
                 "ditongo decrescente"]

                ["vogal alta átona seguida de vogal não alta seguida de vogal alta átona"
                 "C14-RA2d.jpg"
                 ["B: hiato mais ditongo decrescente (menos " [:em "kw gw"] ")"
                  "P: tritongo"]]

                ["duas vogais heterólogas não altas"
                 "C14-RA2e.png"
                 ["B: hiato (sinérese)"
                  "P: ditongo crescente por alçamento de vogais médias ou hiato"]]

                ["uma vogal alta tônica seguida de outra vogal"
                 "C14-RA2f.jpg"
                 "hiato nat"]]]
      [:table.table.is-bordered
       [:thead 
        (into [:tr]
              (for [h headings]
                [:th h]))]
       (into [:tbody]
             (for [[a b _c] rows]
               [:tr
                [:td a]
                [:td [shared/image {:full-url (shared/file b)
                                    :alt (str "fullsize " a)}]]
                [:td.blank]
                [:td.blank]]))])]])

(defn a3
  "Atividades 3"
  []
  [:section.a3
   [:li "Indique a solução fonética preferida para as seguintes descrições de encontros vocálicos entre duas palavras. Dê pelo menos dois exemplos de cada fenômeno, dando tanto a ortografia quanto a transcrição fonética."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c14-r) :same-session]
    (let [headings ["Encontros vocálicos" "Fórmula" "Solução fonética" [:span "Exemplos"]]
          rows [[[:span "duas vogais homólogas (menos " [ipa "/a/"] ")"]
                 "C14-RA3a.png"
                 "fusão com alongamento e mudança de amplitude e tom"]

                [[:span "primeira palavra terminada em " [ipa "/a/"] " átona seguida de vogal átona"]
                 "C14-RA3b.png"
                 [:span "elisão da vogal " [ipa "[A]"]]]

                [[:span "primeira palavra terminada em " [ipa "/a/"] " átona seguida de vogal tônica"]
                 "C14-RA3c.png"
                 [:span "sinalefa com " [:span.ipa2 "[A`„]"]]]

                ["vogal alta átona seguida de outra vogal"
                 "C14-RA3d.jpg"
                 "ditongo crescente"]

                ["vogal não alta seguida de vogal alta átona"
                 "C14-RA3e.jpg"
                 "ditongo decrescente"]

                ["vogal alta átona seguida de vogal não alta seguida de vogal alta átona"
                 "C14-RA3f.jpg"
                 "tritongo"]

                ["duas vogais heterólogas foneticamente não altas"
                 "C14-RA3g.png" 
                 "hiato (ou sinalefa)"]

                ["uma vogal alta tônica seguida de outra vogal"
                 "C14-RA3h.jpg"
                 "hiato natural"]]]
      [:table.table.is-bordered
       [:thead 
        (into [:tr]
              (for [h headings]
                [:th h]))]
       (into [:tbody]
             (for [[a b c] rows]
               [:tr
                [:td a]
                [:td [shared/image {:full-url (shared/file b)
                                    :alt (str "fullsize " c)}]]
                [:td.blank]
                [:td.blank]]))])]])



(defn atividades
  "Chapter 14 atividades"
  []
  [:section.atividades.content
   [:h2 "ATIVIDADES"]
   [:ol.atividades
    [a1]
    [a2]
    [a3]]])

(defn recursos
  "Materials"
  []
  [:section.recursos
   [:h3 "RECURSOS ELETRÔNICOS"]
   (let [aud-items [[:span "Exemplos de fusão vocálica."]
                    [:span "Fusão vocálica entre duas vogais homólogas átonas."]
                    [:span "Fusão vocálica entre as vogais " [ipa "[A]"] " e " [ipa "[a]"] " átonas."]
                    [:span "Fusão vocálica entre o artigo {a} e a vogal " [ipa "[a]"] "."]
                    [:span "Fusão vocálica entre vogais homólogas: vogal alta átona e vogal alta tônica."]
                    [:span "Fusão vocálica entre vogais homólogas: " [ipa "/a/"] " átona e " [ipa "/a/"] " tônica."]
                    [:span "Fusão vocálica entre vogais homólogas: vogal tônica e vogal átona."]
                    [:span "Fusão vocálica entre vogais homólogas: vogal tônica e vogal tônica."]
                    [:span "Alongamento vocálico em português."]
                    [:span "Alongamento vocálico com três vogais homólogas."]
                    [:span "Vogais homólogas entre prefixos e radicais."]
                    [:span "Vogais homólogas entre radicais e conjugações verbais: passear."]
                    [:span "Vogais homólogas entre radicais e conjugações verbais: magoar."]
                    [:span "Vogais homólogas com fusão vocálica dentro de um radical."]
                    [:span "Vogais heterólogas entre palavras com a primeira terminada em " [ipa "/a/"] " átona."]
                    [:span "Golpe de glote em inglês."]
                    [:span "Ditongos crescentes isolados."]
                    [:span "Ditongos crescentes dentro de uma palavra e entre palavras."]
                    [:span "Ditongos crescentes com núcleo tônico ou átono."]
                    [:span "Vogal " [ipa "[i]"] " átona diante de outra vogal na mesma palavra."]
                    [:span "Vogal " [ipa "[i]"] " átona final de palavra diante de outra vogal na palavra seguinte."]
                    [:span "Vogal " [ipa "[u]"] " átona diante de outra vogal na mesma palavra."]
                    [:span "Alçamento das vogais medias átonas que podem resultar em ditongos."]
                    [:span "Ditongos crescentes."]
                    [:span "Ditongos decrescentes isolados."]
                    [:span "Ditongos decrescentes dentro de uma palavra e entre palavras."]
                    [:span "Encontros vocálicos entre duas vogais altas átonas dentro de uma palavra."]
                    [:span "Tritongos."]
                    [:span "Contraste entre ditongos em inglês e em português."]
                    [:span "Encontros entre vogais altas átonas e outra vogal no Brasil e em Portugal."]
                    [:span "Encontros entre a vogal " [ipa "/u/"] " átona e outra vogal no Brasil e em Portugal."]
                    [:span "Encontros entre duas vogais altas sendo uma delas tônica."]
                    [:span "Encontros de vogal alta átona e outra vogal átona em posição final de palavra."]
                    [:span "O verbo {fui} em espanhol e português."]
                    [:span "Contrastes entre Brasil e Portugal quanto ao encontro entre vogal alta átona e outra vogal."]
                    [:span "Contrastes entre vogais plenas e ditongos."]
                    [:span "Contrastes entre formas verbais do espanhol e português."]
                    [:span "Contraste entre ditongos crescentes e decrescentes."]
                    [:span "Tritongos em contraste com vogal mais ditongo."]
                    [:span "Casos de sinérese e sinalefa."]
                    [:span "A palavra {teor}: contraste entre hiato, sinérese e ditongo."]
                    [:span "Contrastes entre hiato e sinalefa."]
                    [:span "Encontros vocálicos com três ou mais vogais seguidas."]
                    [:span "Ditongos crescentes comuns em Portugal e no Brasil."]
                    [:span "Hiato, sinérese e ditongo em Portugal e no Brasil."]
                    [:span "Hiatos naturais ou obrigatórios dentro de uma palavra com a primeira vogal tônica."]
                    [:span "Hiatos naturais ou obrigatórios dentro de uma palavra com a segunda vogal tônica."]
                    [:span "Hiatos naturais ou obrigatórios entre palavras."]
                    [:span "Hiato poético."]
                    [:span "Ditongos crescentes com " [ipa "[w]"]"."]]
         ex-items [[:span "Exercícios de pronúncia: fusão de vogais homólogas."]
                   [:span "Exercícios de pronúncia: vogal " [ipa "/a/"] " átona final de palavra mais outra vogal."]
                   [:span "Exercícios de pronúncia: ditongo crescente/hiato (vogal alta átona mais outra vogal tônica)."]
                   [:span "Exercícios de pronúncia: ditongo crescente/hiato (vogal alta átona mais outra vogal átona)."]
                   [:span "Exercícios de pronúncia: ditongo decrescente (vogal mais vogal alta)."]
                   [:span "Exercícios de pronúncia: contrastes entre vogal plena e ditongo/hiato."]
                   [:span "Exercícios de pronúncia: tritongos."]
                   [:span "Exercícios de pronúncia: sinérese e sinalefa."]
                   [:span "Exercícios de pronúncia: hiato natural."]]
         fonetica-line-item (fn [n i]
                              [:li [shared/media-icon :audio
                                    (shared/file (str "C14-M" (inc n) ".mp3"))] i])
         fonetica-line-item2 (fn [n i]
                               [:li [shared/media-icon :ex
                                     (shared/file (str "C14-M" (+ 51 n) ".mp3"))] i])
         ]
     (into
      [:ol.list]
      (conj 
       (map-indexed fonetica-line-item2 ex-items)
       (map-indexed fonetica-line-item aud-items))))])

(defn render
  "Chapter fourteen view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 14 — Encontros vocálicos"
    "eResource"]
   [:div.content.chapter-14
    [recursos]
    [atividades]]
   [shared/footer]])
