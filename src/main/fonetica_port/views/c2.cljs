(ns fonetica-port.views.c2
  "Chapter 2"
  (:require [fonetica-port.views.components.shared :as shared :refer [ipa femp]]
            [reitit.frontend.easy :as rfe]))

(defn ra1-1
  "The table for phonetics answers"
  []
  [:li "Exemplos de vacilações grafêmicas e fonéticas no texto."
    (let [th ["Grafemas" "Sons" "Exemplos"]
          tb [[[:span "l , ll " [:span.is-family-sans-serif "e "] "lh"]
               [:span "tanto " [ipa "[l]"] " quanto " [ipa "[Y]"]]
               [:span "ella " [ipa "[l]"] "; polho " [ipa "[l]"] "; cavaleiro " [ipa "[Y]"] "; molher " [ipa "[Y]"]"] "]]
              [[:span "n , nn " [:span.is-family-sans-serif "e"] " nh"]
               [:span "tanto " [ipa "[n]"]" quanto " [ipa "[¦]"]]
               [:span "ano " [ipa "[n]"] "; polho " [ipa "[l]"] "; senor " [ipa "[¦]"] "; uĩ has " [ipa "[¦]"]"] "]]
              [[:span "ĩ"]
               [:span.ipa "[Þ¦]"]
               [:span "vizĩ o " [ipa "[Þ¦]"]]]
              [[:span "u"]
               [:span "tanto " [ipa "[u]"] " quanto " [ipa "[v]"]]
               [:span "uender " [ipa "[v]"] "; ouuer " [ipa "[Uv]"] "; seruiã " [ipa "[v]"] "; Guarda " [ipa "[w]"]"] "]]
              [[:span "i" [:span.is-family-sans-serif "e"] "y"]
               [:span "tanto " [ipa "[i]"] " quanto " [ipa "[Z]"]]
               [:span "quiser " [ipa "[i]"] "; depoys " [ipa "[I]"] "; iuiz " [ipa "[Z]"] " e " [ipa "[i]"] "; sey " [ipa "[ZA]"]]]
              [[:span "on " [:span.is-family-sans-serif "e"] " õ"]
               [ipa "[`áä]"]
               [:span "non, nõ"]]
              [[:span "em e ẽ"]
               [ipa "[`Ýã]"]
               [:span "quem, quẽ"]]]]
      [:table.table.is-bordered
       [:thead
        (into [:tr]
              (for [h th]
                [:th h]))]
       (into [:tbody]
             (for [[a b c] tb]
               [:tr [:td.cid a] [:td {:style {"vertical-align" "middle"}} b] [:td.cid c]]
               ))])])

(defn ra1-2
  []
  [:li.ra1-2 "Exemplos de palavras do texto com formas arcaicas e suas formas modernas equivalentes."
   (let [l [[:div "sa (" [:em "hoje:"] " sua)"]
            [:div "delrey (" [:em "hoje:"] " do rei)"]
            [:div "el (" [:em "hoje:"] " ele)"]]
         r [[:div "polho (" [:em "hoje:"] " pelo)"]
            [:div "bẽeções (" [:em "hoje:"] " bênçãos)"]]]
     [:div.grid2.has-text-centered
      (into [:div.l] l)
      (into [:div.r] r)])])

(defn ra1-3 []
  [:li.ra1-3 "Exemplos de estruturas sintáticas arcaicas do texto e suas estruturas modernas equivalentes."
   [:div {:style {:margin-left "10%"}}
    [:div "depoys en outra terra morar quiser (" [:em "hoje:"] " quiser morar)"]
    [:div "en sa casa fogir (" [:em "hoje:"] " fugir para sua casa)"]
    [:div "E quem seu vizinho matar (" [:em "hoje:"] " e quem matar seu vizinho)"]
    [:div "Aquel que casa fezer ou uinha (" [:em "hoje:"] " Aquele que fizer casa ou vinha)"]]])

(defn ra1-4 []
  [:li.ra1-4 "Palavras do texto que caíram em desuso ou que sofreram uma mudança semântica."
   [:div.grid2.has-text-centered
    [:div.l
     [:div "herdade (" [:em "hoje:"] " propiedade)"]
     [:div "termo (" [:em "hoje:"] " propiedade)"]
     [:div "coima (" [:em "hoje:"] " multa)"]
     [:div "ca (" [:em "hoje:"] " porque)"]
     [:div "meydade (" [:em "hoje:"] " metade)"]
     [:div "paaço (" [:em "hoje:"] " palácio, casa real)"]]
    [:div.r
     [:div "y (" [:em "hoje:"] " ali)"]
     [:div "u (" [:em "hoje:"] " onde)"]
     [:div "peytar (" [:em "hoje:"] " pleitear)"]
     [:div "ouver (" [:em "hoje:"] " tiver)"]
     [:div "leixar (" [:em "hoje:"] " deixar)"]
     [:div "ser (" [:em "hoje:"] " estar)"]]]])

(defn ra1 []
  [:section.respostas.atividade-1
   [:h4 "Atividade 1"]
   [ra1-1]
   [ra1-2]
   [ra1-3]
   [ra1-4]])

(defn ra2 []
  [:section.respostas.atividade-1
   [:h4 "Atividade 2"]
   (let [thead ["Fenômeno"
                "Campo básico"
                "Outra perspectiva"]
         tbody [[[:span "No interior de São Paulo é comum que o infinitivo verbal seja pronunciado com o som " [ipa "[P]"] " semelhante ao do inglês em {car}."]
                 "fonética"
                 "dialetologia"]
                [[:span "As crianças pequenas muitas vezes dizem {eu sabo} em vez de {eu sei}."]
                 "morfologia"
                 "adquisição de lenguagem"]
                [[:span "Em São Paulo, alguns pronunciam a palavra " [:em "mulher"] " sem pronunciar o " [:em "lh"] ", dizendo " [ipa "[muj×]"] " em vez de " [ipa "[muY×R]"] "."]
                 "fonética"
                 "sociolinguística"]
                [[:span "Em Lisboa, a pronúncia mais comum da norma culta do " [:em "rr"] " de " [:em "carro"] " era a espanhola
" [ipa "[r]"] ". Agora a francesa " [ipa "[±]"] " é mais comum."]
                 "fonética"
                 "sociolinguística história da língua"]
                [[:span "Alguns dizem “É para eu fazer.” Outros dizem “É para mim fazer.”"]
                 "sintaxe"
                 "sociolinguística"]
                [[:span "O texto do " [:em "Foral da Guarda"] " usa a forma verbal de “ouuerẽ” (de " [:em "auer"] ") para expressar posse. Hoje em dia, o verbo usado seria “ter”."]
                 "semântica"
                 "história da língua"]
                [[:span "O inglês tem um verbo copulativo principal: " [:em "to be"] ". O português tem dois: " [:em "ser e estar"] ". Qual é a relação entre esses dois verbos?"]
                 "semântica"
                 "linguística aplicada"]]]
     [:table.table.is-bordered
      [:thead
       (into [:tr]
             (for [h thead]
               [:th h]))]
      [:tbody
       (for [[t b c] tbody]
         [:tr [:td t] [:td.red b] [:td.red c]])]])])

(defn render-respostas
  "Render function for the routing to avoid one more inclusion"
  []
  [:main.c2.respostas.content
   [shared/fonetica-title "Capítulo 2 — A linguística"
    "Respostas para o eResource"]
   [:ol.list.respostas 
    [ra1]
    [ra2]]])

(defn atividade-1 []
  [:section.atividade-1
   [:h4 "Atividade 1"]
   [:p 
    "A melhor maneira de analisar a linguagem, seja por via oral ou por via escrita, é através dos campos básicos da linguística. Como exemplo de como esses campos podem ser aplicados ao estudo de um texto, apresenta-se aqui o seguinte excerto de "
    [:em "Foral da Guarda"]
    ", uma legislação antiga do século XII para a Cidade da Guarda escrita em português arcaico."]
   [:p "À esquerda apresenta-se o texto em letra antiga1 conservando as estruturas e grafia do original; à direita, apresenta-se uma tradução ao português moderno. O texto foi escolhido para demonstrar como alguns aspectos dos sistemas básicos da linguagem (ou seja a fonética/ fonologia, a morfologia, a sintaxe e a semântica) mudaram através dos anos enquanto outros não."
    [:br]
    "OBS.: Apesar de ser tão antigo, o texto ainda é inteligível, o que não seria possível dizer de um texto inglês da mesma época."]
   [:br]
   (let [left-text "Aquel que casa fezer ou uinha ou sa herdade onrrar e per 1 ano en ella seuer, se depoys en outra terra morar quiser, serua a el toda sa herdade u quer que morar. E, se as quiser uender, uenda a quem quiser per foro de uossa cidade. ... Homẽes da Guarda non pagen penora polho senor da Guarda, nõ por meyr«ho, nẽ seyã pennorados senõ por seu uiz«o. Cavaleyros da Guarda nẽ molheres uiuuas nõ dẽ pousada per foro da Guarda senõ per mãdado do iuiz, Omẽes de uossos termyos que seuerẽ en uossas herdades ou em uossos solares e seus senores non forẽ y, uenã ao synal do iuiz e dẽ fiadores que respõdã o dereyto, quando ueerẽ seus senores. E, se fezerem coomya peytẽ-na a seus a senores e a VII ao paaço, e nõ seruiã outro homẽ senõ a seus senhores em cuio solares seuerẽ.  Senharas e u«has delrey ajã tal foro qual as searas e as uinhas nossas ouuerẽ. E quẽ seu uiz«o matar e en sa casa fogir, quẽ depoys a el entrar e o y matar peyte CCC soldos. ... Omẽ da Guarda que molher ouuer a bẽeçõ, se el ha leyxar, peyte 1 dinheiro ao juiz. Se a molher leyxar seu marido que ouuer a bẽeções, peyte CCC soldos ea meyadade sey de seu marido."

         right-text "Aquele que fizer [para si] casa ou vinha ou que honrar sua herdade [propriedade] morando nela por um ano, se depois quiser morar em outra terra [lugar/outras terras], pertença a ele toda a sua herdade, onde quer que ele resida. E, caso as queira vender, venda a quem quiser pelo foro de vossa cidade. Que os homens da [cidade da] Guarda não paguem penhora ao senhor da Guarda nem sejam penhorados por nenhum meirinho [governador de território ou comarca] senão por seu vizinho. Nem os cavaleiros [cavalheiros] nem as mulheres viúvas do foro da Guarda deem pousada, senão por ordem do juiz. Que os homens [servos/ trabalhadores] de vossos termos [propriedades] que servirem em vossas herdades ou em vossos solares e cujos senhores não estiverem ali, venham ao sinal do juiz e deem fiadores que respondam ao direito, quando vierem [voltarem] seus senhores. E se fizerem coima [forem condenados a pagar multa] pleiteiem-na a seus senhores e [pleiteiem] a 7a [penalidades da Sétima Partida] ao paço [palácio real/ palácio da justiça] e não servirão a ninguém mais senão a seus senhores em cujo solar servirem. As searas e vinhas do rei terão os mesmos direitos que as nossas. E quem matar seu vizinho e para sua [própria] casa fugir, aquele que depois entrar [na casa do assassino] e ali o matar, pleiteie 300 soldos. (…) O homem da Guarda que for casado e deixar a mulher, pleiteie ela 1 dinheiro ao juiz. Caso a mulher deixe o marido com o qual for casada, pleiteie 300 soldos e seja a metade de seu marido."]
     [:table.table.is-fullwidth.comp
      [:tbody
       [:tr
        [:td.left.cid left-text]
        [:td.rigt right-text]]]])
   [:p 
    [:sup "1"]"A fonte utilizada é “Cid”, produzida por Juan-José Marcos (juanjmarcos@gmail.com)."]])

(defn ortografia []
  [:ol.page2
   [:section.ortografia
    [:h5 [:em "Fonética/Fonología/Ortografía"]]
    [:li "Como não havia dicionários na época medieval que prescrevessem a grafia “correta” das palavras, a escrita era variável sem uma grafia fixa. Há exemplos em que a mesma palavra foi escrita de maneiras diferentes e exemplos em que o mesmo grafema foi usado para sons distintos. Esse fenômeno é consequência de dois processos: o primeiro é fonético e o segundo é histórico. O motivo fonético é que, às vezes, é possível representar os sons de determinada palavra mediante grafemas diferentes. Por exemplo: quẽ vs. quem. O motivo histórico é que às vezes era empregada uma grafia arcaica apesar de a pronúncia já ter mudado. Por exemplo: non vs. não. Encontrem na transcrição semidiplomática alguns exemplos desse tipo de variação."
     [shared/media-icon :r (rfe/href :fonetica-port.routes/c2-r) :same-session]]]

   [:section.morfologia
    [:h5 [:em "Morfologia"]]
    [:li
     "No texto, que palavras têm formas diferentes das do português de hoje? Por exemplo: "
     [:em "delrey"]
     " vs. "
     [:em "do rei."]
     [shared/media-icon :r (rfe/href :fonetica-port.routes/c2-r) :same-session]]]
   [:section.syntax
    [:h5 [:em "Sintaxe"]]
    [:li
     "No texto, que estruturas sintáticas são diferentes das do português de hoje?" 
     [shared/media-icon :r (rfe/href :fonetica-port.routes/c2-r) :same-session]]]
   [:section.semantics
    [:h5 [:em "Semântica"]]
    [:li
     "Que palavras usadas no texto caíram em desuso ou sofreram uma mudança semântica?"
     [shared/media-icon :r (rfe/href :fonetica-port.routes/c2-r) :same-session]]]])

(defn atividade-2 []
  [:section.atividade-2
   [:h4 
    "Atividade 2"]
   [:p 
    "Complete a seguinte tabela indicando o campo básico e a outra perspectiva linguística que
seria empregada para estudar os seguintes fenômenos:" 
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c2-r) :same-session]]
   (let [thead ["Fenômeno"
                "Campo básico"
                "Outra perspectiva"]
         tbody [[[:span "No interior de São Paulo é comum que o infinitivo verbal seja pronunciado com o som " [ipa "[P]"] " semelhante ao do inglês em {car}."]
                 nil
                 nil]
                [[:span "As crianças pequenas muitas vezes dizem {eu sabo} em vez de {eu sei}."]
                 nil
                 nil]
                [[:span "Em São Paulo, alguns pronunciam a palavra " [:em "mulher"] " sem pronunciar o " [:em "lh"] ", dizendo " [ipa "[muj×]"] " em vez de " [ipa "[muY×R]"] "."]
                 nil
                 nil]
                [[:span "Em Lisboa, a pronúncia mais comum da norma culta do " [:em "rr"] " de " [:em "carro"] " era a espanhola
" [ipa "[r]"] ". Agora a francesa " [ipa "[±]"] " é mais comum."]
                 nil
                 nil]
                [[:span "Alguns dizem “É para eu fazer.” Outros dizem “É para mim fazer.”"]
                 nil
                 nil
                 ]
                [[:span "O texto do " [:em "Foral da Guarda"] " usa a forma verbal de “ouuerẽ” (de " [:em "auer"] ") para expressar posse. Hoje em dia, o verbo usado seria “ter”."]
                 nil
                 nil
                 ]
                [[:span "O inglês tem um verbo copulativo principal: " [:em "to be"] ". O português tem dois: " [:em "ser e estar"] ". Qual é a relação entre esses dois verbos?"]
                 nil
                 nil]]]
   [:table.table.is-bordered
    [:thead
     (into [:tr]
           (for [h thead]
             [:th h]))]
    [:tbody
     (for [[t _ _] tbody]
       [:tr [:td t] [:td] [:td]])]])])

(defn atividades
  "super section for all activities"
  []
  [:section.atividades
   [:h2 "ATIVIDADES"]
   [atividade-1]
   [ortografia]
   [atividade-2]])

(defn render
  "Chapter two view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 2 — A linguística"]
   [:div.content.chapter-2
    [:section.e-resources
     [:h2 "eResource"]]
    [atividades]
    ]
   [shared/footer]
   ])
