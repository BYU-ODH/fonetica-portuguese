(ns fonetica-port.views.c10
  "Chapter 10"
  (:require [fonetica-port.views.components.shared :as shared :refer [ipa red ipa-like]]
            [reitit.frontend.easy :as rfe]))

(defn ra1
  "Respuestas Activity 1"
  []
  [:section.ra1
   [:li.respostas-a1 "Dê cinco palavras que exemplifiquem cada uma das seguintes sequências fonotáticas. Pode usar a Tabela 10.8 do livro como referência. " [:span.red "Há um exemplo para cada uma, mas são várias as respostas possíveis."]
    [:ol.list.is-lower-alpha
     (let [items [[:li.list-item "#CV {" [red "ma"] "pa}"]
                  [:li.list-item "#CCV {" [red "tro"] "pa}"]
                  [:li.list-item "$CV {pa" [red "la"] "vra}"]
                  [:li.list-item "$CCV {a" [red "pre"] "sentar}"]
                  [:li.list-item "VC# {cart" [red "as"] "}"]
                  [:li.list-item "VCC# {tr" [red "ens"] "}"]
                  [:li.list-item "VC$ {p" [red "ac"] "to}"]
                  [:li.list-item "VCC$ {tr" [red "ans"] "porte}"]]
           [left right] (mapv vec (partition (-> items count (* 0.5)) items))]
       [:ol.list.is-lower-alpha
        [:div.grid2
         (into [:div.l] left)
         (into [:div.r] right)]]
       
       )]]])

(defn ra2
  "Respuestas Activity 2"
  []
  [:section.ra2
   [:li.respostas-a2 "Identifique la estructura silábica interna y la división silábica de las siguientes palabras según el modelo del Cuadro 10.10 del libro."
    (let [rows [["abstrato" ["a"   "0"   "voc"]  "$"  ["b"   "−3"   "obs"]  ["i"   "0"   "voc"]  ["s"   "+3"   "obs"]  "$"  ["t"   "−3"   "obs"]  ["R"   "−2"   "son"]  ["Ò"   "0"   "voc"]  "$"  ["t"   "−3"   "obs"]  ["u"   "0"   "voc"]]
                ["catástrofe" ["k"  "−3"  "obs"] ["a"  "0"  "voc"] "$" ["t"  "−3"  "obs"] ["Ò"  "0"  "voc"] ["s"  "+3"  "obs"] "$" ["t"  "−3"  "obs"] ["R"  "−2"  "son"] ["o"  "0"  "voc"] "$" ["f"  "−3"  "obs"] ["i"  "0"  "voc"]]
                ["clausura" ["k"  "−3"  "obs"] ["l"  "−2"  "son"] ["a"  "0"  "voc"] ["U"  "+1"  "des"] "$" ["z"  "−3"  "obs"] ["Ö"  "0"  "voc"] "$" ["R"  "−2"  "son"] ["A"  "0"  "voc"]]
                ["contém" ["k"  "−3"  "obs"] ["ß"  "0"  "voc"] ["ª"  "+2"  "son"] "$" ["t"  "−3"  "obs"] ["ç"  "0"  "voc"] ["ã"  "+1"  "des"] ]
                ["empenharam" ["Ý"  "0"  "voc"] ["m"  "+2"  "son"] "$" ["p"  "−3"  "obs"] ["Ý"  "0"  "voc"] "$" ["¦"  "−3"  "son"] ["Ò"  "0"  "voc"] "$" ["R"  "−3"  "son"] ["á"  "0"  "voc"] ["ä"  "+1"  "des"]]
                ["exato" ["e"  "0"  "voc"] "$" ["z"  "−3"  "obs"] ["Ò"  "0"  "voc"] "$" ["t"  "−3"  "obs"] ["u"  "0"  "voc"]]
                ["mexicano" ["m"  "−2"  "son"] ["e"  "0"  "voc"] "$" ["S"  "−3"  "obs"] ["i"  "0"  "voc"] "$" ["k"  "−3"  "obs"] ["a"  "0"  "voc"] "$" ["n"  "−2"  "son"] ["u"  "0"  "voc"]]
                ["perspicaz" ["p"  "−3"  "obs"] ["e"  "0"  "voc"] ["R"  "+2"  "son"] ["s"  "+3"  "obs"] "$" ["p"  "−3"  "obs"] ["i"  "0"  "voc"] "$" ["k"  "−3"  "obs"] ["Ò"  "0"  "voc"] ["s"  "+3"  "obs"]]]
          box-me (fn [rest] (for [[phon score type] rest :let [terminal? (= \$ phon)]]
                              (if terminal? [:div.terminal phon]
                                  [:div.fbox
                                   [:div.ipa phon]
                                   [:div.normal score]
                                   [:div.place type]])))] 
      (into [:ol.list.is-lower-alpha]
            (for [[header & rest] rows]
              [:li header
               (into [:div.boxed.red] (box-me rest))])))]])

(defn ra3
  "Respuestas Activity 3"
  []
  [:section.ra3
   [:li.respostas-a3 "Demonstre como a estrutura silábica afeta a realização das vogais fechadas " [ipa "/i u/"] ", seja no ataque silábico, como núcleo silábico, ou na coda silábica."
    [:div.red "No ataque silábico " [ipa "/i u/"] " realizam-se como semiconsoantes: " [ipa "[j w]"] "."]
    [:div.red  "Como núcleo silábico " [ipa "/i u/"] " realizam-se como vogais fechadas/altas: " [ipa "[i u]"] "."]
    [:div.red "Na coda silábica " [ipa "/i u/"] " realizam-se como semivogais: " [ipa "[I U]"] "."]]] )

(defn ra4
  "Respuestas Activity 4"
  []
  [:section.ra4
   [:li.respostas-a4 "Demonstre como a estrutura silábica afeta a realização das consoantes nasais e vibrantes, seja no ataque silábico ou na coda silábica."
    [:div.red "No ataque silábico os fonemas nasais e vibrantes se opõem: " [ipa "/n/"] " -" [ipa "/m/"] " -" [ipa "/¦/"] " e " [ipa "/R/"] " -" [ipa "/r/"] ""]
    [:div.red "Na coda silábica os fonemas nasais e vibrantes se neutralizam: " [ipa "/"] [ipa-like "N"] [ipa "/"] " e " [ipa "/"] [ipa-like "R"] [ipa "/"] ""]]])

(defn ra5
  "Respuestas Activity 5"
  []
  [:section.ra5
   [:li.respostas-a5 "Invente cinco palavras para exemplificar o conceito de lacuna acidental e outras cinco para o conceito de lacuna sistemática."
    [:div.red "As respostas podem variar; aquí se apresentam só alguns exemplos."]
    [:div.red "Lacunas acidentais: traxo, blã, lentro, chirro, tunha."]
    [:div.red "Lacunas sistemáticas: sreír, mbole, serrap, pant, tlan."]]])

(defn render-respostas
  "Chapter 10 respostas"
  []
  [:main.c10.respostas.content
      [shared/fonetica-title "Capítulo 10 — O posicionamento e a sequência de fonemas: a fonotática"
       "Respostas para o eResource"]
   [:section.atividades
    [:h2 "ATIVIDADES"]
    [:ol.atividades.list
     [ra1]
     [ra2]
     [ra3]
     [ra4]
     [ra5]]]])

(defn a1
  "Activity 1"
  []
  [:section.a1
   [:li.a1 "Dê cinco palavras que exemplifiquem cada uma das seguintes sequências fonotáticas. Pode usar a Tabela 10.8 do livro como referência."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c10-r) :same-session]
    [:ol.list.is-lower-alpha
     (let [items [[:li.list-item "#CV "]
                  [:li.list-item "#CCV "]
                  [:li.list-item "$CV "]
                  [:li.list-item "$CCV "]
                  [:li.list-item "VC# "]
                  [:li.list-item "VCC# "]
                  [:li.list-item "VC$ "]
                  [:li.list-item "VCC$ "]]
           [left right] (mapv vec (partition (-> items count (* 0.5)) items))]
       [:ol.list.is-lower-alpha
        [:div.grid2
         (into [:div.l] left)
         (into [:div.r] right)]])]]])

(defn a2
  "Activity 2"
  []
  [:section.a2
   [:li.a2 "Identifique a estrutura silábica interna e a divisão silábica das seguintes palavras de acordo com o modelo da Tabela 10.10 do livro."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c10-r) :same-session]
    (let [rows [["abstrato" ["a"   "0"   "voc"]  "$"  ["b"   "−3"   "obs"]  ["i"   "0"   "voc"]  ["s"   "+3"   "obs"]  "$"  ["t"   "−3"   "obs"]  ["R"   "−2"   "son"]  ["Ò"   "0"   "voc"]  "$"  ["t"   "−3"   "obs"]  ["u"   "0"   "voc"]]
                ["catástrofe" ["k"  "−3"  "obs"] ["a"  "0"  "voc"] "$" ["t"  "−3"  "obs"] ["Ò"  "0"  "voc"] ["s"  "+3"  "obs"] "$" ["t"  "−3"  "obs"] ["R"  "−2"  "son"] ["o"  "0"  "voc"] "$" ["f"  "−3"  "obs"] ["i"  "0"  "voc"]]
                ["clausura" ["k"  "−3"  "obs"] ["l"  "−2"  "son"] ["a"  "0"  "voc"] ["U"  "+1"  "des"] "$" ["z"  "−3"  "obs"] ["Ö"  "0"  "voc"] "$" ["R"  "−2"  "son"] ["A"  "0"  "voc"]]
                ["contém" ["k"  "−3"  "obs"] ["ß"  "0"  "voc"] ["ª"  "+2"  "son"] "$" ["t"  "−3"  "obs"] ["ç"  "0"  "voc"] ["ã"  "+1"  "des"] ]
                ["empenharam" ["Ý"  "0"  "voc"] ["m"  "+2"  "son"] "$" ["p"  "−3"  "obs"] ["Ý"  "0"  "voc"] "$" ["ྱ"  "−3"  "son"] ["Ò"  "0"  "voc"] "$" ["R"  "−3"  "son"] ["á"  "0"  "voc"] ["ä"  "+1"  "des"]]
                ["exato" ["e"  "0"  "voc"] "$" ["z"  "−3"  "obs"] ["Ò"  "0"  "voc"] "$" ["t"  "−3"  "obs"] ["u"  "0"  "voc"]]
                ["mexicano" ["m"  "−2"  "son"] ["e"  "0"  "voc"] "$" ["S"  "−3"  "obs"] ["i"  "0"  "voc"] "$" ["k"  "−3"  "obs"] ["a"  "0"  "voc"] "$" ["n"  "−2"  "son"] ["u"  "0"  "voc"]]
                ["perspicaz" ["p"  "−3"  "obs"] ["e"  "0"  "voc"] ["R"  "+2"  "son"] ["s"  "+3"  "obs"] "$" ["p"  "−3"  "obs"] ["i"  "0"  "voc"] "$" ["k"  "−3"  "obs"] ["Ò"  "0"  "voc"] ["s"  "+3"  "obs"]]]
          [left right] (mapv vec (partition (-> rows count (* 0.5)) rows))
          mli (comp #(do [:li %]) first)]
      [:ol.list.is-lower-alpha
        [:div.grid2
         (into [:div.l] (map mli left))
         (into [:div.r] (map mli right))]])]])

(defn a3
  "Activity 3"
  []
  [:section.a3
   [:li.a3 "Demonstre como a estrutura silábica afeta a realização das vogais fechadas " [ipa "/i u/"] ", seja no ataque silábico, como núcleo silábico, ou na coda silábica."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c10-r) :same-session]]] )

(defn a4
  "Activity 4"
  []
  [:section.a4
   [:li.a4 "Demonstre como a estrutura silábica afeta a realização das consoantes nasais e vibrantes, seja no ataque silábico ou na coda silábica."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c10-r) :same-session]]])

(defn a5
  "Activity 5"
  []
  [:section.a5
   [:li.a5 "Invente cinco palavras para exemplificar o conceito de lacuna acidental e outras cinco para o conceito de lacuna sistemática."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c10-r) :same-session]]])

(defn atividades
  "Chapter 10 atividades"
  []
   [:section.atividades.content
    [:h2 "ATIVIDADES"]
    [:ol.atividades
     [a1]
     [a2]
     [a3]
     [a4]
     [a5]]])

(defn recursos
  "Materials for the chapter"
  []
  [:section.recursos.content
   [:h2 "RECURSOS ELETRÔNICOS"]
   [:ol.list
    [:li  [shared/media-icon :audio (shared/file "C10-M1.mp3")]
     "Vogais epentéticas no Brasil e em Portugal."]]
   ]
  )

(defn render
  "Chapter ten view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 10 — O posicionamento e a sequência de fonemas: a fonotática"
    "eResource"]
   [:div.content.chapter-10
    [recursos]
    [atividades]]
   
   [shared/footer]])
