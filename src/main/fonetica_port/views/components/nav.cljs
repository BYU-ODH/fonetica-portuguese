(ns fonetica-port.views.components.nav
  (:require [reagent.core :as r]
            [reitit.frontend.easy :as rfe]))

(def open-burger? (r/atom false))

(defn toggle-burger [& _]
  (swap! open-burger? not))

(defn burger
  "Burger menu for smaller screen sizes"
  []
  [:a.navbar-burger {:role "button"
                     :aria-label "menu"
                     :aria-expanded "false"
                     :on-click toggle-burger
                     :class (when @open-burger? "is-active")}
   [:span {:aria-hidden true}]
   [:span {:aria-hidden true}]
   [:span {:aria-hidden true}]])

(defn navbar []
  [:nav.navbar {:role "navigation" :aria-label "main nav"}
   [:div.navbar-brand
    [:a.navbar-item.title {:href (rfe/href :fonetica-port.routes/table-of-contents)
                           :title "Indice"}
     "Manual de fonética e fonologia da língua portuguesa"]
    [burger]]
   [:div.navbar-menu {:class (when @open-burger? "is-active")}
    [:div.navbar-start
     [:a.navbar-item {:href (rfe/href :fonetica-port.routes/table-of-contents)}
      "Índice"]]]])
