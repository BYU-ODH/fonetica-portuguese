(ns fonetica-port.views.c8
  "Chapter 8"
  (:require [fonetica-port.views.components.shared :as shared :refer [ipa ipa-like]]
            [reitit.frontend.easy :as rfe]))

(defn ra1
  "Respostas Activity 1"
  []
  [:li.respostas-a1 "Escreva o símbolo do fonema descrito pelos seguintes traços."
   [:ol.list.is-lower-alpha
    (let [left [[["oclusivo"
                  "bilabial"
                  "sonoro"]
                 "/b/"]
                [["fricativo"
                  "palatal"
                  "surdo"]
                 "/S/"]
                [["lateral"
                  "alveolar"
                  "sonoro"]
                 "/l/"]
                [["oclusivo"
                  "dental"
                  "surdo"]
                 "/t/"]
                [["nasal"
                  "palatal"
                  "sonoro"]
                 "/¦/"]
                [["fricativo"
                  "alveolar"
                  "surdo"]
                 "/s/"]]
          right [[["oclusivo"
                   "bilabial"
                   "surdo"]
                  "/p/"]
                 [["vibrante simples"
                   "alveolar"
                   "sonoro"]
                  "/R/"]
                 [["meio-aberto"
                   "anterior"]
                  "/E/"]
                 [["meio-fechado"
                   "posterior"]
                  "/o/"]
                 [["aberto"
                   "central"]
                  "/a/"]
                 [["fechado"
                   "posterior"]
                  "/u/"]]
          build-2x2 (fn [left-or-right]
                      (for [[places ipa-text] left-or-right]
                        [:li [:div.grid2
                              (into [:div.l2]
                                    (for [p places]
                                      [:div p]))
                              [:div.r2.red.ipa.is-size-4 ipa-text]]]))]

      [:div.grid2
       (into [:div.l1] (build-2x2 left))
       (into [:div.r1] (build-2x2 right))]
      )]])

(defn ra2
  "Respostas Activity 2"
  []
  [:li.respostas-a2 "Indique os traços fonológicos dos seguintes fonemas."
   [:ol.list.is-lower-alpha
    (let [items [["/m/"
                  ["nasal"
                   "bilabial"
                   "sonoro"]]
                 ["/k/"
                  ["oclusivo"
                   "velar"
                   "sordo"]]
                 ["/f/"
                  ["fricativo"
                   "labiodental"
                   "surdo"]]
                 ["/n/"
                  ["nasal"
                   "alveolar"
                   "sonoro"]]
                 ["/O/"
                  ["meio-aberto"
                   "posterior"]]
                 ["/d/"
                  ["oclusivo"
                   "dental"
                   "sonoro"]]
                 ["/r/"
                  ["vibrante múltipla"
                   "alveolar"
                   "sonoro"]]
                 ["/g/"
                  ["oclusivo"
                   "velar"
                   "sonoro"]]] 
          build-2x2 (fn [items]
                      (for [[ipa-text places] items]
                        [:li [:div.grid2
                              [:div.l2.ipa.is-size-4 ipa-text]
                              (into [:div.r2.red]
                                    (for [p places]
                                      [:div p]))]]))]
      (into [:div.grid2]
            (build-2x2 items)))]])

(defn ra3
  "Respostas Activity 3"
  []
  [:li.respostas-a3 "Encontre um par mínimo que prove as seguintes oposições. Providencie também a transcrição ortográfica. "
   [:span.blue "As respostas podem variar; aqui só se dá um exemplo para cada um."]
   [:ol.list.is-lower-alpha
    [:div
     (let [n (fn [] [:span.normal "~"])
           rows [[[:span "/s/ " [n] " /t/"] "[pÒsu] [pÒtu]" "{passo} {pato}"]
                 [[:span "/l/ " [n] " /R/"] "[×lA] [×RA]" "{ela} {era}"]
                 [[:span "/b/ " [n] " /S/"] "[ÒbA] [ÒSA]" "{aba} {acha}"]
                 [[:span "/Y/ " [n] " /g/"] "[fÔYA] [fÔgA]" "{filha} {figa}"]
                 [[:span "/m/ " [n] " /¦/"] "[lÔmA] [lÔ¦A]" "{lima} {linha}"]
                 [[:span "/p/ " [n] " /b/"] "[pÔku] [bÔku]" "{pico} {bico}"]
                 [[:span "/e/ " [n] " /o/"] "[vetÒr] [votÒr]" "{vetar} {votar}"]
                 [[:span "/v/ " [n] " /r/"] "[vè] [rè]" "{vim} {rim}"]
                 [[:span "/i/ " [n] " /e/"] "[vÔ] [vÓ]" "{vi} {vê}"]
                 [[:span "/k/ " [n] " /f/"] "[kÖRtu] [fÖRtu]" "{curto} {furto}"]]]
       (into [:ol.list.is-lower-alpha]
             (for [[a b c] rows]
               [:li [:div.grid3
                     [:span.ipa a]
                     [:span.ipa.red b]
                     [:span.red c]]])))]]])

(defn ra4
  "Respostas Activity 4"
  []
  [:li.respostas-a4 "Os seguintes dados fonéticos representam neutralizações. Para cada caso, identifique os fonemas neutralizados e o resultado da neutralização."
   (let [lis
         [["[àm bÒRku] [këmA] [àn lÒGo] [kënA]"
           [:span "Demonstra a neutralização entre " [ipa "/m/"] " e " [ipa "/n/"] " que resulta en no arquifonema " [ipa "/"] [ipa-like "N"] [ipa "/"] "."]]
          ["[kÒsA] [kÒzA] [uskÒhus] [uzgÒtus]"
           [:span "Demonstra a neutralização entre " [ipa "/s/"] " e " [ipa "/z/"] " que resulta en no arquifonema " [ipa "/"] [ipa-like "S"] [ipa "/"] "."]]
          ["[kÒRu] [kÒru] [estudÒR] [estudÒr]"
           [:span "Demonstra a neutralização entre " [ipa "/R/"] " e " [ipa "/r/"] " que resulta en no arquifonema " [ipa "/"] [ipa-like "R"] [ipa "/"] "."]]]]
     (into [:ol.list.is-lower-alpha]
           (for [[a b] lis]
             [:li [:div.ipa a]
              [:div.red b]])))])

(defn render-respostas
  "Chapter 8 respostas"
  []
  [:main.c8.respostas.content
   [shared/fonetica-title "Capítulo 8 — A relação entre fonemas: a oposição e a neutralização"
    "Respostas para o eResource"]
   [:section.atividades
    [:h2 "ATIVIDADES"]
    [:ol.atividades.list
     [ra1]
     [ra2]
     [ra3]
     [ra4]]]])

(defn a1
  " Activity 1"
  []
  [:li.a1 "Escreva o símbolo do fonema descrito pelos seguintes traços."
   [shared/media-icon :r (rfe/href :fonetica-port.routes/c8-r) :same-session]
   [:ol.list.is-lower-alpha
    (let [left [[["oclusivo"
                  "bilabial"
                  "sonoro"]
                 "/b/"]
                [["fricativo"
                  "palatal"
                  "surdo"]
                 "/S/"]
                [["lateral"
                  "alveolar"
                  "sonoro"]
                 "/l/"]
                [["oclusivo"
                  "dental"
                  "surdo"]
                 "/t/"]
                [["nasal"
                  "palatal"
                  "sonoro"]
                 "/¦/"]
                [["fricativo"
                  "alveolar"
                  "surdo"]
                 "/s/"]]
          right [[["oclusivo"
                   "bilabial"
                   "surdo"]
                  "/p/"]
                 [["vibrante simples"
                   "alveolar"
                   "sonoro"]
                  "/R/"]
                 [["meio-aberto"
                   "anterior"]
                  "/E/"]
                 [["meio-fechado"
                   "posterior"]
                  "/o/"]
                 [["aberto"
                   "central"]
                  "/a/"]
                 [["fechado"
                   "posterior"]
                  "/u/"]]
          build-2x2 (fn [left-or-right]
                      (for [[places _] left-or-right]
                        [:li [:div
                              (into [:div]
                                    (for [p places]
                                      [:div p]))]]))]

      [:div.grid2
       (into [:div.l1] (build-2x2 left))
       (into [:div.r1] (build-2x2 right))]
      )]])

(defn a2
  " Activity 2"
  []
  [:li.a2 "Indique os traços fonológicos dos seguintes fonemas."
   [shared/media-icon :r (rfe/href :fonetica-port.routes/c8-r) :same-session]
   [:ol.list.is-lower-alpha
    (let [items [["/m/"
                  ["nasal"
                   "bilabial"
                   "sonoro"]]
                 ["/k/"
                  ["oclusivo"
                   "velar"
                   "sordo"]]
                 ["/f/"
                  ["fricativo"
                   "labiodental"
                   "surdo"]]
                 ["/n/"
                  ["nasal"
                   "alveolar"
                   "sonoro"]]
                 ["/O/"
                  ["meio-aberto"
                   "posterior"]]
                 ["/d/"
                  ["oclusivo"
                   "dental"
                   "sonoro"]]
                 ["/r/"
                  ["vibrante múltipla"
                   "alveolar"
                   "sonoro"]]
                 ["/g/"
                  ["oclusivo"
                   "velar"
                   "sonoro"]]] 
          build-2x2 (fn [items]
                      (for [[ipa-text places] items]
                        [:li [:div.grid2
                              [:div.l2.ipa.is-size-4 ipa-text]
                              [:div.r2.red]
                              
                              ]]))]
      (into [:div.grid2]
            (build-2x2 items)))
    #_(let [items [["/m/"
                  ["nasal"
                   "bilabial"
                   "sonoro"]]
                 ["/k/"
                  ["oclusivo"
                   "velar"
                   "sordo"]]
                 ["/f/"
                  ["fricativo"
                   "labiodental"
                   "surdo"]]
                 ["/n/"
                  ["nasal"
                   "alveolar"
                   "sonoro"]]
                 ["/O/"
                  ["meio-aberto"
                   "posterior"]]
                 ["/d/"
                  ["oclusivo"
                   "dental"
                   "sonoro"]]
                 ["/r/"
                  ["vibrante múltipla"
                   "alveolar"
                   "sonoro"]]
                 ["/g/"
                  ["oclusivo"
                   "velar"
                   "sonoro"]]] 
          build-2x2 (fn [items]
                      (for [[ipa-text _places] items]
                        [:li [:div.l2.ipa.is-size-4 ipa-text]]))]
      (into [:div.grid2] (build-2x2 items))
      )]]) ;; struggling to get the order right here

(defn a3
  " Activity 3"
  []
  [:li.a3 "Encontre um par mínimo que prove as seguintes oposições. Providencie também a transcrição ortográfica. "
   [shared/media-icon :r (rfe/href :fonetica-port.routes/c8-r) :same-session]
   [:ol.list.is-lower-alpha
    [:div
     (let [n (fn [] [:span.normal "~"])
           rows [[[:span "/s/ " [n] " /t/"] "[pÒsu] [pÒtu]" "{passo} {pato}"]
                 [[:span "/l/ " [n] " /R/"] "[×lA] [×RA]" "{ela} {era}"]
                 [[:span "/b/ " [n] " /S/"] "[ÒbA] [ÒSA]" "{aba} {acha}"]
                 [[:span "/Y/ " [n] " /g/"] "[fÔYA] [fÔgA]" "{filha} {figa}"]
                 [[:span "/m/ " [n] " /¦/"] "[lÔmA] [lÔ¦A]" "{lima} {linha}"]
                 [[:span "/p/ " [n] " /b/"] "[pÔku] [bÔku]" "{pico} {bico}"]
                 [[:span "/e/ " [n] " /o/"] "[vetÒr] [votÒr]" "{vetar} {votar}"]
                 [[:span "/v/ " [n] " /r/"] "[vè] [rè]" "{vim} {rim}"]
                 [[:span "/i/ " [n] " /e/"] "[vÔ] [vÓ]" "{vi} {vê}"]
                 [[:span "/k/ " [n] " /f/"] "[kÖRtu] [fÖRtu]" "{curto} {furto}"]]]
       (into [:ol.list.is-lower-alpha]
             (for [[a _b _c] rows]
               [:li [:div.grid3
                     [:span.ipa a] [:span] [:span]]])))]]])

(defn a4
  " Activity 4"
  []
  [:li.a4 "Os seguintes dados fonéticos representam neutralizações. Para cada caso, identifique os fonemas neutralizados e o resultado da neutralização."
   [shared/media-icon :r (rfe/href :fonetica-port.routes/c8-r) :same-session]
   (let [lis
         [["[àm bÒRku] [këmA] [àn lÒGo] [kënA]"
           [:span "Demonstra a neutralização entre " [ipa "/m/"] " e " [ipa "/n/"] " que resulta en no arquifonema " [ipa "/"] [ipa-like "N"] [ipa "/"] "."]]
          ["[kÒsA] [kÒzA] [uskÒhus] [uzgÒtus]"
           [:span "Demonstra a neutralização entre " [ipa "/s/"] " e " [ipa "/z/"] " que resulta en no arquifonema " [ipa "/"] [ipa-like "S"] [ipa "/"] "."]]
          ["[kÒRu] [kÒru] [estudÒR] [estudÒr]"
           [:span "Demonstra a neutralização entre " [ipa "/R/"] " e " [ipa "/r/"] " que resulta en no arquifonema " [ipa "/"] [ipa-like "R"] [ipa "/"] "."]]]]
     (into [:ol.list.is-lower-alpha]
           (for [[a _b] lis]
             [:li [:div.ipa a]])))])

(defn atividades
  "Chapter 8 atividades"
  []
   [:section.atividades.content
    [:h2 "ATIVIDADES"]
    [:ol.atividades
     [a1]
     [a2]
     [a3]
     [a4]]])

(defn recursos
  "Materials"
  []
  [:section.recursos
   [:h3 "RECURSOS ELETRÔNICOS"]
   [:ol
    [:li [shared/media-icon :audio (shared/file "C8-M1.mp3")] "Exemplos de pares mínimos."]
    [:li [shared/media-icon :audio (shared/file "C8-M2.mp3")] "Oposições consonantais baseadas no modo de articulação."]
    [:li [shared/media-icon :audio (shared/file "C8-M3.mp3")] "Oposições consonantais baseadas no ponto de articulação."]
    [:li [shared/media-icon :audio (shared/file "C8-M4.mp3")] "Oposições consonantais baseadas no estado das cordas vocais."]
    [:li [shared/media-icon :audio (shared/file "C8-M5.mp3")] "Oposições vocálicas baseadas no modo de articulação."]
    [:li [shared/media-icon :audio (shared/file "C8-M6.mp3")] "Oposições vocálicas baseadas no ponto de articulação."]
    [:li [shared/media-icon :audio (shared/file "C8-M7.mp3")] "Oposições vocálicas do fonema " [ipa "/a/"] "."]
    [:li [shared/media-icon :audio (shared/file "C8-M8.mp3")] "Oposição entre " [ipa "/m/"] ", " [ipa "/n/"] " e " [ipa "/¦/"] "em posição inicial de sílaba interior de palavra e entre /m/ e /n/ em posição inicial de palavra."]
    [:li [shared/media-icon :audio (shared/file "C8-M9.mp3")] "Neutralização entre as consoantes nasais em posição final de sílaba."]
    [:li [shared/media-icon :audio (shared/file "C8-M10.mp3")] "Oposição entre " [ipa "/R/"] " e " [ipa "/r/" ] " em posição inicial de sílaba interior de palavra."]
    [:li [shared/media-icon :audio (shared/file "C8-M11.mp3")] "Neutralização entre as consoantes vibrantes em posição final de sílaba."]
    [:li [shared/media-icon :audio (shared/file "C8-M12.mp3")] "Oposição entre " [ipa "/s/"] " e " [ipa "/z/" ] " em posição inicial de palavra e inicial de sílaba interior de palavra."]
    [:li [shared/media-icon :audio (shared/file "C8-M13.mp3")] "Neutralização entre as consoantes alveolares em posição final de sílaba interior de palavra e final de palavra."]]])

(defn render
  "Chapter eight view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 8 — A relação entre fonemas: a oposição e a neutralização "
    "eResource"]
   [:div.content.chapter-8
    [recursos]
    [atividades]]
   
   [shared/footer]])
