(ns fonetica-port.views.c20
  "Chapter 20"
  (:require [fonetica-port.views.components.shared :as shared :refer [ipa fempr red ipa-like]]
            [reitit.frontend.easy :as rfe]))

(defn ra1
  "Respuestas activity 1"
  []
  [:section.ra1
   [:li
    [:div "As seguintes palavras ou sequências de palavras contêm encontros consonantais indicados em negrito. Preencha a seguinte tabela com uma transcrição correta indicando a silabação.  Depois indique o tipo de sequência consonantal fonológica e a sua resolução fonética."]
    (let [headers [["Palavra/sequência"]
                   ["Transcrição"]
                   ["Tipo de sequência fonológica"
                    "(consoantes homólogas, encontro consonantal tautossilábico/heterossilábico)"]
                   ["Solução fonética"
                    "(fusão, alofones separados com ou sem vogal epentética)"]]
          items [[[:span "vo" [:strong "z s"] "impática"]
                  [:span "[vØ.sÞm.pÒ.Îi.kA]"]
                  [:span "consoantes homólogas"]
                  [:span "fusão simples"]]
                 [[:span "ob" [:strong "st"] "áculo"]
                  [:span "[o.bis.tÒ.ku.lu]"]
                  [:span "encontro consonantal tautossilábico/heterossilábico"]
                  [:span "alofones separados com vogal epentética"]]
                 [[:span "a" [:strong "st"] "ro"]
                  [:span "[Òs.tRu]"]
                  [:span "encontro consonantal heterossilábica/tautossilábico"]
                  [:span "alofones separados com encontro inseparável"]]
                 [[:span "funi" [:strong "l l"] "otado"]
                  [:span "[fu.nÞK.lo.tÒ.du]"]
                  [:span "consoantes homólogas"]
                  [:span "alofones separados"]]
                 [[:span "e" [:strong "ng"] "enho"]
                  [:span "[å.zç.¦U]"]
                  [:span "encontro consonantal heterossilábico"]
                  [:span "nasalização vocálica sem consoante de transição"]]
                 [[:span "e" [:strong "ng"] "anho"]
                  [:span "[åN.gë.¦U]"]
                  [:span "encontro consonantal heterossilábico"]
                  [:span "nasalização vocálica com consoante de transição"]]
                 [[:span "é" [:strong "br"] "io"]
                  [:span "[×.bRju]"]
                  [:span "encontro onsonantal tautossilábico"]
                  [:span "encontro inseparável"]]
                 [[:span "pó" [:strong "st"] "umo"]
                  [:span "[pØs.tu.mu]"]
                  [:span "encontro consonantal heterossilábico"]
                  [:span "alofones separados"]]
                 [[:span "per" [:strong "sp"] "icaz"]
                  [:span "[peRs.pi.kÒs]"]
                  [:span "encontro consonantal tautossilábico/heterossilábico"]
                  [:span "alofones separados"]]
                 [[:span "a" [:strong "pt"] "itude"]
                  [:span "[a.pi.Îi.tu.Ëi]"]
                  [:span "encontro consonantal heterossilábico"]
                  [:span "alofones separados com vogal epentética"]]
                 [[:span "in" [:strong "cr"] "emento"]
                  [:span "[ÞN.kRe.mçª.tu]"]
                  [:span "encontro onsonantal tautossilábico"]
                  [:span "enconro inseparável"]]
                 [[:span "increme" [:strong "nt"] "o"]
                  [:span "[ÞN.kRe.mçª.tu]"]
                  [:span "encontro consonantal heterossilábico"]
                  [:span "nasalização vocálica com vogal epentética"]]
                 [[:span "ve" [:strong "rs"] "o"]
                  [:span "[v×R.su]"]
                  [:span "encontro consonantal heterossilábica"]
                  [:span "alofones separados"]]
                 [[:span "u" [:strong "m n"] "oivo"]
                  [:span "[à:`n`:ÕI.vu]"]
                  [:span "fusão ambissilábica"]
                  [:span "nasalização vocálica alongada com consoante ambissilábica"]]
                 [[:span "ca" [:strong "mp"] "o"]
                  [:span "[këm.pU]"]
                  [:span "encontro consonantal heterossilábico"]
                  [:span "nasalização vocálica com consoante de transição"]]
                 [[:span "u" [:strong "n m"] "úsculo"]
                  [:span "[à:`m`:Ös.ku.lU]"]
                  [:span "fusão ambissilábica"]
                  [:span "nasalização vocálica alongada com consoante ambissilábica"]]
                 [[:span "a" [:strong "ds"] "crever"]
                  [:span "[a.Ëis.kRe.vÓR]"]
                  [:span "encontro consonantal heterossilábico"]
                  [:span "encontro consonantal com vogal epentética"]]
                 [[:span "ads" [:strong "cr"] "ever"]
                  [:span "[a.Ëis.kRe.vÓR]"]
                  [:span "encontro consonantal tautossilábico"]
                  [:span "encontro inseparável"]]
                 [[:span "tá" [:strong "x"] "i"]
                  [:span "[tÒk.si]"]
                  [:span "encontro consonantal heterossilábico"]
                  [:span "alofones separados sem vogal epentética"]]
                 [[:span "flo" [:strong "r r"] "ara"]
                  [:span "[flØR.hÒ.RA]"]
                  [:span "consoantes homólogas"]
                  [:span "alofones separados"]]]]
      [:table.table.ftable.is-bordered
       [:thead
        (into [:tr]
              (for [[a b] headers]
                [:th a
                 [:div.is-size-7 b]]))]
       (into [:tbody]
             (for [[a b c d] items]
               [:tr
                [:td a]
                [:td.red.ipa.has-text-centered.is-size-4 b]
                [:td.red.has-text-centered.is-size-4 c]
                [:td.red.has-text-centered.is-size-4 d]]))])]])

(defn render-respostas
  "Chapter 20 respostas"
  []
  [:main.c20.respostas.content
   [shared/fonetica-title "Capítulo 20 — Respuestas a las atividades"]
   [:section.atividades
    [:h2 "ACTIVIDADES"]
    [:ol.atividades.list
     [ra1]]]])

(defn a1
  "Activity 1"
  []
  [:section.a1
      [:li
       [:div "As seguintes palavras ou sequências de palavras contêm encontros consonantais indicados em negrito. Preencha a seguinte tabela com uma transcrição correta indicando a silabação.  Depois indique o tipo de sequência consonantal fonológica e a sua resolução fonética."
        [shared/media-icon :r (rfe/href :fonetica-port.routes/c20-r) :same-session]]
    (let [headers [["Palavra/sequência"]
                   ["Transcrição"]
                   ["Tipo de sequência fonológica"
                    "(consoantes homólogas, encontro consonantal tautossilábico/heterossilábico)"]
                   ["Solução fonética"
                    "(fusão, alofones separados com ou sem vogal epentética)"]]
          items [[[:span "vo" [:strong "z s"] "impática"]
                  [:span "[vØ.sÞm.pÒ.Îi.kA]"]
                  [:span "consoantes homólogas"]
                  [:span "fusão simples"]]
                 [[:span "ob" [:strong "st"] "áculo"]
                  [:span "[o.bis.tÒ.ku.lu]"]
                  [:span "encontro consonantal tautossilábico/heterossilábico"]
                  [:span "alofones separados com vogal epentética"]]
                 [[:span "a" [:strong "st"] "ro"]
                  [:span "[Òs.tRu]"]
                  [:span "encontro consonantal heterossilábica/tautossilábico"]
                  [:span "alofones separados com encontro inseparável"]]
                 [[:span "funi" [:strong "l l"] "otado"]
                  [:span "[fu.nÞK.lo.tÒ.du]"]
                  [:span "consoantes homólogas"]
                  [:span "alofones separados"]]
                 [[:span "e" [:strong "ng"] "enho"]
                  [:span "[å.zç.¦U]"]
                  [:span "encontro consonantal heterossilábico"]
                  [:span "nasalização vocálica sem consoante de transição"]]
                 [[:span "e" [:strong "ng"] "anho"]
                  [:span "[åN.gë.¦U]"]
                  [:span "encontro consonantal heterossilábico"]
                  [:span "nasalização vocálica com consoante de transição"]]
                 [[:span "é" [:strong "br"] "io"]
                  [:span "[×.bRju]"]
                  [:span "encontro onsonantal tautossilábico"]
                  [:span "encontro inseparável"]]
                 [[:span "pó" [:strong "st"] "umo"]
                  [:span "[pØs.tu.mu]"]
                  [:span "encontro consonantal heterossilábico"]
                  [:span "alofones separados"]]
                 [[:span "per" [:strong "sp"] "icaz"]
                  [:span "[peRs.pi.kÒs]"]
                  [:span "encontro consonantal tautossilábico/heterossilábico"]
                  [:span "alofones separados"]]
                 [[:span "a" [:strong "pt"] "itude"]
                  [:span "[a.pi.Îi.tu.Ëi]"]
                  [:span "encontro consonantal heterossilábico"]
                  [:span "alofones separados com vogal epentética"]]
                 [[:span "in" [:strong "cr"] "emento"]
                  [:span "[ÞN.kRe.mçª.tu]"]
                  [:span "encontro onsonantal tautossilábico"]
                  [:span "enconro inseparável"]]
                 [[:span "increme" [:strong "nt"] "o"]
                  [:span "[ÞN.kRe.mçª.tu]"]
                  [:span "encontro consonantal heterossilábico"]
                  [:span "nasalização vocálica com vogal epentética"]]
                 [[:span "ve" [:strong "rs"] "o"]
                  [:span "[v×R.su]"]
                  [:span "encontro consonantal heterossilábica"]
                  [:span "alofones separados"]]
                 [[:span "u" [:strong "m n"] "oivo"]
                  [:span "[à:`n`:ÕI.vu]"]
                  [:span "fusão ambissilábica"]
                  [:span "nasalização vocálica alongada com consoante ambissilábica"]]
                 [[:span "ca" [:strong "mp"] "o"]
                  [:span "[këm.pU]"]
                  [:span "encontro consonantal heterossilábico"]
                  [:span "nasalização vocálica com consoante de transição"]]
                 [[:span "u" [:strong "n m"] "úsculo"]
                  [:span "[à:`m`:Ös.ku.lU]"]
                  [:span "fusão ambissilábica"]
                  [:span "nasalização vocálica alongada com consoante ambissilábica"]]
                 [[:span "a" [:strong "ds"] "crever"]
                  [:span "[a.Ëis.kRe.vÓR]"]
                  [:span "encontro consonantal heterossilábico"]
                  [:span "encontro consonantal com vogal epentética"]]
                 [[:span "ads" [:strong "cr"] "ever"]
                  [:span "[a.Ëis.kRe.vÓR]"]
                  [:span "encontro consonantal tautossilábico"]
                  [:span "encontro inseparável"]]
                 [[:span "tá" [:strong "x"] "i"]
                  [:span "[tÒk.si]"]
                  [:span "encontro consonantal heterossilábico"]
                  [:span "alofones separados sem vogal epentética"]]
                 [[:span "flo" [:strong "r r"] "ara"]
                  [:span "[flØR.hÒ.RA]"]
                  [:span "consoantes homólogas"]
                  [:span "alofones separados"]]]]
      [:table.table.ftable.is-bordered
       [:thead
        (into [:tr]
              (for [[a b] headers]
                [:th a
                 [:div.is-size-7 b]]))]
       (into [:tbody]
             (for [[a] items]
               [:tr
                [:td a]
                [:td.blank]
                [:td.blank]
                [:td.blank]]))])]])

(defn atividades
  "Chapter 20 atividades"
  []
  [:section.atividades.content
   [:h2 "ATIVIDADES"]
   [:ol.atividades
    [a1]]])

(defn recursos
  "Materials"
  []
  [:section.recursos
   [:h3 {:style {:text-decoration "none"}} "Materiales en línea"]
   (let [audio-items [[:span "Encontros homólogos com o fonema " [:span.ipa-like "/l/"] "."]
                      [:span "Encontros homólogos com o arquifonema fonema " [:span.ipa-like "/S/"] " sem palatalização."]
                      [:span "Encontros homólogos com o arquifonema fonema " [:span.ipa-like "/S/"] " com palatalização."]
                      [:span "Encontros homólogos com o arquifonema fonema " [:span.ipa-like "/N/"] "."]
                      [:span "Encontros homólogos com o arquifonema fonema " [:span.ipa-like "/R/"] "."]
                      [:span "O contraste entre {sê ruim} e {ser ruim}."]
                      [:span "A sequência fonética" [:span.ipa "[Rh]"] "."]
                      [:span "Consoantes homólogas em inglês com alongamento ou golpe de glote."]
                      [:span "O ensurdecimento de " [:span.ipa-like "/l/"] " trás oclusivas surdas em inglês."]
                      [:span "O ensurdecimento de " [:span.ipa-like "/R/"] " trás oclusivas surdas em inglês."]
                      [:span "O encontro " [:SPAN.IPA-LIKE "/NS/"] " na coda silábica."]
                      [:span "Os encontros " [:SPAN.IPA-LIKE "/RS/"] " e " [:span.ipa-like "/lS/"] " na coda silábica."]
                      [:span "Os encontros " [:span.ipa-like "/bS/"] " e " [:span.ipa-like "/dS/"] " na coda silábica."]
                      [:span "As palavras {perspicaz} e {trens}."]
                      [:span "Palavras com encontros C$C terminando em obstruinte."]
                      [:span "Vogais epentéticas dentro de uma palavra."]
                      [:span "A palatalização resultante de vogal epentética."]
                      [:span "Vogais epentéticas em posição final de palavra."]
                      [:span "Vogais epentéticas em encontros consonantais assistemáticos em posição inicial de palavra."]
                      [:span "As palavras {apto} e {lapso}."]
                      [:span "Encontros C$C começando com " [:span.ipa-like "/b/"] "."]
                      [:span "Encontros C$C começando com " [:span.ipa-like "/d/"] "."]
                      [:span "Encontros C$C começando com " [:span.ipa-like "/g/"] " e " [:span.ipa-like "/f/"] "."]
                      [:span "Encontros entre " [:span.ipa-like "/l/"] " e obstruinte."]
                      [:span "Encontros entre " [:span.ipa-like "/S/"] " e obstruinte."]
                      [:span "Encontros entre " [:span.ipa-like "/N/"] " e obstruinte."]
                      [:span "Encontros entre " [:span.ipa-like "/R/"] " e obstruinte."]
                      [:span "Palavras com encontros C$C terminando em soante."]
                      [:span "Ensurdecimento da vogal epentética."]
                      [:span "Encontros inseparáveis."]
                      [:span "O encontro " [:span.ipa-like "/dl/"] "."]
                      [:span "Encontros entre " [:span.ipa-like "/l/"] " e soante."]
                      [:span "Encontros entre " [:span.ipa-like "/S/"] " e soante."]
                      [:span "Encontros entre " [:span.ipa-like "/N/"] " e soante."]
                      [:span "Encontros entre " [:span.ipa-like "/R/"] " e soante."]
                      [:span "Palavras com encontros C$CC."]
                      [:span "Palavras com encontros CC$C."]
                      [:span "Palavras com encontros CC$CC."]
                      [:span "O encontro " [:span.ipa-like "/mn/"] "."]
                      [:span "Variantes dialetais de {os jatos}."]
]
         ex-items [[:span "Exercícios de pronúncia: encontros de consoantes homólogas."]
                   [:span "Exercícios de pronúncia: encontros consonantais tautossilábicos."]
                   [:span "Exercícios de pronúncia: encontros consonantais heterossilábicos dentro da mesma palavra."]
                   [:span "Exercícios de pronúncia: encontros consonantais heterossilábicos entre palavras."]] 
         fonetica-line-item (fn [n i]
                              [:li.list-item [shared/media-icon :audio
                                              (shared/file (str "C20-M" (inc n) ".mp3"))] i])
         fonetica-line-item2 (fn [n i]
                               (let [ic (-> audio-items count inc) ]
                                 [:li.list-item [shared/media-icon :ex
                                                 (shared/file (str "C20-M" (+ ic n) ".mp3"))] i]))]
     
     (into
      [:ol.list]
      (conj 
       (map-indexed fonetica-line-item2 ex-items)
       (map-indexed fonetica-line-item audio-items))))])

(defn render
  "Chapter twenty view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 20 — Encontros consonantais"
    "eResource"]
   
   [:div.content.chapter-20
    [recursos]
    [atividades]]
   [shared/footer]])
