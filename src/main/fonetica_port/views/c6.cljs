(ns fonetica-port.views.c6
  "Chapter 6"
  (:require [fonetica-port.views.components.shared :as shared :refer [ipa]]
            [reitit.frontend.easy :as rfe]))

(defn render-respostas
  "Chapter 6 respostas"
  []
  [:main.c6.respostas.container.content
   [shared/fonetica-title "Capítulo 6 — A fonética acústica"
    "Respostas para o eResource"]
   [:section.respostas
    [:ol.list
     [:li.a1
      [:ol.list.is-lower-alpha
       [:li "onda inarmônica"]
       [:li "onda harmônica complexa"]
       [:li "onda harmônica simples"]
       [:li "onda quase-harmônica"]]]
     [:li.a2
      [:ol.list.is-lower-alpha
       [:li "duração"]
       [:li "frequência/tom"]
       [:li "timbre/qualidade vocálica"]
       [:li "volume/amplitude"]]]
     [:li.a3
      [:ol.list.is-lower-alpha
       [:li [:div
             [:div "sonograma de banda estreita"]
             [:div "Podem-se ver os harmônicos e medir o tom fundamental."]
             [:div "x= tempo, y= frequência, z (cor clara ou escura)=amplitude"]]]
       [:li [:div
             [:div "forma de onda"]
             [:div "Podem-se ver a amplitude, o tipo de onda e a duração."]
             [:div "x=tempo, y=amplitude"]]]
       [:li [:div
             [:div "onograma de banda larga"]
             [:div "Podem-se ver os formantes, se o som é sonoro/surdo e calcular o tom fundamental."]
             [:div "x= tempo, y= frequência, z (cor clara ou escura)=amplitude"]]]
       [:li [:div
             [:div "seção espectrográfica"]
             [:div "Podem-se ver a amplitude dos formantes num tempo fixo."]
             [:div "x=frequência, y=amplitude"]]]]]
     [:li.a4 "“Em boca fechada, não entra mosca.”"
      [shared/image {:full-url (shared/file "C6-R4.jpg")
                     :alt "“Em boca fechada, não entra mosca.”"}]]]]])

(defn a1
  "Atividades 1"
  []
  [:section.a1
   [:li "Identifique o tipo de onda encontrada nas seguintes representações:"
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c6-r) :same-session]
    [:ol.list.is-lower-alpha
     [:li 
      [shared/image {:full-url (shared/file "C6-A1a.png")
                     :alt "a"} ]]
     [:li 
      [shared/image {:full-url (shared/file "C6-A1b.png")
                     :alt "b"} ]]
     [:li 
      [shared/image {:full-url (shared/file "C6-A1c.png")
                     :alt "c"} ]]
     [:li 
      [shared/image {:full-url (shared/file "C6-A1d.png")
                     :alt "d"} ]]]]])

(defn a2
  "Atividades 2"
  []
  [:section.a2
   [::li "Identifique a propriedade acústica que diferencia as duas ondas em cada um dos seguintes pares de formas de onda. (Veja as páginas 89–91.)"
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c6-r) :same-session]]
   [:ol.list.is-lower-alpha
    [:li [shared/image {:full-url (shared/file "C6-A2a.jpg")}]]
    [:li [shared/image {:full-url (shared/file "C6-A2b.jpg")}]]
    [:li [shared/image {:full-url (shared/file "C6-A2c.jpg")}]]
    [:li [shared/image {:full-url (shared/file "C6-A2d.jpg")}]]]])

(defn a3
  "Atividades 3"
  []
  [:section.a3
   [:li "Identifique o tipo de análise que produziu cada uma das seguintes representações. O que pode ser examinado em cada uma delas? O que é representado em cada eixo?"
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c6-r) :same-session]]
   [:ol.list.is-lower-alpha
    [:li [shared/image {:full-url (shared/file "C6-A3a.png")}]]
    [:li [shared/image {:full-url (shared/file "C6-A3b.png")}]]
    [:li [shared/image {:full-url (shared/file "C6-A3c.png")}]]
    [:li [shared/image {:full-url (shared/file "C6-A3d.png")}]]]])

(defn a4
  "Atividades 4"
  []
  [:section.a4
   [:li "Divida o seguinte sonograma em segmentos, indicando quais segmentos são vogais e quais são consoantes. Identifique cada vogal específica por meio de seus formantes. Descubra qual é o modo e ponto de articulação e o estado das cordas vocais, e, por eles, identifique cada consoante específica. Observe o sonograma e veja se consegue descobrir o que foi dito."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c6-r) :same-session]
    [:div [shared/image {:full-url (shared/file "C6-A4.jpg")}]]]])

(defn atividades
  "Chapter 6  atividades"
  []
   [:section.atividades.content
    [:h2 "ATIVIDADES"]
    [:ol.list
     [a1]
     [a2]
     [a3]
     [a4]]])

(defn render
  "Chapter six view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 6 — A fonética acústica"
    "eResource"]
   [:div.content.chapter-6
    [:section.recursos
     [:h3 "RECURSOS ELETRÔNICOS"]
     [:ol
      [:li [shared/media-icon :video (shared/file "C6-M1.mp4")] "Vídeo de uma onda transmitida por uma corda."]
      [:li [shared/media-icon :video (shared/file "C6-M2.mp4")] "Vídeo de uma onda transmitida pela água numa poça."]
      [:li [shared/media-icon :video (shared/file "C6-M3.mp4")] "Vídeo do movimento de um pêndulo."]
      [:li [shared/media-icon :audio (shared/file "C6-M4.mp3")] "A vogal [a] em volume baixo e alto."]
      [:li [shared/media-icon :audio (shared/file "C6-M5.mp3")] "A vogal [a] em tom baixo/grave e alto/agudo."]
      [:li [shared/media-icon :audio (shared/file "C6-M6.mp3")] "As vogais [a] e [e] no mesmo tom e volume."]
      [:li [shared/media-icon :audio (shared/file "C6-M7.mp3")] "A vogal [a] com duração curta e longa."]
      [:li [shared/media-icon :audio (shared/file "C6-M8.mp3")] "As vogais [a e i o u]."]
      [:li [shared/media-icon :audio (shared/file "C6-M9.mp3")] "Os sons [a z s]."]
      [:li [shared/media-icon :audio (shared/file "C6-M10.mp3")] "“A fonética acústica é fascinante.”"]
      [:li [shared/media-icon :audio (shared/file "C6-M11.mp3")] "As sequências " [ipa "[si se sE sa sO so su]"] "."]
      [:li [shared/media-icon :audio (shared/file "C6-M12.mp3")] "Sequências que demonstram os distintos modos de articulação das consoantes."]
      [:li [shared/media-icon :audio (shared/file "C6-M13.mp3")] "Sequências que demonstram os distintos pontos de articulação das consoantes oclusivas."]
      [:li [shared/media-icon :audio (shared/file "C6-M14.mp3")] "Sequências que demonstram os distintos pontos de articulação das consoantes fricativas."]
      [:li [shared/media-icon :audio (shared/file "C6-M15.mp3")] "Sequências que demonstram a diferença entre sons surdos e sonoros."]
      [:li [shared/media-icon :audio (shared/file "C6-M16.mp3")] "“De músico, poeta e louco, todos temos um pouco.”"]]]
    [atividades]]
   
   [shared/footer]])
