(ns fonetica-port.routes
  (:require [reagent.core :as r]
            [reitit.frontend :as rf]
            [reitit.frontend.easy :as rfe]
            [reitit.frontend.history :as rfh]
            [fonetica-port.views.toc :as toc]
            [fonetica-port.views.c1 :as c1]
            [fonetica-port.views.c2 :as c2]
            [fonetica-port.views.c3 :as c3]
            [fonetica-port.views.c4 :as c4]
            [fonetica-port.views.c5 :as c5]
            [fonetica-port.views.c6 :as c6]
            [fonetica-port.views.c7 :as c7]
            [fonetica-port.views.c8 :as c8]
            [fonetica-port.views.c9 :as c9]
            [fonetica-port.views.c10 :as c10]
            [fonetica-port.views.c11 :as c11]
            [fonetica-port.views.c12 :as c12]
            [fonetica-port.views.c13 :as c13]
            [fonetica-port.views.c14 :as c14]
            [fonetica-port.views.c15 :as c15]
            [fonetica-port.views.c16 :as c16]
            [fonetica-port.views.c17 :as c17]
            [fonetica-port.views.c18 :as c18]
            [fonetica-port.views.c19 :as c19]
            [fonetica-port.views.c20 :as c20]
            [fonetica-port.views.c21 :as c21]
            [fonetica-port.views.c22 :as c22]
            [fonetica-port.views.c23 :as c23]
            [fonetica-port.views.c24 :as c24]
            [goog.string.path :as goop]
            ))

(defn this-location []
  (str (.-location js/window)))

(defn get-resource-path
  "Get a given filenames path, which depends upon whether we are hosted"
  [filename]
  (let [file-dir "Files"]
    (str "/" file-dir "/" filename)))

(defn default-view
  "To show before routes are loaded"
  []
  [:h1 "Loading..."])

(defonce current-view (r/atom default-view) )

(def routes
  (rf/router
   ["/"
    ["" {:name ::table-of-contents
         :view #'toc/render
         }]
    ["c/"
     ["1/"
      [""
       {:name ::chapter-1
        :view #'c1/render}]
      ["m1/"
       {:name ::c1-m1
        :view #'c1/c1-m1}]
      ]
     ["2/"
      [""
       {:name ::chapter-2
        :view #'c2/render}]
      ["respostas/"
       {:name ::c2-r
        :view #'c2/render-respostas}]]
     ["3/"
      [""
       {:name ::chapter-3
        :view #'c3/render}]
      ["print"
       {:name ::c3-print
        :view #'c3/table-blanks}]
      ["respostas/"
       {:name ::c3-r
        :view #'c3/render-respostas}]]
     ["4/"
      [""
       {:name ::chapter-4
        :view #'c4/render}]
      ["m1/"
       {:name ::c4-m1
        :view #'c4/render-m1}]
      ["m2/"
       {:name ::c4-m2
        :view #'c4/render-m2}]
      ["m3/"
       {:name ::c4-m3
        :view #'c4/render-m3}]
      ["m4/"
       {:name ::c4-m4
        :view #'c4/render-m4}]
      ["m5/"
       {:name ::c4-m5
        :view #'c4/render-m5}]
      ["m6/"
       {:name ::c4-m6
        :view #'c4/render-m6}]
      ["m7/"
       {:name ::c4-m7
        :view #'c4/render-m7}]
      ["m8/"
       {:name ::c4-m8
        :view #'c4/render-m8}]
      ["m9/"
       {:name ::c4-m9
        :view #'c4/render-m9}]
      ["m10/"
       {:name ::c4-m10
        :view #'c4/render-m10}]
      ["m11/"
       {:name ::c4-m11
        :view #'c4/render-m11}]]
     ["5/"
      [""
       {:name ::chapter-5
        :view #'c5/render}]
      ["ej15/"
       {:name ::c5-ej15
        :view #'c5/ej-15}]
      ["ej26/"
       {:name ::c5-ej26
        :view #'c5/ej-26}]
      ["ej36/"
       {:name ::c5-ej36
        :view #'c5/ej-36}]
      ["ej37/"
       {:name ::c5-ej37
        :view #'c5/ej-37}]
      ["respostas/"
       {:name ::c5-r
        :view #'c5/render-respostas}]]
     ["6/"
      [""
       {:name ::chapter-6
        :view #'c6/render}]
      ["respostas/"
       {:name ::c6-r
        :view #'c6/render-respostas}]]
     ["7/"
      [""
       {:name ::chapter-7
        :view #'c7/render}]
      ["respostas/"
       {:name ::c7-r
        :view #'c7/render-respostas}]]
     ["8/"
      [""
       {:name ::chapter-8
        :view #'c8/render}]
      ["respostas/"
       {:name ::c8-r
        :view #'c8/render-respostas}]]
     ["9/"
      [""
       {:name ::chapter-9
        :view #'c9/render}]
      ["respostas/"
       {:name ::c9-r
        :view #'c9/render-respostas}]]
     ["10/"
      [""
       {:name ::chapter-10
        :view #'c10/render}]
      ["respostas/"
       {:name ::c10-r
        :view #'c10/render-respostas}]]
     ["11/"
      [""
       {:name ::chapter-11
        :view #'c11/render}]
      ["respostas/"
       {:name ::c11-r
        :view #'c11/render-respostas}]]
     ["12/"
      [""
       {:name ::chapter-12
        :view #'c12/render}]
      ["respostas/"
       {:name ::c12-r
        :view #'c12/render-respostas}]]
     ["13/"
      [""
       {:name ::chapter-13
        :view #'c13/render}]
      ["respostas/"
       {:name ::c13-r
        :view #'c13/render-respostas}]]
     ["14/"
      [""
       {:name ::chapter-14
        :view #'c14/render}]
      ["respostas/"
       {:name ::c14-r
        :view #'c14/render-respostas}]]
     ["15/"
      [""
       {:name ::chapter-15
        :view #'c15/render}]
      ["respostas/"
       {:name ::c15-r
        :view #'c15/render-respostas}]]
     ["16/"
      [""
       {:name ::chapter-16
        :view #'c16/render}]
      ["respostas/"
       {:name ::c16-r
        :view #'c16/render-respostas}]]
     ["17/"
      [""
       {:name ::chapter-17
        :view #'c17/render}]
      ["respostas/"
       {:name ::c17-r
        :view #'c17/render-respostas}]]
     ["18/"
      [""
       {:name ::chapter-18
        :view #'c18/render}]
      ["respostas/"
       {:name ::c18-r
        :view #'c18/render-respostas}]]
     ["19/"
      [""
       {:name ::chapter-19
        :view #'c19/render}]
      ["respostas/"
       {:name ::c19-r
        :view #'c19/render-respostas}]]
     ["20/"
      [""
       {:name ::chapter-20
        :view #'c20/render}]
      ["respostas/"
       {:name ::c20-r
        :view #'c20/render-respostas}]]
     ["21/"
      [""
       {:name ::chapter-21
        :view #'c21/render}]
      ["respostas/"
       {:name ::c21-r
        :view #'c21/render-respostas}]]
     ["22/"
      [""
       {:name ::chapter-22
        :view #'c22/render}]
      ["respostas/"
       {:name ::c22-r
        :view #'c22/render-respostas}]]
     ["23/"
      [""
       {:name ::chapter-23
        :view #'c23/render}]
      ["respostas/"
       {:name ::c23-r
        :view #'c23/render-respostas}]]
     ["24/"
      [""
       {:name ::chapter-24
        :view #'c24/render}]
      ["respostas/"
       {:name ::c24-r
        :view #'c24/render-respostas}]]
     ]

    
    ["Files/:file-name" ;; used for getting the correct resource path
     {:name ::files}]
    ]))

(defn ignore-click?
  "have Reitit ignore the click if it is for a media resource (which should be handled by a lightbox modal)"
  [router e el uri]
  (let [uri-extension (goop/extension (str uri))
        rign (rfh/ignore-anchor-click? router e el uri)
        should-ignore? (and rign
                            (empty? uri-extension))]
    should-ignore?))

(defn init-routes!
  "Start the routing"
  []
  (rfe/start! routes
              (fn [m]
                (reset! current-view (get-in m [:data :view])))
              {:use-fragment false
               :ignore-anchor-click? ignore-click?
               }))
