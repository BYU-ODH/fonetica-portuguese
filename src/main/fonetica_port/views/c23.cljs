(ns fonetica-port.views.c23
  "Chapter 23"
  (:require [fonetica-port.views.components.shared :as shared :refer [ipa fempr red ipa-like]]
            [reitit.frontend.easy :as rfe]))

(defn ra1
  "Respostas activity 1"
  []
  [:section.ra1
   [:li [:div "Os seguintes versos são do poema “A invenção dos jardins” de António Feliciano de Castilho. Para cada verso, indique a silabação, sublinhe as sílabas tônicas e dê o número total de sílabas. Explique qual a metrificação e como os versos com um número diferente de sílabas nela se enquadram."]
    (let [headers ["Verso" "# total de sílabas" "# de sílabas até a última tônica" "# de sílabas após a última tônica (tipo de palavra)"]
          items [[[:span "a/" [:span.is-underlined "qui"]",/ a/" [:span.is-underlined "qui"]","] "4" "4" "0 (oxítona)"]
                 [[:span "que eu/"  [:span.is-underlined "hon"]"/tem/ "  [:span.is-underlined "nés"]"/tes/" [:span.is-underlined " lá"]"/bios"] "7" "6" "1 (paroxítona)"]
                 [[:span [:span.is-underlined "ti"]"/ve o/ pri/" [:span.is-underlined "mei"] "/ro"] "5" "4" "1 (paroxítona)"]
                 [[:span "" [:span.is-underlined "bei"]"/jo/ da/" [:span.is-underlined " mi"]"/nha/" [:span.is-underlined " Flé"]"/ri/da,"] "8" "6" "2 (proparoxítona)"]
                 [[:span "e en/dou/de/" [:span.is-underlined "ci!"]""] "4" "4" "0 (oxítona)"]
                 [[:span "Re/" [:span.is-underlined "cor"]"/das/-te? o/ cre/" [:span.is-underlined "pús"]"/cu/lo"] "8" "6" "2 (proparoxítona)"]
                 [[:span [:span.is-underlined "vi"]"/nha/ ca/" [:em "in"]"/do;"] "5" "4" "1 (paroxítona)"]
                 [[:span "chil/" [:span.is-underlined "ra"]"/va o/"  [:span.is-underlined "ban"] "/do a/" [:span.is-underlined "lí"]"/ge/ro"] "8" "6" "2 (proparoxítona)"]
                 [[:span "" [:span.is-underlined "ho"]"/ra/ d’a/" [:span.is-underlined "mor"]";"] "4" "4" "0 (oxítona)"]
                 [[:span "" [:span.is-underlined "to/do"] "e/ra a/fec/to e/ jú/bi/lo"] "8" "6" "2 (proparoxítona)"]
                 [[:span "teu/"  [:span.is-underlined "ges"]"/to/"  [:span.is-underlined "lin"]"/do;"] "5" "4" "1 (paroxítona)"]
                 [[:span "" [:span.is-underlined "to"]"/" [:span.is-underlined "do eu"]"/ de/" [:span.is-underlined "se"]"/jo/" [:span.is-underlined "s sô"]"/fre/gos"] "8" "6" "2 (proparoxítona)"]
                 [[:span "e in/" [:span.is-underlined "ter"]"/no ar/" [:span.is-underlined "dor"]"."] "4" "4" "0 (oxítona)"]
                 [[:span "Co' o/" [:span.is-underlined " bra"]"/ço ao/" [:span.is-underlined " pei"]"/to/" [:span.is-underlined " trê"]"/mu/lo"] "8" "6" "2 (proparoxítona)"]
                 [[:span "" [:span.is-underlined "eu"]"/ te/ cin/" [:span.is-underlined "gi"]"/a,"] "5" "4" "1 (paroxítona)"]
                 [[:span "" [:span.is-underlined "ven"]"/do em/ teu/" [:span.is-underlined " sei"]"/o/" [:span.is-underlined " cân"]"/di/do"] "8" "6" "2 (proparoxítona)"]
                 [[:span "teu/ sus/pi/" [:span.is-underlined "rar"]"."] "4" "4" "0 (oxítona)"]
                 [[:span "" [:span.is-underlined "Es"]"/te/ fe/" [:span.is-underlined "li"]"/z si/" [:span.is-underlined "lên"]"/cio,"] "8" "6" "2 (proparoxítona)"]
                 [[:span "" [:span.is-underlined "na"]"/da o/ rom/" [:span.is-underlined "pi"]"/a;"] "5" "4" "1 (paroxítona)"]
                 [[:span "" [:span.is-underlined "e"]"/ram/ su/" [:span.is-underlined "a"]"/ves/" [:span.is-underlined " lá"]"/gri/mas"] "8" "6" "2 (proparoxítona)"]
                 [[:span "o/ meu/ fa/" [:span.is-underlined "lar"]"."] "4" "4" "0 (oxítona)"]
                 [[:span "Da/" [:span.is-underlined " mão"]"/ ne/" [:span.is-underlined "va"]"/da e/" [:span.is-underlined " lân"]"/gui/da"] "8" "6" "2 (proparoxítona)"]]]
      [:table.table.ftable.is-bordered
       [:thead
        (into [:tr]
              (for [h headers]
                [:th h]))]
       (into [:tbody]
             (for [[a b c d] items]
               [:tr
                [:td a]
                [:td.has-text-centered b]
                [:td.has-text-centered c]
                [:td.has-text-centered d]]))]) ]])

(defn ra2
  "Respostas activity 2"
  []
  [:section.ra2
   [:li
    [:div "Indique como se pode enfatizar o elemento sublinhado das seguintes orações."]
    (let [items [[[:span "João"  [:span.is-underlined "lhe"] "deu o prato."]
                  [:span "João deu o prato a ele/ela."]]
                 [[:span "" [:span.is-underlined "A Maria"] " festejou demais."]
                  [:span "Foi a Maria quem festejou demais."]
                  [:span "A Maria foi quem festejou demais."]
                  [:span "Quem festejou demais foi a Maria."]]
                 [[:span "Joãozinho comeu"  [:span.is-underlined "a sobremesa."]]
                  [:span "Foi a sobremesa o que Joãozinho comeu."]
                  [:span "O que Joãozinho comeu foi a sobremesa."]]
                 [[:span "O estudante é " [:span.is-underlined "inteligente"]"."]
                  [:span "O estudante é inteligentérrimo."]
                  [:span "O estudante é inteligentíssimo."]
                  [:span "O estudante é muito inteligente."]]
                 [[:span "Martim o fez "  [:span.is-underlined "com Isabel"]"."]
                  [:span "Foi "  [:em "com"] " a Isabel que Martim o fez o trabalho."]]
                 [[:span "Paco "  [:span.is-underlined "presenteou"] " uma rosa a Marisa."]
                  [:span "Paco " [:em "presenteou"] " uma rosa a Marisa."]]
                 [[:span "O buraco era profundo."]
                  [:span "O buraco era profundíssimo."]
                  [:span "O buraco era muito profundo."]]
                 [[:span "Foram no " [:span.is-underlined "seu"] " carro."]
                  [:span "Foram no carro dele/dela."]]
                 [[:span "O cachorro comeu "  [:span.is-underlined "o sorvete"]"."]
                  [:span "O que o cachorro comeu foi o sorvete."]
                  [:span "Foi o sorvete o que o cachorro comeu."]]
                 [[:span "Chegaram ao "  [:span.is-underlined "centro"] " da cidade."]
                  [:span "Foi ao " [:em "centro"] " da cidade que eles chegaram."]]]]
      (into [:ol.list.is-lower-alpha]
            (for [[a & b] items :let [b-comp (into [:div.red]
                                                   (map (fn [s] [:div.b s]) b))]]
              [:li
               [:div.grid2 a b-comp]])))]])

(defn render-respostas
  "Chapter 23 respostas"
  []
  [:main.c23.respostas.content
   [shared/fonetica-title "Capítulo 23 — A sílaba e a silabação"
    "Respostas para o eResource"]
   [:section.atividades
    [:h2 "ATIVIDADES"]
    [:ol.atividades.list
     [ra1]
     [ra2]]]])

(defn a1
  "Activity 1"
  []
  [:section.a1
   [:li [:div "Os seguintes versos são do poema “A invenção dos jardins” de António Feliciano de Castilho. Para cada verso, indique a silabação, sublinhe as sílabas tônicas e dê o número total de sílabas. Explique qual a metrificação e como os versos com um número diferente de sílabas nela se enquadram."
         [shared/media-icon :r (rfe/href :fonetica-port.routes/c23-r) :same-session]]
    (let [poem "aqui, aqui,
que hontem néstes lábios
tive o primeiro
beijo da minha Flérida,
e endoudeci!
Recordas-te? o crepúsculo
vinha caindo;
chilrava o bando alígero
hora d’amor;
todo era affecto e jubilo
teu gesto lindo;
todo eu desejos sôfregos
e interno ardor.
Co' o braço ao peito trêmulo
eu te cingia,
vendo em teu seio cândido
teu suspirar.
Este feliz silêncio,
nada o rompia;
eram suaves lágrimas
o meu fallar.
Da mão nevada e lânguida"]
      [:pre.ipa-like.is-size-6 poem])]])

(defn a2
  "Activity 2"
  []
  [:section.a2
   [:li [:div "Indique como se pode enfatizar o elemento sublinhado das seguintes orações."
         [shared/media-icon :r (rfe/href :fonetica-port.routes/c23-r) :same-session]]
    (let [items [[:span "João " [:span.is-underlined "lhe"] " deu o prato."]
                 [:span [:span.is-underlined "A Maria"] " festejou demais."]
                 [:span "Joãozinho comeu " [:span.is-underlined "a sobremesa"]"."]
                 [:span "O estudante é " [:span.is-underlined "inteligente"] "."]
                 [:span "Martim o fez " [:span.is-underlined "com Isabel"] "."]
                 [:span "Paco " [:span.is-underlined " presenteou"] " uma rosa a Mariza."]
                 [:span "O buraco era " [:span.is-underlined "profundo"]"."]
                 [:span "Foram no " [:span.is-underlined "seu"] " carro."]
                 [:span "O cachorro comeu " [:span.is-underlined "o sorvete"]"."]
                 [:span "Chegaram ao " [:span.is-underlined "centro"] " da cidade."]]]
      (into [:ol.list.is-lower-alpha]
            (for [i items] [:li i])))]])


(defn atividades
  "Chapter 23 atividades"
  []
  [:section.atividades.content
   [:h2 "ATIVIDADES"]
   [:ol.atividades
    [a1]
    [a2]]])

(defn recursos
  "Materials"
  []
  [:section.recursos
   [:h3 "RECURSOS ELETRÔNICOS"]
   (let [audio-items ["Consoantes homólogas do inglês."
                      "Consoantes homólogas do português."
                      "O ritmo na poesia do inglês."
                      "O ritmo na poesia do português."
                      "O ritmo em “Canção do exílio” de Gonçalves Dias."
                      "O ritmo acentual do inglês."
                      "Exemplos de ênfase em inglês e português."
                      "Exemplos de ênfase ou foco na sílaba em português."
                      "Exemplos de ênfase ou foco na palavra em português."
                      "Exemplos de ênfase ou foco em palavras átonas em inglês."
                      "Exemplos de acento afetivo em português."
                      "Exemplos de ênfase ou foco no sintagma em português."
                      "Exemplos de ênfase com Nível 4 na oração “I want John to do the work”."
                      "Exemplos de ênfase com Nível 3 na oração “Eu quero que o André lave a louça”."
                      "Exemplos de ênfase morfológica em português."
                      "Exemplos de ênfase sintática em português."
                      "Exemplos de ênfase sintática em português."
                      "Exemplos de ênfase em palavras átonas em inglês e português."
                      "Exemplos de ênfase fonética em português."
                      "Exemplos de ênfase semântica com intensificadores em inglês e português."
                      "Exemplos de ênfase semântica com reduplicação em inglês e português."
                      "Exemplos de ênfase paralinguística em português."]
         ex-items ["Exercícios de pronúncia: a duração."
                   "Exercícios de pronúncia: o ritmo."]
         
         fonetica-line-item (fn [n i]
                              [:li.list-item [shared/media-icon :audio
                                              (shared/file (str "C23-M" (inc n) ".mp3"))] i])
         fonetica-line-item2 (fn [n i]
                               (let [ic (-> audio-items count inc) ]
                                 [:li.list-item [shared/media-icon :ex
                                                 (shared/file (str "C23-M" (+ ic n) ".mp3"))] i]))]

     (into
      [:ol.list]
      (conj 
       (map-indexed fonetica-line-item2 ex-items)
       (map-indexed fonetica-line-item audio-items))))])

(defn render
  "Chapter 23 view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 23 — A duração, o ritmo e a ênfase"
    "eResource"]
   [:div.content.chapter-23
    [recursos]
    [atividades]]
   [shared/footer]])
