(ns fonetica-port.views.c19
  "Chapter 19"
  (:require [fonetica-port.views.components.shared :as shared :refer [ipa fempr red ipa-like]]
            [reitit.frontend.easy :as rfe]))

(defn ra1
  "Respostas activity 1"
  []
  [:section.ra1
   [:li
    [:div "Escreva o símbolo do alofone descrito pelos seguintes traços fonéticos."]
    (let [items
          [["vibrante múltiplo"
            "alveolar"
            "sonoro"
            "[r]"]

           ["lateral"
            "dental"
            "sonoro"
            "[¢]"]
           
           ["lateral"
            "alveolar"
            "sonoro"
            "[l]"]

           ["lateral"
            "palatal"
            "sonoro"
            "[Y]"]

           ["vibrante simples"
            "alveolar"
            "sonoro"
            "[R]"]

           ["lateral"
            "palatalizado"
            "sonoro"
            "[ù]"]
           
           ["fricativo"
            "uvular"
            "surdo"
            "[X]"]

           ["lateral"
            "alveolar velarizado"
            "sonoro"
            "[L]"]
           
           ["vibrante múltiplo"
            "uvular"
            "sonoro"
            "[±]"]
           
           ["fricativo"
            "uvular"
            "sonoro"
            "[³]"]]]
      [shared/list-2x2x3-1 items])]])

(defn ra2
  "Respostas activity 2"
  []
  [:section.ra2
   [:li
    [:div "Indique os traços fonéticos de cada alofone da lista."]
    (let [items [["[R]"
                  "vibrante simples"
                  "alveolar"
                  "sonoro"]

                 ["[í]"
                  "lateral"
                  "dental velarizado"
                  "sonoro"]

                 ["[Y]"
                  "lateral"
                  "palatal"
                  "sonoro"]

                 ["[r]"
                  "vibrante múltiplo"
                  "alveolar"
                  "sonoro"]

                 ["[h]"
                  "fricativo"
                  "glotal"
                  "surdo"]

                 ["[l]"
                  "lateral"
                  "alveolar"
                  "sonoro"]

                 ["[X]"
                  "fricativo"
                  "uvular"
                  "surdo"]

                 ["[±]"
                  "vibrante múltiplo"
                  "uvular"
                  "sonoro"]]]
      [shared/list-2x2x1-3 items])]])

(defn ra3
  "Respostas activity 3"
  []
  [:section.ra3
   [:li
    [:div
     [:span "O fonema " [ipa-like "/l/"] " tem quatro alofones: " [ipa "[l K L í ]"] ". Preencha o espaço em branco com o alofone
correto de acordo com o contexto, indicando se é a norma do Brasil ou de Portugal."]]
    (let [items [
                 [[:span  [ipa "[a tÓza]"] ] [:span "Br: " [ipa "[í]"] ] [:span "Po: " [ipa "[í]"] ]]
                 [[:span  [ipa "[si vÒR]"] ] [:span "Br: " [ipa "[K]"] ] [:span "Po: " [ipa "[L]"] ]]
                 [[:span  [ipa "[Ö timu]"] ] [:span "Br: " [ipa "[K]"] ] [:span "Po: " [ipa "[L]"] ]]
                 [[:span  [ipa "[ Òdu]"] ] [:span "Br: " [ipa "[l]"] ] [:span "Po: " [ipa "[l]"] ]]
                 [[:span  [ipa "[a agÒdu]"] ] [:span "Br: " [ipa "[l]"] ] [:span "Po: " [ipa "[l]"] ]]
                 [[:span  [ipa "[a fÒ fa]"] ] [:span "Br: " [ipa "[K]"] ] [:span "Po: " [ipa "[L]"] ]]
                 [[:span  [ipa "[a tÒr]"] ] [:span "Br: " [ipa "[í]"] ] [:span "Po: " [ipa "[í]"] ]]
                 [[:span  [ipa "[p ënu]"] ] [:span "Br: " [ipa "[l]"] ] [:span "Po: " [ipa "[l]"] ]]
                 [[:span  [ipa "[mi naves]"] ] [:span "Br: " [ipa "[K]"] ] [:span "Po: " [ipa "[L]"] ]]
                 [[:span  [ipa "[mi Ónju]"] ] [:span "Br: " [ipa "[l]"] ] [:span "Po: " [ipa "[l]"] ]]
                 [[:span  [ipa "[a kësa]"] ] [:span "Br: " [ipa "[K]"] ] [:span "Po: " [ipa "[L]"] ]]
                 [[:span  [ipa "[Ò du]"] ] [:span "Br: " [ipa "[í]"] ] [:span "Po: " [ipa "[í]"]]]] ]
      [shared/list-2x1x3 items])]])

(defn ra4
  "Respostas activity 4"
  []
  [:section.ra4
   [:li
    [:div "Indique se os grafemas {r} ou {rr} correspondem aos fonemas " [ipa "/R/"] " ou " [ipa "/r/"] ", ou ao arquifonema
" [ipa-like "/R/"] " nas seguintes palavras ou grupos de palavras."]
    (let [items [["rato" "fonema" "/r/"]
                 ["ferra" "fonema" "/r/"]
                 ["ferro" "fonema" "/r/"]
                 ["caro" "fonema" "/R/"]
                 ["amarro" "fonema" "/r/"]
                 ["honra" "fonema" "/r/"]
                 ["régua" "fonema" "/r/"]
                 ["Israel" "fonema" "/r/"]
                 ["astro" "fonema" "/R/"]
                 ["prato" "fonema" "/R/"]
                 ["sairarqui" "fonema" "/R/"]
                 ["partearqui" "fonema" "/R/"]
                 ["aro" "fonema" "/R/"]
                 ["rima" "fonema" "/r/"]]]
      (into [:ol.list.is-lower-alpha]
            (for [[a b c] items]
              [:li [:div.grid2
                    [:div.l a]
                    [:div.r.red  [:span b [ipa c]]]]])))]])

(defn ra5
  "Respostas activity 5"
  []
  [:section.ra5
   [:li
    [:div "O arquifonema /R/ tem vários alofones. Preencha o espaço em branco nas seguintes transcrições com o símbolo correto. Use o " [red "*"] " para indicar que o som pode ser qualquer dos seguintes: " [ipa "[r R h X ± ³]"] "."]
    (let [items [[:span.ipa "[ká" [:sup.ipa "ª"] "tÒ" [red "R"] "Øpe" [red "R"] "A]"]
                 [:span.ipa "[pa" [:span.normal.red "*"] "ÎÔ" [:span.normal.red "*"] "]"]
                 [:span.ipa "[comÓ" [:span.normal.red "*"] "kÒ" [:span.normal.red "*"] "ni]"]
                 [:span.ipa "[po" [:span.normal.red "*"] "favÕ" [:span.normal.red "*"] "]"]
                 [:span.ipa "[pa" [:span.normal.red "*"] "ÎikulÒ" [:span.normal.red "*"] "]"]
                 [:span.ipa "[estÒ" [red "R"] "at" [red "R"] "azÒdu]"]
                 [:span.ipa "[Ò" [:span.normal.red "*"] "vo" [red "R"] "i]"]
                 [:span.ipa "[po" [red "R"] "Óli]"]
                 [:span.ipa "[dez×" [:span.normal.red "*"] "tu]"]
                 [:span.ipa "[moÓ" [red "R"] "ot" [red "R"] "Ôgu]"]
                 [:span.ipa "[abe" [:span.normal.red "*"] "tÖ" [red "R"] "A]"]
                 [:span.ipa "[Ø" [:span.normal.red "*"] "bi]"]]]
      [shared/list-2x1 items])]])

(comment items [[:span.ipa "[ká" [ipa "ª"] "tÒRØpeRA]"]
                [:span.ipa "[pa*ÎÔ*]"]
                [:span.ipa "[comÓ*kÒ*ni]"]
                [:span.ipa "[po*favÕ*]"]
                [:span.ipa "[pa*ÎikulÒ*]"]
                [:span.ipa "[estÒRatRazÒdu]"]
                [:span.ipa "[Ò*voRi]"]
                [:span.ipa "[poRÓli]"]
                [:span.ipa "[dez×*tu]"]
                [:span.ipa "[moÓRotRÔgu]"]
                [:span.ipa "[abe*tÖRA]"]
                [:span.ipa "[Ø*bi]"]])

(defn render-respostas
  "Chapter 19 respostas"
  []
  [:main.c19.respostas.content
   [shared/fonetica-title "Capítulo 19 — Respostas a las atividades"]
   [:section.atividades
    [:h2 "ACTIVIDADES"]
    [:ol.atividades.list
     [ra1]
     [ra2]
     [ra3]
     [ra4]
     [ra5]]]])

(defn a1
  "Activity 1"
  []
  [:section.a1
   [:li
    [:div "Escreva o símbolo do alofone descrito pelos seguintes traços fonéticos."
     [shared/media-icon :r (rfe/href :fonetica-port.routes/c19-r) :same-session]]
    (let [items
          [["vibrante múltiplo"
            "alveolar"
            "sonoro"
            "[r]"]

           ["lateral"
            "dental"
            "sonoro"
            "[¢]"]
           
           ["lateral"
            "alveolar"
            "sonoro"
            "[l]"]

           ["lateral"
            "palatal"
            "sonoro"
            "[Y]"]

           ["vibrante simples"
            "alveolar"
            "sonoro"
            "[R]"]

           ["lateral"
            "palatalizado"
            "sonoro"
            "[ù]"]
           
           ["fricativo"
            "uvular"
            "surdo"
            "[X]"]

           ["lateral"
            "alveolar velarizado"
            "sonoro"
            "[L]"]
           
           ["vibrante múltiplo"
            "uvular"
            "sonoro"
            "[±]"]
           
           ["fricativo"
            "uvular"
            "sonoro"
            "[³]"]]]
      [shared/list-2x2x3-1 items :no-answers])]])

(defn a2
  "Activity 2"
  []
  [:section.a2
   [:li
    [:div "Indique os traços fonéticos de cada alofone da lista."
     [shared/media-icon :r (rfe/href :fonetica-port.routes/c19-r) :same-session]]
    (let [items [["[R]"
                  "vibrante simples"
                  "alveolar"
                  "sonoro"]

                 ["[í]"
                  "lateral"
                  "dental velarizado"
                  "sonoro"]

                 ["[Y]"
                  "lateral"
                  "palatal"
                  "sonoro"]

                 ["[r]"
                  "vibrante múltiplo"
                  "alveolar"
                  "sonoro"]

                 ["[h]"
                  "fricativo"
                  "glotal"
                  "surdo"]

                 ["[l]"
                  "lateral"
                  "alveolar"
                  "sonoro"]

                 ["[X]"
                  "fricativo"
                  "uvular"
                  "surdo"]

                 ["[±]"
                  "vibrante múltiplo"
                  "uvular"
                  "sonoro"]]]
      [shared/list-2x2x1-3 items :no-answers])]])

(defn a3
  "Activity 3"
  []
  [:section.a3
   [:li
    [:div
     [:span "O fonema " [ipa-like "/l/"] " tem quatro alofones: " [ipa "[l K L í ]"] ". Preencha o espaço em branco com o alofone
correto de acordo com o contexto, indicando se é a norma do Brasil ou de Portugal."]
     [shared/media-icon :r (rfe/href :fonetica-port.routes/c19-r) :same-session]]
    (let [items [
                 [[:span  [:span.ipa "[a" [:span.ipa-like "_"] "tÓza]"] ] [:span "Br: " [:span.ipa "[í]"] ] [:span "Po: " [:span.ipa "[í]"] ]]
                 [[:span  [:span.ipa "[si" [:span.ipa-like "_"] "vÒR]"] ] [:span "Br: " [:span.ipa "[K]"] ] [:span "Po: " [:span.ipa "[L]"] ]]
                 [[:span  [:span.ipa "[Ö" [:span.ipa-like "_"] "timu]"] ] [:span "Br: " [:span.ipa "[K]"] ] [:span "Po: " [:span.ipa "[L]"] ]]
                 [[:span  [:span.ipa "[" [:span.ipa-like "_"] "Òdu]"] ] [:span "Br: " [:span.ipa "[l]"] ] [:span "Po: " [:span.ipa "[l]"] ]]
                 [[:span  [:span.ipa "[a" [:span.ipa-like "_"] "agÒdu]"] ] [:span "Br: " [:span.ipa "[l]"] ] [:span "Po: " [:span.ipa "[l]"] ]]
                 [[:span  [:span.ipa "[a" [:span.ipa-like "_"] "fÒ fa]"] ] [:span "Br: " [:span.ipa "[K]"] ] [:span "Po: " [:span.ipa "[L]"] ]]
                 [[:span  [:span.ipa "[a" [:span.ipa-like "_"] "tÒr]"] ] [:span "Br: " [:span.ipa "[í]"] ] [:span "Po: " [:span.ipa "[í]"] ]]
                 [[:span  [:span.ipa "[p" [:span.ipa-like "_"] "ënu]"] ] [:span "Br: " [:span.ipa "[l]"] ] [:span "Po: " [:span.ipa "[l]"] ]]
                 [[:span  [:span.ipa "[mi" [:span.ipa-like "_"] "naves]"] ] [:span "Br: " [:span.ipa "[K]"] ] [:span "Po: " [:span.ipa "[L]"] ]]
                 [[:span  [:span.ipa "[mi" [:span.ipa-like "_"] "Ónju]"] ] [:span "Br: " [:span.ipa "[l]"] ] [:span "Po: " [:span.ipa "[l]"] ]]
                 [[:span  [:span.ipa "[a" [:span.ipa-like "_"] "kësa]"] ] [:span "Br: " [:span.ipa "[K]"] ] [:span "Po: " [:span.ipa "[L]"] ]]
                 [[:span  [:span.ipa "[Ò" [:span.ipa-like "_"] "du]"] ] [:span "Br: " [:span.ipa "[í]"] ] [:span "Po: " [:span.ipa "[í]"]]]] ]
      [shared/list-2x1x3 items :no-answers])]])

(defn a4
  "Activity 4"
  []
  [:section.a4
   [:li
    [:div "Indique se os grafemas {r} ou {rr} correspondem aos fonemas " [ipa "/R/"] " ou " [ipa "/r/"] ", ou ao arquifonema
" [ipa-like "/R/"] " nas seguintes palavras ou grupos de palavras."
     [shared/media-icon :r (rfe/href :fonetica-port.routes/c19-r) :same-session]]
    (let [items [["rato" "fonema" "/r/"]
                 ["ferra" "fonema" "/r/"]
                 ["ferro" "fonema" "/r/"]
                 ["caro" "fonema" "/R/"]
                 ["amarro" "fonema" "/r/"]
                 ["honra" "fonema" "/r/"]
                 ["régua" "fonema" "/r/"]
                 ["Israel" "fonema" "/r/"]
                 ["astro" "fonema" "/R/"]
                 ["prato" "fonema" "/R/"]
                 ["sairarqui" "fonema" "/R/"]
                 ["partearqui" "fonema" "/R/"]
                 ["aro" "fonema" "/R/"]
                 ["rima" "fonema" "/r/"]]]
      (into [:ol.list.is-lower-alpha]
            (for [[a _b _c] items]
              [:li [:div.grid2
                    [:div.l a]
                    [:div.blank]]])))]])

(defn a5
  "Activity 5"
  []
  [:section.a5
   [:li
    [:div "O arquifonema /R/ tem vários alofones. Preencha o espaço em branco nas seguintes transcrições com o símbolo correto. Use o " [red "*"] " para indicar que o som pode ser qualquer dos seguintes: " [ipa "[r R h X ± ³]"] "."
     [shared/media-icon :r (rfe/href :fonetica-port.routes/c19-r) :same-session]]
    (let [items [[:span.ipa "[ká" [:sup.ipa "ª"] "tÒ" [:span.ipa-like "_"] "Øpe" [:span.ipa-like "_"] "A]"]
                 [:span.ipa "[pa" [:span.ipa-like "_"] "ÎÔ" [:span.ipa-like "_"] "]"]
                 [:span.ipa "[comÓ" [:span.ipa-like "_"] "kÒ" [:span.ipa-like "_"] "ni]"]
                 [:span.ipa "[po" [:span.ipa-like "_"] "favÕ" [:span.ipa-like "_"] "]"]
                 [:span.ipa "[pa" [:span.ipa-like "_"] "ÎikulÒ" [:span.ipa-like "_"] "]"]
                 [:span.ipa "[estÒ" [:span.ipa-like "_"] "at" [:span.ipa-like "_"] "azÒdu]"]
                 [:span.ipa "[Ò" [:span.ipa-like "_"] "vo" [:span.ipa-like "_"] "i]"]
                 [:span.ipa "[po" [:span.ipa-like "_"] "Óli]"]
                 [:span.ipa "[dez×" [:span.ipa-like "_"] "tu]"]
                 [:span.ipa "[moÓ" [:span.ipa-like "_"] "ot" [:span.ipa-like "_"] "Ôgu]"]
                 [:span.ipa "[abe" [:span.ipa-like "_"] "tÖ" [:span.ipa-like "_"] "A]"]
                 [:span.ipa "[Ø" [:span.ipa-like "_"] "bi]"]]]
      [shared/list-2x1 items])]])

(defn atividades
  "Chapter 19 atividades"
  []
  [:section.atividades.content
   [:h2 "ATIVIDADES"]
   [:ol.atividades
    [a1]
    [a2]
    [a3]
    [a4]
    [a5]]])

(defn recursos
  "Materials"
  []
  [:section.recursos
   [:h3 "RECURSOS ELETRÔNICOS"]
   (let [audio-items [[:span "A oposição entre os fonemas laterais " [ipa "/l Y/"] "."]
                      [:span "A fonotática dos fonemas laterais " [ipa "/l Y/"] "."]
                      [:span "Pares mínimos com o fonema " [ipa "/l/"] "."]
                      [:span "Distribuição complementar do fonema " [ipa "/l/"] " no português brasileiro e europeu."]
                      [:span "A palavra " [ipa "[kÒlA]"] "."]
                      [:span "A palavra " [ipa-like "{alto}"] " no português brasileiro e europeu."]
                      [:span "A palavra " [ipa-like "{mal}"] " " [ipa "[mÒK]"] " no português brasileiro."]
                      [:span "A comparação da palavra " [ipa-like "{mal}"] " no português europeu " [ipa "[mÒL]"] " e no português brasileiro " [ipa "[mÒK]"] "."]
                      [:span "O " [ipa "[L]"] " velarizado do inglês."]
                      [:span "O ensurdecimento do " [ipa "[l]"] " em inglês."]
                      [:span "A comparação entre " [ipa "[U]"] " e " [ipa "[K]"] " no português brasileiro."]
                      [:span "A palavra " [ipa "[pÒYA]"] "."]
                      [:span "O contraste entre " [ipa-like "{filhar/filiar}"] " e {cilhar/ciliar}."]
                      [:span "O contraste entre " [ipa "[Y]"] ", " [ipa "[lj]"] " e " [ipa "[Lj]"] " em português e inglês."]
                      [:span "O contraste entre " [ipa-like "{all you}"] " do inglês e {alho} do português."]
                      [:span "A oposição entre os fonemas vibrantes " [ipa "/R r/"] "."]
                      [:span "A distribuição fonotática dos fonemas vibrantes e o arquifonema " [ipa-like "/R/"] "."]
                      [:span "A distribuição fonotática do fonema vibrante simples " [ipa "/R/"] "."]
                      [:span "As palavras " [ipa "[kÒRA]"] " e " [ipa "[kÒdA]"] "."]
                      [:span "O fonema " [ipa "/R/"] " nos seus três contornos fonotáticos."]
                      [:span "As palavras " [ipa "[kÒRA]"] " e " [ipa "[tÕURu]"] "."]
                      [:span "As palavras " [ipa-like "{três}"] " do português e {trace} do inglês."]
                      [:span "As palavras " [ipa "[kÒRu]"] " e " [ipa "[kÒru]"] "."]
                      [:span "A distribuição fonotática do fonema vibrante múltiplo " [ipa "/r/"] "."]
                      [:span "As variantes dialetais do fonema vibrante múltiplo " [ipa "/r/"] "."]
                      [:span "O alofone " [ipa "[h]"] " nas palavras " [ipa "[hÔmA]"] " e " [ipa "[kÒhu]"] "."]
                      [:span "O alofone " [ipa "[X]"] " nas palavras " [ipa "[XÔmA]"] " e " [ipa "[kÒXo]"] "."]
                      [:span "Os alofones " [ipa "[x]"] " e " [ipa "[X]"] " nas palavras " [ipa "[xÔmA]"] ", " [ipa "[kÒXo]"] " e " [ipa "[bÖXo]"] "."]
                      [:span "Animação da vibrante múltipla alveolar " [ipa "[r]"] "."]
                      [:span "As palavras " [ipa "[kÒru]"] " e " [ipa "[kÒRu]"] "."]
                      [:span "A vibração do papelzinho."]
                      [:span "O alofone " [ipa "[±]"] " nas palavras " [ipa "[±ÔmA]"] " e " [ipa "[kÒ±u]"] "."]
                      [:span "Animação da vibrante múltipla uvular " [ipa "[±]"] "."]
                      [:span "O alofone " [ipa "[³]"] " nas palavras " [ipa "[³ÔmA]"] " e " [ipa "[kÒ³o]"] "."]
                      [:span "Variantes prováveis e improváveis de " [ipa-like "{carro rápido}"] "."]
                      [:span "A distribuição fonotática do arquifonema vibrante " [ipa-like "/R/"] "."]
                      [:span "A distribuição fonotática do arquifonema vibrante " [ipa-like "/R/"] " com as variantes dialetais."]
                      [:span "O ensurdecimento e a sonorização dos alofones do arquifonema vibrante " [ipa-like "/R/"] "."]
                      [:span "Os alofones " [ipa "[X]"] " e " [ipa "[x]"] " do arquifonema vibrante " [ipa-like "/R/"] "."]
                      [:span "Os alofones " [ipa "[h]"] " e o zero fonético para o arquifonema vibrante " [ipa-like "/R/"] "."]
                      [:span "O alofone " [ipa "[Q]"] " no dialeto caipira para o arquifonema vibrante " [ipa-like "/R/"] "."]]
         ex-items [[:span "Exercícios de pronúncia: o fonema " [ipa "/l/"] "."]
                   [:span "Exercícios de pronúncia: o fonema " [ipa "/Y/"] "."]
                   [:span "Exercícios de pronúncia: o fonema " [ipa "/R/"] "."]
                   [:span "Exercícios de pronúncia: o fonema " [ipa "/r/"] "."]
                   [:span "Exercícios de pronúncia: o arquifonema " [ipa-like "/R/"] "."]] 
         fonetica-line-item (fn [n i]
                              [:li.list-item [shared/media-icon :audio
                                              (shared/file (str "C19-M" (inc n) ".mp3"))] i])
         fonetica-line-item2 (fn [n i]
                               (let [ic (-> audio-items count inc) ]
                                 [:li.list-item [shared/media-icon :ex
                                                 (shared/file (str "C19-M" (+ ic n) ".mp3"))] i]))]
     
     (into
      [:ol.list]
      (conj 
       (map-indexed fonetica-line-item2 ex-items)
       (map-indexed fonetica-line-item audio-items))))])

(defn render
  "Chapter nineteen view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 19 — Os fonemas laterais e vibrantes"
    "eResource"]
   [:div.content.chapter-19
    [recursos]
    [atividades]]
   [shared/footer]])
