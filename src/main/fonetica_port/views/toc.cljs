(ns fonetica-port.views.toc
  "Overall Table of Contents"
  (:require [reitit.frontend.easy :as rfe]))

(defn fonetica-title
  "Title Hero for the page"
  []
  [:section.hero
   [:h1.container.box.is-title
    "Manual de fonética e fonologia da língua portuguesa"]])

(defn toc-link
  "An li and href for a toc entry"
  [text href]
  [:li [:a {:href href} text]])

(defn render
  "Front table of contents view"
  []
  [:main#toc.container
   #_[:h1.has-text-warning {:style {:background-color "#006400"
                                  :font-size "1rem"
                                  :margin-bottom "1rem"}}
    "This site is under active development; some chapters are not yet complete"]
   [:section
    [:h4 "SEÇÃO I: Introdução"]
    [:ul 
     [toc-link "Capítulo 1 — A comunicação humana"
      (rfe/href :fonetica-port.routes/chapter-1)]
     [toc-link "Capítulo 2 — A linguística"
      (rfe/href :fonetica-port.routes/chapter-2)]
     [toc-link "Capítulo 3 — A fonética e a fonologia"
      (rfe/href :fonetica-port.routes/chapter-3)]
     [toc-link "Capítulo 4 — Sistemas de escrita"
      (rfe/href :fonetica-port.routes/chapter-4)]]]
   
   [:section
    [:h4 "SEÇÃO II: A fonética"]
    [:ul 
     [toc-link "Capítulo 5 — A fonética articulatória"
      (rfe/href :fonetica-port.routes/chapter-5)]
     [toc-link "Capítulo 6 — A fonética acústica"
      (rfe/href :fonetica-port.routes/chapter-6)]
     [toc-link "Capítulo 7 — A fonética auditiva"
      (rfe/href :fonetica-port.routes/chapter-7)]]]
   
   [:section
    [:h4 "SEÇÃO III: A fonologia"]
    [:ul 
     [toc-link "Capítulo 8 — A relação entre fonemas: a oposição e a neutralização"
      (rfe/href :fonetica-port.routes/chapter-8)]
     [toc-link "Capítulo 9 — A relação entre fonemas e alofones: a distribuição"
      (rfe/href :fonetica-port.routes/chapter-9)]
     [toc-link "Capítulo 10 — O posicionamento e a sequência de fonemas: a fonotática"
      (rfe/href :fonetica-port.routes/chapter-10)]]]
   
   [:section
    [:h4 "SEÇÃO IV: Os fonemas vocálicos e seus sons"]
    [:ul 
     [toc-link "Capítulo 11 — Os sistemas vocálicos do português e do inglês"
      (rfe/href :fonetica-port.routes/chapter-11)]
     [toc-link "Capítulo 12 — Os fonemas vocálicos"
      (rfe/href :fonetica-port.routes/chapter-12)]
     [toc-link "Capítulo 13 — As vogais meio-abertas e meio-fechadas"
      (rfe/href :fonetica-port.routes/chapter-13)]
     [toc-link "Capítulo 14 — Encontros vocálicos"
      (rfe/href :fonetica-port.routes/chapter-14)]
     [toc-link "Capítulo 15 — A nasalização vocálica"
      (rfe/href :fonetica-port.routes/chapter-15)]]]
   
   [:section
    [:h4 "SEÇÃO V: Os fonemas consonantais e seus sons"]
    [:ul 
     [toc-link "Capítulo 16 — Os fonemas oclusivos"
      (rfe/href :fonetica-port.routes/chapter-16)]
     [toc-link "Capítulo 17 — Os fonemas fricativos"
      (rfe/href :fonetica-port.routes/chapter-17)]
     [toc-link "Capítulo 18 — Os fonemas nasais"
      (rfe/href :fonetica-port.routes/chapter-18)]
     [toc-link "Capítulo 19 — Os fonemas laterais e vibrantes"
      (rfe/href :fonetica-port.routes/chapter-19)]
     [toc-link "Capítulo 20 — Encontros consonantais"
      (rfe/href :fonetica-port.routes/chapter-20)]]]
   [:section
    [:h4 "SECCIÓN VI: Los elementos suprasegmentales"]
    [:ul
     [toc-link "Capítulo 21 — A sílaba e a silabação"
      (rfe/href :fonetica-port.routes/chapter-21)]
     [toc-link "Capítulo 22 — A tonicidade ou acento fonético"
      (rfe/href :fonetica-port.routes/chapter-22)]
     [toc-link "Capítulo 23 — A duração, o ritmo e a ênfase"
      (rfe/href :fonetica-port.routes/chapter-23)]
     [toc-link "Capítulo 24 — A entonação"
      (rfe/href :fonetica-port.routes/chapter-24)]]]
   ];; MAIN
  )
