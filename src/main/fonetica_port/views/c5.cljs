(ns fonetica-port.views.c5
  "Chapter 5"
  (:require [fonetica-port.views.components.shared :as shared :refer [ipa]]
            [reitit.frontend.easy :as rfe]))

(defn render-respostas
  "Chapter 5 respostas"
  []
  [:main.c5.respostas.container.content
   [shared/fonetica-title "Capítulo 5 — A fonética articulatória"
    "Respostas para o eResource"]
   [:ol.list.is-lower-alpha
    [:li [:span "longo " [ipa "[léN go]"] " " [:span.red "1) " [ipa "[é]"] " antecipante/nasalização; 2) " [ipa "[N]"] " antecipante/ponto de articulação"]]]
    [:li [:span "inferior " [ipa "[ÞM feRiÕR]"] " " [:span.red "1) " [ipa "[Þ]"] " antecipante/nasalização; 2) " [ipa "[M]"] " antecipante/ponto de articulação"]]]
    [:li [:span "adiar " [ipa "[aËiÒR]"] " " [:span.red "1) " [ipa "[Ë]"] " antecipante/ponto e modo de articulación"]]]
    [:li [:span "oito " [ipa "[ÕItÍ]"] " " [:span.red "1) " [ipa "[Í4]"] " perseverante/estado das cordas vocais"]]]
    [:li [:span "as damas " [ipa "[azdÒmAs]"] " " [:span.red "1) " [ipa "[z]"] " antecipante/estado da cordas vocais"]]]
    [:li [:span "um beijo " [ipa "[àm bÓIZu]"] " " [:span.red "1) " [ipa "[à]"] " antecipante/nasalização; 2) " [ipa "[m]"] " antecipante/ponto de articulação"]]]
    [:li [:span "un dente " [ipa "[àª dçø Îi]"] " " [:span.red "1) " [ipa "[à]"] " antecipante/nasalização; 2) " [ipa "[ª]"] " antecipante/ponto de articulação; 3) " [ipa "[ç]"] " antecipante/nasalização; 4) " [ipa "[ø]"] " antecipante/ponto de articulação"]]]
    [:li [:span "as tapas " [ipa "[astÒpAs]"] " " [:span.red "1) " [ipa "[s]"] " anticipante/estado das cordas vocais"]]]
    [:li [:span "os outros " [ipa "[uzÕUtRus]"] " " [:span.red "1) " [ipa "[z]"] " antecipante/estado das cordas vocais"]]]
    [:li [:span "un dia " [ipa "[àø ËÔA]"] " " [:span.red "1) " [ipa "[à]"] " antecipante/nasalização; 2) " [ipa "[ø]"] " antecipante/ponto e modo de articulação"]]]]])

(defn ej-15
  "Capítulo 5: Nota 15"
  []
  (shared/render-ej {:index "Capítulo 5: Nota 15"
                     :title "Os pontos de articulação"
                     :body
                     [:section.nota15
                      "Identifique cada ponto de articulação na figura da esquerda e escreva o nome linguístico da cada um na linha numerada correspondente à direita."
                      [:img {:src (shared/file "C5-M15.jpg")
                             :alt "Llene los espacios en blanco con el término lingüístico para los distintos lugares de articulación de las consonantes de español."}]]}))

(defn ej-26
  "Capítulo 5: Nota 26"
  []
  (shared/render-ej {:index "Capítulo 5: Nota 26"
                     :title "O quadro consonantal (para estudar)"
                     :body
                     [:section.nota26
                      "Estude o seguinte quadro até sabê-lo bem."
                      [:img {:src (shared/file "C5-M26a.jpg")
                             :alt "Estude o seguinte quadro até sabê-lo bem."}]
                      [:section.nota26b
                       [shared/fonetica-title "El cuadro consonántico (para practicar)"]
                       [:p "Imprima o seguinte quadro e preencha-o com os sons do português usando os símbolos do Alfabeto Fonético Internacional. Repita o processo até conseguir preencher o quadro sem consulta e sem errar."]
                       [:img {:src (shared/file "C5-M26b.jpg")
                              :alt "El cuadro consonántico (para practicar)"}]]]}))

(defn ej-36
  "Capítulo 5: Nota 36"
  []
  (shared/render-ej {:index "Capítulo 5: Nota 36"
                     :title "O quadro vocálico (para estudar)"
                     :body
                     [:section.nota36
                      "Estude o seguinte quadro até sabê-lo bem."
                      [:img {:src (shared/file "C5-M36a.jpg")
                             :alt "Estudie bien el siguiente cuadro hasta saberlo bien."}]
                      [:section.nota36b
                       [shared/fonetica-title "O quadro vocálico (para praticar)"]
                       [:p "Imprima o seguinte quadro e preencha-o com os sons do português usando os símbolos do Alfabeto Fonético Internacional. Repita o processo até conseguir preencher o quadro sem consulta e sem errar."]
                       [:img {:src (shared/file "C5-M36b.jpg")
                              :alt "Imprima el siguiente cuadro y rellénelo según los sonidos del español con los símbolos"}]]]}))

(defn ej-37
  "Capítulo 5: Nota 37"
  []
  (shared/render-ej {:index "Capítulo 5: Nota 37"
                     :title "A transcrição fonética"
                     :body
                     [:section.nota37
                      [:h3 "O texto ortográfico"]
                      [:p.quote "“SÍTIO — Vendo. Barbada. Ótima localização. Água à vontade. Árvores frutíferas. Caça
abundante. Um paraíso. Antigos ocupantes despejados por questões morais. Ideal para casal de
mais idade. Negócio de Pai para filhos. Tratar com Deus.”" [:sup "1"]]
                      [:div.transcripcion-paulistana
                       [:h3 "Transcrição fonética—norma culta paulistana"]
                       [:span.ipa.is-size-5 "[sÔÎiU\\vç" [:sup "ª"] "du\\barbÒdA\\ØÎimAlokalizasëä\\Ògwavß" [:sup "ª"] "tÒËi\\ÒRvoRisfRuÎÔf eRAs\\kÒsabà" [:sup "ª"] "dëøÎi\\à" [:sup "m"] "paraÔzu\\áøÎÔguzokupëøÎizdespeZÒduspoRkestéãz moRÒIs\\ideÒKpRakazÒKËimÒIzidÒËi\\negØsjuËipÒIpRafÔYus\\tRatÒRkß" [:sup "ª"] " dÓUs]"] ]
                      
                      [:div.transcripcion-lisboeta
                       [:h3 "Transcrição fonética—norma culta lisboeta"]
                       [:span.ipa.is-size-5 "[sÔtju\\vç" [:sup "ª"] "du\\bA³bÒdA\\Øti" [:sup "m"] "AlukAlizAsëä\\Ògwavß" [:sup "ª"] "tÒdš\\Ò³vuRšSfRutÔfe RAS\\kÒsabà" [:sup "ª"] "dë" [:sup "ª"] "øtš\\à" [:sup "m"] "pArAÔzu\\á" [:sup "ª"] "tÔguzOkupëøtšzd‰Sp‰ZÒduSpo³k‰StéãZm uRÒIS\\idjÒLpRAkAzÒLdšmÒIzidÒdš\\n‰gØsjudšpÒIpRAfÔYuS\\ tRAtÒ³kß" [:sup "ª"] "dÓUS]"] ]
                      
                      [:div.endnote [:sup "1"] "Quiroga, Horacio. “El síncope blanco”."]]}))

(defn atividade
  "Chapter 5 atividades"
  []
  [:section.ativitade.content
   [:h2 "ATIVIDADE"]
   [:p "As seguintes palavras contêm exemplos de coarticulação/assimilação. Para cada palavra, identifique o tipo (ou tipos) de coarticulação/assimilação exemplificado(s). Qual é o traço fonético que se assimila?"
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c5-r) :same-session]]
   [:ol.list.is-lower-alpha 
    [:li "longo " [ipa [:span "[lé" [:sup"N"]" go]"]]]
    [:li "inferior " [:span.ipa "[Þ" [:sup "M"] " feRiÕR]"]]
    [:li "adiar " [ipa "[aËiÒR]"]]
    [:li "oIto " [ipa "[ÕItÍ]"]]
    [:li "as damas " [ipa "[azdÒmAs]"]]
    [:li "um beijo " [:span.ipa "[à" [:sup "m"] " bÓIZu]"]]
    [:li "un dente " [:span.ipa "[à" [:sup "ª"] " dç" [:sup "ø"]" Îi]"]]
    [:li "as tapas " [ipa "[astÒpAs]"]]
    [:li "os outros " [ipa "[uzÕUtRus]"]]
    [:li "un dia " [:span.ipa "[à" [:sup "ø"] " ËÔA]"]]

    ]])

(defn render
  "Chapter three view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 5 — A fonética articulatória"
    "eResource"]
   [:div.content.chapter-5
    [:section.recursos
     [:h3 "RECURSOS ELETRÔNICOS"]
     [:ol
      [:li [shared/media-icon :video (shared/file "C5-M1-voice_onset_video_3.mp4")] "Vídeo de alta velocidade da ação das cordas vocais mostrando distintos estados da glote."]
      [:li [shared/media-icon :video (shared/file "C5-M2-videoPaper-Blow-Side.mp4")] "Vídeo mostrando o princípio de Bernoulli soprando entre duas papeletas."]
      [:li [shared/media-icon :video (shared/file "C5-M3-Vocal-Chord-Movement.mov.ff.mp4")] "Vídeo da vibração das cordas vocais feito com um laringoscópio da KayPENTAX."]
      [:li [shared/media-icon :video (shared/file "C5-M4-Flouroscopy.mov.ff.mp4")] "Vídeo fluoroscópico da pronúncia de várias orações."]
      [:li[shared/media-icon :audio (shared/file "C5-M5.mp3")] "Sons orais."]
      [:li[shared/media-icon :audio (shared/file "C5-M6.mp3")] "Sons nasais."]
      [:li[shared/media-icon :audio (shared/file "C5-M7.mp3")] "Sons oronasais."]
      [:li[shared/media-icon :audio (shared/file "C5-M8.mp3")] "Consoantes oclusivas."]
      [:li[shared/media-icon :audio (shared/file "C5-M9.mp3")] "Consoantes fricativas."]
      [:li[shared/media-icon :audio (shared/file "C5-M10.mp3")] "Consoantes africadas."]
      [:li[shared/media-icon :audio (shared/file "C5-M11.mp3")] "Consoantes nasais."]
      [:li[shared/media-icon :audio (shared/file "C5-M12.mp3")] "Consoantes laterais."]
      [:li[shared/media-icon :audio (shared/file "C5-M13.mp3")] "Consoante vibrante simples."]
      [:li[shared/media-icon :audio (shared/file "C5-M14.mp3")] "Consoantes vibrantes múltiplas."]
      [:li[shared/media-icon :ej (rfe/href :fonetica-port.routes/c5-ej15) :same-session] "Os pontos de articulação."]
      [:li[shared/media-icon :audio (shared/file "C5-M16.mp3")] "Consoantes bilabiais."]
      [:li[shared/media-icon :audio (shared/file "C5-M17.mp3")] "Consoantes labiodentais."]
      [:li[shared/media-icon :audio (shared/file "C5-M18.mp3")] "Consoantes dentais."]
      [:li[shared/media-icon :audio (shared/file "C5-M19.mp3")] "Consoantes alveolares."]
      [:li[shared/media-icon :audio (shared/file "C5-M20.mp3")] "Consoantes palatais."]
      [:li[shared/media-icon :audio (shared/file "C5-M21.mp3")] "Consoantes velares."]
      [:li[shared/media-icon :audio (shared/file "C5-M22.mp3")] "Consoantes uvulares."]
      [:li[shared/media-icon :audio (shared/file "C5-M23.mp3")] "Consoante glotal."]
      [:li[shared/media-icon :audio (shared/file "C5-M24.mp3")] "Consoantes sonoras."]
      [:li[shared/media-icon :audio (shared/file "C5-M25.mp3")] "Consoantes surdas."]
      [:li [shared/media-icon :text (rfe/href :fonetica-port.routes/c5-ej26) :same-session]
       [shared/media-icon :ej (rfe/href :fonetica-port.routes/c5-ej26) :same-session]
       "Quadro fonético consonantal completo (para estudar) e quadro fonético em branco (para praticar)."]
      [:li[shared/media-icon :audio (shared/file "C5-M27.mp3")] "Semiconsoantes."]
      [:li[shared/media-icon :audio (shared/file "C5-M28.mp3")] "Semivogais."]
      [:li[shared/media-icon :audio (shared/file "C5-M29.mp3")] "Vogais fechadas."]
      [:li[shared/media-icon :audio (shared/file "C5-M30.mp3")] "Vogais meio-fechadas."]
      [:li[shared/media-icon :audio (shared/file "C5-M31.mp3")] "Vogais meio-abertas."]
      [:li[shared/media-icon :audio (shared/file "C5-M32.mp3")] "Vogal aberta."]
      [:li[shared/media-icon :audio (shared/file "C5-M33.mp3")] "Vogais anteriores."]
      [:li[shared/media-icon :audio (shared/file "C5-M34.mp3")] "Vogais centrais."]
      [:li[shared/media-icon :audio (shared/file "C5-M35.mp3")] "Vogais posteriores."]
      [:li [shared/media-icon :text (rfe/href :fonetica-port.routes/c5-ej36) :same-session]
       [shared/media-icon :ej (rfe/href :fonetica-port.routes/c5-ej36) :same-session]
       "Quadro fonético vocálico completo (para estudiar) y quadro fonético en blanco (para practicar)."]
      [:li [shared/media-icon :text (rfe/href :fonetica-port.routes/c5-ej37) :same-session] "Transcrições fonéticas."]
      #_[:li [shared/media-icon :text (rfe/href :fonetica-port.routes/c5-ej35) :same-session] "Vogais posteriores."]
      [:li[shared/media-icon :audio (shared/file "C5-M38.mp3")] "Exemplo de facilidade de articulação: " [ipa "[Òznu]"] "."]
      [:li[shared/media-icon :audio (shared/file "C5-M39.mp3")] "Exemplo de separação perceptiva suficiente: " [ipa "[kÒRu] [kÒru]" ]"."]
      [:li[shared/media-icon :audio (shared/file "C5-M40.mp3")] "Exemplos de coarticulação antecipatória."]
      [:li[shared/media-icon :audio (shared/file "C5-M41.mp3")] "Exemplos de coarticulação perseveratória."]
      [:li[shared/media-icon :audio (shared/file "C5-M42.mp3")] "Exemplos de coarticulação recíproca."]
      [:li[shared/media-icon :audio (shared/file "C5-M43.mp3")] "Exemplos de coarticulação convergente."]]]
    [atividade]]
   
   [shared/footer]])
