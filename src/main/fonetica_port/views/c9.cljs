(ns fonetica-port.views.c9
  "Chapter 9"
  (:require [fonetica-port.views.components.shared :as shared :refer [ipa ipa-like]]
            [reitit.frontend.easy :as rfe]))

(defn ra1
  "Respostas Activity 1"
  []
  [:section.ra1
   [:li.respostas-a1 "Escreva o símbolo do alofone descrito pelos seguintes traços."
    (let [items [[["fricativo"
                   "palatal"
                   "sonoro"]
                  "[Z]"] ;a

                 [["fricativo"
                   "uvular"
                   "sonoro"]
                  "[³]"] ;b

                 [["lateral"
                   "alveolar velarizado"
                   "sonoro"]
                  "[L]"] ;c

                 [["africado"
                   "palatal"
                   "surdo"]
                  "[Î]"] ;d

                 [["nasal"
                   "labiodental"
                   "sonoro"]
                  "[M]"] ;e

                                  [["fricativo"
                   "uvular"
                   "surdo"]
                                   "[h]"] ;f
                 
                 [["oclusivo"
                   "velar"
                   "surdo"]
                  "[k]"] ;g

                 [["lateral"
                   "palatal"
                   "sonoro"]
                  "[Y]"] ;h

                 [["semiconsonante"
                   "anterior"
                   "oral"]
                  "[j]"] ;i
                 

                 [["fechado"
                   "central"
                   "oral"]
                  "[š]"] ;j

                 [["meio-aberto"
                   "central"
                   "oronasal"]
                  "[á]"] ;k

                 [["meio-fechado"
                   "anterior"
                   "oral"]
                  "[e]"] ;l


                 ]

          build-2x2 (fn [items]
                      (for [[places ipa-text] items]
                        [:li [:div.grid2
                              (into [:div.left]
                                    (for [p places]
                                      [:div p]))
                              [:div.right.ipa.red.is-size-4 ipa-text]]]))
          grid-items (into [:div.grid2] (build-2x2 items))]
      [:ol.list.is-lower-alpha
       grid-items])]])

(defn ra2
  "Respostas Activity 2"
  []
  [:section.ra2
   [:li.respostas-a2 "Indique os traçç fonéticos básicos dos seguintes alofones."
    (let [items
          [["[ª]"
            ["nasal"
             "dental"
             "sonoro"]]
           ["[U]"
            ["semivogal"
             "posterior"]]
           ["[Î]"
            ["africado"
             "palatal"
             "surdo"]]
           ["[Ë]"
            ["africado"
             "palatal"
             "sonoro"]]
           ["[v]"
            ["fricativo"
             "labiodental"
             "sonoro"]]
           ["[r]"
            ["vibrante múltipla"
             "alveolar"
             "sonoro"]]
           ["[N]"
            ["nasal"
             "velar"
             "sonoro"]]
           ["[X]"
            ["fricativo"
             "uvular"
             "surdo"]]]
          build-2x2 (fn [items]
                      (for [[ipa-text places] items]
                        [:li [:div.grid2
                              [:div.left.ipa ipa-text]
                              (into [:div.right.red]
                                    (for [p places]
                                      [:div p]))]]))
          grid-items (into [:div.grid2] (build-2x2 items))]
      [:ol.list.is-lower-alpha
       grid-items])]])

(defn ra3
  "Respostas Activity 3"
  []
  [:section.ra3
   [:li.respostas-a3 "Faça uma cadeia fonética, mudando um só traço de cada vez."
    [:div.red 
     "As respostas variam; é só preciso verificar se apenas um traço foi mudado de cada vez, seja o modo de articulação, o ponto de articulação, ou o estado das cordas vocais."]]])

(defn ra4
  "Respostas Activity 4"
  []
  [:section.ra4
   [:li.respostas-a4 "Para as seguintes regras de distribuição: (a) identifique o tipo de distribuição, (b) interprete a regra em palavras e (c) dê exemplos de sua aplicação, tendo o cuidado de exemplificar todos os aspectos da regra."
    [:ol.list.is-lower-alpha
     (let [items
           [[{:src "C9-A4a.png" :height 2} [:span "(a) distribuição única; (b) o fonema " [ipa "/f/"] " é sempre representado pelo seu único alofone
" [ipa "[f]"] "; (c) " [ipa "[fÒtu]"] ]]
            [{:src "C9-A4b.jpg" :height 6} [:span "(a) distribuição complementar do arquifonema " [ipa "/"] [ipa-like "S"] [ipa "/"] " nos dialetos com palatalização; (b) o arquifonema " [ipa "/"] [ipa-like "S"] [ipa "/"] " é representado pelo alofone palatal sonoro " [ipa "[Z]"] " diante de qualquer consoante sonora, pelo alofone " [ipa "[z]"] " em posição final de palavra diante de uma vogal e pelo alofone palatal surdo " [ipa "[S]"] " nos outros lugares; (c) " [ipa "[deZËi]"] "
" [ipa "[uzÕUtruS]"] " " [ipa "[d×S]"] ]]
            [{:src "C9-A4c.jpg" :height 4} [:span "(a) distribuição livre; (b) o fonema " [ipa "/r/"] " pode ser representado indistintamente pelos cinco alofones segundo o dialeto; (c) " [ipa "[kÒhu]"] " " [ipa "[kÒXu]"] " " [ipa "[kÒru]"] " " [ipa "[kÒ±u]"] " " [ipa "[kÒ³u]"] ]]
            [{:src "C9-A4d.jpg" :height 10} [:span "(a) distribuição complementar do fonema " [ipa "/a/"] " no Brasil; (b) o fonema " [ipa "/a/"] " no Brasil é representado pelo alofone " [ipa "[A]"] " quando é átono em posição final de palavra, pelo alofone " [ipa "[á]"] " quando aparece diante do arquifonema nasal " [ipa "/"] [ipa-like "N"] [ipa "/"] " ou quando é tônico diante de uma consoante nasal em posição inicial de sílaba e é realizado pelo alofone " [ipa "[a]"] " nos outros lugares; (c) " [ipa "[mÓzA]"] " " [ipa "[këmpu]"] " " [:span.ipa "[kë" [:sup "m"] "A]"] " " [ipa "[kÒhu]"] ]]
            [{:src "C9-A4e.jpg" :height 4} [:span "a) distribuição mista; (b) o arquifonema " [ipa "/"] [ipa-like "R"] [ipa "/"] " é representado pelo alófono " [ipa "[R]"] " em posição final de palavra diante de vogal; nos outros lugares, pode ser representado indistintamente pelos seis alofones segundo o dialeto/falante; (c) " [ipa "[sÓRê]"] " " [ipa "[sÓRDÕIs]"] " " [ipa "[sÓhDÕIs]"] " " [ipa "[sÓXDÕIs]"] " " [ipa "[sÓrDÕIs]"] " " [ipa "[sÓ±DÕIs]"] " " [ipa "[sÓ³DÕIs]"] ]]
            [{:src "C9-A4f.jpg" :height 12} [:span "(a) distribuição complementar; (b) o arquifonema " [ipa "/"] [ipa-like "N"] [ipa "/"] " sempre nasaliza a vogal anterior e produz uma consoante nasal de transição no messmo ponto de articulação da consoante oclusiva, africada ou fricativa labiodental que antecede; nos outros lugares há nasalização da vogal sem a criação de uma consoante de transição; (c) " [:span.ipa "[kë" [:sup "m"] "pu]"] " 
" [:span.ipa "[à" [:sup "M"] "faRÕK]"] " " [:span.ipa "[dÝ" [:sup "ª"] "tÒK]"] " " [:span.ipa "[dç" [:sup "ø"] "Îi]"] " " [:span.ipa "[më" [:sup "N"] "gA]"] " " [:span.ipa "[kësA]"] ]]]
          build-2x2 (fn [items]
                      (for [[image-map annotation] items :let [{:keys [src height]} image-map]]
                        [:li.list-item [:div.grid2
                                        [:div.left
                                         [:img {:src (shared/file src)
                                                :style {"height" (str height "em")} :alt src}]]
                              [:div.right.red annotation]]]))
           grid-items (build-2x2 items)]
      [:ol.list.is-lower-alpha
       grid-items])]]])

(defn render-respostas
  "Chapter 9 respostas"
  []
  [:main.c9.respostas.content
   [shared/fonetica-title "Capítulo 9 — A relação entre fonemas e alofones: a distribuição"
    "Respostas para o eResource"]
   [:section.atividades
    [:h2 "ATIVIDADES"]
    [:ol.atividades.list
     [ra1]
     [ra2]
     [ra3]
     [ra4]]]])

(defn a1
  "Activity 1"
  []
  [:section.a1
   [:li.a1 "Escreva o símbolo do alofone descrito pelos seguintes traços."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c9-r) :same-session]
    (let [items [[["fricativo"
                   "palatal"
                   "sonoro"]
                  "[Z]"] ;a

                 [["fricativo"
                   "uvular"
                   "sonoro"]
                  "[³]"] ;b

                 [["lateral"
                   "alveolar velarizado"
                   "sonoro"]
                  "[L]"] ;c

                 [["africado"
                   "palatal"
                   "surdo"]
                  "[Î]"] ;d

                 [["nasal"
                   "labiodental"
                   "sonoro"]
                  "[M]"] ;e

                                  [["fricativo"
                   "uvular"
                   "surdo"]
                                   "[h]"] ;f
                 
                 [["oclusivo"
                   "velar"
                   "surdo"]
                  "[k]"] ;g

                 [["lateral"
                   "palatal"
                   "sonoro"]
                  "[Y]"] ;h

                 [["semiconsonante"
                   "anterior"
                   "oral"]
                  "[j]"] ;i
                 

                 [["fechado"
                   "central"
                   "oral"]
                  "[š]"] ;j

                 [["meio-aberto"
                   "central"
                   "oronasal"]
                  "[á]"] ;k

                 [["meio-fechado"
                   "anterior"
                   "oral"]
                  "[e]"] ;l


                 ]

          build-2x2 (fn [items]
                      (for [[places _ipa-text] items]
                        [:li [:div.grid2
                              (into [:div.left]
                                    (for [p places]
                                      [:div p]))
                              [:div]]]))
          grid-items (into [:div.grid2] (build-2x2 items))]
      [:ol.list.is-lower-alpha
       grid-items])]])

(defn a2
  "Activity 2"
  []
  [:section.ra2
   [:li.a2 "Indique os traços fonéticos básicos dos seguintes alofones."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c9-r) :same-session]
    (let [items
          [["[ª]"
            ["nasal"
             "dental"
             "sonoro"]]
           ["[U]"
            ["semivogal"
             "posterior"]]
           ["[Î]"
            ["africado"
             "palatal"
             "surdo"]]
           ["[Ë]"
            ["africado"
             "palatal"
             "sonoro"]]
           ["[v]"
            ["fricativo"
             "labiodental"
             "sonoro"]]
           ["[r]"
            ["vibrante múltipla"
             "alveolar"
             "sonoro"]]
           ["[N]"
            ["nasal"
             "velar"
             "sonoro"]]
           ["[X]"
            ["fricativo"
             "uvular"
             "surdo"]]]
          build-2x2 (fn [items]
                      (for [[ipa-text _places] items]
                        [:li [:div.grid2
                              [:div.left.ipa ipa-text]
                              [:div]]]))
          grid-items (into [:div.grid2] (build-2x2 items))]
      [:ol.list.is-lower-alpha
       grid-items])]])

(defn a3
  "Activity 3"
  []
  [:section.a3
   [:li.a3.list-item "Faça uma cadeia fonética, mudando um só traço de cada vez."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c9-r) :same-session]
    [:div "Exemplo: " [ipa "[b] > [p] > [m] > [N]"]]
    [:p "No primeiro passo, só se muda o estado das cordas vocais de sonoro para surdo."]
    [:p "No segundo passo, só se muda o modo de articulação de oclusivo para nasal."]
    [:p "No terceiro passo, só se muda o ponto de articulação de bilabial para velar."]
    (let [items ["[Z] > [ ] > [ ] > [ ]"
                 "[Î] > [ ] > [ ] > [ ]"
                 "[l] > [ ] > [ ] > [ ]"
                 "[m] > [ ] > [ ] > [ ]"
                 "[R] > [ ] > [ ] > [ ]"]]
      (into [:ol.list.is-lower-alpha]
            (for [i items] [:li.ipa.list-item i])))]])

(defn a4
  "Activity 4"
  []
  [:section.a4
   [:li.a4 "Para as seguintes regras de distribuição: (a) identifique o tipo de distribuição, (b) interprete a regra em palavras e (c) dê exemplos de sua aplicação, tendo o cuidado de exemplificar todos os aspectos da regra."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c9-r) :same-session]
    
    [:ol.list.is-lower-alpha
     (let [items
           [[{:src "C9-A4a.png" :height 2} [:span "(a) distribuição única; (b) o fonema " [ipa "/f/"] " é sempre representado pelo seu único alofone
" [ipa "[f]"] "; (c) " [ipa "[fÒtu]"] ]]
            [{:src "C9-A4b.jpg" :height 6} [:span "(a) distribuição complementar do arquifonema " [ipa "/"] [ipa-like "S"] [ipa "/"] " nos dialetos com palatalização; (b) o arquifonema " [ipa "/"] [ipa-like "S"] [ipa "/"] " é representado pelo alofone palatal sonoro " [ipa "[Z]"] " diante de qualquer consoante sonora, pelo alofone " [ipa "[z]"] " em posição final de palavra diante de uma vogal e pelo alofone palatal surdo " [ipa "[S]"] " nos outros lugares; (c) " [ipa "[deZËi]"] "
" [ipa "[uzÕUtruS]"] " " [ipa "[d×S]"] ]]
            [{:src "C9-A4c.jpg" :height 4} [:span "(a) distribuição livre; (b) o fonema " [ipa "/r/"] " pode ser representado indistintamente pelos cinco alofones segundo o dialeto; (c) " [ipa "[kÒhu]"] " " [ipa "[kÒXu]"] " " [ipa "[kÒru]"] " " [ipa "[kÒ±u]"] " " [ipa "[kÒ³u]"] ]]
            [{:src "C9-A4d.jpg" :height 10} [:span "(a) distribuição complementar do fonema " [ipa "/a/"] " no Brasil; (b) o fonema " [ipa "/a/"] " no Brasil é representado pelo alofone " [ipa "[A]"] " quando é átono em posição final de palavra, pelo alofone " [ipa "[á]"] " quando aparece diante do arquifonema nasal " [ipa "/"] [ipa-like "N"] [ipa "/"] " ou quando é tônico diante de uma consoante nasal em posição inicial de sílaba e é realizado pelo alofone " [ipa "[a]"] " nos outros lugares; (c) " [ipa "[mÓzA]"] " " [ipa "[këmpu]"] " " [:span.ipa "[kë" [:sup "m"] "A]"] " " [ipa "[kÒhu]"] ]]
            [{:src "C9-A4e.jpg" :height 4} [:span "a) distribuição mista; (b) o arquifonema " [ipa "/"] [ipa-like "R"] [ipa "/"] " é representado pelo alófono " [ipa "[R]"] " em posição final de palavra diante de vogal; nos outros lugares, pode ser representado indistintamente pelos seis alofones segundo o dialeto/falante; (c) " [ipa "[sÓRê]"] " " [ipa "[sÓRDÕIs]"] " " [ipa "[sÓhDÕIs]"] " " [ipa "[sÓXDÕIs]"] " " [ipa "[sÓrDÕIs]"] " " [ipa "[sÓ±DÕIs]"] " " [ipa "[sÓ³DÕIs]"] ]]
            [{:src "C9-A4f.jpg" :height 12} [:span "(a) distribuição complementar; (b) o arquifonema " [ipa "/"] [ipa-like "N"] [ipa "/"] " sempre nasaliza a vogal anterior e produz uma consoante nasal de transição no messmo ponto de articulação da consoante oclusiva, africada ou fricativa labiodental que antecede; nos outros lugares há nasalização da vogal sem a criação de uma consoante de transição; (c) " [:span.ipa "[kë" [:sup "m"] "pu]"] " 
" [:span.ipa "[à" [:sup "M"] "faRÕK]"] " " [:span.ipa "[dÝ" [:sup "ª"] "tÒK]"] " " [:span.ipa "[dç" [:sup "ø"] "Îi]"] " " [:span.ipa "[më" [:sup "N"] "gA]"] " " [:span.ipa "[kësA]"] ]]]
          build-2x2 (fn [items]
                      (for [[image-map] items :let [{:keys [src height]} image-map]]
                        [:li.list-item [:div.grid2
                                        [:div.left
                                         [:img {:src (shared/file src)
                                                :style {"height" (str height "em")} :alt src}]]
                              [:div.right]]]))
           grid-items (build-2x2 items)]
      [:ol.list.is-lower-alpha
       grid-items])]]])

(defn atividades
  "Chapter 9 atividades"
  []
   [:section.atividades.content
    [:h2 "ATIVIDADES"]
    [:ol.atividades
     [a1]
     [a2]
     [a3]
     [a4]]])

(defn fuentes
  "The fuentes section"
  []
  [:section.fuentas
   [:h3 "FUENTES DE LOS EJERCICIOS DE TRANSCRIPCIÓN"]

   [:ol
    [:li "“Envidia,” " [:em "Historias de la Artámila"] " Ana María Matute"]
    [:li "“Los alambradores,” " [:em "Historias de la Artámila"] " Ana María Matute"]
    [:li "“A la deriva,” " [:em "Cuentos de amor"] " de locura y de muerte, Horacio Quiroga"]
    [:li "“La fotografía,” " [:em "Después del temporal"] " Enrique Amorim"]
    [:li "“Rosarito,” " [:em "Femeninas: Seis historias amorosas"] " Ramón María del Valle-Inclán"]
    [:li "“Chac Mool,” " [:em "Los días enmascarados"] " Carlos Fuentes"]
    [:li [:em "Platero y yo"] ", Juan Ramón Jiménez"]
    [:li [:em "San Manuel Bueno, mártir"] ", Miguel de Unamuno"]]])

(defn recursos
  "Materials"
  []
  [:section.recursos
   [:h3 "RECURSOS ELETRÔNICOS"]
   [:ol
    [:li [shared/media-icon :audio (shared/file "C9-M1.mp3")] "Exemplos da distribuição complementar optativa dos fonemas "[ipa "/t d/"] "."]    
    [:li [shared/media-icon :audio (shared/file "C9-M2.mp3")] "Exemplos da distribuição complementar do arquifonema " [ipa "/"] [ipa-like "S"] [ipa "/"] " sem palatalização."]    
    [:li [shared/media-icon :audio (shared/file "C9-M3.mp3")] "Exemplos da distribuição complementar do arquifonema " [ipa "/"] [ipa-like "S"] [ipa "/"] " com palatalização."]    
    [:li [shared/media-icon :audio (shared/file "C9-M4.mp3")] "Exemplos da distribuição complementar do arquifonema " [ipa "/"] [ipa-like "N"] [ipa "/"] "."]    
    [:li [shared/media-icon :audio (shared/file "C9-M5.mp3")] "Exemplos do efeito do arquifonema " [ipa "/"] [ipa-like "N"] [ipa "/"] " na vogal " [ipa "/e/"] " átona em posição final de palavra."]    
    [:li [shared/media-icon :audio (shared/file "C9-M6.mp3")] "Exemplos dos ditongos oronasais."]    
    [:li [shared/media-icon :audio (shared/file "C9-M7.mp3")] "Exemplos da distribuição complementar do fonema "[ipa "/l/"] " no Brasil."]    
    [:li [shared/media-icon :audio (shared/file "C9-M8.mp3")] "Exemplos da distribuição complementar do fonema "[ipa "/l/"] " em Portugal."]    
    [:li [shared/media-icon :audio (shared/file "C9-M9.mp3")] "Exemplos da distribuição complementar dos fonemas "[ipa "/i u/"] " no Brasil."]    
    [:li [shared/media-icon :audio (shared/file "C9-M10.mp3")] "Exemplos da distribuição complementar dos fonemas "[ipa "/e o/"] " no Brasil."]    
    [:li [shared/media-icon :audio (shared/file "C9-M11.mp3")] "Exemplos da distribuição complementar do fonema "[ipa "/a/"] " no Brasil."]    
    [:li [shared/media-icon :audio (shared/file "C9-M12.mp3")] "Exemplos da distribuição complementar do fonema "[ipa "/i/"] " em Portugal."]
    [:li [shared/media-icon :audio (shared/file "C9-M13.mp3")] "Exemplos da distribuição complementar do fonema "[ipa "/u/"] " em Portugal."]
    [:li [shared/media-icon :audio (shared/file "C9-M14.mp3")] "Exemplos da distribuição complementar do fonema "[ipa "/e/"] " em Portugal."]
    [:li [shared/media-icon :audio (shared/file "C9-M15.mp3")] "Exemplos da distribuição complementar do fonema "[ipa "/o/"] " em Portugal."]
    [:li [shared/media-icon :audio (shared/file "C9-M16.mp3")] "Exemplos da distribuição complementar do fonema "[ipa "/a/"] " em Portugal."]
    [:li [shared/media-icon :audio (shared/file "C9-M17.mp3")] "Exemplos da distribuição livre do fonema "[ipa "/r/"] "."]
    [:li [shared/media-icon :audio (shared/file "C9-M18.mp3")] "Exemplos da distribuição mista do arquifonema " [ipa "/"] [ipa-like "R"] [ipa "/"] "."]]])

(defn render
  "Chapter nine view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 9 — A relação entre fonemas e alofones:a distribuição"
    "eResource"]
   [:div.content.chapter-9
    ;[fuentes]
    [recursos]
    [atividades]]
   
   [shared/footer]])
