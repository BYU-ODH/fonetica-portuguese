1(ns fonetica-port.views.c11
  "Chapter 11"
  (:require [fonetica-port.views.components.shared :as shared :refer [ipa red]]
            [reitit.frontend.easy :as rfe]))


(defn ra1
  "Respostas Activity 4"
  []
  [:section.ra1
   [:li "Qual é a vogal que se produz com as seguintes conformações bucais?"
    [:ol.list.is-lower-alpha
     (let [items [
                  ["C11-R1-ao.jpg" "[o]"]
                  ["C11-R1-bi.jpg" "[i]"]
                  ["C11-R1-ce.jpg" "[e]"]
                  ["C11-R1-du.jpg" "[u]"]
                  ["C11-R1-ea.jpg" "[a]"]]]
       [:ol.list.is-lower-alpha
        (into [:div.grid2]
              (for [[route txt] items]
                [:li.list-item 
                 [:div.grid2
                  [shared/image {:full-url (shared/file route)}]
                  [:span.ipa.red txt]]]))])]]])

(defn ra2
  "Respostas Activity 2"
  []
  [:section.ra2
   [:li "Liste pares mínimos que provem as seguintes oposições vocálicas. "
    [:span.blue "As respostas podem variar; aqui só se dá um exemplo para cada um."]
    (let [items [["/a/ ∼ /E/" " [pÒsu] [p×su]" " {passo} {peço}"]
                 ["/i/ ∼ /O/" " [fÔku] [fØku]" " {fico} {foco}"]
                 ["/e/ ∼ /E/" " [pÓzu] [p×zu]" " {peso} subst. {peso} vb."]
                 ["/o/ ∼ /E/" " [kÕRu] [k×Ru]" " {coro} {quero}"]
                 ["/u/ ∼ /o/" " [pê¦u] [pé¦u]" " {punho} {ponho}"]
                 ["/i/ ∼ /e/" " [vÔ] [vÓ]" " {vi} {vê}"]
                 ["/E/ ∼ /O/" " [p×] [pØ]" " {pé} {pó}"]
                 ["/u/ ∼ /a/" " [lÖtA] [lÒtA]" " {luta} {lata}"]]]
      (into [:ol.list.is-lower-alpha]
            (for [[ipa red plain] items]
              [:li.list-item
               [:span.ipa ipa]
               [:span.ipa.red red]
               [:span.plain plain]])))]])

(defn ra3
  "Respostas Activity 3"
  []
  [:section.ra3
   [:li "Explique e exemplifique o alçamento vocálico em posição átona não final tanto no Brasil quanto em Portugal."
    [:table.table.is-bordered
     (let [items [["C11-R3l.jpg" ["pressa "[ipa "[pr×sA]"] " apressado "[ipa "[apresÒdu]"] 
                              "forte "[ipa "[fØRÎi]"] " fortalecer "[ipa "[foRtalesÓR]"]]]
              ["C11-R3r.jpg" [[:span "medo " [ipa "[mÓdu]"] " medroso " [ipa "[mšdRÕzu]"]]
                              [:span "belo " [ipa "[b×lu]"] " beleza " [ipa "[bšlÓzA]"]]
                              [:span "sal " [ipa "[sÒL]"] " saleiro " [ipa "[sAlA1IRu]"]]
                              [:span "porta " [ipa "[pØRtA]"] " porteiro " [ipa "[puRtA1IRu]"]]
                              [:span "fogo " [ipa "[fÕgu]"] " fogeira " [ipa "[fugA1IRA]"]]
                              [:span "examinar " [ipa "[izAminÒ³]"]]
                              [:span.black "Porém há muitos casos do não alçamento em
que "[ipa "[e E a O o]"] " aparecem nessa posição."]]]] ]
       (into [:tr]
             (for [[src txts] items]
               [:td.red {:style {:vertical-align "top"}} [shared/image {:full-url (shared/file src)}]
                (into [:section.ra3-texts] (for [t txts] [:div.is-size-6 {:style {:margin-top "1em"}} t]))])))]]])

(defn ra4
  "Respostas Activity 4"
  []
  [:section.ra4
   [:li "Explique e exemplifique o alçamento vocálico em posição átona final tanto no Brasil quanto em Portugal."
    [:table.table.is-bordered
     (let [items [["C11-R4l.jpg" [[:span "parte " [ipa "[pÒRÎi]"] " parto " [ipa "[pÒRtu]"] " parta " [ipa "[pÒRtA]"]]]]
                  ["C11-R4r.jpg" [[:span "parte " [ipa "[pÒ³tš]"] " parto " [ipa "[pÒ³tu]"] " parta " [ipa "[pÒ³tA]"]]]]] ]
       (into [:tr]
             (for [[src txts] items]
               [:td.red {:style {:vertical-align "top"}} [shared/image {:full-url (shared/file src)}]
                (into [:section.ra3-texts] (for [t txts] [:div.is-size-6 {:style {:margin-top "1em"}} t]))])))]]])

(defn render-respostas
  "Chapter 11 respostas"
  []
  [:main.c11.respostas.content
   [shared/fonetica-title "Capítulo 11 — Os sistemas vocálicos do português e do inglês"
    "Respostas para o eResource"]
   [:section.atividades
    [:h2 "ACTIVIDADES"]
    [:ol.atividades.list
     [ra1]
     [ra2]
     [ra3]
     [ra4]]]])

(defn a1
  "Actividades 1"
  []
  [:section.a1
   [:li "Qual é a vogal que se produz com as seguintes conformações bucais?"
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c11-r) :same-session]
    [:ol.list.is-lower-alpha
     (let [items [
                  ["C11-R1-ao.jpg" "[o]"]
                  ["C11-R1-bi.jpg" "[i]"]
                  ["C11-R1-ce.jpg" "[e]"]
                  ["C11-R1-du.jpg" "[u]"]
                  ["C11-R1-ea.jpg" "[a]"]]]
       [:ol.list.is-lower-alpha
        (into [:div.grid2]
              (for [[route #_txt] items]
                [:li.list-item 
                 [:div.grid2
                  [shared/image {:full-url (shared/file route)}]]]))])]]])

(defn a2
  "Actividades 2"
  []
  [:section.a2
   [:li "Liste pares mínimos que provem as seguintes oposições vocálicas. "
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c11-r) :same-session]
    (let [items [["/a/ ∼ /E/" " [pÒsu] [p×su]" " {passo} {peço}"]
                 ["/i/ ∼ /O/" " [fÔku] [fØku]" " {fico} {foco}"]
                 ["/e/ ∼ /E/" " [pÓzu] [p×zu]" " {peso} subst. {peso} vb."]
                 ["/o/ ∼ /E/" " [kÕRu] [k×Ru]" " {coro} {quero}"]
                 ["/u/ ∼ /o/" " [pê¦u] [pé¦u]" " {punho} {ponho}"]
                 ["/i/ ∼ /e/" " [vÔ] [vÓ]" " {vi} {vê}"]
                 ["/E/ ∼ /O/" " [p×] [pØ]" " {pé} {pó}"]
                 ["/u/ ∼ /a/" " [lÖtA] [lÒtA]" " {luta} {lata}"]]]
      (into [:ol.list.is-lower-alpha]
            (for [[ipa _red _plain] items]
              [:li.list-item
               [:span.ipa ipa]])))]])

(defn a3
  "Actividades 3"
  []
  [:section.a3
   [:li "Explique e exemplifique o alçamento vocálico em posição átona não final tanto no Brasil quanto em Portugal."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c11-r) :same-session]]])

(defn a4
  "Actividades 4"
  []
  [:section.a4
   [:li "Explique e exemplifique o alçamento vocálico em posição átona final tanto no Brasil quanto em Portugal."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c11-r) :same-session]]])

(defn atividades
  "Chapter 11 atividades"
  []
  [:section.atividades.content
   [:h2 "ATIVIDADES"]
   [:ol.atividades
    [a1]
    [a2]
    [a3]
    [a4]]])

(defn recursos
  "Materials"
  []
  [:section.recursos
   [:h3 "RECURSOS ELETRÔNICOS"]
   [:ol
    [:li [shared/media-icon :video (shared/file "C11-M1.mp4")] "Vídeo da abertura progressiva das sequências vocálicas."]
    [:li [shared/media-icon :audio (shared/file "C11-M2.mp3")] "O ensurdecimento vocálico."]
    [:li [shared/media-icon :audio (shared/file "C11-M3.mp3")] "As vogais sonoras e as ensurdecidas."]
    [:li [shared/media-icon :audio (shared/file "C11-M4.mp3")] "Palavras com vogais oronasais."]
    [:li [shared/media-icon :audio (shared/file "C11-M5.mp3")] "Palavras com vogais nasalizadas."]
    [:li [shared/media-icon :audio (shared/file "C11-M6.mp3")] "A comparação entre as séries de vogais orais e oronasais."]
    [:li [shared/media-icon :video (shared/file "C11-M7.mp4")] "As vogais não arredondadas e as arredondadas."]
    [:li [shared/media-icon :audio (shared/file "C11-M8.mp3")] "As vogais átonas e as tônicas."]
    [:li [shared/media-icon :audio (shared/file "C11-M9.mp3")] "O alongamento vocálico."]
    [:li [shared/media-icon :audio (shared/file "C11-M10.mp3")] "O ataque vocálico crescente e o ataque vocálico abrupto."]
    [:li [shared/media-icon :audio (shared/file "C11-M11.mp3")] "A cessação vocálica abrupta e a cessação vocálica decrescente."]
    [:li [shared/media-icon :audio (shared/file "C11-M12.mp3")] "Pares mínimos dos fonemas vocálicos do português."]
    [:li [shared/media-icon :audio (shared/file "C11-M13.mp3")] "Exemplos dos fonemas vocálicos do inglês."]
    [:li [shared/media-icon :audio (shared/file "C11-M14.mp3")] "As vogais inglesas longas e curtas."]
    [:li [shared/media-icon :audio (shared/file "C11-M15.mp3")] "Reducão vocálica em Portugal."]
    [:li [shared/media-icon :audio (shared/file "C11-M16.mp3")] "Não redução vocálica diante de " [ipa "[L]"] " em Portugal."]
    [:li [shared/media-icon :audio (shared/file "C11-M17.mp3")] "Não redução vocálica em ditongos átonos decrescentes em Portugal."]
    [:li [shared/media-icon :audio (shared/file "C11-M18.mp3")] "Não redução vocálica em substantivos terminados em " [ipa "[ER]"] " em Portugal."]
    [:li [shared/media-icon :audio (shared/file "C11-M19.mp3")] "Não redução vocálica em posição inicial de palavra em Portugal."]
    [:li [shared/media-icon :audio (shared/file "C11-M20.mp3")] "Não redução vocálica em palavras diminutivas e aumentativas com “acento secundário” em Portugal."]
    [:li [shared/media-icon :audio (shared/file "C11-M21.mp3")] "Redução vocálica em palavras diminutivas e aumentativas sem “acento secundário” em Portugal."]
    [:li [shared/media-icon :audio (shared/file "C11-M22.mp3")] "Não redução vocálica em advérbios em {-mente} em Portugal."]
    [:li [shared/media-icon :audio (shared/file "C11-M23.mp3")] "Não redução irregular em Portugal."]
    [:li [shared/media-icon :audio (shared/file "C11-M24.mp3")] "As vogais reduzidas inglesas."]
    [:li [shared/media-icon :audio (shared/file "C11-M25.mp3")] "As vogais tônicas, átonas e reduzidas do inglês."]
    [:li [shared/media-icon :audio (shared/file "C11-M26.mp3")] "A redução vocálica ao zero fonético em inglês."]
    [:li [shared/media-icon :audio (shared/file "C11-M27.mp3")] "A comparação entre as vogais tônicas do português e do inglês."]
    [:li [shared/media-icon :audio (shared/file "C11-M28.mp3")] "O contraste entre " [ipa "[e]"] " e " [ipa "[eI]"] " em português."]
    [:li [shared/media-icon :audio (shared/file "C11-M29.mp3")] "Alçamento das vogais átonas no Brasil."]
    [:li [shared/media-icon :audio (shared/file "C11-M30.mp3")] "Alçamento e centralização das vogais átonas em Portugal."]]])
(defn render
  "Chapter eleven view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 11 — Os sistemas vocálicos do português e do inglês"
    "eResource"]
   [:div.content.chapter-11
    [recursos]
    [atividades]]
   [shared/footer]])
