(ns fonetica-port.core
  (:require [reagent.dom :as rdom]
            [fonetica-port.views.styles :as style]
            [fonetica-port.routes :as routes]
            [fonetica-port.views.base :as base]
            [fonetica-port.views.components.nav :as nav]
            [fonetica-port.views.components.modal :as modal]))

(defn mount-components []
  (rdom/render [#'nav/navbar] (.getElementById js/document "fonetica-nav"))
  (rdom/render [#'modal/render-modal] (.getElementById js/document "modal-container"))  
  (rdom/render [#'base/main-view] (.getElementById js/document "app")))

(defn init! []  
  (routes/init-routes!)
  (mount-components)
  (when goog.DEBUG ;; use cljs styles for styles in dev
    (println "DEV: We are generating styles in CLJS")
    (style/mount-style (style/fonetica))))
