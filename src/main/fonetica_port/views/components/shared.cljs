(ns fonetica-port.views.components.shared
  "Shared components that may be used on multiple views"
  (:require [reagent.core :as r]
            [goog.dom :as dom]
            [goog.string :as gstr]
            [fonetica-port.views.components.modal :as modal]
            [reitit.frontend.easy :as rfe])
  (:import goog.ui.AnimatedZippy))

(defn accordion [{:keys [header-text content-body]}]
  (let [header-id (gstr/createUniqueString)
        content-id (gstr/createUniqueString)]
    (r/create-class
     {:display-name "zippy"
      :reagent-render (fn [id] [:div.zippy-container
                                [:div {:id (str "wrap-" header-id)}
                                 [:h1.title.is-5 {:id header-id} header-text]
                                 [:div{:id content-id} content-body]]])
      :component-did-mount #(AnimatedZippy. (dom/$ header-id)
                                            (dom/$ content-id))})))
(defn fonetica-title
  "Title Hero for the page"
  [text & [subtext]]
  [:section.hero
   [:div.container.box
    [:h1
     text]
    (when-let [s subtext]
      [:h3.subtext s])]])

(def media-type-filenames
  "Filenames for the given media-types"
  {:airplane "Airplane-icon.png"
   :audio "audio-icon.png"
   :cellphone "Cellphone-icon.png"
   :ej "EJ-icon.png"
   :ejb "EJ-icon-bold.png"
   :envelope "Envelope-icon.png"
   :ex "EX-icon.png"
   :exb "EX-icon-bold.png"
   :image "image-icon.png"
   :imageb "image-icon-bold.png"
   :lightbulb "Lightbulb-icon.png"
   :m-w "M-W-icon.png"
   :mouse "Mouse-icon.png"
   :r "R-icon.png"
   :text "text-icon.png"
   :text-ej "text-EJ-icon.png"
   :text-fat "text-icon-fat.png"
   :video "video-icon.png"
   :videob "video-icon-bold.png"
   :wrench "Wrench-icon.png"})

(defn media-icon
  "Render a media icon and its attached link"
  [icon-type href & [to-page?]]
  (let [base "/Files/"
        file-name (media-type-filenames icon-type)
        file-src (str base file-name)
        opt-map (cond->
                    {:href href
                     :on-click
                     (fn [e]
                       (.preventDefault e)
                       (modal/populate-modal href))}
                  to-page? (dissoc :on-click))]
    [:a.ficon opt-map
     [:img.ficon {:src file-src}]]))

(defn file
  "get the filepath for `filename`"
  [file-name]
  (rfe/href :fonetica-port.routes/files {:file-name file-name}))

(defn item-grid
  "a table-like grid"
  [items]
  (into [:div.photogrid]
        (for [i items]
           [:div.image-container {"border-style" :solid
                                  "border-width" "1px"
                                  "border-color" "black"}
            i])))

(defn footer
  "Return to Chapter Index on every page"
  []
  [:div.footer
   [:a {:href (rfe/href :fonetica-port.routes/table-of-contents)} "VOLTAR PARA O ÍNDICE DE CAPÍTULOS"]])

(defn ipa
  "Render `s` as ipa font"
  [s & more]
  (if (empty? more)
          [:span.ipa s]
          (into [:span.ipa s] more)))

(defn ipa-like
  "Render `s` as ipa font"
  [s & more]
  (if (empty? more)
          [:span.ipa-like s]
          (into [:span.ipa-like s] more)))

(defn femp
  "Emphasize a portion mid-word"
  [s & more]
  (if (empty? more)
          [:span.femp s]
          (into [:span.femp s] more)))

(defn fempr
  "Emphasize a portion mid-word, underline and red"
  [s & more]
  (if (empty? more)
    [:span.fempr s]
    (into [:span.fempr s] more)))

(defn render-ej
  "Chapter 5 ej helper"
  [{:keys [body index title]}]
  [:main.c5.ej.content
   [:div.identifier index]
   [fonetica-title title]
   body])

(defn red
  "red span on `s`"
  [s]
  [:span.red s])

(def populate-modal modal/populate-modal)
(def clear-modal modal/clear-modal)

(defn image
  "create an image, optionally with thumbnail url `:thumb`, that links to the fullsize."
  [{:keys [full-url thumb alt class]}]
  [:a {:href full-url}
   [:img.fimage {:src (or thumb full-url)
                 :alt alt
                 :class class
                 :on-click
                 (fn [e]
                   (.preventDefault e)
                   (modal/populate-modal full-url))}]])

(defn list-2x2x3-1
  "Produce a two-column output with each column itself having two column, three items on the left and red ipa on the right.
  Each item in `items` is a sequence of four strings, the first being IPA."
  [items & [no-answers?]]
  (let [item-count (count items)
        half-count (Math/ceil (* 0.5 item-count))
        [litems ritems] (partition half-count half-count nil items)
        make-twocol (fn  [[l1 l2 l3 r :as items]]
                      (if-not items
                        [:div.empty]
                        [:li
                         [:div.grid2
                          [:div.l.3-1
                           [:div l1]
                           [:div l2]
                           [:div l3]]
                          (if no-answers?
                            [:div.r.blank]
                            [:div.r.ipa.red.is-size-4 r])]]))]
    [:ol.list.is-lower-alpha
     [:div.grid2
      (into [:div.l] (map make-twocol litems))
      (into [:div.r] (map make-twocol ritems))]]))

(defn list-2x2x1-3
  "Produce a two-column output with each column itself having two column, ipa on the left and three red items on the right.
  Each item in `items` is a sequence of four strings, the first being IPA."
  [items & [no-answers?]]
  (let [item-count (count items)
        half-count (Math/ceil (* 0.5 item-count))
        [litems ritems] (partition half-count half-count nil items)
        make-twocol (fn  [[l r1 r2 r3 :as items]]
                      (if-not items
                        [:div.empty]
                        [:li
                         [:div.grid2.1-3
                          [:div.l.ipa.is-size-4 l]
                          (if no-answers?
                            [:div.r.blank]
                            [:div.r.red
                             [:div r1]
                             [:div r2]
                             [:div r3]])]]))]
    [:ol.list.is-lower-alpha
     [:div.grid2
      (into [:div.l] (map make-twocol litems))
      (into [:div.r] (map make-twocol ritems))]]))

(defn list-3x1
  "A three-column list with headers on 2 and 3, which have red answers"
  [headers items & [no-answers?]]
  (into [:ol.list.is-lower-alpha
         (into [:div.grid3 [:span.blank]]
               headers)]
        (for [[a b c] items]
          [:li.list-item
           (if-not no-answers?
             [:div.grid3
              [:span a]
              [:span.ipa.red b]
              [:span.ipa.red c]]

             [:div.grid3
              [:span a]
              [:span.blank]
              [:span.blank]])])))

(defn list-2x1x3
  "Produce a two-column output with each column itself having two column, one item on the left and two red ipa on the right.
  Each item in `items` is a sequence of three reagent vector components"
  [items & [no-answers?]]
  (let [item-count (count items)
        half-count (Math/ceil (* 0.5 item-count))
        [litems ritems] (partition half-count half-count nil items)
        make-twocol (fn  [[a b c :as items]]
                      (if-not items
                        [:div.empty]
                        [:li
                         [:div
                          [:div.1-3
                           a
                           (when-not no-answers?
                             [:div.red.grid2 {:style {"margin-left" "1em" 
                                                      "display" "inline-grid"}} b c])]
                          ]]))]
    [:ol.list.is-lower-alpha
     [:div.grid2
      (into [:div.l] (map make-twocol litems))
      (into [:div.r] (map make-twocol ritems))]]))

(defn list-2x1
  "A simple two-column list with vertically ordered alpha"
  [items & [no-answers?]]
(let [item-count (count items)
      half-count (Math/ceil (* 0.5 item-count))
      [litems ritems] (partition half-count half-count nil items)
      make-li (fn  [i]
                (if-not i
                  [:div.empty]
                  [:li
                   [:div.2-1 i]]))]
  [:ol.list.is-lower-alpha
     [:div.grid2
      (into [:div.l] (map make-li litems))
      (into [:div.r] (map make-li ritems))]]))
