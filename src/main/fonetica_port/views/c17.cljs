(ns fonetica-port.views.c17
  "Chapter 17"
  (:require [fonetica-port.views.components.shared :as shared :refer [ipa femp red ipa-like]]
            [reitit.frontend.easy :as rfe]))

(defn ra1
  "Respostas activity 1"
  []
  [:section.ra1
   [:li "Escreva o símbolo do alofone descrito pelos seguintes traços fonéticos."

    (let [items [["fricativo"
                  "palatal"
                  "sonoro"
                  "[Z]"]
                 ["fricativo"
                  "labiodental"
                  "surdo"
                  "[f]"]
                 ["fricativo"
                  "palatal"
                  "surdo"
                  "[S]"]
                 ["fricativo"
                  "alveolar"
                  "surdo"
                  "[s]"]
                 ["fricativo"
                  "labiodental"
                  "sonoro"
                  "[v]"]
                 ["fricativo"
                  "alveolar"
                  "sonoro"
                  "[z]"]]
          [litems ritems] (partition (* 0.5 (count items)) items)
          make-twocol (fn  [[l1 l2 l3 r]]
                        [:li
                         [:div.grid2
                          [:div.l
                           [:div l1]
                           [:div l2]
                           [:div l3]]
                          [:div.r.red.ipa.is-size-4 r]]])]
      
      [:ol.list.is-lower-alpha
       [:div.grid2
        (into [:div.l] (map make-twocol litems))
        (into [:div.r] (map make-twocol ritems))]])]])

(defn ra2
  "Respostas activity 2"
  []
  [:section.ra2
   [:li "Indique os traços fonéticos de cada alofone da lista."

    (let [items [["[v]"
                  "fricativo"
                  "labiodental"
                  "sonoro"]

                 ["[s]"
                  "fricativo"
                  "alveolar"
                  "surdo"]

                 ["[Z]"
                  "fricativo"
                  "palatal"
                  "sonoro"]

                 ["[S]"
                  "fricativo"
                  "palatal"
                  "surdo"]
                 
                 ["[z]"
                  "fricativo"
                  "alveolar"
                  "sonoro"]

                 ["[f]"
                  "fricativo"
                  "labiodental"
                  "surdo"]]

          [litems ritems] (partition (* 0.5 (count items)) items)
          make-twocol (fn  [[l r1 r2 r3]]
                        [:li
                         [:div.grid2
                          [:div.l.ipa l]
                          [:div.r.red
                           [:div r1]
                           [:div r2]
                           [:div r3]]]])]
      
      [:ol.list.is-lower-alpha
       [:div.grid2
        (into [:div.l] (map make-twocol litems))
        (into [:div.r] (map make-twocol ritems))]])]])

(defn ra3
  "Respostas activity 3"
  []
  [:section.ra3
   [:li "Faça uma cadeia fonética, mudando um só traço de cada vez."
    [:div.red "As respostas podem variar; só é preciso tomar cuidado de mudar um só traço de cada vez."]
    ]])

(defn ra4
  "Respostas activity 4"
  []
  [:section.ra4
   [:li "Transcreva as seguintes palavras ou grupos de palavras com o arquifonema " [ipa-like "/S/"] " para um dialeto sem palatalização do arquifonema " [ipa-like "/S/"] " e para um dialeto com essa palatalização."
    (let [items [["carros" [ipa "[kÒhus]"] [ipa "[kÒhuS]"]]
                 ["carros antigos" [ipa "[kÒhuzá" [:sup "ø"] "ÎÔgus]"] [ipa "[kÒhuzá" [:sup "ø"] "ÎÔguS]"]]
                 ["carros novos" [ipa "[kÒhuznØvus]"] [ipa "[kÒhuZnØvuS]"]]
                 ["mesmo" [ipa "[mÓzmu]"] [ipa "[mÓZmu]"]]
                 ["este" [ipa "[ÓsÎi]"] [ipa "[ÓSÎi]"]]
                 ["nascer" [ipa "[nasÓR]"] [ipa "[naSsÓR]"]]
                 ["paz" [ipa "[pÒs]"] [ipa "[pÒS]"]]
                 ["paz eterna" [ipa "[pÒzet×RnA]"] [ipa "[pÒzet×RnA]"]]
                 ["paz temporal" [ipa "[pÒstÝ" [:sup "m"] "poRÒK]"] [ipa "[pÒStÝ" [:sup "m"] "poRÒK]"]]
                 ["pasmo" [ipa "[pÒzmu]"] [ipa "[pÒZmu]"]]
                 ["pasta" [ipa "[pÒstA]"] [ipa "[pÒStA]"]]
                 ["asmático" [ipa "[azmÒÎiku]"] [ipa "[aZmÒÎiku]"]]]]
      (into [:ol.list.is-lower-alpha
             [:div.grid3 [:span.blank] [:span.is-underlined "sem palatalização"] [:span.is-underlined "com palatalização"]]]
            (for [[a b c] items]
              [:li.list-item [:div.grid3
                              [:span a]
                              [:span.ipa.red b]
                              [:span.ipa.red c]]])))]])

(defn ra5
  "Respostas activity 5"
  []
  [:section.ra5
   [:li "Para as seguintes palavras indique se os grafemas {s} ou {z} correspondem aos fonemas " [ipa "/s/"] " ou " [ipa "/z/"] ", ou ao arquifonema " [ipa-like "/S/"] "."
    (let [items [
                 ["zero" "fonema " [ipa "/z/"]]
                 ["paz" "arquifonema " [ipa-like "/S/"]]
                 ["estimo" "arquifonema " [ipa-like "/S/"]]
                 ["fazes" "fonema " [ipa "/z/"]]
                 ["esgotado" "arquifonema " [ipa-like "/S/"]]
                 ["fugaz" "arquifonema " [ipa-like "/S/"]]
                 ["razoável" "fonema " [ipa "/z/"]]
                 ["peso" "fonema " [ipa "/z/"]]
                 ["azoado" "fonema " [ipa "/z/"]]
                 ["depois" "arquifonema " [ipa-like "/S/"]]
                 ["suado" "fonema " [ipa "/s/"]]
                 ["passo" "fonema " [ipa "/s/"]]]]
      (into [:ol.list.is-lower-alpha]
            (for [[a b] items]
              [:li.list-item [:div.grid3
                              [:span a]
                              [:span.red b]
                              ]])))]])

(defn ra6
  "Respostas activity 6"
  []
  [:section.ra6
   [:li "Para as seguintes palavras ou grupos de palavras transcritas de um dialeto com palatalização do arquifonema " [ipa-like "/S/"] ", indique se o som " [ipa "[Z]"] " corresponde ao fonema " [ipa "/Z/"] " ou ao arquifonema " [ipa-like "/S/"] ". Depois, esceva a palavra ortograficamente."
    (let [items [[[:span "[ÒZu]"] [:span "fonema " [:span.ipa "/Z/"]] "{ajo}" ]
                 [[:span "[UZgÒtuS]"] [:span "arquifonema " [ipa-like "/S/"]] "{os gatos}" ]
                 [[:span "[pÒZmà" [:sup "ª"] "djÒL]"] [:span "arquifonema " [ipa-like "/S/"]] "{paz mundial}" ]
                 [[:span "[fuZÔR]"] [:span "fonema " [:span.ipa "/Z/"]] "{fugir}" ]
                 [[:span "[ÒZnu]"] [:span "arquifonema " [ipa-like "/S/"]] "{asno}" ]
                 [[:span "[aZÖ" [:sup "ª"] "tÒR]"] [:span "fonema " [:span.ipa "/Z/"]] "{ajuntar}" ]
                 [[:span "[ZÒkA]"] [:span "fonema " [:span.ipa "/Z/"]] "{jaca}" ]
                 [[:span "[ZwÔS]"] [:span "fonema " [:span.ipa "/Z/"]] "{juíz}" ]
                 [[:span "[ZiRÒR]"] [:span "fonema " [:span.ipa "/Z/"]] "{girar}" ]
                 [[:span "[aZdëmAS]"] [:span "arquifonema " [ipa-like "/S/"]] "{as damas}" ]
                 [[:span "[aZeItÒR]"] [:span "fonema " [:span.ipa "/Z/"]] "{ajeitar}" ]
                 [[:span "[fÞZÔR]"] [:span "fonema " [:span.ipa "/Z/"]] "{fingir}" ]]]
      (into [:ol.list.is-lower-alpha
             [:div.grid3]]
            (for [[a b c] items]
              [:li.list-item [:div.grid3
                              [:span.ipa a]
                              [:span.red b]
                              [:span.red c]]])))]])

(defn ra7
  "Respostas activity 7"
  []
  [:section.ra7
   [:li "Indique a representação fonológica para o grafema {x} nas seguintes palavras."
    (let [items [["exato" [:span "fonema " [ipa-like "/z/"]]]
                 ["excelente" [:span "arquifonema " [ipa-like "/S/"]]]
                 ["eximir" [:span "fonema " [ipa-like "/z/"]]]
                 ["experto" [:span "arquifonema " [ipa-like "/S/"]]]
                 ["faixa" [:span "fonema " [ipa "/S/"]]]
                 ["fixo" [:span "fonemas " [ipa-like "/ks/"]]]
                 ["máximo" [:span "fonema " [ipa-like "/s/"]]]
                 ["próximo" [:span "fonema " [ipa-like "/s/"]]]
                 ["sexta" [:span "arquifonema " [ipa-like "/S/"]]]
                 ["taxa" [:span "fonema " [ipa "/S/"]]]
                 ["táxi" [:span "fonemas " [ipa-like "/ks/"]]]
                 ["xarope" [:span "fonema " [ipa "/S/"]]]]]
      (into [:ol.list.is-lower-alpha
             [:div.grid3]]
            (for [[a b] items]
              [:li.list-item [:div.grid3
                              [:span a]
                              [:span.red b]]])))]])

(defn render-respostas
  "Chapter 17 respostas"
  []
  [:main.c17.respostas.content
   [shared/fonetica-title "Capítulo 17 — Os fonemas fricativos"
    "Respostas para o eResource"
    ]
   [:section.atividades
    [:h2 "ATIVIDADES"]
    [:ol.atividades.list
     [ra1]
     [ra2]
     [ra3]
     [ra4]
     [ra5]
     [ra6]
     [ra7]]]])

(defn a1
  "Activity 1"
  []
  [:section.a1
   [:li "Escreva o símbolo do alofone descrito pelos seguintes traços fonéticos."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c17-r) :same-session]
    (let [items [["fricativo"
                  "palatal"
                  "sonoro"
                  "[Z]"]
                 ["fricativo"
                  "labiodental"
                  "surdo"
                  "[f]"]
                 ["fricativo"
                  "palatal"
                  "surdo"
                  "[S]"]
                 ["fricativo"
                  "alveolar"
                  "surdo"
                  "[s]"]
                 ["fricativo"
                  "labiodental"
                  "sonoro"
                  "[v]"]
                 ["fricativo"
                  "alveolar"
                  "sonoro"
                  "[z]"]]
          [litems ritems] (partition (* 0.5 (count items)) items)
          make-twocol (fn  [[l1 l2 l3 r]]
                        [:li
                         [:div.grid2
                          [:div.l
                           [:div l1]
                           [:div l2]
                           [:div l3]]
                          [:div.blank]]])]
      
      [:ol.list.is-lower-alpha
       [:div.grid2
        (into [:div.l] (map make-twocol litems))
        (into [:div.r] (map make-twocol ritems))]])]])

(defn a2
  "Activity 2"
  []
  [:section.a2
   [:li "Indique os traços fonéticos de cada alofone da lista."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c17-r) :same-session]
    (let [items [["[v]"
                  "fricativo"
                  "labiodental"
                  "sonoro"]

                 ["[s]"
                  "fricativo"
                  "alveolar"
                  "surdo"]

                 ["[Z]"
                  "fricativo"
                  "palatal"
                  "sonoro"]

                 ["[S]"
                  "fricativo"
                  "palatal"
                  "surdo"]
                 
                 ["[z]"
                  "fricativo"
                  "alveolar"
                  "sonoro"]

                 ["[f]"
                  "fricativo"
                  "labiodental"
                  "surdo"]]

          [litems ritems] (partition (* 0.5 (count items)) items)
          make-twocol (fn  [[l]]
                        [:li
                         [:div.grid2
                          [:div.l.ipa l]
                          [:div.r.blank]]])]
      
      [:ol.list.is-lower-alpha
       [:div.grid2
        (into [:div.l] (map make-twocol litems))
        (into [:div.r] (map make-twocol ritems))]])]])

(defn a3
  "Activity 3"
  []
  [:section.a3
   [:li "Faça uma cadeia fonética, mudando um só traço de cada vez."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c17-r) :same-session]
    (let [items ["[f] > [ ] > [ ] > [ ]"
                 "[s] > [ ] > [ ] > [ ]"
                 "[Z] > [ ] > [ ] > [ ]"
                 "[v] > [ ] > [ ] > [ ]"]]
      (into [:ol.list.is-lower-alpha
             (for [i items]
               [:li.ipa i])]))]])

(defn a4
  "Activity 4"
  []
  [:section.a4
   [:li "Transcreva as seguintes palavras ou grupos de palavras com o arquifonema " [ipa-like "/S/"] " para um dialeto sem palatalização do arquifonema " [ipa-like "/S/"] " e para um dialeto com essa palatalização."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c17-r) :same-session]
    
    (let [items [["carros" [ipa "[kÒhus]"] [ipa "[kÒhuS]"]]
                 ["carros antigos" [ipa "[kÒhuzá" [:sup "ø"] "ÎÔgus]"] [ipa "[kÒhuzá" [:sup "ø"] "ÎÔguS]"]]
                 ["carros novos" [ipa "[kÒhuznØvus]"] [ipa "[kÒhuZnØvuS]"]]
                 ["mesmo" [ipa "[mÓzmu]"] [ipa "[mÓZmu]"]]
                 ["este" [ipa "[ÓsÎi]"] [ipa "[ÓSÎi]"]]
                 ["nascer" [ipa "[nasÓR]"] [ipa "[naSsÓR]"]]
                 ["paz" [ipa "[pÒs]"] [ipa "[pÒS]"]]
                 ["paz eterna" [ipa "[pÒzet×RnA]"] [ipa "[pÒzet×RnA]"]]
                 ["paz temporal" [ipa "[pÒstÝ" [:sup "m"] "poRÒK]"] [ipa "[pÒStÝ" [:sup "m"] "poRÒK]"]]
                 ["pasmo" [ipa "[pÒzmu]"] [ipa "[pÒZmu]"]]
                 ["pasta" [ipa "[pÒstA]"] [ipa "[pÒStA]"]]
                 ["asmático" [ipa "[azmÒÎiku]"] [ipa "[aZmÒÎiku]"]]]]
      (into [:ol.list.is-lower-alpha
             [:div.grid3 [:span.blank] [:span.is-underlined "sem palatalização"] [:span.is-underlined "com palatalização"]]]
            (for [[a b c] items]
              [:li.list-item [:div.grid3
                              [:span a]
                              [:span.blank]
                              [:span.blank]]])))]])

(defn a5
  "Activity 5"
  []
  [:section.a5
   [:li "Para as seguintes palavras indique se os grafemas " [ipa "/s/"] " ou " [ipa "/z/"] ", ou ao arquifonema " [ipa-like "/S/"] "."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c17-r) :same-session]
    (let [items [
                 ["zero" "fonema " [ipa "/z/"]]
                 ["paz" "arquifonema " [ipa-like "/S/"]]
                 ["estimo" "arquifonema " [ipa-like "/S/"]]
                 ["fazes" "fonema " [ipa "/z/"]]
                 ["esgotado" "arquifonema " [ipa-like "/S/"]]
                 ["fugaz" "arquifonema " [ipa-like "/S/"]]
                 ["razoável" "fonema " [ipa "/z/"]]
                 ["peso" "fonema " [ipa "/z/"]]
                 ["azoado" "fonema " [ipa "/z/"]]
                 ["depois" "arquifonema " [ipa-like "/S/"]]
                 ["suado" "fonema " [ipa "/s/"]]
                 ["passo" "fonema " [ipa "/s/"]]]]
      (into [:ol.list.is-lower-alpha]
            (for [[a] items]
              [:li.list-item [:div.grid3
                              [:span a]
                              [:span.blank]]])))]])

(defn a6
  "Activity 6"
  []
  [:section.a6
   [:li "Para as seguintes palavras ou grupos de palavras transcritas de um dialeto com palatalização do arquifonema " [ipa-like "/S/"] ", indique se o som " [ipa "[Z]"] " corresponde ao fonema " [ipa "/Z/"] " ou ao arquifonema " [ipa-like "/S/"] ". Depois, esceva a palavra ortograficamente."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c17-r) :same-session]
    (let [items [[[:span "[ÒZu]"] [:span "fonema " [:span.ipa "/Z/"]] "{ajo}" ]
                 [[:span "[UZgÒtuS]"] [:span "arquifonema " [ipa-like "/S/"]] "{os gatos}" ]
                 [[:span "[pÒZmà" [:sup "ª"] "djÒL]"] [:span "arquifonema " [ipa-like "/S/"]] "{paz mundial}" ]
                 [[:span "[fuZÔR]"] [:span "fonema " [:span.ipa "/Z/"]] "{fugir}" ]
                 [[:span "[ÒZnu]"] [:span "arquifonema " [ipa-like "/S/"]] "{asno}" ]
                 [[:span "[aZÖ" [:sup "ª"] "tÒR]"] [:span "fonema " [:span.ipa "/Z/"]] "{ajuntar}" ]
                 [[:span "[ZÒkA]"] [:span "fonema " [:span.ipa "/Z/"]] "{jaca}" ]
                 [[:span "[ZwÔS]"] [:span "fonema " [:span.ipa "/Z/"]] "{juíz}" ]
                 [[:span "[ZiRÒR]"] [:span "fonema " [:span.ipa "/Z/"]] "{girar}" ]
                 [[:span "[aZdëmAS]"] [:span "arquifonema " [ipa-like "/S/"]] "{as damas}" ]
                 [[:span "[aZeItÒR]"] [:span "fonema " [:span.ipa "/Z/"]] "{ajeitar}" ]
                 [[:span "[fÞZÔR]"] [:span "fonema " [:span.ipa "/Z/"]] "{fingir}" ]]]
      (into [:ol.list.is-lower-alpha
             [:div.grid3]]
            (for [[a] items]
              [:li.list-item [:div.grid3
                              [:span.ipa a]
                              [:span.blank]
                              [:span.blank]]])))]])

(defn a7
  "Activity 7"
  []
  [:section.a7
   [:li "Indique a representação fonológica para o grafema {x} nas seguintes palavras."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c17-r) :same-session]
    (let [items [["exato" [:span "fonema " [ipa-like "/z/"]]]
                 ["excelente" [:span "arquifonema " [ipa-like "/S/"]]]
                 ["eximir" [:span "fonema " [ipa-like "/z/"]]]
                 ["experto" [:span "arquifonema " [ipa-like "/S/"]]]
                 ["faixa" [:span "fonema " [ipa "/S/"]]]
                 ["fixo" [:span "fonemas " [ipa-like "/ks/"]]]
                 ["máximo" [:span "fonema " [ipa-like "/s/"]]]
                 ["próximo" [:span "fonema " [ipa-like "/s/"]]]
                 ["sexta" [:span "arquifonema " [ipa-like "/S/"]]]
                 ["taxa" [:span "fonema " [ipa "/S/"]]]
                 ["táxi" [:span "fonemas " [ipa-like "/ks/"]]]
                 ["xarope" [:span "fonema " [ipa "/S/"]]]]]
      (into [:ol.list.is-lower-alpha
             [:div.grid3]]
            (for [[a b] items]
              [:li.list-item [:div.grid3
                              [:span a]
                              [:span.blank]]])))]])


(defn atividades
  "Chapter 17 atividades"
  []
  [:section.atividades.content
   [:h2 "ATIVIDADES"]
    [:ol.atividades.list
     [a1]
     [a2]
     [a3]
     [a4]
     [a5]
     [a6]
     [a7]]])

(defn recursos
  "Materials"
  []
  [:section.recursos
   [:h3 "RECURSOS ELETRÔNICOS"]
   (let [audio-items [[:span "A oposição entre os fonemas fricativos."]
                      [:span "Fonemas " [ipa "/f/"] " e " [ipa "/v/"] " seguidos de líquidas e fonema " [ipa "/f/"] " em posição final."]
                      [:span "Oposição e neutralização entre " [ipa "/s/"] " e " [ipa "/z/"] "."]
                      [:span "A distensão do som fricativo " [ipa "[s]"] "."]
                      [:span "Oposições com o fonema " [ipa "/f/"] "."]
                      [:span "Distribuição fonotática do fonema " [ipa "/f/"] "."]
                      [:span "Fonema " [ipa "/f/"] " em acrônimos e palavras emprestadas."]
                      [:span "A palavra {fofo}."]
                      [:span "Oposições com o fonema " [ipa "/v/"] "."]
                      [:span "Distribuição fonotática do fonema " [ipa "/v/"] "."]
                      [:span "Fonema " [ipa "/v/"] " em acrônimos e estrangeirismos."]
                      [:span "A palavra {vovô}."]
                      [:span "Oposições com o fonema " [ipa "/s/"] "."]
                      [:span "Correspondências grafêmicas do fonema " [ipa "/s/"] "."]
                      [:span "Posicionamentos fonológicos do fonema " [ipa "/s/"] "."]
                      [:span "Distribuição fonotática do fonema " [ipa "/s/"] "."]
                      [:span "A palavra {saci}."]
                      [:span "Oposições com o fonema " [ipa "/z/"] "."]
                      [:span "Correspondências grafêmicas do fonema " [ipa "/z/"] "."]
                      [:span "Posicionamentos fonológicos do fonema " [ipa "/z/"] "."]
                      [:span "Distribuição fonotática do fonema " [ipa "/z/"] "."]
                      [:span "A palavra {Zezé}."]
                      [:span "Pares mínimos que provam a oposição entre os fonemas " [ipa "/s/"] " e " [ipa "/z/"] "."]
                      [:span "Palavras com o arquifonema " [ipa "/S/"] " em posição final."]
                      [:span "A distribuição do arquifonema " [ipa "/S/"] " sem palatalização."]
                      [:span "A distribuição do arquifonema " [ipa "/S/"] " com palatalização."]
                      [:span "Correspondências grafêmicas do arquifonema " [ipa "/S/"] "."]
                      [:span "Distribuição fonotática do arquifonema " [ipa "/S/"] "."]
                      [:span "A palavra {pasta} sem e com palatalização."]
                      [:span "O arquifonema " [ipa "/S/"] " em posição final de sílaba interior de palavra em Portugal."]
                      [:span "A ditongação carioca diante do arquifonema " [ipa "/S/"] "."]
                      [:span "A assimilação dos alofones do arquifonema " [ipa "/S/"] " em posição interior de palavra."]
                      [:span "A assimilação dos alofones do arquifonema " [ipa "/S/"] " em posição final de palavra."]
                      [:span "Oposições com o fonema " [ipa "/S/"] "."]
                      [:span "Correspondências grafêmicas do fonema " [ipa "/S/"] "."]
                      [:span "Distribuição fonotática do fonema " [ipa "/S/"] "."]
                      [:span "A palavra {chuchu}."]
                      [:span "Oposições com o fonema " [ipa "/Z/"] "."]
                      [:span "Correspondências grafêmicas do fonema " [ipa "/Z/"] "."]
                      [:span "Distribuição fonotática do fonema " [ipa "/Z/"] "."]
                      [:span "A palavra {já já}."]
                      [:span "As correspondências fonemáticas do grafema {x}."]
                      [:span "As palavras {exato} e {inexato}."]
                      [:span "A sequência {xc} mais vogal em Portugal e no Brasil."]
                      [:span "A palavra {sintaxe} em Portugal e no Brasil."]]
         ex-items [[:span "Exercícios de pronúncia: o fonema " [ipa "/f/"] "."]
                   [:span "Exercícios de pronúncia: o fonema " [ipa "/v/"] "."]
                   [:span "Exercícios de pronúncia: o fonema " [ipa "/s/"] "."]
                   [:span "Exercícios de pronúncia: o fonema " [ipa "/z/"] "."]
                   [:span "Exercícios de pronúncia: o arquifonema " [ipa "/S/"] "."]
                   [:span "Exercícios de pronúncia: o fonema " [ipa "/S/"] "."]
                   [:span "Exercícios de pronúncia: o fonema " [ipa "/Z/"] "."]
                   [:span "Exercícios de pronúncia: o grafema {x}."]] ;; TODO put ipa in the text
         fonetica-line-item (fn [n i]
                              [:li.list-item [shared/media-icon :audio
                                              (shared/file (str "C17-M" (inc n) ".mp3"))] i])
         fonetica-line-item2 (fn [n i]
                               [:li.list-item [shared/media-icon :ex
                                               (shared/file (str "C17-M" (+ 46 n) ".mp3"))] i])]
     
     (into
      [:ol.list]
      (conj 
       (map-indexed fonetica-line-item2 ex-items)
       (map-indexed fonetica-line-item audio-items))))])

(defn render
  "Chapter seventeen view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 17 — Os fonemas fricativos"
    "eResource"]
   [:div.content.chapter-17
    [recursos]
    [atividades]]
   [shared/footer]])
