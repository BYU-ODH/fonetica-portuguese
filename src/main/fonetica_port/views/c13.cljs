(ns fonetica-port.views.c13
  "Chapter 13"
  (:require [fonetica-port.views.components.shared :as shared :refer [ipa femp red]]
            [reitit.frontend.easy :as rfe]))

(defn ra1
  "Respostas Actividades 1"
  []
  [:section.ra1
   [:li "Complete a seguinte tabela referente aos substantivos do português:"
    (let [hrs [[:span ""]
               [:span "% com vogal tônica meio-fechada " [ipa "/e/"] " " [ipa "/o/"]]
               [:span "% com vogal tônica meio-aberta " [ipa "/E/"] " " [ipa "/O/"]]
               [:span "% com forma singular " [ipa "/o/"] " e forma plural " [ipa "/O/"]]]
          rows [[[:span "Palavras terminadas em {o}"]
                 [:span "73,1%"]
                 [:span "25,6%"]
                 [:span "1,3%"]]
                [[:span "Palavras terminadas em {a} ou {e}"]
                 [:span "57,0%"]
                 [:span "43,0%"]
                 [:span "⸺"]]]]
      [:table.table.is-bordered
       [:thead 
        (into [:tr]
              (for [h hrs]
                [:th h]))]
       (into [:tbody]
             (for [[a b c d] rows]
               [:tr
                [:td a]
                [:td.red.has-text-centered.is-size-4 b]
                [:td.red.has-text-centered.is-size-4 c]
                [:td.red.has-text-centered.is-size-4 d]]
               ))])]])

(defn ra2
  "Respostas Actividades 2"
  []
  [:section.ra2
   [:li "Transcreva as formas singular e plural dos seguintes substantivos:"
    (let [hrs ["Substantivo"
               "Forma singular"
               "Forma plural"]
          rows [["esposo"
                 "[espÕzu]"
                 "[espÕzus]"]
                
                ["alfabeto"
                 "[aKfab×tu]"
                 "[aKfab×tus]"]
                
                ["escolho"
                 "[eskÕYu]"
                 "[eskØYus]"]
                
                ["terra"
                 "[t×hA]"
                 "[t×hAs]"]
                
                ["alteza"
                 "[aKtÓzA]"
                 "[aKtÓzAs]"]
                
                ["caroço"
                 "[kaRÕsu]"
                 "[kaRØsus]"]
                
                ["hoje"
                 "[ÕZi]"
                 "[ÕZis]"]
                
                ["prego"
                 "[pr×gu]"
                 "[pr×gus]"]
                
                ["trecho"
                 "[tRÓSu]"
                 "[tRÓSus]"]
                
                ["horta"
                 "[ØrtA]"
                 "[ØrtAs]"]]]
      [:table.table.is-bordered
       [:thead 
        (into [:tr]
              (for [h hrs]
                [:th.has-text-centered h]))]
       (into [:tbody]
             (for [[a b c] rows]
               [:tr
                [:td a]
                [:td.red.has-text-centered.ipa b]
                [:td.red.has-text-centered.ipa c]]))])]])

(defn ra3
  "Respostas Actividades 3"
  []
  [:section.ra3
   [:li "Complete a seguinte tabela referente aos substantivos do português:"
    (let [hrs [""
               "% sem alternância vocálica"
               "% com alternância vocálica"
               "irregularidades diversas"]
          
          rows [["Verbos da 1ª conjugação"
                 "51,6%"
                 "47,5%"
                 "0,9%"]
                ["Verbos da 2ª conjugação"
                 "23,9%"
                 "73,4%"
                 "2,7%"]]]
      [:table.table.is-bordered
       [:thead 
        (into [:tr]
              (for [h hrs]
                [:th h]))]
       (into [:tbody]
             (for [[a b c d] rows]
               [:tr
                [:td a]
                [:td.red.has-text-centered.is-size-4 b]
                [:td.red.has-text-centered.is-size-4 c]
                [:td.red.has-text-centered.is-size-4 d]]))])]])

(defn ra4
  "Respostas Actividades 4"
  []
  [:section.ra4
   [:li "Transcreva as formas dos seguintes verbos no tempo presente do indicativo e do subjuntivo/conjuntivo:"
    (let [hrs ["Verbo"
               "Presente do indicativo"
               "Presente do subjuntivo/conjuntivo"]
          items [["bocejar"
                  "[bosÓZu]"
                  "[boseZÒmus]"
                  "[bosÓZi]"
                  "[boseZemus]"
                  "[bosÓZAs]"
                  "[boseZÒIs]"
                  "[bosÓZis]"
                  "[boseZÓIs]"
                  "[bosÓZA]"
                  "[bosÓZëä]"
                  "[bosÓZi]"
                  "[boseZçã]"]

                 ["tossir"
                  "[tÖsu]"
                  "[tosÔmus]"
                  "[tÖsA]"
                  "[tusÒmus]"
                  "[tØsis]"
                  "[tosÔs]"
                  "[tÖsAs]"
                  "[tusÒIs]"
                  "[tØsi]"
                  "[tØsçã]"
                  "[tÖsA]"
                  "[tÖsáä]"]

                 ["encher"
                  "[çSu]"
                  "[çSÓmus]"
                  "[çSA]"
                  "[çSÒmus]"
                  "[çSis]"
                  "[çSÓIs]"
                  "[çSAs]"
                  "[çSÒIs]"
                  "[çSi]"
                  "[çSçã]"
                  "[çSA]"
                  "[çSëä]"]

                 ["gelar"
                  "[Z×lu]"
                  "[ZelÒmus]"
                  "[Z×li]"
                  "[ZelÓmus]"
                  "[Z×lAs]"
                  "[ZelÒIs]"
                  "[Z×lis]"
                  "[ZelÓIs]"
                  "[Z×lA]"
                  "[Z×lëä]"
                  "[Z×li]"
                  "[Z×lçã]"]

                 ["apetecer"
                  "[apetÓsu]"
                  "[apetesemus]"
                  "[apetÓsA]"
                  "[apetesÒmus]"
                  "[apet×sis]"
                  "[apetesÓIs]"
                  "[apetÓsAs]"
                  "[apetesÒIs]"
                  "[apet×si]"
                  "[apet×sçã]"
                  "[apetÓsA]"
                  "[apetÓsáä]"]

                 ["refletir"
                  "[heflÔtu]"
                  "[hefleÎimus]"
                  "[heflÔtA]"
                  "[heflitÒmus]"
                  "[hefl×Îis]"
                  "[hefleÎÔ100s]"
                  "[heflÔtAs]"
                  "[heflitÒIs]"
                  "[hefl×Îi]"
                  "[hefl×tÝã]"
                  "[heflÔtA]"
                  "[heflÔtáä]"]]]
      [:table.table.is-bordered
       [:thead 
        (into [:tr]
              (for [h hrs]
                [:th.has-text-centered h]))]
       (into [:tbody]
             (for [[verbo a1 a2 a3 a4 b1 b2 b3 b4 c1 c2 c3 c4] items]
               [:tr
                [:td verbo]
                [:td.ipa {:style {"padding" 0}} ;; column 2 
                 [:table {:style {"margin" 0}}
                  [:tbody.red
                   [:tr [:td a1] [:td a2]]
                   [:tr [:td b1] [:td b2]]
                   [:tr [:td c1] [:td c2]]]]]
                [:td.ipa {:style {"padding" 0}} ;; column 3
                 [:table {:style {"margin" 0}} 
                  [:tbody.red
                   [:tr [:td a3] [:td a4]]
                   [:tr [:td b3] [:td b4]]
                   [:tr [:td c3] [:td c4]]]]]]))])]])

(defn ra5
  "Respostas Actividades 5"
  []
  [:section.ra5
   [:li "Transcreva as formas dos seguintes verbos no pretérito perfeito do indicativo " [:em "(preterite)"] " e no pretérito imperfeito do subjuntivo/conjuntivo " [:em "(imperfect subjunctive)"] "."
    (let [hrs ["Verbo"
               "Pretérito perfeito do indicativo"
               "pretérito imperfeito do subjuntivo"]
          items [
                 ["correr"
                  "[cohÔ]"
                  "[cohÓRmus]"
                  "[cohÓsi]"
                  "[cohÓsemus]"
                  "[cohÓsÎi]"
                  "[cohÓsÎis]"
                  "[cohÓsis]"
                  "[cohÓseIs]"
                  "[cohÓU]"
                  "[cohÓRáä]"
                  "[cohÓsi]"
                  "[cohÓsÝã]"]
                 ["ter"
                  "[ÎÔvi]"
                  "[ÎivÓmus]"
                  "[Îiv×si]"
                  "[Îiv×semus]"
                  "[ÎivÓsÎi]"
                  "[ÎivÓsÎis]"
                  "[Îiv×sis]"
                  "[Îiv×seIs]"
                  "[tÓvi]"
                  "[Îiv×Ráä]"
                  "[Îiv×si]"
                  "[Îiv×sÝã]"]


                 ]]
      [:table.table.is-bordered
       [:thead 
        (into [:tr]
              (for [h hrs]
                [:th.has-text-centered h]))]
       (into [:tbody]
             (for [[verbo a1 a2 a3 a4 b1 b2 b3 b4 c1 c2 c3 c4] items]
               [:tr
                [:td verbo]
                [:td.ipa {:style {"padding" 0}} ;; column 2 
                 [:table {:style {"margin" 0}}
                  [:tbody.red
                   [:tr [:td a1] [:td a2]]
                   [:tr [:td b1] [:td b2]]
                   [:tr [:td c1] [:td c2]]]]]
                [:td.ipa {:style {"padding" 0}} ;; column 3
                 [:table {:style {"margin" 0}} 
                  [:tbody.red
                   [:tr [:td a3] [:td a4]]
                   [:tr [:td b3] [:td b4]]
                   [:tr [:td c3] [:td c4]]]]]]))])]])

(defn ra6
  "Respostas Actividades 6"
  []
  [:section.ra6
   [:li "Transcreva as formas dos seguintes verbos no pretérito perfeito do indicativo " [:em "(preterite)"] " e no pretérito imperfeito do subjuntivo/conjuntivo " [:em "(imperfect subjunctive)"] "."
    (let [hrs ["Verbo"
               "Pretérito perfeito do indicativo"
               "Pretérito imperfeito do subjuntivo"]
          items [["fazer"
                  "[fazÓR]"
                  "[fazÓRmus]"
                  "[fiz×R]"
                  "[fiz×Rmus]"
                  "[fazÓRis]"
                  "[fazÓRËis]"
                  "[fiz×Ris]"
                  "[fiz×RËis]"
                  "[fazÓR]"
                  "[fazÓRÝã]"
                  "[fiz×R]"
                  "[fiz×RÝã]"]
                 ["escrever"
                  "[eskRevÓR]"
                  "[eskRevÓRmus]"
                  "[eskRevÓR]"
                  "[eskRevÓRmus]"
                  "[eskRevÓRis]"
                  "[eskRevÓRËis]"
                  "[eskRevÓRis]"
                  "[eskRevÓRËis]"
                  "[eskRevÓR]"
                  "[eskRevÓRÓã]"
                  "[eskRevÓR]"
                  "[eskRevÓRÝã]"]]]
      [:table.table.is-bordered
       [:thead 
        (into [:tr]
              (for [h hrs]
                [:th.has-text-centered h]))]
       (into [:tbody]
             (for [[verbo a1 a2 a3 a4 b1 b2 b3 b4 c1 c2 c3 c4] items]
               [:tr
                [:td verbo]
                [:td.ipa {:style {"padding" 0}} ;; column 2 
                 [:table {:style {"margin" 0}}
                  [:tbody.red
                   [:tr [:td a1] [:td a2]]
                   [:tr [:td b1] [:td b2]]
                   [:tr [:td c1] [:td c2]]]]]
                [:td.ipa {:style {"padding" 0}} ;; column 3
                 [:table {:style {"margin" 0}} 
                  [:tbody.red
                   [:tr [:td a3] [:td a4]]
                   [:tr [:td b3] [:td b4]]
                   [:tr [:td c3] [:td c4]]]]]]))])]])

(defn ra7
  "Respostas Actividades 7"
  []
  [:section.ra7
   [:li " Complete a seguinte tabela referente à porcentagem dos adjetivos do português com vogais tônicas médias:"
    (let [hrs [[:span "Vogal tônica " [ipa "/e/"] " " [ipa "/o/"]]
               [:span "Vogal tônica " [ipa "/E/"] " " [ipa "/O/"]]
               [:span "Alternância " [ipa "/o/"] " " [ipa "/O/"]]
               "Outros padrões"]
          row ["52,7%"
                "33,3%"
                "12,7%"
                "1,3%"]]
      [:table.table.is-bordered
       [:thead 
        (into [:tr]
              (for [h hrs]
                [:th h]))]
       [:tbody
        (into [:tr.red.is-size-4]
              (for [r row] [:td r]))]])]])

(defn ra8
  "Respostas Actividades 8"
  []
  [:section.ra8
   [:li "Transcreva as formas dos seguintes adjetivos:"
    (let [hrs ["Adjetivo"
               "Formas"]
          rows [["novo"
                 "[nÕvu]"
                 "[nØvus]"
                 "[nØvA]"
                 "[nØvAs]"]

                ["preso"
                 "[pRÓzu]"
                 "[pRÓzus]"
                 "[pRÓzA]"
                 "[pRÓzAs]"]

                ["predileto"
                 "[pReËil×tu]"
                 "[pReËil×tus]"
                 "[pReËil×tA]"
                 "[pReËil×tAs]"]

                ["oco"
                 "[Õku]"
                 "[Õkus]"
                 "[ÕkA]"
                 "[ÕkAs]"]
                
                ["malicioso"
                 "[maliciÕzu]"
                 "[maliciØzus]"
                 "[maliciØzA]"
                 "[maliciØzAs]"]

                ["aberto"
                 "[ab×Rtu]"
                 "[ab×Rtus]"
                 "[ab×RtA]"
                 "[ab×RtAs]"]]]
      [:table.table.is-bordered
       [:thead 
        (into [:tr]
              (for [h hrs]
                [:th.has-text-centered h]))]
       (into [:tbody]
             (for [[adjetivo a1 a2 b1 b2] rows]
               [:tr
                [:td adjetivo]
                [:td.red {:style {"padding" 0}}
                 [:table  {:style {"margin" 0}}
                  [:tbody.red.ipa
                          [:tr [:td a1] [:td a2]]
                          [:tr [:td b1] [:td b2]]]]]]))])]])

(defn render-respostas
  "Chapter 13 respostas"
  []
  [:main.c13.respostas.content
   [shared/fonetica-title "Capítulo 13 — As vogais meio-abertas e meio-fechadas"
    "Respostas para o eResource"]
   [:section.atividades
    [:h2 "ATIVIDADES"]
    [:ol.atividades.list
     [ra1]
     [ra2]
     [ra3]
     [ra4]
     [ra5]
     [ra6]
     [ra7]
     [ra8]]]])

(defn a1
  "Actividades 1"
  []
  [:section.a1
   [:li "Complete a seguinte tabela referente aos substantivos do português:"
[shared/media-icon :r (rfe/href :fonetica-port.routes/c13-r) :same-session]
    (let [hrs [[:span ""]
               [:span "% com vogal tônica meio-fechada " [ipa "/e/"] " " [ipa "/o/"]]
               [:span "% com vogal tônica meio-aberta " [ipa "/E/"] " " [ipa "/O/"]]
               [:span "% com forma singular " [ipa "/o/"] " e forma plural " [ipa "/O/"]]]
          rows [[[:span "Palavras terminadas em {o}"]
                 [:span "73,1%"]
                 [:span "25,6%"]
                 [:span "1,3%"]]
                [[:span "Palavras terminadas em {a} ou {e}"]
                 [:span "57,0%"]
                 [:span "43,0%"]
                 [:span "⸺"]]]]
      [:table.table.is-bordered
       [:thead 
        (into [:tr]
              (for [h hrs]
                [:th h]))]
       (into [:tbody]
             (for [[a b c d] rows]
               [:tr
                [:td a]
                [:td.red.has-text-centered.is-size-4]
                [:td.red.has-text-centered.is-size-4]
                [:td.red.has-text-centered.is-size-4]]))])]])

(defn a2
  "Actividades 2"
  []
  [:section.a2
   [:li "Transcreva as formas singular e plural dos seguintes substantivos:"
[shared/media-icon :r (rfe/href :fonetica-port.routes/c13-r) :same-session]
    (let [hrs ["Substantivo"
               "Forma singular"
               "Forma plural"]
          rows [["esposo"
                 "[espÕzu]"
                 "[espÕzus]"]
                
                ["alfabeto"
                 "[aKfab×tu]"
                 "[aKfab×tus]"]
                
                ["escolho"
                 "[eskÕYu]"
                 "[eskØYus]"]
                
                ["terra"
                 "[t×hA]"
                 "[t×hAs]"]
                
                ["alteza"
                 "[aKtÓzA]"
                 "[aKtÓzAs]"]
                
                ["caroço"
                 "[kaRÕsu]"
                 "[kaRØsus]"]
                
                ["hoje"
                 "[ÕZi]"
                 "[ÕZis]"]
                
                ["prego"
                 "[pr×gu]"
                 "[pr×gus]"]
                
                ["trecho"
                 "[tRÓSu]"
                 "[tRÓSus]"]
                
                ["horta"
                 "[ØrtA]"
                 "[ØrtAs]"]]]
      [:table.table.is-bordered
       [:thead 
        (into [:tr]
              (for [h hrs]
                [:th.has-text-centered h]))]
       (into [:tbody]
             (for [[a b c] rows]
               [:tr
                [:td a]
                [:td.red.has-text-centered.ipa ]
                [:td.red.has-text-centered.ipa ]]))])]])

(defn a3
  "Actividades 3"
  []
  [:section.a3
   [:li "Complete a seguinte tabela referente aos substantivos do português:"
[shared/media-icon :r (rfe/href :fonetica-port.routes/c13-r) :same-session]
    (let [hrs [""
               "% sem alternância vocálica"
               "% com alternância vocálica"
               "irregularidades diversas"]
          
          rows [["Verbos da 1ª conjugação"
                 "51,6%"
                 "47,5%"
                 "0,9%"]
                ["Verbos da 2ª conjugação"
                 "23,9%"
                 "73,4%"
                 "2,7%"]]]
      [:table.table.is-bordered
       [:thead 
        (into [:tr]
              (for [h hrs]
                [:th h]))]
       (into [:tbody]
             (for [[a b c d] rows]
               [:tr
                [:td a]
                [:td.red.has-text-centered.is-size-4 ]
                [:td.red.has-text-centered.is-size-4 ]
                [:td.red.has-text-centered.is-size-4 ]]))])]])

(defn a4
  "Actividades 4"
  []
  [:section.a4
   [:li "Transcreva as formas dos seguintes verbos no tempo presente do indicativo e do subjuntivo/conjuntivo:"
[shared/media-icon :r (rfe/href :fonetica-port.routes/c13-r) :same-session]
    (let [hrs ["Verbo"
               "Presente do indicativo"
               "Presente do subjuntivo/conjuntivo"]
          items [["bocejar"
                  "[bosÓZu]"
                  "[boseZÒmus]"
                  "[bosÓZi]"
                  "[boseZemus]"
                  "[bosÓZAs]"
                  "[boseZÒIs]"
                  "[bosÓZis]"
                  "[boseZÓIs]"
                  "[bosÓZA]"
                  "[bosÓZëä]"
                  "[bosÓZi]"
                  "[boseZçã]"]

                 ["tossir"
                  "[tÖsu]"
                  "[tosÔmus]"
                  "[tÖsA]"
                  "[tusÒmus]"
                  "[tØsis]"
                  "[tosÔs]"
                  "[tÖsAs]"
                  "[tusÒIs]"
                  "[tØsi]"
                  "[tØsçã]"
                  "[tÖsA]"
                  "[tÖsáä]"]

                 ["encher"
                  "[çSu]"
                  "[çSÓmus]"
                  "[çSA]"
                  "[çSÒmus]"
                  "[çSis]"
                  "[çSÓIs]"
                  "[çSAs]"
                  "[çSÒIs]"
                  "[çSi]"
                  "[çSçã]"
                  "[çSA]"
                  "[çSëä]"]

                 ["gelar"
                  "[Z×lu]"
                  "[ZelÒmus]"
                  "[Z×li]"
                  "[ZelÓmus]"
                  "[Z×lAs]"
                  "[ZelÒIs]"
                  "[Z×lis]"
                  "[ZelÓIs]"
                  "[Z×lA]"
                  "[Z×lëä]"
                  "[Z×li]"
                  "[Z×lçã]"]

                 ["apetecer"
                  "[apetÓsu]"
                  "[apetesemus]"
                  "[apetÓsA]"
                  "[apetesÒmus]"
                  "[apet×sis]"
                  "[apetesÓIs]"
                  "[apetÓsAs]"
                  "[apetesÒIs]"
                  "[apet×si]"
                  "[apet×sçã]"
                  "[apetÓsA]"
                  "[apetÓsáä]"]

                 ["refletir"
                  "[heflÔtu]"
                  "[hefleÎimus]"
                  "[heflÔtA]"
                  "[heflitÒmus]"
                  "[hefl×Îis]"
                  "[hefleÎÔ100s]"
                  "[heflÔtAs]"
                  "[heflitÒIs]"
                  "[hefl×Îi]"
                  "[hefl×tÝã]"
                  "[heflÔtA]"
                  "[heflÔtáä]"]]]
      [:table.table.is-bordered
       [:thead 
        (into [:tr]
              (for [h hrs]
                [:th.has-text-centered h]))]
       (into [:tbody]
             (for [[verbo a1 a2 a3 a4 b1 b2 b3 b4 c1 c2 c3 c4] items]
               [:tr
                [:td verbo]
                [:td.ipa {:style {"padding" 0}} ;; column 2 
                 [:table {:style {"margin" 0}}
                  [:tbody.red
                   [:tr [:td ] [:td]]
                   [:tr [:td ] [:td]]
                   [:tr [:td ] [:td]]]]]
                [:td.ipa {:style {"padding" 0}} ;; column 3
                 [:table {:style {"margin" 0}} 
                  [:tbody.red
                   [:tr [:td ] [:td ]]
                   [:tr [:td ] [:td ]]
                   [:tr [:td ] [:td ]]]]]]))])]])

(defn a5
  "Actividades 5"
  []
  [:section.a5
   [:li "Transcreva as formas dos seguintes verbos no pretérito perfeito do indicativo " [:em "(preterite)"] " e no pretérito imperfeito do subjuntivo/conjuntivo " [:em "(imperfect subjunctive)"] "."
[shared/media-icon :r (rfe/href :fonetica-port.routes/c13-r) :same-session]
    (let [hrs ["Verbo"
               "Pretérito perfeito do indicativo"
               "pretérito imperfeito do subjuntivo"]
          items [
                 ["correr"
                  "[cohÔ]"
                  "[cohÓRmus]"
                  "[cohÓsi]"
                  "[cohÓsemus]"
                  "[cohÓsÎi]"
                  "[cohÓsÎis]"
                  "[cohÓsis]"
                  "[cohÓseIs]"
                  "[cohÓU]"
                  "[cohÓRáä]"
                  "[cohÓsi]"
                  "[cohÓsÝã]"]
                 ["ter"
                  "[ÎÔvi]"
                  "[ÎivÓmus]"
                  "[Îiv×si]"
                  "[Îiv×semus]"
                  "[ÎivÓsÎi]"
                  "[ÎivÓsÎis]"
                  "[Îiv×sis]"
                  "[Îiv×seIs]"
                  "[tÓvi]"
                  "[Îiv×Ráä]"
                  "[Îiv×si]"
                  "[Îiv×sÝã]"]


                 ]]
      [:table.table.is-bordered
       [:thead 
        (into [:tr]
              (for [h hrs]
                [:th.has-text-centered h]))]
       (into [:tbody]
             (for [[verbo a1 a2 a3 a4 b1 b2 b3 b4 c1 c2 c3 c4] items]
               [:tr
                [:td verbo]
                [:td.ipa {:style {"padding" 0}} ;; column 2 
                 [:table {:style {"margin" 0}}
                  [:tbody.red
                   [:tr [:td ] [:td ]]
                   [:tr [:td ] [:td ]]
                   [:tr [:td ] [:td ]]]]]
                [:td.ipa {:style {"padding" 0}} ;; column 3
                 [:table {:style {"margin" 0}} 
                  [:tbody.red
                   [:tr [:td ] [:td ]]
                   [:tr [:td ] [:td ]]
                   [:tr [:td ] [:td ]]]]]]))])]])

(defn a6
  "Actividades 6"
  []
  [:section.a6
   [:li "Transcreva as formas dos seguintes verbos no pretérito perfeito do indicativo " [:em "(preterite)"] " e no pretérito imperfeito do subjuntivo/conjuntivo " [:em "(imperfect subjunctive)"] "."
[shared/media-icon :r (rfe/href :fonetica-port.routes/c13-r) :same-session]
    (let [hrs ["Verbo"
               "Pretérito perfeito do indicativo"
               "Pretérito imperfeito do subjuntivo"]
          items [["fazer"
                  "[fazÓR]"
                  "[fazÓRmus]"
                  "[fiz×R]"
                  "[fiz×Rmus]"
                  "[fazÓRis]"
                  "[fazÓRËis]"
                  "[fiz×Ris]"
                  "[fiz×RËis]"
                  "[fazÓR]"
                  "[fazÓRÝã]"
                  "[fiz×R]"
                  "[fiz×RÝã]"]
                 ["escrever"
                  "[eskRevÓR]"
                  "[eskRevÓRmus]"
                  "[eskRevÓR]"
                  "[eskRevÓRmus]"
                  "[eskRevÓRis]"
                  "[eskRevÓRËis]"
                  "[eskRevÓRis]"
                  "[eskRevÓRËis]"
                  "[eskRevÓR]"
                  "[eskRevÓRÓã]"
                  "[eskRevÓR]"
                  "[eskRevÓRÝã]"]]]
      [:table.table.is-bordered
       [:thead 
        (into [:tr]
              (for [h hrs]
                [:th.has-text-centered h]))]
       (into [:tbody]
             (for [[verbo a1 a2 a3 a4 b1 b2 b3 b4 c1 c2 c3 c4] items]
               [:tr
                [:td verbo]
                [:td.ipa {:style {"padding" 0}} ;; column 2 
                 [:table {:style {"margin" 0}}
                  [:tbody.red
                   [:tr [:td ] [:td ]]
                   [:tr [:td ] [:td ]]
                   [:tr [:td ] [:td ]]]]]
                [:td.ipa {:style {"padding" 0}} ;; column 3
                 [:table {:style {"margin" 0}} 
                  [:tbody.red
                   [:tr [:td ] [:td ]]
                   [:tr [:td ] [:td ]]
                   [:tr [:td ] [:td ]]]]]]))])]])

(defn a7
  "Actividades 7"
  []
  [:section.a7
   [:li " Complete a seguinte tabela referente à porcentagem dos adjetivos do português com vogais tônicas médias:"
[shared/media-icon :r (rfe/href :fonetica-port.routes/c13-r) :same-session]
    (let [hrs [[:span "Vogal tônica " [ipa "/e/"] " " [ipa "/o/"]]
               [:span "Vogal tônica " [ipa "/E/"] " " [ipa "/O/"]]
               [:span "Alternância " [ipa "/o/"] " " [ipa "/O/"]]
               "Outros padrões"]
          row ["52,7%"
                "33,3%"
                "12,7%"
                "1,3%"]]
      [:table.table.is-bordered
       [:thead 
        (into [:tr]
              (for [h hrs]
                [:th h]))]
       [:tbody
        (into [:tr.red.is-size-4]
              (for [r row] [:td #_r]))]])]])

(defn a8
  "Actividades 8"
  []
  [:section.a8
   [:li "Transcreva as formas dos seguintes adjetivos:"
[shared/media-icon :r (rfe/href :fonetica-port.routes/c13-r) :same-session]
    (let [hrs ["Adjetivo"
               "Formas"]
          rows [["novo"
                 "[nÕvu]"
                 "[nØvus]"
                 "[nØvA]"
                 "[nØvAs]"]

                ["preso"
                 "[pRÓzu]"
                 "[pRÓzus]"
                 "[pRÓzA]"
                 "[pRÓzAs]"]

                ["predileto"
                 "[pReËil×tu]"
                 "[pReËil×tus]"
                 "[pReËil×tA]"
                 "[pReËil×tAs]"]

                ["oco"
                 "[Õku]"
                 "[Õkus]"
                 "[ÕkA]"
                 "[ÕkAs]"]
                
                ["malicioso"
                 "[maliciÕzu]"
                 "[maliciØzus]"
                 "[maliciØzA]"
                 "[maliciØzAs]"]

                ["aberto"
                 "[ab×Rtu]"
                 "[ab×Rtus]"
                 "[ab×RtA]"
                 "[ab×RtAs]"]]]
      [:table.table.is-bordered
       [:thead 
        (into [:tr]
              (for [h hrs]
                [:th.has-text-centered h]))]
       (into [:tbody]
             (for [[adjetivo a1 a2 b1 b2] rows]
               [:tr
                [:td adjetivo]
                [:td.red {:style {"padding" 0}}
                 [:table  {:style {"margin" 0}}
                  [:tbody.red.ipa
                          [:tr [:td ] [:td ]]
                          [:tr [:td ] [:td ]]]]]]))])]])


(defn atividades
  "Chapter 13 atividades"
  []
  [:section.atividades.content
   [:h2 "ATIVIDADES"]
   [:ol.atividades
    [a1]
    [a2]
    [a3]
    [a4]
    [a5]
    [a6]
    [a7]
    [a8]]])

(defn recursos
  "Resources"
  []
  [:section.recursos
   [:h3 "RECURSOS ELETRÔNICOS"]
   (let [items
         [[:span "Pares mínimos contrastando as vogais médias e todas as demais vogais do português conforme a Tab. 13.1."]
          [:span "A redução da vogal meio-aberta " [ipa "[E]"] " em posição átona."]
          [:span "Oposição inglesa " [ipa "/eI E/"] " e portuguesa " [ipa "/e E eI/"] "."]
          [:span "Oposição portuguesa " [ipa "/o O oU/"] " e inglesa " [ipa "/oU O/"] "."]
          [:span "A pronúncia da vogal " [ipa "[E]"] "."]
          [:span "Exemplos de " [ipa "[e E O o]"] "."]
          [:span "Exemplos de " [ipa "[e E O o]"] "."]
          [:span "Exemplos de " [ipa "[e E O o]"] "."]
          [:span "Exemplos de " [ipa "[e E O o]"] "."]
          [:span "Exemplos de " [ipa "[o O]"] "."]
          [:span "As palavras da Tab. 13.4."]
          [:span "Alternância vocálica nas formas singular e plural de substantivos."]
          [:span "Não alternância vocálica nas formas singular e plural de substantivos."]
          [:span "Pares análogos com alternância vocálica nos substantivos."]
          [:span "Palavras com “acento secundário”."]
          [:span "Palavras com vogais médias diante de " [ipa "/l/"] " final de palavra."]
          [:span "A conjugação dos verbos {levar} e {notar} da 1ª conjugação no presente do indicativo e subjuntivo com alternância vocálica."]
          [:span "A conjugação do verbo {chegar} da 1ª conjugação no presente do indicativo e subjuntivo sem alternância vocálica."]
          [:span "A conjugação dos verbos {beber} e {mover} da 2ª conjugação no presente do indicativo e subjuntivo com alternância vocálica."]
          [:span "A conjugação do verbo {romper} da 2ª conjugação no presente do indicativo e subjuntivo sem alternância vocálica."]
          [:span "Formas verbais do verbo {comer} da 2ª conjugação no presente do indicativo em Portugal e no Brasil."]
          [:span "A conjugação dos verbos {seguir} e {dormir} da 3ª conjugação no presente do indicativo e subjuntivo com alternância vocálica."]
          [:span "A conjugação dos verbos {agredir} e {sortir} da 3ª conjugação no presente do indicativo e subjuntivo com alternância vocálica."]
          [:span "A conjugação do verbo {pedir} e {medir} da 3ª conjugação no presente do indicativo e subjuntivo com alternância vocálica."]
          [:span "A conjugação do verbo {sentir} da 3ª conjugação no presente do indicativo e subjuntivo com alternância vocálica."]
          [:span "Formas do imperfeito do subjuntivo."]
          [:span "A conjugação dos verbos {dever} e {ter} no imperfeito do subjuntivo."]
          [:span "O pretérito perfeito do indicativo e o imperfeito do subjuntivo dos verbos {dever} e {ter}."]
          [:span "A conjugação dos verbos {dever} e {ter} no futuro do subjuntivo."]
          [:span "As formas irregulares de 3ª pessoa plural no pretérito perfeito do indicativo."]
          [:span "A forma irregular dos verbos {ser/ir} em 3ª pessoa plural no pretérito perfeito do indicativo e nas formas baseadas nela."]
          [:span "As formas do verbo {fazer} no infinitivo pessoal e do futuro do subjuntivo."]
          [:span "As formas de adjetivos com vogais tônicas meio-fechadas."]
          [:span "As formas de adjetivos com vogais tônicas meio-abertas."]
          [:span "As formas de adjetivos terminados em {-oso} com alternância vocálica."]
          [:span "As formas de adjetivos cujo masculino singular termina em {-ão}."]
          [:span "As formas de adjetivos cujo masculino singular termina em {-eu}."]
          [:span "Os pronomes tônicos."]
          [:span "Os pronomes átonos."]
          [:span "Os artigos definidos."]
          [:span "Os demonstrativos."]
          [:span "Os possessivos."]
          [:span "Os números cardinais."]
          [:span "Os números ordinais."]
          [:Span "Advérbios."]
          [:span "Preposições simples."]
          [:span "Preposições acidentais e compostas."]
          [:span "Contrastes entre substantivos e formas verbais."]
          [:span "Casos em que os substantivos e a forma verbal têm a mesma vogal."]
          [:span "Contrastes entre a forma verbal e o adjetivo."]
          [:span "Contrastes entre substantivos e adjetivos."]
          [:span "Contrastes entre substantivos."]
          [:span "Os exemplos da Tab. 13.5 da redução e não redução no português europeu."]]
         ex-items [[:span "Pares mínimos/análogos que contrastam " [ipa "/e/"] " e " [ipa "/E/"] "."]
                   [:span "Pares mínimos/análogos que contrastam " [ipa "/o/"] " e " [ipa "/O/"] "."]
                   [:span "Contrastes entre verbos e substantivos com " [ipa "/e/"] " e " [ipa "/E/"] "."]
                   [:span "Contrastes entre verbos e substantivos com " [ipa "/o/"] " e " [ipa "/O/"] "."]]
         fonetica-line-item (fn [n i]
                              [:li [shared/media-icon :audio
                                    (shared/file (str "C13-M" (inc n) ".mp3"))] i])
         fonetica-line-item2 (fn [n i]
                               [:li [shared/media-icon :ex
                                     (shared/file (str "C13-M" (+ 54 n) ".mp3"))] i])]
     (into
      [:ol.list]
      (conj 
       (map-indexed fonetica-line-item2 ex-items)
       (map-indexed fonetica-line-item items))))])

(defn render
  "Chapter thirteen view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 13 — As vogais meio-abertas e meio-fechadas"
    "eResource"]
   [:div.content.chapter-13
    [recursos]
    [atividades]]
   [shared/footer]])

                                        ; [shared/media-icon :r (rfe/href :fonetica-port.routes/c13-r) :same-session]
