(ns fonetica-port.views.c3
  "Chapter 3"
  (:require [fonetica-port.views.components.shared :as shared :refer [ipa femp]]
            [reagent.core :as reagent]
            [reitit.frontend.easy :as rfe]))

(defn render-respostas
  "Chapter 3 respostas"
  []
  [:main.c3.respostas.container.content
   [shared/fonetica-title "Capítulo 3 — A fonética e a fonologia"
    "Respostas para o eResource"]
   [:ol
    [:li.c3-r-1
     "O som para /S/ em {os gatos} é produzido com vibração das cordas vocais. Isso ocorre porque as cordas vocais também vibram ao produzir a consonante seguinte: [g]. O fenômeno descrito é portanto FONÉTICO, porque descreve as características físicas que regem a seleção do som emitido."]
    [:li.c3-r-2
     "O fenômeno descrito é FONOLÓGICO, porque manifesta o processo mental em que dois sons fisicamente diferentes entre si são considerados idênticos. Quer dizer que, o fato de pronunciar a consonante final do artigo definido {os} como [s] em " [ipa "[uskÒhus]"] " e como [z] em " [ipa "[uzgÒtus]"] " não altera seu significado."]]])

(defn table-blanks
  "Render just the table blanks"
  []
  (let [rows [["Âmbito" [:span "fala/indivíduo"] [:span "língua/sociedade"]]
                    ["Unidade básica" [:span "som"] [:span "fonema"]]
                    ["Representação gráfica" [:span [ipa "[kav×RnA]"] " entre colchetes"] [:span [ipa "[kav×Rna]"] " entre barras"]]
                    ["Processos" [:span "físicos"] [:span "mentais"]]
                    ["Enfoquesons" "sons individuais" "sistema de sons"]
                    ["Disciplinas"
                     [:span [:strong "DESCRIÇÕES:"] [:div [:ol
			                                   [:li "articulatórias"]
			                                   [:li "acústicas"]
			                                   [:li "auditivas"]]]]
                     [:span [:strong "RELAÇÕES:"] [:div [:ol
			                                 [:li "oposições entre fonemas"]
			                                 [:li "distribuições de alofones"]
			                                 [:li "fonotática"]]]]]]]
          [:section.table-blanks
           [:table.ftable.table.is-bordered.is-fullwidth
            [:thead [:tr [:td.blank] [:td "Fonética"] [:td "Fonologia"]]]
            (into [:tbody]
                  (for [[a _ _] rows]
                    [:tr [:td [:strong a]] [:td ] [:td ]]))]
           [:table.ftable.table.is-bordered.is-fullwidth
            [:thead [:tr [:td.blank] [:td "Fonética"] [:td "Fonologia"]]]
            (into [:tbody]
                  (for [[a _ _] rows]
                    [:tr [:td [:strong a]] [:td ] [:td ]]))]
           ;; use CSS magic to hide this on the wrong page but show it on the print page
           [:button.button.is-primary.no-print.print-button
            {:on-click #(.print js/window)}
            "Print"]]))

(defn _table-blanks
  "Renderer with on-print popup for the blank tables"
  []
  (let []   
    (reagent/create-class
     {:display-name  "table-blanks"

      :component-did-mount
      (fn [this] 
        #_(.print js/window))
      
      :reagent-render table-blanks})))

(defn a4-tables
  "The three tables at the bottom af c3"
  []
  [:section.a4-tables
     (let [print-window-id "fonetica-print"
        rows [["Âmbito" [:span "fala/indivíduo"] [:span "língua/sociedade"]]
              ["Unidade básica" [:span "som"] [:span "fonema"]]
              ["Representação gráfica" [:span [ipa "[kav×RnA]"] " entre colchetes"] [:span [ipa "[kav×Rna]"] " entre barras"]]
              ["Processos" [:span "físicos"] [:span "mentais"]]
              ["Enfoquesons" "sons individuais" "sistema de sons"]
              ["Disciplinas"
               [:span [:strong "DESCRIÇÕES:"] [:div [:ol
			                             [:li "articulatórias"]
			                             [:li "acústicas"]
			                             [:li "auditivas"]]]]
               [:span [:strong "RELAÇÕES:"] [:div [:ol
			                           [:li "oposições entre fonemas"]
			                           [:li "distribuições de alofones"]
			                           [:li "fonotática"]]]]]]]
    [:section.tables
     [:table.ftable.table.is-bordered
      [:thead [:tr [:td.blank] [:td "Fonética"] [:td "Fonologia"]]]
      (into [:tbody]
            (for [[a b c] rows]
              [:tr [:td [:strong a]] [:td b] [:td c]]))]
     [table-blanks]
     
     [:a.button.is-primary {:href (rfe/href :fonetica-port.routes/c3-print)}
      "Print Blanks"]])])

(defn activities
  "Chapter 3 activities"
  []
   [:section.activities.content
    [:h2 "ATIVIDADES"]
    [:ol.list
     [:li.a1 "Em português ao dizer {os carros} " [ipa "[uskÒhus]"] [shared/media-icon :audio (shared/file "C3-A1-1_carros.mp3")] ", observa-se que na pronúncia tanto do " [ipa "[s]"] " como do " [ipa "[k]"] " que as cordas vocais não vibram. Qual é a diferença entre essa pronúncia e a pronúncia da sequência {os gatos} " [ipa "[uzgÒtus]"] [shared/media-icon :audio (shared/file "C3-A1-2_osgatos.mp3")] "? Por que, no artigo “os”, a pronúncia de /S/é diferente? Esse é um fenômeno fonético ou fonológico? Justifique sua resposta. " [shared/media-icon :r (rfe/href :fonetica-port.routes/c3-r) :same-session]]

     [:li.a2
      "Em português, em posição final de palavra existe um só arquifonema /S/, que pode ser representado fisicamente por até quatro sons diferentes, dependendo do dialeto. Em dois deles as cordas vocais não vibram [s S] " [shared/media-icon :audio (shared/file "C3-A2-1_s_sh.mp3")] " , mas nos outros dois as cordas vocais vibram sim [z Z] " [shared/media-icon :audio (shared/file "C3-A2-2_z_zh.mp3")] ". O fato de que os quatro sons diferentes representam um só conceito mental é um fenômeno fonético ou fonológico? Justifique sua resposta. " [shared/media-icon :r (rfe/href :fonetica-port.routes/c3-r) :same-session]]

     [:li.a3
      "Diga as sequências " [ipa "[uskÒhus]"] " e " [ipa "[uzgÒtus]"] " com cuidado para usar o som certo para representar o /S/ no final do artigo definido “os”."]
     
     [:li.a4
      "É importante entender bem as distinções entre a fonética e a fonologia resumidas na Tabela 3.4 do livro. Imprima a tabela em branco abaixo e pratique o preenchimento da tabela até saber fazê-lo bem."]]

    [a4-tables]] )

(defn render
  "Chapter three view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 3 — A fonética e a fonologia"
    "eResource"]
   [:div.content.chapter-3
    [activities]]
   
   [shared/footer]])
