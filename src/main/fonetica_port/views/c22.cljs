(ns fonetica-port.views.c22
  "Chapter 22"
  (:require [fonetica-port.views.components.shared :as shared :refer [ipa fempr red ipa-like]]
            [reitit.frontend.easy :as rfe]))

(defn ra1
  "Respostas activity 1"
  []
  [:section.ra1
   [:li [:div "Escute a gravação das seguintes palavras. Sublinhe as vogais tônicas de cada uma e escreva os acentos ortográficos caso sejam necessários. Explique por que o acento ortográfico é necessário ou por que não é."]
    (let [items [[[:span "acent" [:span.is-underlined "u"] "o"] [:span "As palavras paroxítonas terminadas nas vogais /e a o/ não levam acento ortográfico."]]
                 [[:span "ansi" [:span.is-underlined "o"] "sam" [:span.is-underlined "e"] "nte"] [:span "Os advérbios terminados em -mente levam dois acentos. A palavra ansiosa termina em /a/, e, sendo paroxítona, não leva acento ortográfico. O morfema -mente termina em /e/, e, sendo paroxítona, não leva acento ortográfico."]]
                 [[:span "arru" [:span.is-underlined "í"] "na"] [:span "As palavras paroxítonas terminadas nas vogais /e a o/ geralmente não levam acento ortográfico. Nesse caso, porém, devido à preferência do português pelo ditongo decrescente, sem o acento ortográfico no {i}, o acento fonético recairia no {u}."]]
                 [[:span [:span.is-underlined "á"] "spide"] [:span "As palavras proparoxítonas sempre precisam de acento ortográfico."]]
                 [[:span "ba" [:span.is-underlined "ú"] ""] [:span "As palavras oxítonas terminadas nas vogais /i u/ geralmente não levam acento ortográfico. Nesse caso, porém, devido à preferência do português pelo ditongo decrescente, sem o acento ortográfico no {u}, o acento fonético recairia no {a}."]]
                 [[:span "c" [:span.is-underlined "â"] "ntara"] [:span "As palavras proparoxítonas sempre precisam de acento ortográfico. Nesse caso o acento é circunflexo porque ocorre diante do arquifonema /N/."]]
                 [[:span "car" [:span.is-underlined "á"] "ter"] [:span "A menos que sejam oxítonas, as palavras terminadas em consoantes (com exceção de {s m}) precisam de acento ortográfico."]]
                 [[:span "urub" [:span.is-underlined "u"] ""] [:span "As palavras oxítonas terminadas nas vogais /i u/ não levam acento ortográfico."]]
                 [[:span "estud" [:span.is-underlined "á"] "-lo"] [:span "A menos que sejam paroxítonas, as palavras terminadas nas vogais /e a o/ precisam de acento ortográfico."]]
                 [[:span "d" [:span.is-underlined "e"] "u"] [:span "A palavra não precisa de acento ortográfico, porque devido à preferência do português pelo ditongo decrescente, o acento fonético natural é no /e/."]]
                 [[:span "esdr" [:span.is-underlined "ú"] "xula"] [:span "As palavras proparoxítonas sempre precisam de acento ortográfico."]]
                 [[:span "estud" [:span.is-underlined "a"] "is"] [:span "A palavra é oxítona terminada em /S/ precedido de /i/ sem acento ortográfico, portanto acento fonético recai no /a/."]]
                 [[:span "fel" [:span.is-underlined "i"] "z"] [:span "As palavras oxítonas terminadas em consoantes (com exceção de {s m}) não levam acento ortográfico."]]
                 [[:span "fr" [:span.is-underlined "i"] "am" [:span.is-underlined "e"] "nte"] [:span "Os advérbios terminados em -mente levam dois acentos. A palavra fria termina em /a/, e, sendo paroxítona, não leva acento ortográfico. O morfema -mente termina em /e/, e, sendo paroxítona, não leva acento ortográfico."]]
                 [[:span "f" [:span.is-underlined "u"] "i"] [:span "A palavra não precisa de acento ortográfico porque, devido à preferência do português pelo ditongo decrescente, o acento fonético natural é no /u/."]]
                 [[:span "her" [:span.is-underlined "o"] "ico"] [:span "As palavras paroxítonas terminadas nas vogais /e a o/ não levam acento ortográfico. Como [oI] é um ditongo decrescente, o /o/ leva o acento fonético."]]
                 [[:span "h" [:span.is-underlined "ó"] "spede"] [:span "As palavras proparoxítonas sempre precisam de acento ortográfico. Esse acento é agudo porque a vogal é meio-aberta [Ø]."]]
                 [[:span "jesu" [:span.is-underlined "í"] "tico"] [:span "As palavras proparoxítonas sempre precisam de acento ortográfico."]]
                 [[:span "ra" [:span.is-underlined "i"] "nha"] [:span "As palavras paroxítonas terminadas nas vogais /e a o/ geralmente não levam acento ortográfico. A sequência de vogal mais {i} ou {u} normalmente forma um ditongo decrescente, se o [i] ou o [u] fossem tônicos precisariam de acento ortográfico. Há uma exceção: Antes de {nh} ou arquifonema /N/, o /i/ ou /u/ levam acento fonético sem necessidade de acento ortográfico."]]
                 [[:span "l" [:span.is-underlined "á"] "pis"] [:span "Normalmente as palavras terminadas em /S/ precedido de /i/ sem acento ortográfico são oxítonas, mas como lápis é paroxítona, precisa levar acento ortográfico."]]
                 [[:span "per" [:span.is-underlined "í"] "odo"] [:span "As palavras proparoxítonas sempre precisam de acento ortográfico. Sem o acento ortográfico, a palavra seria paroxítona com acento fonético no /o/."]]
                 [[:span "pro" [:span.is-underlined "í"] "bo"] [:span "As palavras paroxítonas terminadas nas vogais /e a o/ não costumam levar acento ortográfico. Nesse caso, porém, devido à preferência do português pelo ditongo decrescente, sem o acento ortográfico, o acento fonético recairia no /o/."]]
                 [[:span "pontap" [:span.is-underlined "é"] ""] [:span "Exceto pelas palavras paroxítonas, as palavras terminadas nas vogais /e a o/ precisam levar acento ortográfico."]]
                 [[:span "vi" [:span.is-underlined "ú"] "va"] [:span "As palavras paroxítonas terminadas nas vogais /e a o/ não costumam levar acento ortográfico. Nesse caso, porém, devido à preferência do português pelo ditongo decrescente, sem o acento ortográfico, o acento fonético recairia no /i/."]]
                 [[:span "sent" [:span.is-underlined "i"] "-lo"] [:span "As palavras oxítonas terminadas nas vogais /i u/ não levam acento ortográfico."]]
                 [[:span "m" [:span.is-underlined "u"] "ito"] [:span "As palavras paroxítonas terminadas nas vogais /e a o/ geralmente não levam acento ortográfico. Como a preferência do português e pelo ditongo decrescente, a vogal /u/ do ditongo [uI] leva o acento fonético."]]
]]
      (into [:ol.list.is-lower-alpha]
            (for [[a b] items]
              [:li
               [:div.grid2.no-gap.red.bordered a b]])))]])

(defn ra2
  "Respostas activity 2"
  []
  [:section.ra2
   [:li [:div "Escute a gravação das seguintes orações. Sublinhe todas as vogais tônicas de cada uma e escreva os acentos ortográficos caso sejam necessários. Quanto às palavras monossilábicas, explique por que levam acento ortográfico, ou por que não levam."]
    (let [items [[[:div "Jo" [:span.is-underlined "ã"] "o o f" [:span.is-underlined "e"] "z s" [:span.is-underlined "i"] "m."]

                  [:div "João: substantivo (tônico), não precisa ser diferenciado por acento gráfico"]
                  [:div "o: artigo (átono, sofre alçamento vocálico)"]
                  [:div "fez: verbo (tônico), não precisa ser diferenciado por acento gráfico"]
                  [:div "sim: advérbio (tônico), não precisa ser diferenciado por acento gráfico"]]

                 [[:div "N" [:span.is-underlined "ã"] "o hav" [:span.is-underlined "i"] "a termin" [:span.is-underlined "a"] "do a" [:span.is-underlined "i"] "nda."]

                  [:div "não: advérbio (tônico), não precisa ser diferenciado por acento gráfico"]]

                 [[:div "S" [:span.is-underlined "ê"]  "bonz" [:span.is-underlined "i"] "nho!"]

                  [:div "sê: verbo (tônico—sem alçamento), leva acento para diferenciar-se do"  [:em " se"]  " (pronome, que sofre alçamento)"]]

                 [[:div "N" [:span.is-underlined "ó"] "s tom" [:span.is-underlined "a"] "mos ch" [:span.is-underlined "á"]  "as tr" [:span.is-underlined "ê"] "s."]

                  [:div "nós: pronome nominativo (tônico—sem alçamento), leva acento para diferenciar-se do " [:em "nos"] " (pronome oblíquo, que sofre alçamento)"]
                  [:div "chá: substantivo (tônico)não sofre alçamento"]
                  [:div "as: artigo (átono)"]
                  [:div "três: (número), não sofre alçamento "]]

                 [[:div "N" [:span.is-underlined "ã"] "o imp" [:span.is-underlined "o"] "rta c" [:span.is-underlined "o"] "mo o f" [:span.is-underlined "e"] "z."]

                  [:div "não: advérbio (tônico), não precisa ser diferenciado por acento gráfico"]
                  [:div "como: palavra interrogativa"]
                  [:div "fez: verbo (tônico), não precisa ser diferenciado por acento gráfico"]]

                 [[:div "" [:span.is-underlined "I"] "sso n" [:span.is-underlined "ã"] "o se f" [:span.is-underlined "a"] "z aqu" [:span.is-underlined "i"] "."]

                  [:div "não: advérbio (tônico), não precisa ser diferenciado por acento gráfico"]
                  [:div "faz: verbo (tônico), não precisa ser diferenciado por acento gráfico"]]

                 [[:div "N" [:span.is-underlined "ã"] "o f" [:span.is-underlined "a"] "le com aqu" [:span.is-underlined "e"] "le."]

                  [:div "não: advérbio (tônico), não precisa ser diferenciado por acento gráfico"]]

                 [[:div "" [:span.is-underlined "E"] "u j" [:span.is-underlined "á"]  "lhe pagu" [:span.is-underlined "e"] "i o qu" [:span.is-underlined "a"] "nto dev" [:span.is-underlined "i"] "a."]

                  [:div "eu: pronome (tônico), não precisa ser diferenciado por acento gráfico"]
                  [:div "já: advérbio (tônico), não sofre alçamento"]
                  [:div "lhe: pronome átono, sofre alçamento"]
                  [:div "o: artigo, sofre alçamento"]]

                 [[:div "A" [:span.is-underlined "o"] "nde v" [:span.is-underlined "a"] "i?"]

                  [:div "vai: verbo (tônico), não precisa ser diferenciado por acento gráfico"]]

                 [[:div "D" [:span.is-underlined "e"] "ve traz" [:span.is-underlined "e"] "r vinte e d" [:span.is-underlined "o"] "is ou m" [:span.is-underlined "a"] "is."]

                  [:div "dois: não precisa ser diferenciado por acento gráfico"]
                  [:div "ou: (conjunção átona)"]
                  [:div "mais: advérbio (tônico), não precisa ser diferenciado por acento gráfico"]]

                 [[:div "" [:span.is-underlined "É"]  "necess" [:span.is-underlined "á"] "rio p" [:span.is-underlined "ô"] "r o ac" [:span.is-underlined "e"] "nto."]

                  [:div "é: verbo (tônico), leva acento para diferenciar-se de " [:em "e"] " (conjunção, que sofre alçamento)"]
                  [:div "pôr: verbo (tônico), leva acento para diferenciar-se de por (preposição átona)"]
                  [:div "o: artigo, sofre alçamento"]]

                 [[:div "Obrig" [:span.is-underlined "a"] "do por t" [:span.is-underlined "ê"] "-lo f" [:span.is-underlined "e"] "ito!"]

                  [:div "por: preposição (átona), diferencia-se de"[:em " pôr "] "(verbo tônico)"]
                  [:div "tê-: verbo (tônico), não sofre alçamento"]
                  [:div "-lo: pronome oblíquo átono, sofre alçamento"]]]]
      (into [:ol.list.is-lower-alpha]
            (for [[a & b] items :let [b-comp (into [:div.b-comp]
                                                   (map (fn [s] [:div.b s]) b))]]
              [:li
               [:div.grid2.no-gap.red.bordered a b-comp]])))]])

(defn render-respostas
  "Chapter 22 respostas"
  []
  [:main.c22.respostas.content
   [shared/fonetica-title "Capítulo 22 — A sílaba e a silabação"
    "Respostas para o eResource"]
   [:section.atividades
    [:h2 "ATIVIDADES"]
    [:ol.atividades.list
     [ra1]
     [ra2]]]])

(defn a1
  "Activity 1"
  []
  [:section.a1
   [:li [:div "Escute a gravação das seguintes palavras. Sublinhe as vogais tônicas de cada uma e escreva os acentos ortográficos caso sejam necessários. Explique por que o acento ortográfico é necessário ou por que não é."
         [shared/media-icon :audio (shared/file "C22-A1.mp3")]
         [shared/media-icon :r (rfe/href :fonetica-port.routes/c22-r) :same-session]]
    (let [items ["acentuo"
                 "ansiosamente"
                 "arruina"
                 "aspide"
                 "bau"
                 "cantara"
                 "carater"
                 "urubu"
                 "estuda-lo"
                 "deu"
                 "esdruxula"
                 "estudais"
                 "feliz"
                 "friamente"
                 "fui"
                 "heroico"
                 "hospede"
                 "jesuitico"
                 "rainha"
                 "lapis"
                 "periodo"
                 "proibo"
                 "pontape"
                 "viuva"
                 "senti-lo"
                 "muito"]]
      [shared/list-2x1 items])]])

(defn a2
  "Activity 2"
  []
  [:section.a2
   [:li [:div "Escute a gravação das seguintes orações. Sublinhe todas as vogais tônicas de cada uma e escreva os acentos ortográficos caso sejam necessários. Quanto às palavras monossilábicas, explique por que levam acento ortográfico, ou por que não levam."
         [shared/media-icon :audio (shared/file "C22-A2.mp3")]
         [shared/media-icon :r (rfe/href :fonetica-port.routes/c22-r) :same-session]]
    (let [items ["João o fez sim."
                 "Não havia terminado ainda."
                 "Se bonzinho!"
                 "Nos tomamos cha as tres."
                 "Não importa como o fez."
                 "Isso não se faz aqui."
                 "Não fale com aquele."
                 "Eu ja lhe paguei o quanto devia."
                 "Aonde vai?"
                 "Deve trazer vinte e dois ou mais."
                 "É necessario por o acento."
                 "Obrigado por te-lo feito!"]]
            [shared/list-2x1 items])]])


(defn atividades
  "Chapter 22 atividades"
  []
  [:section.atividades.content
   [:h2 "ATIVIDADES"]
   [:ol.atividades
    [a1]
    [a2]]])

(defn recursos
  "Materials"
  []
  [:section.recursos
   [:h3 "RECURSOS ELETRÔNICOS"]
   (let [audio-items [[:span "Variabilidade na posição de acento em inglês e português."]
                      [:span "A palavra inglesa multiplication."]
                      [:span "A Tab. 22.4."]
                      [:span "A Tab. 22.5."]
                      [:span "A Tab. 22.6."]
                      [:span "A Tab. 22.7."]
                      [:span "A Tab. 22.8."]
                      [:span "A Tab. 22.9."]
                      [:span "A Tab. 22.10."]
                      [:span "A Tab. 22.11."]
                      [:span "A Tab. 22.12."]
                      [:span "A Tab. 22.13."]
                      [:span "A Tab. 22.14."]
                      [:span "A Tab. 22.15."]
                      [:span "A Tab. 22.16."]
                      [:span "A Tab. 22.17."]
                      [:span "Variabilidade contrastiva na posição de acento em inglês."]
                      [:span "Variabilidade contrastiva na posição de acento em português. (Tab. 22.19)"]
                      [:span "Variabilidade optativa na posição de acento em inglês e português."]
                      [:span "Variabilidade posicional do acento em inglês."]
                      [:span "Acento “primário” e “secundário” em inglês."]
                      [:span "Os pronomes pessoais."]
                      [:span "O número 523.230.389.249."]
                      [:span "As palavras que, quando, como, quem, onde, aonde, por que, qual, quanto, quão, cujo."]
                      [:span "A tonicidade nos advérbios terminados em -mente."]
                      [:span "As palavras compostas."]
                      [:span "Palavras derivadas com -z-."]
                      [:span "Palavras oxítonas."]
                      [:span "Palavras paroxítonas."]
                      [:span "Palavras proparoxítonas."]
                      [:span "Palavras monossilábicas."]
                      [:span "Ditongos crescentes ou hiatos em posição final de palavra."]
                      [:span "Ditongos decrescentes ou hiatos em posição final de palavra."]
                      [:span "Ditongos ou hiatos em posição não final de palavra."]
                      [:span "Ditongos tônicos com vogais abertas em palavras oxítonas."]
                      [:span "As palavras compostas."]
                      [:span "Diferenças na forma verbal {demos} entre o português brasileiro e o europeo."]
                      [:span "Exercícios de pronúncia: a posição do acento fonético."]]
         fonetica-line-item (fn [n i]
                              [:li.list-item [shared/media-icon :audio
                                              (shared/file (str "C22-M" (inc n) ".mp3"))] i])]
     
     (into [:ol.list]
      (map-indexed fonetica-line-item audio-items)))])

(defn render
  "Chapter 22 view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 22 — A tonicidade ou acento fonético"
    "eResource"]
   [:div.content.chapter-22
    [recursos]
    [atividades]]
   [shared/footer]])
