(ns fonetica-port.views.c7
  "Chapter 7"
  (:require [fonetica-port.views.components.shared :as shared :refer [ipa red]]
            [reitit.frontend.easy :as rfe]))

(defn ra1
  "Respostas 1"
  []
  [:li 
   [:section.respostas1
    [:div "Há quatro componentes que se empregam na audição: o ouvido externo, o ouvido médio, o ouvido interno e o sistema nervoso. Identifique o componente no qual se encontram os seguintes elementos."]
    [:div.grid-center     
     [:ol.list.is-lower-alpha
      (let [left [[:li "canal auditivo " [red "ouvido externo"]]
                  [:li "membrana timpânica " [red "ouvido médio"]]
                  [:li "martelo " [red "ouvido médio"]]
                  [:li "janela oval " [red "ouvido interno"]]
                  [:li "escala timpânica " [red "ouvido interno"]]
                  [:li "nervo auditivo " [red "sistema nervoso"]]
                  [:li "estribo " [red "ouvido médio"]]
                  [:li "órgão de Córti " [red "ouvido interno"]]]
            right [[:li "orelha " [red "ouvido externo"]]
                   [:li "membrana basilar " [red "ouvido interno"]]
                   [:li "área de Wernicke " [red "ouvido interno"]]
                   [:li "fibras ciliadas " [red "ouvido interno"]]
                   [:li "bigorna " [red "ouvido médio"]]
                   [:li "membrana tectória " [red "ouvido interno"]]
                   [:li "cortiça auditiva central " [red "sistema nervoso"]]
                   [:li "escala média " [red "ouvido interno"]]]]
        [:div.grid2
         (into [:div.l] left)
         (into [:div.r] right)])]]]])

(defn ra2
  "Respostas 2"
  []
  [:li 
   [:section.respostas2
    [:div "Complete a seguinte tabela referente aos componentes do processo da audição."]
    (let [rows
          [["o ouvido externo"
            "conduzir a onda sonora à membrana timpânica e amplificá-la"
            "transmissão acústica através do ar"]
           ["o ouvido médio"
            "amplificar a energia do som"
            "transmissão mecânica através da cadeia ossicular"]
           ["o ouvido interno""dividir a onda complexa para reconhecer em que frequências encontra-se a energia"
            "transmissão hidráulica através do líquido da cóclea"]
           ["o sistema nervoso"
            "transmitir o espectro acústico do som para o cérebro"
            "transmissão neural através do nervo auditivo"]]]
      [:table.table.ftable.is-bordered
       [:thead [:tr
                [:th "Componente"]
                [:th "Qual é sua função?"]
                [:th "Como a energia é transmitida?"]]]
       (into [:tbody]
             (for [[a b c] rows]
               [:tr [:td a] [:td.red b] [:td.red c]]))])]])

(defn ra3
  "Respostas 3"
  []
  [:section.respostas3
   [:li 
    [:div "Complete a seguinte tabela sobre o processo da percepção:"]
    (let [headers [
                   "Etapa"
                   "Processo"
                   "Produto linguístico"
                   "Número e tipo de elementos"
                   "Exemplo"]
          rows
          [["Segmentação"
            "Dividir uma cadeia fluída de impulsos em segmentos"
            "Segmentos discretos a serem identificados"
            "Número infinito de sons"
            [:span "A palavra {nave}" [shared/media-icon :audio (shared/file "C7-R1.mp3")]
             " contém quatro segmentos"]]
           ["Identificação"
            "Correlacionar o segmento com um som ou alofone"
            "Uma sequência de sons identificados"
            "Número finito de alofones"
            [:span "A palavra {nave} é uma sequência de quatro alofones: " [ipa "[nÒvi]"] ""]]
           ["Sistematização" 
            "Correlacionar o alofone com seu fonema"
            "Uma sequência de fonemas"
            "Número finito e reduzido de fonemas"
            [:span "A palavra {nave} é uma sequência de quatro fonemas: " [ipa "/nÒve/"]]]]]
      
      [:table.table.ftable.is-bordered
       [:thead (into [:tr]
                     (for [h headers] [:th h]))]
       (into [:tbody]
             (for [[a b c d e] rows]
               [:tr [:td a] [:td.red b] [:td.red c] [:td.red d] [:td.red e]]))])]])

(defn render-respostas
  "Chapter 7 respostas"
  []
  [:main.c7.respostas.container.content
   [shared/fonetica-title "Capítulo 7 — A fonética auditiva"
    "Respostas para o eResource"]
   [:ol.list
    [ra1]
    [ra2]
    [ra3]]])

(defn a1
  "Atividade 1"
  []
  [:li 
   [:section.atividade1
    [:div "Há quatro componentes que se empregam na audição: o ouvido externo, o ouvido médio, o ouvido interno e o sistema nervoso. Identifique o componente no qual se encontram os seguintes elementos."
     [shared/media-icon :r (rfe/href :fonetica-port.routes/c7-r) :same-session]]
    [:div.grid-center     
     [:ol.list.is-lower-alpha
      (let [left [[:li "canal auditivo " ]
                  [:li "membrana timpânica " ]
                  [:li "martelo " ]
                  [:li "janela oval " ]
                  [:li "escala timpânica " ]
                  [:li "nervo auditivo " ]
                  [:li "estribo " ]
                  [:li "órgão de Córti " ]]
            right [[:li "orelha " ]
                   [:li "membrana basilar " ]
                   [:li "área de Wernicke " ]
                   [:li "fibras ciliadas " ]
                   [:li "bigorna " ]
                   [:li "membrana tectória " ]
                   [:li "cortiça auditiva central " ]
                   [:li "escala média " ]]]
        [:div.grid2
         (into [:div.l] left)
         (into [:div.r] right)])]]]])

(defn a2
  "Atividade 2"
  []
  [:li 
   [:section.atividade2
    [:div "Complete a seguinte tabela referente aos componentes do processo da audição."
     [shared/media-icon :r (rfe/href :fonetica-port.routes/c7-r) :same-session]]
    (let [rows
          [["o ouvido externo"
            "conduzir a onda sonora à membrana timpânica e amplificá-la"
            "transmissão acústica através do ar"]
           ["o ouvido médio"
            "amplificar a energia do som"
            "transmissão mecânica através da cadeia ossicular"]
           ["o ouvido interno""dividir a onda complexa para reconhecer em que frequências encontra-se a energia"
            "transmissão hidráulica através do líquido da cóclea"]
           ["o sistema nervoso"
            "transmitir o espectro acústico do som para o cérebro"
            "transmissão neural através do nervo auditivo"]]]
      [:table.table.ftable.is-bordered
       [:thead [:tr
                [:th "Componente"]
                [:th "Qual é sua função?"]
                [:th "Como a energia é transmitida?"]]]
       (into [:tbody]
             (for [[a _b _c] rows]
               [:tr [:td a] [:td] [:td]]))])]])

(defn a3
  "Atividade 3"
  []
  [:section.atividade3
   [:li 
    [:div "Complete a seguinte tabela sobre o processo da percepção:"
     [shared/media-icon :r (rfe/href :fonetica-port.routes/c7-r) :same-session]]
    (let [headers [
                   "Etapa"
                   "Processo"
                   "Produto linguístico"
                   "Número e tipo de elementos"
                   "Exemplo"]
          rows
          [["Segmentação"
            "Dividir uma cadeia fluída de impulsos em segmentos"
            "Segmentos discretos a serem identificados"
            "Número infinito de sons"
            [:span "A palavra {nave}" [shared/media-icon :audio (shared/file "C7-R1.mp3")]
             " contém quatro segmentos"]]
           ["Identificação"
            "Correlacionar o segmento com um som ou alofone"
            "Uma sequência de sons identificados"
            "Número finito de alofones"
            [:span "A palavra {nave} é uma sequência de quatro alofones: " [ipa "[nÒvi]"] ""]]
           ["Sistematização" 
            "Correlacionar o alofone com seu fonema"
            "Uma sequência de fonemas"
            "Número finito e reduzido de fonemas"
            [:span "A palavra {nave} é uma sequência de quatro fonemas: " [ipa "/nÒve/"]]]]]
      
      [:table.table.ftable.is-bordered
       [:thead (into [:tr]
                     (for [h headers] [:th h]))]
       (into [:tbody]
             (for [[a _b _c _d _e] rows]
               [:tr [:td a] [:td] [:td] [:td] [:td]]))])]])


(defn atividades
  "Chapter 7  atividades"
  []
   [:section.atividades.content
    [:h2 "ATIVIDADES"]
    [:ol.list
     [a1]
     [a2]
     [a3]]])

(defn render
  "Chapter seven view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 7 — A fonética auditiva"
    "eResource"]
   [:div.content.chapter-7
    [atividades]]
   
   [shared/footer]])
