(ns fonetica-port.views.c24
  "Chapter 24"
  (:require [fonetica-port.views.components.shared :as shared :refer [ipa fempr red ipa-like]]
            [reitit.frontend.easy :as rfe]))

(defn ra1
  "Respostas activity 1"
  []
  (let [activities [["{veio à justiça" [:span.ipa2.red "ô \\"] " ele pôs-se a contar o sucedido" [:span.red [:span.ipa2 "ð "] "|"] " com o compadre pobre" [:span.ipa2.red "ð /"] " e levaram este preso" [:span.ipa2.red "ô \\"] "no caminho" [:span.ipa2.red "ò /"] " os guardas quiseram descansar" [:span.ipa2.red "ô /"] " amarraram o pobre a uma árvore" [:span.ipa2.red "ð /"] " e deitaram-se a dormir a sesta" [:span.ipa2.red "ô \\"] " passou um pastor" [:span.red [:span.ipa2 " ð "] "|"] " com uns carneiros" [:span.ipa2.red "ð /"] " e perguntou-lhe o que era" [:span.ipa2.red "ô"] "}"]

                    ["{ora o irmão" [:span.red [:span.ipa2 "ð "] "|"] " que estava preso" [:span.ipa2.red "ô /"] " tinha um cão" [:span.ipa2.red "ð /"] " e um gato" [:span.ipa2.red "ô /"] " e estes" [:span.ipa2.red "ò /"] " logo que souberam que seu dono estava na cadeia" [:span.ipa2.red "ð /"] " trataram de lá ir ter com ele" [:span.ipa2.red "ô \\"] " uma vez chegados" [:span.ipa2.red "ò /"] " tomaram conhecimento de que o conde" [:span.ipa2.red "ò /"] " irmão do seu dono" [:span.ipa2.red "ò /"] " lhe tinha roubado a caixa" [:span.ipa2.red "ô /"] " e cuidaram ambos" [:span.ipa2.red "ò /"] " de ir ao palácio dele" [:span.ipa2.red "ð /"] " para a trazer" [:span.ipa2.red "ô \\"] " para esse fim" [:span.ipa2.red "ò /"] " fizeram um batel de casca de abóbora" [:span.ipa2.red "ô /"] " pois tinham de atravessar o mar" [:span.ipa2.red "ô"]"}"]


                    ["{havia fora da cidade" [:span.ipa2.red "ò /"] " uma quadrilha de ladrões" [:span.ipa2.red "ô /"] " e o capitão deles" [:span.ipa2.red "ð /"] " andava à espera da ocasião da partida do mercador" [:span.ipa2.red "ô \\"] "assim que soube o dia em que ele saiu da cidade" [:span.ipa2.red "ò /"] " vestiu-se com trajes de mendigo" [:span.ipa2.red "ô /"] " e ao anoitecer" [:span.ipa2.red "ð /"] " estava toda a sua quadrilha" [:span.ipa2.red "ð /"] " no canto da rua onde moravam as três meninas" [:span.ipa2.red "ô"] "}"]


                    ["{mas no bar" [:span.ipa2.red "ð /"] " já se sabia da entrevista mal sucedida" [:span.ipa2.red "ô /"] " da cólera de ramiro" [:span.ipa2.red "ô \\"] " exageravam-se os fatos" [:span.ipa2.red "ô /"] " que houvera bate-boca" [:span.red [:span.ipa2 "ð "] "|"] " violento" [:span.ipa2.red "ô /"] " que o velho político expulsara altino" [:span.red [:span.ipa2 "ð "] "|"] " de sua casa" [:span.ipa2.red "ô /"] " que este fora mandado por mundinho" [:span.red [:span.ipa2 "ð "] "|"] " propor acordos" [:span.ipa2.red "ô /"] " pedir trégua" [:span.red [:span.ipa2 "ð "] "|"] " e clemência"[:span.ipa2.red "ô"]"}"]

                    ["{andava o genro do rei" [:span.ipa2.red "ð /"] " visitando as suas tropas" [:span.ipa2.red "ô /"] " quando viu o seu desalmado companheiro" [:span.ipa2.red "ð /"] " alistado em um dos batalhões do reino" [:span.ipa2.red "ô \\"] " mandou-o ir ao palácio" [:span.ipa2.red "ð /"] " e deu-se a conhecer" [:span.ipa2.red "ô \\"] " ficou o malvado aflito" [:span.ipa2.red "ô /"] " mas o príncipe disse-lhe" [:span.ipa2.red "ô /"] " que não lhe tencionava fazer mal algum" [:span.ipa2.red "ð /"] " apesar da infâmia que ele praticara" [:span.ipa2.red "ô"]"}"]


                    ["{o cavaleiro recebeu o vidro" [:span.ipa2.red "ð /"] " mandou avançar o leão" [:span.ipa2.red "ð /"] " e matou a velha" [:span.ipa2.red "ô \\"] " depois desencantou todos os que estavam na torre" [:span.ipa2.red "ô \\"] " o irmão" [:span.ipa2.red "ò /"] " porém" [:span.ipa2.red "ò /"] " apenas soube que a mulher" [:span.ipa2.red "ò /"] " por engano" [:span.ipa2.red "ò /"] " havia quebrado os laços conjugais" [:span.ipa2.red "ô/"] " assassinou o seu salvador" [:span.ipa2.red "ô"]"}"]


                    ["{no dia seguinte" [:span.ipa2.red "ò /"] " foi ele ao curral" [:span.ipa2.red "ò /"] " tirou os olhos a cinco cabras" [:span.ipa2.red "ð /"] " guardou-os no bolso" [:span.ipa2.red "ð /"] " e foi visitar a noiva" [:span.ipa2.red "ô \\"] " mal a viu" [:span.ipa2.red "ò /"] " atirou-lhe com os olhos das cabras" [:span.ipa2.red "ð /"] " e passou-lhe pela cara as mãos cheias de sangue" [:span.ipa2.red "ô \\"] " a rapariga indignou-se" [:span.ipa2.red "ô /"] " repreendeu-o asperamente" [:span.ipa2.red "ð /"] " e pô-lo" [:span.red [:span.ipa2 "ð "] "|"] " fora de casa" [:span.ipa2.red "ô"]"}"]


                    ["{a rainha" [:span.ipa2.red "ò /"] " sob a impressão das injúrias do marido " [:span.ipa2.red "ò /"] " feriu o braço" [:span.red [:span.ipa2 "ð "] "|"] " com um alfinete" [:span.ipa2.red "ô /"] " e nas três pingas de sangue" [:span.ipa2.red "ô /"] " molhou a canetaò" [:span.ipa2.red "ð /"] " e com ela escreveu o seu nome" [:span.ipa2.red "ð /"] " num papel que o cavaleiro lhe apresentou" [:span.ipa2.red "ô \\"] " em seguida" [:span.ipa2.red "ò /"] " o cavaleiro guardou o documento" [:span.ipa2.red "ð /"] " e desapareceu" [:span.ipa2.red "ô"]"}"]]]
    [:section.ra1
     [:div.respostas.activities
      (into [:ol.activity-list]
            (for [[n a] (map-indexed vector activities)]
              [:li (into [:span] a)
               [shared/media-icon :audio
                (shared/file (str "C24-A" (inc n) ".mp3"))]]))]]
    ))

(defn render-respostas
  "Chapter 24 respostas"
  []
  [:main.c24.respostas.content
   [shared/fonetica-title "Capítulo 24 — A entonação"
    "Respostas para o eResource"]
   [:section.atividades
    [:h2 "ATIVIDADES"]
    [:ol.atividades.list
     [ra1]]]])

(defn a1
  "Activities"
  []
  (let [preamble [:div "Escute as seguintes gravações e indique as pausas entre os grupos fônicos e os tonemas
  correspondentes."
                  [:em "(Contos Populares Portugueses: Antologia"]
                  ", Ed. Francisco Lyon de Castro (Publicações Europa- América Lda, 40ª ed., s/d)."
                  [shared/media-icon :r (rfe/href :fonetica-port.routes/c24-r) :same-session]]
        activities ["{veio à justiça ele pôs-se a contar o sucedido com o compadre pobre e levaram este preso no caminho os guardas quiseram descansar amarraram o pobre a uma árvore e deitaram-se a dormir a sesta passou um pastor com uns carneiros e perguntou-lhe o que era}"
     "{ora o irmão que estava preso tinha um cão e um gato e estes logo que souberam que seu dono estava na cadeia trataram de lá ir ter com ele uma vez chegados tomaram conhecimento de que o conde irmão do seu dono lhe tinha roubado a caixa e cuidaram ambos de ir ao palácio dele para a trazer para esse fim fizeram um batel de casca de abóbora pois tinham de atravessar o mar}"
     "{havia fora da cidade uma quadrilha de ladrões e o capitão deles andava à espera da ocasião
  da partida do mercador assim que soube o dia em que ele saiu da cidade vestiu-se com trajes
  de mendigo e ao anoitecer estava toda a sua quadrilha no canto da rua onde moravam as três
  meninas}"
     "{mas no bar já se sabia da entrevista mal sucedida da cólera de ramiro exageravam-se os fatos que houvera bate-boca violento que o velho político expulsara altino de sua casa que este fora mandado por mundinho propor acordos pedir trégua e clemência}"
     "{andava o genro do rei visitando as suas tropas quando viu o seu desalmado companheiro alistado em um dos batalhões do reino mandou-o ir ao palácio e deu-se a conhecer ficou o malvado aflito mas o príncipe disse-lhe que não lhe tencionava fazer mal algum apesar da infâmia que ele praticara}"
     "{o cavaleiro recebeu o vidro mandou avançar o leão e matou a velha depois desencantou todos os que estavam na torre o irmão porém apenas soube que a mulher por engano havia quebrado os laços conjugais assassinou o seu salvador}"
     "{no dia seguinte foi ele ao curral tirou os olhos a cinco cabras guardou-os no bolso e foi visitar a noiva mal a viu atirou-lhe com os olhos das cabras e passou-lhe pela cara as mãos cheias de sangue a rapariga indignou-se repreendeu-o asperamente e pô-lo fora de casa}"
     "{a rainha sob a impressão das injúrias do marido feriu o braço com um alfinete e nas três pingas de sangue molhou a caneta e com ela escreveu o seu nome num papel que o cavaleiro lhe apresentou em seguida o cavaleiro guardou o documento e desapareceu}"]  ]
    [:section.a1
     preamble
     [:div.activities
      (into [:ol.activity-list]
            (for [[n a] (map-indexed vector activities)]
              [:li a [shared/media-icon :audio
                      (shared/file (str "C24-A" (inc n) ".mp3"))]]))]]))

(defn atividades
  "Chapter 24 atividades"
  []
  [:section.atividades.content
   [:h2 "ATIVIDADES"]
   [:ol.atividades
    [a1]]])

(defn recursos
  "Materials"
  []
  [:section.recursos
   [:h3 "RECURSOS ELETRÔNICOS"]
   (let [audio-items ["Frases declarativas e interrogativas."
                      "O sirrema é indivisível."
                      "O grupo fônico."
                      "Grupos fônicos com pausa final."
                      "Grupos fônicos enumerativos"
                      "Grupos fônicos explicativos."
                      "Grupos fônicos com modificadores adverbiais antepostos."
                      "Contraste entre orações explicativas e restritivas."
                      "Contraste entre elementos explicativos e restritivos."
                      "Frases que demonstram a propriedade associativa linguística."
                      "Grupos fônicos optativos."
                      "Grupos fônicos assistemáticos."
                      "Frases em que a entonação altera o significado."
                      "Frases exemplares para a transcrição da entonação."
                      "Frases exemplares para os tonemas do português."
                      "Frases exemplares para os tonemas do inglês."
                      "A entonação das frases declarativas de um só grupo fônico."
                      "A entonação das frases declarativas de dois grupos fônicos."
                      "A entonação das frases declarativas de três grupos fônicos ou mais."
                      "Séries com conjunções e sem elas."
                      "Orações e sintagmas explicativos e casos de hipérbato."
                      "Modificadores adverbiais de oração."
                      "Locuções e sintagmas adversativos."
                      "Comentários parentéticos."
                      "Frases declarativas no discurso direto e indireto."
                      "Vocativos nas declarativas."
                      "Perguntas fechadas."
                      "Perguntas fechadas com resposta implícita."
                      "Perguntas com palavras interrogativas."
                      "Perguntas com palavras interrogativas com cortesia."
                      "Perguntas repetidas."
                      "Perguntas disjuntivas."
                      "Perguntas de confirmação simples."
                      "Perguntas de confirmação com repetição do verbo."
                      "Perguntas indiretas."
                      "Vocativos nas interrogativas."
                      "Perguntas com valor afetivo"
                      "Perguntas com mais de um grupo fônico."
                      "Frases imperativas"
                      "Vocativos nas imperativas."
                      "Expressões exclamativas."
                      "Exemplos de entonação dialetal."]
         ex-items ["Exercícios de pronúncia: entonação."
                   "⟝ Exercício 1"
                   "⟝ Exercício 2"
                   "⟝ Exercício 3"
                   "⟝ Exercício 4"
                   "⟝ Exercício 5"
                   "∟ Exercício 6"] ;; TODO issue here because there are five audio of them
         
         fonetica-line-item (fn [n i]
                              [:li.list-item [shared/media-icon :audio
                                              (shared/file (str "C24-M" (inc n) ".mp3"))] i])
         fonetica-line-item2 (fn [n i]
                               (let [ic (-> audio-items count inc) ]
                                 [:li.list-item.exercises [shared/media-icon :ex
                                                 (shared/file (str "C24-M43-" (inc n) ".mp3"))] i]))]
     [:div.list-items 
      (into [:ol.list]
            (cons (map-indexed fonetica-line-item audio-items)
                  [[:li (first ex-items)]]
                  ))
      (into [:ul.list.ex-items]
            (map-indexed fonetica-line-item2 (rest ex-items))
            )])])

(defn render
  "Chapter 24 view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 24 — A entonação"
    "eResource"]
   [:div.content.chapter-24
    [recursos]
    [atividades]]
   [shared/footer]])
