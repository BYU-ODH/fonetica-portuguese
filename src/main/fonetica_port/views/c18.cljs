(ns fonetica-port.views.c18
  "Chapter 18"
  (:require [fonetica-port.views.components.shared :as shared :refer [ipa femp red ipa-like]]
            [reitit.frontend.easy :as rfe]))

(defn ra1
  "Respuestas activity 1"
  []
  [:section.ra1
   [:li
    "Escreva o símbolo do alofone descrito pelos seguintes traços fonéticos."

    (let [items [["nasal"
                  "velar"
                  "sonoro"
                  "[N]"]

                 ["nasal"
                  "dental"
                  "sonoro"
                  "[ª]"]

                 ["nasal"
                  "alveolar"
                  "sonoro"
                  "[n]"]

                 ["nasal"
                  "bilabial"
                  "sonoro"
                  "[m]"]

                 ["nasal"
                  "labiodental"
                  "sonoro"
                  "[M]"]

                 ["nasal"
                  "palatal"
                  "sonoro"
                  "[¦]"]

                 ["nasal"
                  "palatalizada"
                  "sonoro"
                  "[ø]"]]]
      [shared/list-2x2x3-1 items])]])

(defn ra2
  "Respuestas activity 2"
  []
  [:section.ra2
   [:li
    "Indique os traços fonéticos de cada alofone da lista."
    (let [items [["[ø]"
                  "nasal"
                  "palatalizado"
                  "sonoro"]

                 ["[M]"
                  "nasal"
                  "labiodental"
                  "sonoro"]

                 ["[m]"
                  "nasal"
                  "bilabial"
                  "sonoro"]

                 ["[n]"
                  "nasal"
                  "alveolar"
                  "sonoro"]

                 ["[ª]"
                  "nasal"
                  "dental"
                  "sonoro"]

                 ["[¦]"
                  "nasal"
                  "palatal"
                  "sonoro"]

                 ["[N]"
                  "nasal"
                  "velar"
                  "sonoro"]]]
      (shared/list-2x2x1-3 items))]])

(defn ra3
  "Respostas activity 3"
  []
  [:section.ra3
   [:li "Transcreva as seguintes palavras ou grupos de palavras na norma culta de São Paulo e na norma culta de Lisboa."
    (let [headers [[:span.is-underlined "norma culta de São Paulo"] [:span.is-underlined "norma culta de Lisboa"]]
          items [["ando"
                  [:span "[ë" [:sup "ª"] "du]"]
                  [:span  "[ë" [:sup "ª"] "du]"]]
                 ["ânsia" "[ësjA]" "[ësjA]"]
                 ["campo" [:span "[kë" [:sup "m"] "pu]"] [:span "[kë" [:sup "m"] "pu]"]]
                 ["cem milhas" "[sçãmÔYAs]" "[sçãmÔYAs]"]
                 ["entre" [:span "[ç" [:sup "ª"] "tRi]"] [:span "[ç" [:sup "ª"] "tR" [:sup "š"] "]"]]
                 ["gente" [:span "[Zç" [:sup "ø"] "Îi]"] [:span "[Zç" [:sup "ª"] "t" [:sup "š"] "]"]]
                 ["homem" "[émÝã]" "[omÝã]"]
                 ["informe" [:span "[Þ" [:sup "M"] "fØRmi]"] [:span "[Þ" [:sup "M"] "fØRm" [:sup "š"] "]"]]
                 ["ingresso" [:span "[Þ" [:sup "N"] "gR×su]"] [:span "[Þ" [:sup "N"] "gR×su]"]]
                 ["leões" "[leéãs]" "[leéãS]"]
                 ["mãe" "[mëã]" "[mëã]"]
                 ["manga" [:span "[më" [:sup "N"] "gA]"] [:span "[më" [:sup "N"] "gA]"]]
                 ["manto" [:span "[më" [:sup "ª"] "tu]"] [:span "[më" [:sup "ª"] "tu]"]]
                 ["mão" "[mëä]" "[mëä]"]
                 ["muito" "[mêãtu]" "[mêãtu]"]
                 ["o macaco" "[umakÒku]" "[umAkÒku]"]
                 ["ontem" [:span "[é" [:sup "ª"] "tÝã]"] [:span "[é" [:sup "ª"] "tÝã]"]]
                 ["um macaco" "[à:m:akÒku]" "[à:m:AkÒku]"]
                 ["um voto" [:span "[à" [:sup "M"] "vØtu]"] [:span "[à" [:sup "M"] "vØtu]"]]
                 ["ungir" "[àZÔR]" "[àZÔ³]"]
                 ["unhão" "[u¦ëä]" "[u¦ëä]"]
                 ["união" "[uniëä]" "[unjëä]"]
                 ["uniforme" "[unifØRmi]" [:span "[unifØRm" [:sup "š"] "]"]]
                 ["vinha" "[vè¦A]" "[vÔ¦A]"]
                 ["voltam" "[vØKtáä]" "[vØLtáä]"]
                 ["zumbir" [:span "[zà" [:sup "m"] "bÔR]"] [:span "[zà" [:sup "m"] "bÔ³]"]]]]
      (shared/list-3x1 headers items))]])

(defn ra4
  "Respostas activity 4"
  []
  [:section.ra4
   [:li "Indique se os grafemas {m} ou {n} correspondem aos fonemas /n/ ou /m/, ou ao arquifonema /N/ nas seguintes palavras." ;; ipa-like
    (let [items [
                 ["alonga" "arquifonema /N/"]
                 ["ama" "fonema /m/"]
                 ["amplo" "arquifonema /N/"]
                 ["anjo" "arquifonema /N/"]
                 ["ano" "fonema /n/"]
                 ["come" "fonema /m/"]
                 ["em" "arquifonema /N/"]
                 ["emprego" "arquifonema /N/"]
                 ["falam" "arquifonema /N/"]
                 ["fundo" "arquifonema /N/"]
                 ["funil" "fonema /n/"]
                 ["lona" "fonema /n/"]]]
      (into [:ol.list.is-lower-alpha
             [:div.grid3]]
            (for [[a b] items]
              [:li.list-item [:div.grid3
                              [:span a]
                              [:span.red b]]])))]])

(defn ra5
  "Respostas activity 5"
  []
  [:section.ra5
   [:li "Indique se a vogal indicada é oral, nasalizada ou oronasal nas seguintes palavras ou grupos de palavras. Depois, indique se há uma consoante nasal de transição ou não."
    (let [items [
                 [[:span "c" [:strong "a"] "nso"] "oronasal" "não"]
                 [[:span "c" [:strong "o"] "ncha"] "oronasal" "não"]
                 [[:span "f" [:strong "u"] "mo"] "nasalizadad"]
                 [[:span [:strong "í"] "n" [:strong "t"] "imo"] "oronasal/oral" "sim/não"]
                 [[:span "l" [:strong "í"] "ngua"] "oronasal" "sim"]
                 [[:span "sarg" [:strong "e"] "nto"] "oronasal" "sim"]
                 [[:span [:strong "u"] "m barco"] "oronasal" "sim"]
                 [[:span [:strong "u"] "m fato"] "oronasal" "sim"]
                 [[:span [:strong "u"] "m jato"] "oronasal" "não"]
                 [[:span [:strong "u"] "m sapo"] "oronasal" "não"]
                 [[:span [:strong "u"] "nir"] "oral"]
                 [[:span "vag" [:strong "e"] "m"] "ditongo oronasal"]]]
      (into [:ol.list.is-lower-alpha
             [:div.grid3 [:span.blank]
              [:span.is-underlined "qualidade vocálica"] [:span.is-underlined "consoante de transição"]]]
            (for [[a b c] items]
              [:li.list-item [:div.grid3
                              [:span a]
                              [:span.ipa.red b]
                              [:span.ipa.red c]]])))]])

(comment "actividad5"
         ["canso" "oronasal" "não"]
                 ["concha" "oronasal" "não"]
                 ["fumo" "nasalizadad"]
                 ["íntimo" "oronasal/oral" "sim/não"]
                 ["língua" "oronasal" "sim"]
                 ["sargento" "oronasal" "sim"]
                 ["um barco" "oronasal" "sim"]
                 ["um fato" "oronasal" "sim"]
                 ["um jato" "oronasal" "não"]
                 ["um sapo" "oronasal" "não"]
                 ["unir" "oral"]
                 ["vagem" "ditongo oronasal"])
(defn render-respostas
  "Chapter 18 respostas"
  []
  [:main.c18.respostas.content
   [shared/fonetica-title "Capítulo 18 — Os fonemas nasais"
    "Respostas para o eResource"]
   [:section.atividades
    [:h2 "ATIVIDADES"]
    [:ol.atividades.list
     [ra1]
     [ra2]
     [ra3]
     [ra4]
     [ra5]]]])

(defn a1
  "Respuestas activity 1"
  []
  [:section.a1
   [:li
    "Escreva o símbolo do alofone descrito pelos seguintes traços fonéticos."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c18-r) :same-session]
    
    (let [items [["nasal"
                  "velar"
                  "sonoro"
                  "[N]"]

                 ["nasal"
                  "dental"
                  "sonoro"
                  "[ª]"]

                 ["nasal"
                  "alveolar"
                  "sonoro"
                  "[n]"]

                 ["nasal"
                  "bilabial"
                  "sonoro"
                  "[m]"]

                 ["nasal"
                  "labiodental"
                  "sonoro"
                  "[M]"]

                 ["nasal"
                  "palatal"
                  "sonoro"
                  "[¦]"]

                 ["nasal"
                  "palatalizada"
                  "sonoro"
                  "[ø]"]]
          item-count (count items)
          half-count (Math/ceil (* 0.5 item-count))
          [litems ritems] (partition half-count half-count nil items)
          make-twocol (fn  [[l1 l2 l3 _r  :as items]]
                        (if-not items
                          [:div.empty]
                          [:li
                           [:div.grid2
                            [:div.l
                             [:div l1]
                             [:div l2]
                             [:div l3]]
                            [:div.r.red.ipa.is-size-4 nil]]]))]
      [:ol.list.is-lower-alpha
       [:div.grid2
        (into [:div.l] (map make-twocol litems))
        (into [:div.r] (map make-twocol ritems))]])]])

(defn a2
  "Respuestas activity 2"
  []
  [:section.a2
   [:li
    "Indique os traços fonéticos de cada alofone da lista."
    (let [items [["[ø]"
                  "nasal"
                  "palatalizado"
                  "sonoro"]

                 ["[M]"
                  "nasal"
                  "labiodental"
                  "sonoro"]

                 ["[m]"
                  "nasal"
                  "bilabial"
                  "sonoro"]

                 ["[n]"
                  "nasal"
                  "alveolar"
                  "sonoro"]

                 ["[ª]"
                  "nasal"
                  "dental"
                  "sonoro"]

                 ["[¦]"
                  "nasal"
                  "palatal"
                  "sonoro"]

                 ["[N]"
                  "nasal"
                  "velar"
                  "sonoro"]]]
      (shared/list-2x2x1-3 items :no-answers))]])

(defn a3
  "Respostas activity 3"
  []
  [:section.a3
   [:li "Transcreva as seguintes palavras ou grupos de palavras na norma culta de São Paulo e na norma culta de Lisboa."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c18-r) :same-session]
    (let [headers [[:span.is-underlined "norma culta de São Paulo"]
                   [:span.is-underlined "norma culta de Lisboa"]]
          items [["ando" "[ëªdu]" "[ëªdu]"]
                 ["ânsia" "[ësjA]" "[ësjA]"]
                 ["campo" "[këmpu]" "[këmpu]"]
                 ["cem milhas" "[sçãmÔYAs]" "[sçãmÔYAs]"]
                 ["entre" [:span "[ç" [:sup "ª"] "tRi]"] [:span "[ç" [:sup "ª"] "tR" [:sup "š"] "]"]]
                 ["gente" [:span "[Zç" [:sup "ø"] "Îi]"] [:span "[Zç" [:sup "ª"] "t" [:sup "š"] "]"]]
                 ["homem" "[émÝã]" "[omÝã]"]
                 ["informe" [:span "[Þ" [:sup "M"] "fØRmi]"] [:span "[Þ" [:sup "M"] "fØRm" [:sup "š"] "]"]]
                 ["ingresso" "[ÞNgR×su]" "[ÞNgR×su]"]
                 ["leões" "[leéãs]" "[leéãS]"]
                 ["mãe" "[mëã]" "[mëã]"]
                 ["manga" [:span "[më" [:sup "N"] "gA]"] [:span "[më" [:sup "N"] "gA]"]]
                 ["manto" [:span "[më" [:sup "ª"] "tu]"] [:span "[më" [:sup "ª"] "tu]"]]
                 ["mão" "[mëä]" "[mëä]"]
                 ["muito" "[mêãtu]" "[mêãtu]"]
                 ["o macaco" "[umakÒku]" "[umAkÒku]"]
                 ["ontem" "[éªtÝã]" "[éªtÝã]"]
                 ["um macaco" "[à:m:akÒku]" "[à:m:AkÒku]"]
                 ["um voto" "[àMvØtu]" "[àMvØtu]"]
                 ["ungir" "[àZÔR]" "[àZÔ³]"]
                 ["unhão" "[u¦ëä]" "[u¦ëä]"]
                 ["união" "[uniëä]" "[unjëä]"]
                 ["uniforme" "[unifØRmi]" [:span "[unifØRm" [:sup "š"] "]"]]
                 ["vinha" "[vè¦A]" "[vÔ¦A]"]
                 ["voltam" "[vØKtáä]" "[vØLtáä]"]
                 ["zumbir" [:span "[zà" [:sup "m"] "bÔR]"] [:span "[zà" [:sup "m"] "bÔ³]"]]]]
      (shared/list-3x1 headers items :no-answers))]])

(defn a4
  "Respostas activity 4"
  []
  [:section.a4
   [:li "Indique se os grafemas {m} ou {n} correspondem aos fonemas /n/ ou /m/, ou ao arquifonema /N/ nas seguintes palavras." ;; ipa-like
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c18-r) :same-session]
    (let [items [
                 ["alonga" "arquifonema /N/"]
                 ["ama" "fonema /m/"]
                 ["amplo" "arquifonema /N/"]
                 ["anjo" "arquifonema /N/"]
                 ["ano" "fonema /n/"]
                 ["come" "fonema /m/"]
                 ["em" "arquifonema /N/"]
                 ["emprego" "arquifonema /N/"]
                 ["falam" "arquifonema /N/"]
                 ["fundo" "arquifonema /N/"]
                 ["funil" "fonema /n/"]
                 ["lona" "fonema /n/"]]]
      (into [:ol.list.is-lower-alpha]
            (for [[a _b] items]
              [:li.list-item [:div.grid2
                              [:span a]
                              [:span.blank]]])))]])

(defn a5
  "Respostas activity 5"
  []
  [:section.a5
   [:li "Indique se a vogal indicada é oral, nasalizada ou oronasal nas seguintes palavras ou grupos de palavras. Depois, indique se há uma consoante nasal de transição ou não."
    (let [headers [[:span.is-underlined "qualidade vocálica"] [:span.is-underlined "consoante de transição"]]
          items [[[:span "c" [:strong "a"] "nso"] "oronasal" "não"]
                 [[:span "c" [:strong "o"] "ncha"] "oronasal" "não"]
                 [[:span "f" [:strong "u"] "mo"] "nasalizadad"]
                 [[:span [:strong "í"] "n" [:strong "t"] "imo"] "oronasal/oral" "sim/não"]
                 [[:span "l" [:strong "í"] "ngua"] "oronasal" "sim"]
                 [[:span "sarg" [:strong "e"] "nto"] "oronasal" "sim"]
                 [[:span [:strong "u"] "m barco"] "oronasal" "sim"]
                 [[:span [:strong "u"] "m fato"] "oronasal" "sim"]
                 [[:span [:strong "u"] "m jato"] "oronasal" "não"]
                 [[:span [:strong "u"] "m sapo"] "oronasal" "não"]
                 [[:span [:strong "u"] "nir"] "oral"]
                 [[:span "vag" [:strong "e"] "m"] "ditongo oronasal"]]]
      [shared/list-3x1 headers items :no-answers])]])

(defn atividades
  "Chapter 18 atividades"
  []
  [:section.atividades.content
   [:h2 "ATIVIDADES"]
   [:ol.atividades
    [a1]
    [a2]
    [a3]
    [a4]
    [a5]]])

(defn recursos
  "Materials"
  []
  [:section.recursos
   [:h3 "RECURSOS ELETRÔNICOS"]
   (let [audio-items [[:span "Os três fonemas nasais " [ipa "/m n ¦/"] "."]
                      [:span "realização de" [ipa "[këmpu]"] " e" [ipa "[këªtu]"] "."]
                      [:span "realização de" [ipa "[àmbÒRku]"] " e" [ipa "[àªdÓdu]"] "."]
                      [:span "realização de" [ipa "[mÒpA]"] " e" [ipa "[këmA]"] "."]
                      [:span "realização de" [ipa "[këmA]"] "."]
                      [:span "realização de" [ipa "[kënA]"] "."]
                      [:span "realização de" [ipa "[kë¦A]"] "."]
                      [:span "realização de" [ipa "[ká¦ëä]"] "."]
                      [:span "realização de" [ipa "[uniëä]"] " e" [ipa "[à¦ëä]"] "."]
                      [:span "realização de" [ipa "[mëä]"] " e" [ipa "[nëä]"] "."]
                      [:span "O arquifonema /N/ em posição final de palavra e em posição final de sílaba interior de palavra."]
                      [:span "Os alofones do arquifonema /N/ seguidos de consoantes."]
                      [:span "realização de" [ipa "[ëmbus]"] "."]
                      [:span "realização de {o mato} e {um mato}."]
                      [:span "realização de" [ipa "[ÝMfazi]"] "."]
                      [:span "realização de" [ipa "[këªtu]"] "."]
                      [:span "realização de" [ipa "[kësu]"] "."]
                      [:span "realização de " [ipa "{o navio}"] " e " [ipa "{um navio}"] "."]
                      [:span "realização de" [ipa "[Ýsu]"] "."]
                      [:span "realização de" [ipa "[àøËÔA]"] "."]
                      [:span "realização de" [ipa "[këNgA]"] "."]
                      [:span "realização de" [ipa "[à±ëmu]"] " e" [ipa "[àhëmu]"] "."]
                      [:span "realização de " [ipa-like "{sim}"] " em português e " [ipa-like "{sing}"] " em inglês."]]
         ex-items [
                   [:span "Exercícios de pronúncia: o fonema " [ipa "/m/"] "."]
                   [:span "Exercícios de pronúncia: o fonema " [ipa "/n/"] "."]
                   [:span "Exercícios de pronúncia: o fonema " [ipa "/¦/"] "."]
                   [:span "Exercícios de pronúncia: o arquifonema " [ipa "/N/"] "."]
                   ] ;; TODO put ipa in the text
         fonetica-line-item (fn [n i]
                              [:li.list-item [shared/media-icon :audio
                                              (shared/file (str "C18-M" (inc n) ".mp3"))] i])
         fonetica-line-item2 (fn [n i]
                               [:li.list-item [shared/media-icon :ex
                                               (shared/file (str "C18-M" (+ 24 n) ".mp3"))] i])]
     
     (into
      [:ol.list]
      (conj 
       (map-indexed fonetica-line-item2 ex-items)
       (map-indexed fonetica-line-item audio-items))))])

(defn render
  "Chapter eighteen view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 18 — Os fonemas nasais"
    "eResource"]
   [:div.content.chapter-18
    [recursos]
    [atividades]]
   [shared/footer]])
