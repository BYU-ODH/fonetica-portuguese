(ns fonetica-port.views.c15
  "Chapter 15"
  (:require [fonetica-port.views.components.shared :as shared :refer [ipa femp red ipa-like]]
            [reitit.frontend.easy :as rfe]))

(defn ra1
  "Atividades 1"
  []
  [:section.ra1
   [:li "Indique se as vogais em negrito são orais, nasalizadas ou oronasais, depois, transcreva a palavra. Indique se a transcrição é para o Brasil ou Portugal."
    (let [items
          [[[:span [:strong "a"] "nalfabeto"] ["oral"] ["[anaKfab×tu]"] ["[AnaLfAb×tu]"]]
           [[:span [:strong "a"] "nfíbio"] ["oronasal"] ["[á" [:sup "M"] "fÔbju]"] ["[á" [:sup "M"] "fÔbju]"]]
           [[:span "av" [:strong "i"] "ncar"] ["oronasal"] ["[avÞ" [:sup "N"] "kÒR]"] ["[AvÞ" [:sup "N"] "kÒ³]"]]
           [[:span "c" [:strong "ã"] "es"] ["ditongo oronasal"] ["[këãs]"] ["[këãS]"]]
           [[:span "c" [:strong "o"] "m" [:strong "u"] "m"] ["oral/oronasal"] ["[komê]"] ["[kumê]"]]
           [[:span "coraç" [:strong "õe"] "s"] ["ditongo oronasal"] ["[koRaséãs]"] ["[kuRAséãS]"]]
           [[:span "" [:strong "e"] "n" [:strong "a"] "morado"] ["oral/oral"] ["[enamoRÒdu]"] ["[inAmuRÒdu]"]]
           [[:span "" [:strong "e"] "ncabulado"] ["oronasal"] ["[Ý" [:sup "N"] "kabulÒdu]"] ["[Ý" [:sup "N"] "kAbulÒdu]"]]
           [[:span "" [:strong "ê"] "nfase"] ["oronsal"] ["[ç" [:sup "M"] "fazi]"] ["[ç" [:sup "M"] "fAzš]"]]
           [[:span "" [:strong "e"] "nobrecer"] ["oral"] ["[enobResÓR]"] ["[inubR‰sÓ³]"]]
           [[:span "f" [:strong "a"] "ma"] ["nasalizada/oral"] ["[fë1mA]"] ["[fA1mA]"]]
           [[:span "f" [:strong "u"] "mo"] ["nasalizada/oral"] ["[fêmu]"] ["[fÖmu]"]]
           [[:span "h" [:strong "i"] "no"] ["nasalizada/oral"] ["[ènu]"] ["[Ônu]"]]
           [[:span "m" [:strong "a"] "rr" [:strong "o"] "m"] ["oral/oronasal"] ["[mahé]"] ["[ma³é]"]]
           [[:span "m" [:strong "a"] "to"] ["oral"] ["[mÒtu]"] ["[mÒtu]"]]
           [[:span "m" [:strong "ui"] "to"] ["ditongo oronasal"] ["[mêãtu]"] ["[mêãtu]"]]
           [[:span "nen" [:strong "é"] "m"] ["ditongo oronasal"] ["[nençã]"] ["[nenëã]"]]
           [[:span "" [:strong "o"] "mbro"] ["oronasal"] ["[é" [:sup "m"] "bRu]"] ["[é" [:sup "m"] "bRu]"]]
           [[:span "" [:strong "o"] "nça"] ["oronasal"] ["[ésA]"] ["[ésA]"]]
           [[:span "p" [:strong "o"] "nho"] ["oronasal"] ["[pé¦u]"] ["[pé¦u]"]]
           [[:span "sab" [:strong "ão"] ""] ["ditongo oronasal"] ["[sabëä]"] ["[sAbëä]"]]
           [[:span "" [:strong "u"] "nir"] ["oral"] ["[unÔR]"] ["[unÔ³]"]]
           [[:span "" [:strong "u"] "ntar"] ["oronasal"] ["[à" [:sup "ª"] "tÒR]"] ["[à" [:sup "ª"] "tÒ³]"]]
           [[:span "v" [:strong "i"] "nagre"] ["oral"] ["[vinÒgRi]"] ["[vinÒgRš]"]]
           [[:span "v" [:strong "i"] "nha"] ["oronasal"] ["[vè¦A]"] ["[vè¦A]"]]
           [[:span "viv" [:strong "a"] "m"] ["ditongo oronasal"] ["[vÔváä]"] ["[vÔváä]"]]]]
      (into [:ol.list.is-lower-alpha
             [:li.list-item.grid4 [:div.blank] [:div.blank]
              [:div.is-underlined "Brasil"]
              [:div.is-underlined "Portugal"]
              ]]
            (for [[a b c d] items]
              [:li.list-item
               [:div.grid4
                a
                (into [:span.red] b)
                (into [:span.red.ipa] c)
                (into [:span.red.ipa] d)]])))]])

(defn ra2
  "Actividades 2"
  []
  [:section.ra2
   [:li "Indique se a realização da consoante nasal ortográfica em negrito resulta numa consoante nasal fonética de transição ou não, depois, transcreva a palavra ou frase. Indique se a transcrição é para o Brasil ou Portugal."
    (let [items
          [
           [[:span "âncora"] ["com transição"] ["[ë" [:sup "N"] "koRA]"] ["[ë" [:sup "N"] "kuRA]"]]
           [[:span "Angola"] ["com transição"] ["[á" [:sup "N"] "gØlA]"] ["[á" [:sup "N"] "gØlA]"]]
           [[:span "ânsia"] ["sem transição"] ["[ësja]"] ["[ësja]"]]
           [[:span "anzol"] ["sem transição"] ["[ázØK]"] ["[ázØL]"]]
           [[:span "em dia"] ["com transição"] ["[Ýã" [:sup "ø"] "ËÔA]"] ["[áã" [:sup "ª"] "dÔA]"]]
           [[:span "em vão"] ["com transição"] ["[Ýã" [:sup "M"] "vëä]"] ["[áã" [:sup "M"] "vëä]"]]
           [[:span "encher"] ["sem transição"] ["[ÝSÓR]"] ["[ÝSÓ³]"]]
           [[:span "enrolado"] ["sem transição"] ["[ÝholÒdu]"] ["[Ý³olÒdu]"]]
           [[:span "ente"] ["com transição"] ["[ç" [:sup "ø"] "Îi]"] ["[ç" [:sup "ª"] "tš]"]]
           [[:span "imbatível"] ["com transição"] ["[Þ" [:sup "m"] "baÎÔveK]"] ["[Þ" [:sup "m"] "bAtÔvEL]"]]
           [[:span "impacto"] ["com transição"] ["[Þ" [:sup "m"] "pÒkitu]"] ["[Þ" [:sup "m"] "pÒkštu]"]]
           [[:span "índole"] ["com transição"] ["[è" [:sup "ª"] "doli]"] ["[è" [:sup "ª"] "dulš]"]]
           [[:span "injúria"] ["sem transição"] ["[ÞZÖRjA]"] ["[ÞZÖRjA]"]]
           [[:span "inteiro"] ["com transição"] ["[Þ" [:sup "ª"] "tÓIRu]"] ["[Þ" [:sup "ª"] "tA1IRu]"]]
           [[:span "linfa"] ["com transição"] ["[lè" [:sup "M"] "fA]"] ["[lè" [:sup "M"] "fA]"]]
           [[:span "um lado"] ["sem transição"] ["[àlÒdu]"] ["[àlÒdu]"]]
           [[:span "um macaco"] ["fusão/alongamento"] ["[à:m:akÒku]"] ["[à:m:AkÒku]"]]
           [[:span "um neto"] ["fusão/alongamento"] ["[à:n:×tu]"] ["[à:n:×tu]"]]]]
      (into [:ol.list.is-lower-alpha
             [:li.list-item.grid4 [:div.blank] [:div.blank]
              [:div.is-underlined "Brasil"]
              [:div.is-underlined "Portugal"]
              ]]
            (for [[a b c d] items]
              [:li.list-item
               [:div.grid4
                a
                (into [:span.red] b)
                (into [:span.red.ipa] c)
                (into [:span.red.ipa] d)]])))]])

(defn render-respostas
  "Chapter 15 respostas"
  []
  [:main.c15.respostas.content
   [shared/fonetica-title "Capítulo 15 — A nasalização vocálica"
    "Respostas para o eResource"]
   [:section.atividades
    [:h2 "ATIVIDADES"]
    [:ol.atividades.list
     [ra1]
     [ra2]]]])

(defn a1
  "Actividades 1"
  []
  [:section.a1
   [:li "Indique se as vogais em negrito são orais, nasalizadas ou oronasais, depois, transcreva a palavra. Indique se a transcrição é para o Brasil ou Portugal."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c15-r) :same-session]
    (let [items
          [[[:span [:strong "a"] "nalfabeto"] ["oral"] ["[anaKfab×tu]"] ["[AnaLfAb×tu]"]]
           [[:span [:strong "a"] "nfíbio"] ["oronasal"] ["[á" [:sup "M"] "fÔbju]"] ["[á" [:sup "M"] "fÔbju]"]]
           [[:span "av" [:strong "i"] "ncar"] ["oronasal"] ["[avÞ" [:sup "N"] "kÒR]"] ["[AvÞ" [:sup "N"] "kÒ³]"]]
           [[:span "c" [:strong "ã"] "es"] ["ditongo oronasal"] ["[këãs]"] ["[këãS]"]]
           [[:span "c" [:strong "o"] "m" [:strong "u"] "m"] ["oral/oronasal"] ["[komê]"] ["[kumê]"]]
           [[:span "coraç" [:strong "õe"] "s"] ["ditongo oronasal"] ["[koRaséãs]"] ["[kuRAséãS]"]]
           [[:span "" [:strong "e"] "n" [:strong "a"] "morado"] ["oral/oral"] ["[enamoRÒdu]"] ["[inAmuRÒdu]"]]
           [[:span "" [:strong "e"] "ncabulado"] ["oronasal"] ["[Ý" [:sup "N"] "kabulÒdu]"] ["[Ý" [:sup "N"] "kAbulÒdu]"]]
           [[:span "" [:strong "ê"] "nfase"] ["oronsal"] ["[ç" [:sup "M"] "fazi]"] ["[ç" [:sup "M"] "fAzš]"]]
           [[:span "" [:strong "e"] "nobrecer"] ["oral"] ["[enobResÓR]"] ["[inubR‰sÓ³]"]]
           [[:span "f" [:strong "a"] "ma"] ["nasalizada/oral"] ["[fë1mA]"] ["[fA1mA]"]]
           [[:span "f" [:strong "u"] "mo"] ["nasalizada/oral"] ["[fêmu]"] ["[fÖmu]"]]
           [[:span "h" [:strong "i"] "no"] ["nasalizada/oral"] ["[ènu]"] ["[Ônu]"]]
           [[:span "m" [:strong "a"] "rr" [:strong "o"] "m"] ["oral/oronasal"] ["[mahé]"] ["[ma³é]"]]
           [[:span "m" [:strong "a"] "to"] ["oral"] ["[mÒtu]"] ["[mÒtu]"]]
           [[:span "m" [:strong "ui"] "to"] ["ditongo oronasal"] ["[mêãtu]"] ["[mêãtu]"]]
           [[:span "nen" [:strong "é"] "m"] ["ditongo oronasal"] ["[nençã]"] ["[nenëã]"]]
           [[:span "" [:strong "o"] "mbro"] ["oronasal"] ["[é" [:sup "m"] "bRu]"] ["[é" [:sup "m"] "bRu]"]]
           [[:span "" [:strong "o"] "nça"] ["oronasal"] ["[ésA]"] ["[ésA]"]]
           [[:span "p" [:strong "o"] "nho"] ["oronasal"] ["[pé¦u]"] ["[pé¦u]"]]
           [[:span "sab" [:strong "ão"] ""] ["ditongo oronasal"] ["[sabëä]"] ["[sAbëä]"]]
           [[:span "" [:strong "u"] "nir"] ["oral"] ["[unÔR]"] ["[unÔ³]"]]
           [[:span "" [:strong "u"] "ntar"] ["oronasal"] ["[à" [:sup "ª"] "tÒR]"] ["[à" [:sup "ª"] "tÒ³]"]]
           [[:span "v" [:strong "i"] "nagre"] ["oral"] ["[vinÒgRi]"] ["[vinÒgRš]"]]
           [[:span "v" [:strong "i"] "nha"] ["oronasal"] ["[vè¦A]"] ["[vè¦A]"]]
           [[:span "viv" [:strong "a"] "m"] ["ditongo oronasal"] ["[vÔváä]"] ["[vÔváä]"]]]]
      (into [:ol.list.is-lower-alpha]
            (for [[a] items]
              [:li.list-item a])))]])

(defn a2
  "Actividades 2"
  []
  [:section.ra2
   [:li "Indique se a realização da consoante nasal ortográfica em negrito resulta numa consoante nasal fonética de transição ou não, depois, transcreva a palavra ou frase. Indique se a transcrição é para o Brasil ou Portugal."
        [shared/media-icon :r (rfe/href :fonetica-port.routes/c15-r) :same-session]
    (let [items
          [
           [[:span "âncora"] ["com transição"] ["[ë" [:sup "N"] "koRA]"] ["[ë" [:sup "N"] "kuRA]"]]
           [[:span "Angola"] ["com transição"] ["[á" [:sup "N"] "gØlA]"] ["[á" [:sup "N"] "gØlA]"]]
           [[:span "ânsia"] ["sem transição"] ["[ësja]"] ["[ësja]"]]
           [[:span "anzol"] ["sem transição"] ["[ázØK]"] ["[ázØL]"]]
           [[:span "em dia"] ["com transição"] ["[Ýã" [:sup "ø"] "ËÔA]"] ["[áã" [:sup "ª"] "dÔA]"]]
           [[:span "em vão"] ["com transição"] ["[Ýã" [:sup "M"] "vëä]"] ["[áã" [:sup "M"] "vëä]"]]
           [[:span "encher"] ["sem transição"] ["[ÝSÓR]"] ["[ÝSÓ³]"]]
           [[:span "enrolado"] ["sem transição"] ["[ÝholÒdu]"] ["[Ý³olÒdu]"]]
           [[:span "ente"] ["com transição"] ["[ç" [:sup "ø"] "Îi]"] ["[ç" [:sup "ª"] "tš]"]]
           [[:span "imbatível"] ["com transição"] ["[Þ" [:sup "m"] "baÎÔveK]"] ["[Þ" [:sup "m"] "bAtÔvEL]"]]
           [[:span "impacto"] ["com transição"] ["[Þ" [:sup "m"] "pÒkitu]"] ["[Þ" [:sup "m"] "pÒkštu]"]]
           [[:span "índole"] ["com transição"] ["[è" [:sup "ª"] "doli]"] ["[è" [:sup "ª"] "dulš]"]]
           [[:span "injúria"] ["sem transição"] ["[ÞZÖRjA]"] ["[ÞZÖRjA]"]]
           [[:span "inteiro"] ["com transição"] ["[Þ" [:sup "ª"] "tÓIRu]"] ["[Þ" [:sup "ª"] "tA1IRu]"]]
           [[:span "linfa"] ["com transição"] ["[lè" [:sup "M"] "fA]"] ["[lè" [:sup "M"] "fA]"]]
           [[:span "um lado"] ["sem transição"] ["[àlÒdu]"] ["[àlÒdu]"]]
           [[:span "um macaco"] ["fusão/alongamento"] ["[à:m:akÒku]"] ["[à:m:AkÒku]"]]
           [[:span "um neto"] ["fusão/alongamento"] ["[à:n:×tu]"] ["[à:n:×tu]"]]]]
      (into [:ol.list.is-lower-alpha]
            (for [[a] items]
              [:li.list-item a])))]])

(defn atividades
  "Chapter 15 atividades"
  []
  [:section.atividades.content
   [:h2 "ATIVIDADES"]
   [:ol.atividades
    [a1]
    [a2]]])

(defn recursos
  "Materials"
  []
  [:section.recursos
   [:h3 "RECURSOS ELETRÔNICOS"]
   (let [audio-items [[:span "Supostos pares mínimos entre vogais orais e vogais oronasais."]
                      [:span "Alternâncias morfológicas com vogais oronasais e consoantes nasais."]
                      [:span "Alongamento de vogais oronasais."]
                      [:span "Vogais oronasais seguidas ou não de consoante nasal de transição."]
                      [:span "Pares de palavras que contrastam uma vogal oral com vogal mais arquifonema " [ipa "/"] [ipa-like "N"] [ipa "/"] "."]
                      [:span "Vogais oronasais em posição átona."]
                      [:span "Vogais nasalizadas em posição tônica."]
                      [:span "Vogais tônicas e átonas em sílabas abertas diante de consoante nasal."]
                      [:span "Vogais oronasais simples tônicos e átonos em posição interior e final de palavra. (Tab. 15.1)"]
                      [:span "Ditongos oronasais tônicos e átonos em posição interior e final de palavra. (Tab. 15.3)"]
                      [:span "Exemplos do ditongo oronasal " [ipa "[áä]"] "."]
                      [:span "Outros exemplos do ditongo oronasal " [ipa "[áä]"] "."]
                      [:span "Mais exemplos do ditongo oronasal " [ipa "[áä]"] "."]
                      [:span "Ainda mais exemplos do ditongo oronasal " [ipa "[áä]"] "."]
                      [:span "Exemplos do ditongo oronasal " [ipa "[áã]"] "."]
                      [:span "Outros exemplos do ditongo oronasal " [ipa "[áã]"] "."]
                      [:span "Exemplos do ditongo oronasal " [ipa "[áã]"] " em Portugal."]
                      [:span "Exemplos do ditongo oronasal " [ipa "[Ýã]"] " no Brasil e em Portugal."]
                      [:span "Exemplos do ditongo oronasal " [ipa "[ßã]"] "."]
                      [:span "Exemplo do ditongo oronasal " [ipa "[àã]"] ": " [ipa "[mêãtu]"] "."]
                      [:span "Vogais nasalizadas diante das três consoantes nasais."]
                      [:span "Vogais orais/nasalizadas diante de consoantes nasais de acordo com a tonicidade."]
                      [:span "O problema de " [ipa "/Ò"] [ipa-like "N"] [ipa "/"] " em posição final de palavra: " [ipa "[masë]"] " e " [ipa "[pëä]"] "."]
                      [:span "Vogais oronasais simples tônicos em posição final de palavra: singular e plural."]
                      [:span "Ditongos oronasais tônicos em posição final de palavra: singular e plural."]
                      [:span "Formas singular e plural de substantivos terminados em {-ão}."]
                      [:span "Alternâncias morfológicas de substantivos com vogais oronasais."]
                      [:span "Alternâncias morfológicas de adjetivos com vogais oronasais."]
                      [:span "O grau de nasalização em distintos contornos fonotáticos no Rio de Janeiro."]
                      [:span "O grau de nasalização em distintos contornos fonotáticos em Lisboa."]
                      [:span "O grau de nasalização diante das três consoantes nasais no Rio de Janeiro."]
                      [:span "O grau de nasalização diante das três consoantes nasais em Lisboa."]
                      [:span "O grau de nasalização em sílabas abertas diante de consoante nasal de acordo com a tonicidade em São Paulo."]
                      [:span "A nasalização perseverante em São Paulo: {mito} e {minto}."]
                      [:span "Contraste entre {canta} em espanhol e português."]
                      [:span "Alongamento vocálico e grau de nasalização em {o mato} e {um mato}."]
                      [:span "Alongamento vocálico e grau de nasalização em {seda} e {senda}."]
                      [:span "Exemplos da vogal simples oronasal " [ipa "[Þ]"] " em posição inicial, medial e final."]
                      [:span "O alofone " [ipa "[Þ]"] " no Brasil."]
                      [:span "O alofone " [ipa "[Þ]"] " em Portugal."]
                      [:span "Exemplos da vogal simples oronasal " [ipa "[à]"] " em posição inicial, medial e final."]
                      [:span "O alofone " [ipa "[à]"] " no Brasil."]
                      [:span "O alofone " [ipa "[à]"] " em Portugal."]
                      [:span "Exemplos da vogal simples oronasal " [ipa "[Ý]"] " em posição inicial, medial e final."]
                      [:span "O alofone " [ipa "[Ý]"] " no Brasil."]
                      [:span "O alofone " [ipa "[Ý]"] " em Portugal."]
                      [:span "Exemplos da vogal simples oronasal " [ipa "[ß]"] " em posição inicial, medial e final."]
                      [:span "O alofone " [ipa "[ß]"] " no Brasil."]
                      [:span "O alofone " [ipa "[ß]"] " em Portugal."]
                      [:span "Exemplos da vogal simples oronasal " [ipa "[á]"] " em posição inicial, medial e final."]
                      [:span "O alofone " [ipa "[á]"] " no Brasil."]
                      [:span "O alofone " [ipa "[á]"] " em Portugal."]
                      [:span "O ditongo " [ipa "[áä]"] "."]
                      [:span "O ditongo " [ipa "[Ýã]"] "."]
                      [:span "O ditongo " [ipa "[ßã]"] "."]
                      [:span "O ditongo " [ipa "[àã]"] "."]
                      [:span "O ditongo " [ipa "[áã]"] "."]]
         ex-items [[:span "Exercícios de pronúncia: vogais oronasais simples."]
                   [:span "Exercícios de pronúncia: vogais simples orais/nasalizadas."]
                   [:span "Exercícios de pronúncia: ditongos oronasais."]
                   [:span "Exercícios de pronúncia: ditongos orais e oronasais."]]
         fonetica-line-item (fn [n i]
                              [:li [shared/media-icon :audio
                                    (shared/file (str "C15-M" (inc n) ".mp3"))] i])
         fonetica-line-item2 (fn [n i]
                               [:li [shared/media-icon :ex
                                     (shared/file (str "C15-M" (+ 58 n) ".mp3"))] i])]
     (into
      [:ol.list]
      (conj 
       (map-indexed fonetica-line-item2 ex-items)
       (map-indexed fonetica-line-item audio-items))))])

(defn render
  "Chapter fifteen view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 15 — A nasalização vocálica"
    "eResource"]
   [:div.content.chapter-15
    [recursos]
    [atividades]]
   [shared/footer]])
