(ns fonetica-port.views.c21
  "Chapter 21"
  (:require [fonetica-port.views.components.shared :as shared :refer [ipa fempr red ipa-like]]
            [reitit.frontend.easy :as rfe]))

(defn ra1
  "Respostas activity 1"
  []
  [:section.ra1
   [:li [:div "Transcreva as seguintes orações separando as sílabas fonossintáticas por pontos."]
    [:div "Exemplo: O outro dia fomos ao evento:"
     [:span.red.ipa {:style {"margin-left" "3em"}} "[woU.tRu.ËÔ.a.fÕ.mu.saU.e.vç" [:sup "ª"] ".tu]"]]

    (let [items [[["Vi o Marcos na praça."]
                  [:div "BR: " [:span.ipa "[vÔU.mÒR.kuz.na.pRÒ.sA]"]]
                  [:div "PO: " [:span.ipa "[vÔU.mÒR.kuz.nA.pRÒ.sA]"]]]
                 [["Estava com o Nico, o italiano."]
                  [:div "BR: " [:span.ipa "[es.tÒ.vA.kßU.nÔ.ku/wi.ta.li.ë.nu]"]]
                  [:div "PO: " [:span.ipa "[šS.tÒ.vA.kßU.nÔ.ku/wi.ta.ljA.nu]"]]]
                 [["Eles queriam falar a sós."]
                  [:div "BR: " [:span.ipa "[Ó.lis.ke.RÔ.áä.fa.lÒ.Ra.sØs]"]]
                  [:div "PO: " [:span.ipa "[Ó.lšS.kš.RÔ.áä.fA.lÒ.RA.sØs]"]]]
                 [["Escrevi umas cartas a Ana."]
                  [:div "BR: " [:span.ipa "[es.kRe.vÔU.mAs.kÒR.tA.sa.á.nA]"]]
                  [:div "PO: " [:span.ipa "[šS.kR‰.vÔU.mAS.kÒ³.tA.sA.A.nA]"]]]
                 [["Os soldados saíram da estação."]
                  [:div "BR: " [:span.ipa "[u.soK.dÒ.du.sa.Ô.RáU.dA`es.ta.sáä]"]]
                  [:div "PO: " [:span.ipa "[u.soL.dÒ.du.sa.Ô.RáU.dIS.tA.sáä]"]]]
                 [["Tinham uma sede dolorosa."]
                  [:div "BR: " [:span.ipa "[ÎÔ.¦áä.mA.sÓ.Ëi.do.lo.RØ.zA]"]]
                  [:div "PO: " [:span.ipa "[tÔ.¦áä.mA.sÓ.dš.du.lu.RØ.zA]"]]]
                 [["Dizem que o fio é dela."]
                  [:div "BR: " [:span.ipa "[ËÔ.zÝã.kju.fÞU.×.d×.lA]"]]
                  [:div "PO: " [:span.ipa "[dÔ.zÝã.kju.fÞU.×.d×.lA]"]]]
                 [["Acusaram o menino de ocioso."]
                  [:div "BR: " [:span.ipa "[a.ku.zÒ.Ráä.me.nÔ.nu.Ëjo.si.Õ.zu]"]]
                  [:div "PO: " [:span.ipa "[A.ku.zÒ.Ráä.mi.nÔ.nu.djo.sjÕ.zu]"]]]
                 [["A menina aprendeu a ser muito industriosa."]
                  [:div "BR: " [:span.ipa "[a.me.nÔ.na.pRÝ" [:sup "ª"] ".de.wa.sÓR.mêã" [:sup "ª"] ".twÞ" [:sup "ª"] ".dus.tRi.Ø.zA]"]]
                  [:div "PO: " [:span.ipa "[A.mš.nÔ.nA.pRÝª.de.wa.sÓR.mêã" [:sup "ª"] ".twÞ" [:sup "ª"] ".duS.tRjØ.zA]"]]]
                 [["O advogado não quis ser razoável."]
                  [:div "BR: " [:span.ipa "[wa.Ëi.vo.gÒ.du.nëä.kÔ.sÓR.ha.zo.Ò.vÓK]"]]
                  [:div "PO: " [:span.ipa "[wA.d" [:sup "š"] ".vu.gÒ.du.nëä.kÔ.sÓ.³A.zwÒ.vÓL]"]]]
                 [["Os filhos de Joaquim foram de mal a pior."]
                  [:div "BR: " [:span.ipa "[us.fÔ.Yus.Ëi.Zo`a.kè.fÕ.Ráä.Ëi.mÒK.a.pi.ÕR]"]]
                  [:div "PO: " [:span.ipa "[uS.fÔ.YuZ.d" [:sup "š"] ".ZwA.kè.fÕ.Ráä.d" [:sup "š"] ".mÒ.La.pjÕR]"]]]
                 [["Vou pensar no teu caso."]
                  [:div "BR: " [:span.ipa "[vÕU.pÝ.sÒR.nu.teU.kÒ.zu]"]]
                  [:div "PO: " [:span.ipa "[vÕU.pÝ.sÒR.nu.teU.kÒ.zu]"]]]]]
      (into [:ol.list.is-lower-alpha]
            (for [[sum br po] items]
              [:li
               (into [:div] sum)
               [:span.red br]
               [:span.red po]])))]])

(defn render-respostas
  "Chapter 21 respostas"
  []
  [:main.c21.respostas.content
   [shared/fonetica-title "Capítulo 21 — A sílaba e a silabação"
    "Respostas para o eResource"]
   [:section.atividades
    [:h2 "ATIVIDADES"]
    [:ol.atividades.list
     [ra1]]]])

(defn a1
  "Activity 1"
  []
  [:section.a1
   [:li [:div "Transcreva as seguintes orações separando as sílabas fonossintáticas por pontos."
         [shared/media-icon :r (rfe/href :fonetica-port.routes/c21-r) :same-session]]
    [:div "Exemplo: O outro dia fomos ao evento:"
     [:span.red.ipa {:style {"margin-left" "3em"}} "[woU.tRu.ËÔ.a.fÕ.mu.saU.e.vç" [:sup "ª"] ".tu]"]]

    (let [items [[["Vi o Marcos na praça."]
                  [:div "BR: " [:span.ipa "[vÔU.mÒR.kuz.na.pRÒ.sA]"]]
                  [:div "PO: " [:span.ipa "[vÔU.mÒR.kuz.nA.pRÒ.sA]"]]]
                 [["Estava com o Nico, o italiano."]
                  [:div "BR: " [:span.ipa "[es.tÒ.vA.kßU.nÔ.ku/wi.ta.li.ë.nu]"]]
                  [:div "PO: " [:span.ipa "[šS.tÒ.vA.kßU.nÔ.ku/wi.ta.ljA.nu]"]]]
                 [["Eles queriam falar a sós."]
                  [:div "BR: " [:span.ipa "[Ó.lis.ke.RÔ.áä.fa.lÒ.Ra.sØs]"]]
                  [:div "PO: " [:span.ipa "[Ó.lšS.kš.RÔ.áä.fA.lÒ.RA.sØs]"]]]
                 [["Escrevi umas cartas a Ana."]
                  [:div "BR: " [:span.ipa "[es.kRe.vÔU.mAs.kÒR.tA.sa.á.nA]"]]
                  [:div "PO: " [:span.ipa "[šS.kR‰.vÔU.mAS.kÒ³.tA.sA.A.nA]"]]]
                 [["Os soldados saíram da estação."]
                  [:div "BR: " [:span.ipa "[u.soK.dÒ.du.sa.Ô.RáU.dA`es.ta.sáä]"]]
                  [:div "PO: " [:span.ipa "[u.soL.dÒ.du.sa.Ô.RáU.dIS.tA.sáä]"]]]
                 [["Tinham uma sede dolorosa."]
                  [:div "BR: " [:span.ipa "[ÎÔ.¦áä.mA.sÓ.Ëi.do.lo.RØ.zA]"]]
                  [:div "PO: " [:span.ipa "[tÔ.¦áä.mA.sÓ.dš.du.lu.RØ.zA]"]]]
                 [["Dizem que o fio é dela."]
                  [:div "BR: " [:span.ipa "[ËÔ.zÝã.kju.fÞU.×.d×.lA]"]]
                  [:div "PO: " [:span.ipa "[dÔ.zÝã.kju.fÞU.×.d×.lA]"]]]
                 [["Acusaram o menino de ocioso."]
                  [:div "BR: " [:span.ipa "[a.ku.zÒ.Ráä.me.nÔ.nu.Ëjo.si.Õ.zu]"]]
                  [:div "PO: " [:span.ipa "[A.ku.zÒ.Ráä.mi.nÔ.nu.djo.sjÕ.zu]"]]]
                 [["A menina aprendeu a ser muito industriosa."]
                  [:div "BR: " [:span.ipa "[a.me.nÔ.na.pRÝª.de.wa.sÓR.mêã .twÞ .dus.tRi.Ø.zA]"]]
                  [:div "PO: " [:span.ipa "[A.mš.nÔ.nA.pRÝª.de.wa.sÓR.mêã .twÞ .duS.tRjØ.zA]"]]]
                 [["O advogado não quis ser razoável."]
                  [:div "BR: " [:span.ipa "[wa.Ëi.vo.gÒ.du.nëä.kÔ.sÓR.ha.zo.Ò.vÓK]"]]
                  [:div "PO: " [:span.ipa "[wA.dš.vu.gÒ.du.nëä.kÔ.sÓ.³A.zwÒ.vÓL]"]]]
                 [["Os filhos de Joaquim foram de mal a pior."]
                  [:div "BR: " [:span.ipa "[us.fÔ.Yus.Ëi.Zo`a.kè.fÕ.Ráä.Ëi.mÒK.a.pi.ÕR]"]]
                  [:div "PO: " [:span.ipa "[uS.fÔ.YuZ.dš.ZwA.kè.fÕ.Ráä.dš.mÒ.La.pjÕR]"]]]
                 [["Vou pensar no teu caso."]
                  [:div "BR: " [:span.ipa "[vÕU.pÝ.sÒR.nu.teU.kÒ.zu]"]]
                  [:div "PO: " [:span.ipa "[vÕU.pÝ.sÒR.nu.teU.kÒ.zu]"]]]]]
      (into [:ol.list.is-lower-alpha]
            (for [[sum _br _po] items]
              [:li
               (into [:div] sum)
               [:span.blank]
               [:span.blank]])))]])

(defn atividades
  "Chapter 21 atividades"
  []
  [:section.atividades.content
   [:h2 "ATIVIDADES"]
   [:ol.atividades
    [a1]]])

(defn recursos
  "Materials"
  []
  [:section.recursos
   [:h3 "RECURSOS ELETRÔNICOS"]
   (let [audio-items [[:span "As várias pronúncias de {laboratory} em inglês."]
                      [:span "A pronúncia de {hire} e {higher} em inglês."]
                      [:span "A pronúncia de {general} em português e inglês."]
                      [:span "A pronúncia de consoantes simples no ataque."]
                      [:span "A pronúncia de encontros consonantais inseparáveis no ataque."]
                      [:span "O elemento vocálico no ataque."]
                      [:span "Exemplos dos tipos de hiato."]
                      [:span "A palavra {mountain} em inglês."]
                      [:span "A pronúncia de consoantes simples na coda."]
                      [:span "A pronúncia de duas consoantes na coda."]
                      [:span "A pronúncia canônica de {general} em português e inglês."]
                      [:span "A pronúncia de {Pode ver as outras casas aqui.} em sílabas fonéticas."]
                      [:span "A silabação do português e do inglês com sílabas prototípicas segundo Regra 1."]
                      [:span "A silabação de palavras segundo as Regras 2 e 3."]
                      [:span "A pronúncia de palavras terminadas em " [ipa-like "/l/"] " seguidas de outra palavra."]
                      [:span "A pronúncia de palavras terminadas em " [ipa-like "/S/"] " seguidas de outra palavra."]
                      [:span "A pronúncia de palavras terminadas em " [ipa-like "/N/"] " seguidas de outra palavra."]
                      [:span "A pronúncia de palavras terminadas em " [ipa-like "/R/"] " seguidas de outra palavra."]
                      [:span "A pronúncia de palavras terminadas em vogal seguidas de outra palavra iniciada em vogal."]
                      [:span "A pronúncia de palavras terminadas em vogal alta tônica seguidas de outra palavra iniciada em vogal."]
                      [:span "A pronúncia de palavras terminadas em " [ipa-like "/l/"] " seguidas de outra palavra iniciada em " [ipa-like "/l/"] "."]
                      [:span "A pronúncia de palavras terminadas em " [ipa-like "/S/"] " seguidas de outra palavra iniciada em " [ipa-like "/s/"] " ou " [ipa-like "/z/"] "."]
                      [:span "A pronúncia de palavras terminadas em " [ipa-like "/N/"] " seguidas de outra palavra iniciada em " [ipa-like "/m/"] " ou " [ipa-like "/n/"] "."]
                      [:span "A pronúncia de palavras terminadas em " [ipa-like "/R/"] " seguidas de outra palavra iniciada em " [ipa-like "/r/"] "."]
                      [:span "Dois tipos de fusão consonantal."]
                      [:span "A pronúncia das palavras {nascer} e {paciência} no Brasil e em Portugal."]
                      [:span "A pronúncia do encontro de duas vogais altas."]
                      [:span "A pronúncia do encontro de duas vogais não altas."]
                      [:span "A pronúncia do encontro de três vogais em uma sílaba: tritongo ou ditongo crescente mais sinalefa/ sinérese."]
                      [:span "A pronúncia do encontro de três vogais em uma sílaba: sinalefa/sinérese mais ditongo decrescente."]
                      [:span "A pronúncia do encontro de quatro vogais em uma sílaba."]
                      [:span "Deslizantes e soantes no ataque."]
                      [:span "Obstruintes no ataque."]
                      [:span "Sílabas sem ataque."]
                      [:span "Deslizantes na coda."]
                      [:span "Soantes na coda."]
                      [:span "Obstruintes na coda."]
                      [:span "A silabação fonética de palavras."]
                      [:span "A silabação fonética de um texto."]]
         ex-items [[:span "Exercícios de pronúncia: a silabação de textos."]]
         fonetica-line-item (fn [n i]
                              [:li.list-item [shared/media-icon :audio
                                              (shared/file (str "C21-M" (inc n) ".mp3"))] i])
         fonetica-line-item2 (fn [n i]
                               (let [ic (-> audio-items count inc) ]
                                 [:li.list-item [shared/media-icon :ex
                                                 (shared/file (str "C21-M" (+ ic n) ".mp3"))] i]))]
     
     (into
      [:ol.list]
      (conj 
       (map-indexed fonetica-line-item2 ex-items)
       (map-indexed fonetica-line-item audio-items))))])

(defn render
  "Chapter 21 view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 21 — A sílaba e a silabação"
    "eResource"]
   [:div.content.chapter-21
    [recursos]
    [atividades]]
   [shared/footer]])
