(ns fonetica-port.views.c1
  "Chapter 1"
  (:require [reitit.frontend.easy :as rfe]
            [fonetica-port.views.components.shared :as shared]))

(defn c1-m1
  "Renderer for the Material 1 view for chapter 1"
  []
  [:main.content
   [shared/fonetica-title "Capítulo 1: Nota 1"]
   [:div.is-size-4 "Fotos de diferentes comunicações não verbais. O que é comunicado em cada um deles?"]
   (into [:div.photogrid]
         (for [file ["C1-M1-A1.jpg"
                     "C1-M1-A2.jpg"
                     "C1-M1-A3.jpg"
                     "C1-M1-B1.jpg"
                     "C1-M1-B2.jpg"
                     "C1-M1-B3.jpg"
                     "C1-M1-C1.jpg"
                     "C1-M1-C2.jpg"
                     "C1-M1-C3.jpg"]]
           [:div.image-container {"border-style" :solid
                                  "border-width" "2px"
                                  "border-color" "black"}
            [:img {:width "200px"
                   :src (shared/file file)}]]))])

(defn materials
  "Material links"
  []
  [:section.materials
   [:h2.has-text-centered "eResource"]
   [:h3.is-size-5 {:class "materials"} "RECURSOS ELETRÔNICOS"]
   [:ol  
    [:li
     [shared/media-icon :image (rfe/href :fonetica-port.routes/c1-m1) :in-session]
     "Fotos de diferentes comunicações não verbais."]
    [:li
     [shared/media-icon :audio (shared/file "C1-M2.mp3")]
     "O apito do árbitro numa partida atlética."]
    [:li 
     [shared/media-icon :audio (shared/file "C1-M3.mp3")]
     "Um estalo produzido por um vaqueiro."]
    [:li 
     [shared/media-icon :audio (shared/file "C1-M4.mp3")]
     "Exclamações que demonstram diferenças paralinguísticas."]
    [:li
     [shared/media-icon :video (shared/file "C1-M5.mp4")]
     "Vídeo do gesto de chamar uma criança."]
    [:li
     [shared/media-icon :image (shared/file "C1-M6.jpg")]
     "Foto de um cliente que quer dois ingressos."]
    [:li
     [shared/media-icon :video (shared/file "C1-M7.mp4")]
     "Vídeo de reguladores numa conversa."]
    [:li 
     [shared/media-icon :image (shared/file "C1-M8.jpg")]
     "Foto de uma expressão de emoção de uma pessoa furiosa."]
    [:li 
     [shared/media-icon :audio (shared/file "C1-M9.mp3")]
     "Emissão acústica da palavra livro."]
    [:li [shared/media-icon :video (shared/file "C1-M10.mp4")]
     "Gesto que representa a palavra livro em LIBRAS e em LGP."]]])

(defn a1
  "Activity 1"
  []
   [:section.a1
    [:li "Os seguintes exemplos de comunicação são verbais ou não verbais? Justifique sua classificação e identifique a via de transmissão. (Clique no ícone para ver/escutar o exemplo.)"
     [shared/item-grid [[shared/media-icon :video (shared/file "C1-A1-A1.mp4")]
                        [shared/media-icon :audio (shared/file "C1-A1-A2.mp3")]
                        [shared/image
                         {:full-url (shared/file "C1-A1-A3.jpg")
                          :alt "Document"}]
                        [shared/image {:full-url (shared/file "C1-A1-B1.jpg")
                                       :alt "sleep_v1.jpg"}]
                        [shared/image {:full-url (shared/file "C1-A1-B2.jpg")
                               :alt "plane.jpg" :class "WPImageStyle" :width "95" :height "92"}]
                        [shared/image {:full-url (shared/file "C1-A1-B3.jpg")
                               :alt "heart Brazil" :class "WPImageStyle" :width "175" :height "70"}]
                        [shared/image {:full-url (shared/file "C1-A1-C1.png")
                               :alt "handshake" :class "WPImageStyle" :width "139" :height "92"}]
                        [shared/media-icon :video (shared/file "C1-A1-C2.mp4")]
                        [shared/image {:full-url (shared/file "C1-A1-C3.jpg")
                               :alt "c1a1-9chart.gif" :class "WPImageStyle" :width "123" :height "92"}]]]]])

(defn a2
  "Activity 2"
  []
  [:section.a2
   [:li "Quais dos seguintes exemplos são palavras do português? Justifique sua identificação. (Clique no ícone para escutar o exemplos orais.)"
    [shared/item-grid [[:span "arena"]
                       [shared/media-icon :audio (shared/file "C1-A2-A2.mp3")]
                       [shared/media-icon :audio (shared/file "C1-A2-A3.mp3")]
                       [shared/media-icon :audio (shared/file "C1-A2-B1.mp3")]
                       [:span "splanot"]
                       [:span "rating"]
                       [:span "modela"]
                       [shared/media-icon :audio (shared/file "C1-A2-C2.mp3")]
                       [:span "aquena"]]]]])

(defn a3
  "Activity 3"
  []
  [:section.a3
   [:li "Qual a piada na seguinte tirinha?"
    [shared/image {:full-url (shared/file "C1-A3.jpg")
                   :alt "c1a3.jpg"}]]])

(defn a4
  "Activity 4"
  []
   [:section.a4
    [:li "Quais são situações que favorecem a comunicação verbal por via oral? Quais são situações que favorecem a comunicação verbal por via escrita? Justifique sua opinião."]])

(defn a5
  "Activity 5"
  []
  [:section.a5
   [:li "Identifique o aspecto do modelo de comunicação e a via de transmissão destacados nos
seguintes eventos comunicativos."
    [shared/item-grid [[shared/image {:full-url (shared/file "C1-A5-A1.jpg")
                              :alt "writing_v2.gif" :class "WPImageStyle" :width "173" :height "115"}]
                       [shared/image {:full-url (shared/file "C1-A5-A2.jpg")
                              :alt "talking_v2.jpg" :class "WPImageStyle" :width "139" :height "115"}]
                       [shared/image {:full-url (shared/file "C1-A5-A3.jpg")
                              :alt "listening_v1.jpg" :class "WPImageStyle" :width "115" :height "138"}]
                       [shared/image {:full-url (shared/file "C1-A5-B1.jpg")
                              :alt "signing_v3.jpg" :class "WPImageStyle" :width "115" :height "138"}]
                       [shared/image {:full-url (shared/file "C1-A5-B2.jpg")
                              :alt "watchingsigning_v2.jpg" :class "WPImageStyle" :width "175" :height "115"}]
                       [shared/image {:full-url (shared/file "C1-A5-B3.jpg")
                              :alt "reading.jpg" :class "WPImageStyle" :width "116" :height "138"}]]]]])

(defn a6
  "Activity 6"
  []
  [:section.a6
   [:li "O que os seguintes ícones representam? Para que podem ser usados?"
    [shared/item-grid [ [shared/image {:full-url (shared/file "C1-A6-A1.png")
                               :alt "iconlightbulb.gif" :class "WPImageStyle" :width "49" :height "55"}]
                       [shared/image {:full-url (shared/file "C1-A6-A2.png")
                              :alt "iconenvelope.gif" :class "WPImageStyle" :width "49" :height "36"}]
                       [shared/image {:full-url (shared/file "C1-A6-A3.png")
                              :alt "iconwrench.gif" :class "WPImageStyle" :width "56" :height "55"}]
                       [shared/image {:full-url (shared/file "C1-A6-B1.png")
                              :alt "iconmouse.gif" :class "WPImageStyle" :width "37" :height "55"}]
                       [shared/image {:full-url (shared/file "C1-A6-B2.png")
                              :alt "iconcellphone.gif" :class "WPImageStyle" :width "76" :height "55"}]
                       [shared/image {:full-url (shared/file "C1-A6-B3.png")
                              :alt "iconm-w.gif" :class "WPImageStyle" :width "55" :height "55"}]]]
    [:p "Quais são outros exemplos de ícones e o que eles representam?"]]])

(defn a7
  "Activity 7"
  []
  [:section.a7
   [:li "O que os seguintes gestos representam?"
    [shared/item-grid [[shared/image {:full-url (shared/file "C1-A7-1.jpg")
                              :alt "ojo.jpg" :class "WPImageStyle" :width "112" :height "138"}]
                       [shared/image {:full-url (shared/file "C1-A7-2.jpg")
                              :alt "callgesture.jpg" :class "WPImageStyle" :width "139" :height "138"}]
                       [shared/image {:full-url (shared/file "C1-A7-3.jpg")
                              :alt "tres.jpg" :class "WPImageStyle" :width "106" :height "138"}]
                       [shared/image {:full-url (shared/file "C1-A7-4.jpg")
                              :alt "thumbsup.gif" :class "WPImageStyle" :width "166" :height "103"}]
                       [shared/image {:full-url (shared/file "C1-A7-5.jpg")
                              :alt "pointing.jpg" :class "WPImageStyle" :width "166" :height "110"}]
                       [shared/image {:full-url (shared/file "C1-A7-6.jpg")
                              :alt "bowing.jpg" :class "WPImageStyle" :width "92" :height "138"}]]]
    [:p "Quais são outros exemplos de gestos e o que eles representam? Quais mudam de acordo com a sociedade em que se produzem?"]]])

(defn a8
  "Activity 8"
  []
  [:section.a8
   [:li "O que os seguintes signos acústicos representam?"]
   [shared/item-grid [[shared/media-icon :audio (shared/file "C1-A8-1.mp3")]
                      [shared/media-icon :audio (shared/file "C1-A8-2.mp3")]
                      [shared/media-icon :audio (shared/file "C1-A8-3.mp3")]
                      [shared/media-icon :audio (shared/file "C1-A8-4.mp3")]
                      [shared/media-icon :audio (shared/file "C1-A8-5.mp3")]
                      [shared/media-icon :audio (shared/file "C1-A8-6.mp3")]]]
   [:p "Quais são outros exemplos de signos acústicos e o que eles representam?"]])

(defn activities
  "Render all the activities"
  []
  [:section.activities
   [:h2.activities "ATIVIDADES"]
   [:ol.list
    [a1]
    [a2]   
    [a3]
    [a4]
    [a5]
    [a6]
    [a7]
    [a8]]])

(defn render
  "Chapter one view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 1 — A comunicação humana"]
   [:div.content.chapter-1
    [materials]
    [activities]]
   [shared/footer]])
