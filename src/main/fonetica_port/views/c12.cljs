(ns fonetica-port.views.c12
  "Chapter 12"
  (:require [fonetica-port.views.components.shared :as shared :refer [ipa ipa-like]]
            [reitit.frontend.easy :as rfe]))

(defn ra1
  "Respostas Activity 1"
  []
  [:section.ra1
   [:li "Escreva o símbolo do alofone descrito pelos seguintes traços."
    (let [items [[["meio-fechada"
                   "anterior"
                   "oral/sonora"]
                  "[e]"]

                 [["aberta"
                   "central"
                   "oral/sonora"]
                  "[a]"]

                 [["fechada"
                   "posterior"
                   "oronasal/sonora"]
                  "[à]"]

                 [["semi-vogal"
                   "posterior"
                   "oral/sonora"]
                  "[U]"]

                 [["meio-aberta"
                   "posterior"
                   "oral/sonora"]
                  "[O]"]

                 [["fechada"
                   "central"
                   "oral/ensurdecida"]
                  "[Ë]"] ;; wrong char rendered

                 [["semi-consoante"
                   "anterior"
                   "oral/sonora"]
                  "[j]"]

                 [["meio-aberta"
                   "central"
                   "oral/ensurdecida"]
                  "[È]"] ;; wrong char rendered

                 [["meio-aberta"
                   "anterior"
                   "oral/sonora"]
                  "[E]"]

                 [["semi-consoante"
                   "posterior"
                   "oral/sonora"]
                  "[w]"]] ]
      
      [:ol.list.is-lower-alpha
       (into [:div.grid2.parent]
             (for [[l r] items]
               [:li
                [:div.grid2 (into [:div.l]
                                  (for [i l]
                                    [:div i]))
                 [:div.r.ipa2.red.is-size-4 r]]]))])]])

(defn ra2
  "Respostas Activity 2"
  []
  [:section.ra2
   [:li "Indique os traços fonéticos para os seguintes alofones."
    (let [items
          [["[w]"
            ["semi-consoante"
             "posterior"
             "oral/sonora"]]
           ["[I]"
            ["semi-vogal"
             "anterior"
             "oral/sonora"]]
           ["[E]"
            ["meio-aberta"
             "anterior"
             "oral/sonora"]]
           ["[ß]"
            ["meio-fechada"
             "posterior"
             "oronasal/sonora"]]
           ["[É]"
            ["meio-fechada"
             "anterior"
             "oral/ensurdecida"]]
           ["[a]"
            ["aberta"
             "central"
             "oral/sonora"]]
           ["[à]"
            ["fechada"
             "posterior"
             "oronasal/sonora"]]
           ["[Þ]"
            ["fechada"
             "anterior"
             "oronasal/sonora"]]
           ["[j]"
            ["semi-consoante"
             "anterior"
             "oral/sonora"]]
           ["[Ý]"
            ["meio-fechada"
             "anterior"
             "oronasal/sonora"]]]
          ]
      [:div.grid2.parent
       [:ol.list.is-lower-alpha
        (into [:div.grid2.parent]
              (for [[l r] items]
                [:li
                 [:div.grid2
                  [:div.l.ipa2 l]
                  (into [:div.r.red]
                        (for [i r]
                          [:div i]))]]))]])]])

(defn ra3
  "Respostas Activity 3"
  []
  [:section.ra3
   [:li "Para as seguintes regras de distribuição: (a) indique se a regra é para o PB ou PE, (b) interprete a regra em palavras e (c) dê exemplos de sua aplicação, tendo cuidado para exemplificar todos os aspectos da regra."
    (let [items
          [[[shared/image {:full-url (shared/file "C12-RA3a.jpg")}] [:span "PB 1) O fonema " [ipa "/a/"] " alça-se a " [ipa "[A]"] " quando átona em posição final de palavra " [ipa "[kÒzA]"] ".  2) Alça-se e nasaliza-se a " [ipa "[á]"] " diante do arquifonema " [ipa "/"] [ipa-like "N"] [ipa "/"] " seguido ou não de " [ipa "/"] [ipa-like "S"] [ipa "/"] " em posição final de sílaba " [ipa "[ësjA]"] ". 3) Alça-se e nasaliza-se a " [ipa "[ë]"] " quando tônica diante de uma consoante nasal no começo da próxima sílaba " [ipa "[këmA]"] ". 4) Realiza-se como " [ipa "[a]"] " nos outros lugares " [ipa "[kÒbu]"] " " [ipa "[akademÔA]"] "."]]

           [[shared/image {:full-url (shared/file "C12-RA3b.jpg")}] [:span "PE 1) O fonema " [ipa "/o/"] " realiza-se como " [ipa "[w]"] " quando átono diante de uma vogal tônica, formando assim um ditongo crescente " [ipa "[kwÒtRu]"] ". 2) Alça-se à vogal fechada " [ipa "[u]"] " quando átona " [ipa "[uSpitÒL]"] " " [ipa "[kÒ³u]"] ". 3) Alça-se e nasaliza-se como semi-vogal " [ipa "[ä]"] " entre uma vogal oronasal e o arquifonema " [ipa "/"] [ipa-like "N"] [ipa "/"] ", formando assim um ditongo oronasal decrescente " [ipa "[mëä]"] ". 4) Nasaliza-se diante do arquifonema " [ipa "/"] [ipa-like "N"] [ipa "/"] " seguido ou não de " [ipa "/"] [ipa-like "S"] [ipa "/"] " " [ipa "[késtA]"] ". Quando tônica diante de uma consoante nasal no começo da próxima sílaba " [ipa "[këmA]"] ". 5) Realiza-se como " [ipa "[o]"] " nos outros lugares " [ipa "[kÕRu]"] "."]]

           [[shared/image {:full-url (shared/file "C12-RA3c.jpg")}] [:span "PB 1) O fonema " [ipa "/e/"] " realiza-se como " [ipa "[j]"] " quando átona em posição final de palavra diante de uma vogal nuclear em posição inicial de palavra " [ipa "[kwÒzjÕItu]"] ". 2) Alça-se a " [ipa "[i]"] " quando átona em posição final de palavra " [ipa "[klÒsis]"] ". 3) Alça-se e nasaliza-se como semi- vogal " [ipa "[ã]"] " entre uma vogal oronasal e o arquifonema " [ipa "/"] [ipa-like "N"] [ipa "/"] ", formando assim um ditongo oronasal decrescente " [ipa "[mëã]"] ".  4) Ditonga-se e nasaliza-se em " [ipa "[Ýã]"] " diante do arquifonema " [ipa "/"] [ipa-like "N"] [ipa "/"] " seguido ou não de " [ipa "/"] [ipa-like "S"] [ipa "/"] " em posição final de palavra " [ipa "[vçã]"] ". 5) Nasaliza-se a " [ipa "[Ý]"] " diante do arquifonema " [ipa "/"] [ipa-like "N"] [ipa "/"] " seguido ou não de " [ipa "/"] [ipa-like "S"] [ipa "/"] " " [ipa "[çSi]"] ". 6) Realiza-se como " [ipa "[e]"] " nos outros lugares " [ipa "[hesebÓU]"] "."]]

           [[shared/image {:full-url (shared/file "C12-RA3d.jpg")}] [:span "PB/PE Um fonema vocálico nasaliza-se diante do arquifonema " [ipa "/"] [ipa-like "N"] [ipa "/"] " " [ipa "[àZÔR]"] "."]]


           [[shared/image {:full-url (shared/file "C12-RA3e.jpg")}] [:span "PB/PE 1) O fonema " [ipa "/u/"] " realiza-se como " [ipa "[w]"] " quando átona diante de uma vogal nuclear " [ipa "[kwÒtRu]"] ". 2) Realiza-se como " [ipa "[ä]"] " quando átona depois de uma vogal nuclear " [ipa "[kÒUzA]"] ". 3) Nasaliza-se em " [ipa "[à]"] " diante do arquifonema " [ipa "/"] [ipa-like "N"] [ipa "/"] " seguido ou não de " [ipa "/"] [ipa-like "S"] [ipa "/"] " em posição final de sílaba " [ipa "[komê]"] ". 5) Realiza-se como " [ipa "[u]"] " nos outros lugares " [ipa "[uRubÖ]"] "."]]


           [[shared/image {:full-url (shared/file "C12-RA3f.jpg")}] [:span "PB/PE 1) Uma vogal pode ensurdecer-se entre uma consoante surda (seguida ou não de uma líquida também ensurdecida) e uma pausa (precedida ou não de " [ipa "/"] [ipa-like "S"] [ipa "/"] "). 2) Pode ensurdecer-se também quando átona entre duas consoantes surdas " [ipa "[polÔÎÊkÍ]"] ". 3) A vogal epentética inserida entre uma consoante surda e outra consoante pode ensurdecer-se " [ipa "[ÒpÊtu]"] "."]]]]
      [:div
       (into [:ol.list.is-lower-alpha]
             (for [[image text] items]
               [:li.list-item [:div.grid2.red
                               image
                               text]]))])]])

(defn ra4
  "Respostas Activity 4"
  []
  [:section.ra4
   [:li "Quais as diferenças entre as regras de distribuição dos seguintes fonemas para o Brasil e para Portugal. Dê exemplos."
    (let [items [[[ipa "/a/"]
                  [:span.red "1) Há nasalização e alçamento da vogal tônica em sílabas abertas diante de um fonema nasal " [ipa "/m n ¦/"] " no Brasil " [ipa "[këmA]"] "; em Portugal há só alçamento sem nasalização " [ipa "[kA1mA]"] ". 2) No Brasil há alçamento só em posição átona final " [ipa "[akamÒdA]"] "; em Portugal o alçamento costuma ocorrer em toda posição átona " [ipa "[AkAmÒdA]"] "."]]

                 [[ipa "/i/"]
                  [:span.red "1) Quando átona diante de uma vogal tônica há hiato " [ipa "[pi.ë.nu]"] "; em Portugal costuma formar-se um ditongo crescente " [ipa "[pjA1.nu]"] ". 2) No Brasil a vogal átona não final é " [ipa "[i]"] " " [ipa "[aËivÔR]"] "; em Portugal, costuma ser " [ipa "[š]"] " " [ipa "[AdšvÔ³]"] ", porém há exceções " [ipa "[akamÒdA]"] "."]]

                 [[ipa "/o/"]
                  [:span.red "1) No Brasil a vogal átona não final é " [ipa "[o]"] " " [ipa "[on×stu]"] "; em Portugal costuma ser " [ipa "[u][un×Stu]"] ", porém, há exceções " [ipa "[vOsÓ]"] "."]]

                 [[ipa "/e/"]
                  [:span.red "1) No Brasil a vogal " [ipa "/e/"] " átona em posição inicial de palavra realiza-se como " [ipa "[e]"] " " [:span.ipa "[elemç" [:sup "ª"] "tu]"] " " [ipa "[eskØlA]"] "; em Portugal pode realizar-se como " [ipa "[i]"] " " [:span.ipa "[ilšmç" [:sup "ª"] "tu]"] " ou diante do arquifonema " [ipa "/" [ipa-like "S"] [ipa "/"] " pode eliminar-se " [ipa "[SkØlA]"] ". 2) Em posição átona não final no Brasil ª realiza-se como " [ipa "[e]"] " " [:span.ipa "[elemç" [:sup "ª"] "tu]"] ", em Portugal, como " [ipa "[š]"] " " [:span.ipa "[ilšmç" [:sup "ª"] "tu]"] ". 3) No Brasil, quando átona diante de outra vogal tônica, forma-se uma sinérese " [ipa "[pele`ÒR]"] "; em Portugal, um ditongo crescente " [ipa "[pšljÒ³]"] ". 4) No Brasil, " [:span.ipa "/Ó"] [ipa-like "N"] [ipa "/"] " realiza-se como " [ipa "[çã]"] " " [:span.ipa "[tá" [:sup "m"] "bçã]"] "; em Portugal, como " [ipa "[ëã]"] " " [:span.ipa "[tá" [:sup "m"] "bëã]"] ". 5) No Brasil, realiza-se como " [ipa "[e]"] " diante de consoantes palatais " [ipa "[tÓYA]"] " " [ipa "[bÓIZu]"] "; em Portugal, como " [ipa "[A]"] " " [ipa "[tA1YA]"] " " [ipa "[bA1IZu]"] "."]]

                  [[ipa "/E/"]
                   [:span.red "1) No Brasil a vogal " [ipa "[E]"] " sempre se reduz à vogal " [ipa "[e]"] " em posição átona " [ipa "[h×KvA]"] " " [ipa "[heKvÒdu]"] "; em Portugal há casos de não redução " [ipa "[±×LvA]"] " " [ipa "[±ElvÒdu]"] ". 2) No Brasil, a forma plural de substantivos terminados em " [ipa "/×l/"] " formam-se com " [ipa "[×Is]"] " " [ipa "[pap×Is]"] "; em Portugal, com " [ipa "[A1IS]"] " " [ipa "[pApA1IS]"] "."]]]]]
      [:div
       (into [:ol.list.is-lower-alpha]
             (for [[phon text] items]
               [:li.list-item [:div.grid2
                               phon
                               text]]))])]])

(defn render-respostas
  "Chapter 12 respostas"
  []
  [:main.c12.respostas.content
   [shared/fonetica-title "Capítulo 12 — Os fonemas vocálicos"
    "Respostas para o eResource"]
   [:section.atividades
    [:h2 "ATIVIDADES"]
    [:ol.atividades.list
     [ra1]
     [ra2]
     [ra3]
     [ra4]]]])


(defn a1
  "Activity 1"
  []
  [:section.a1
   [:li "Escreva o símbolo do alofone descrito pelos seguintes traços."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c12-r) :same-session]
    (let [items [[["meio-fechada"
                   "anterior"
                   "oral/sonora"]
                  "[e]"]

                 [["aberta"
                   "central"
                   "oral/sonora"]
                  "[a]"]

                 [["fechada"
                   "posterior"
                   "oronasal/sonora"]
                  "[à]"]

                 [["semi-vogal"
                   "posterior"
                   "oral/sonora"]
                  "[U]"]

                 [["meio-aberta"
                   "posterior"
                   "oral/sonora"]
                  "[O]"]

                 [["fechada"
                   "central"
                   "oral/ensurdecida"]
                  "[Ë]"] ;; wrong char rendered

                 [["semi-consoante"
                   "anterior"
                   "oral/sonora"]
                  "[j]"]

                 [["meio-aberta"
                   "central"
                   "oral/ensurdecida"]
                  "[È]"] ;; wrong char rendered

                 [["meio-aberta"
                   "anterior"
                   "oral/sonora"]
                  "[E]"]

                 [["semi-consoante"
                   "posterior"
                   "oral/sonora"]
                  "[w]"]] ]
      
      [:ol.list.is-lower-alpha
       (into [:div.grid2.parent]
             (for [[l _] items]
               [:li
                [:div.grid2 (into [:div.l]
                                  (for [i l]
                                    [:div i]))
                 [:div.r.ipa2.red.is-size-4]]]))])]])

(defn a2
  "Activity 2"
  []
  [:section.a2
   [:li "Indique os traços fonéticos para os seguintes alofones."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c12-r) :same-session]
    (let [items
          [["[w]"
            ["semi-consoante"
             "posterior"
             "oral/sonora"]]
           ["[I]"
            ["semi-vogal"
             "anterior"
             "oral/sonora"]]
           ["[E]"
            ["meio-aberta"
             "anterior"
             "oral/sonora"]]
           ["[ß]"
            ["meio-fechada"
             "posterior"
             "oronasal/sonora"]]
           ["[É]"
            ["meio-fechada"
             "anterior"
             "oral/ensurdecida"]]
           ["[a]"
            ["aberta"
             "central"
             "oral/sonora"]]
           ["[à]"
            ["fechada"
             "posterior"
             "oronasal/sonora"]]
           ["[Þ]"
            ["fechada"
             "anterior"
             "oronasal/sonora"]]
           ["[j]"
            ["semi-consoante"
             "anterior"
             "oral/sonora"]]
           ["[Ý]"
            ["meio-fechada"
             "anterior"
             "oronasal/sonora"]]]
          ]
      [:div.grid2.parent
       [:ol.list.is-lower-alpha
        (into [:div.grid2.parent]
              (for [[l _] items]
                [:li
                 [:div.grid2
                  [:div.l.ipa2 l]
                  [:div.empty]]]))]])]])

(defn a3
  "Activity 3"
  []
  [:section.a3
   [:li "Para as seguintes regras de distribuição: (a) indique se a regra é para o PB ou PE, (b) interprete a regra em palavras e (c) dê exemplos de sua aplicação, tendo cuidado para exemplificar todos os aspectos da regra."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c12-r) :same-session]
    (let [items
          [[[shared/image {:full-url (shared/file "C12-RA3a.jpg")}] [:span "PB 1) O fonema " [ipa "/a/"] " alça-se a " [ipa "[A]"] " quando átona em posição final de palavra " [ipa "[kÒzA]"] ".  2) Alça-se e nasaliza-se a " [ipa "[á]"] " diante do arquifonema " [ipa "/"] [ipa-like "N"] [ipa "/"] " seguido ou não de " [ipa "/"] [ipa-like "S"] [ipa "/"] " em posição final de sílaba " [ipa "[ësjA]"] ". 3) Alça-se e nasaliza-se a " [ipa "[ë]"] " quando tônica diante de uma consoante nasal no começo da próxima sílaba " [ipa "[këmA]"] ". 4) Realiza-se como " [ipa "[a]"] " nos outros lugares " [ipa "[kÒbu]"] " " [ipa "[akademÔA]"] "."]]

           [[shared/image {:full-url (shared/file "C12-RA3b.jpg")}] [:span "PE 1) O fonema /o/ realiza-se como " [ipa "[w]"] " quando átono diante de uma vogal tônica, formando assim um ditongo crescente " [ipa "[kwÒtRu]"] ". 2) Alça-se à vogal fechada " [ipa "[u]"] " quando átona " [ipa "[uSpitÒL]"] " " [ipa "[kÒ³u]"] ". 3) Alça-se e nasaliza-se como semi-vogal " [ipa "[ä]"] " entre uma vogal oronasal e o arquifonema /N/, formando assim um ditongo oronasal decrescente " [ipa "[mëä]"] ". 4) Nasaliza-se diante do arquifonema " [ipa "/"] [ipa-like "N"] [ipa "/"] " seguido ou não de " [ipa "/"] [ipa-like "S"] [ipa "/"] " " [ipa "[késtA]"] ". quando tônica diante de uma consoante nasal no começo da próxima sílaba " [ipa "[këmA]"] ". 5) Realiza-se como " [ipa "[o]"] " nos outros lugares " [ipa "[kÕRu]"] "."]]

           [[shared/image {:full-url (shared/file "C12-RA3c.jpg")}] [:span "PB 1) O fonema " [ipa "/e/"] " realiza-se como " [ipa "[j]"] " quando átona em posição final de palavra diante de uma vogal nuclear em posição inicial de palavra " [ipa "[kwÒzjÕItu]"] ". 2) Alça-se a " [ipa "[i]"] " quando átona em posição final de palavra " [ipa "[klÒsis]"] ". 3) Alça-se e nasaliza-se como semi- vogal " [ipa "[ã]"] " entre uma vogal oronasal e o arquifonema /N/, formando assim um ditongo oronasal decrescente " [ipa "[mëã]"] ".  4) Ditonga-se e nasaliza-se em " [ipa "[Ýã]"] " diante do arquifonema " [ipa "/"] [ipa-like "N"] [ipa "/"] " seguido ou não de " [ipa "/"] [ipa-like "S"] [ipa "/"] " em posição final de palavra " [ipa "[vçã]"] ". 5) Nasaliza-se a " [ipa "[Ý]"] " diante do arquifonema " [ipa "/"] [ipa-like "N"] [ipa "/"] " seguido ou não de " [ipa "/"] [ipa-like "S"] [ipa "/"] " " [ipa "[çSi]"] ". 6) Realiza-se como " [ipa "[e]"] " nos outros lugares " [ipa "[hesebÓU]"] "."]]

           [[shared/image {:full-url (shared/file "C12-RA3d.jpg")}] [:span "PB/PE Um fonema vocálico nasaliza-se diante do arquifonema /N/ " [ipa "[àZÔR]"] "."]]


           [[shared/image {:full-url (shared/file "C12-RA3e.jpg")}] [:span "PB/PE 1) O fonema " [ipa "/u/"] " realiza-se como " [ipa "[w]"] " quando átona diante de uma vogal nuclear " [ipa "[kwÒtRu]"] ". 2) Realiza-se como " [ipa "[ä]"] " quando átona depois de uma vogal nuclear " [ipa "[kÒUzA]"] ". 3) Nasaliza-se em " [ipa "[à]"] " diante do arquifonema " [ipa "/"] [ipa-like "N"] [ipa "/"] " seguido ou não de " [ipa "/"] [ipa-like "S"] [ipa "/"] " em posição final de sílaba " [ipa "[komê]"] ". 5) Realiza-se como " [ipa "[u]"] " nos outros lugares " [ipa "[uRubÖ]"] "."]]


           [[shared/image {:full-url (shared/file "C12-RA3f.jpg")}] [:span "PB/PE 1) Uma vogal pode ensurdecer-se entre uma consoante surda (seguida ou não de uma líquida também ensurdecida) e uma pausa (precedida ou não de /S/). 2) Pode ensurdecer-se também quando átona entre duas consoantes surdas " [ipa "[polÔÎÊkÍ]"] ". 3) A vogal epentética inserida entre uma consoante surda e outra consoante pode ensurdecer-se " [ipa "[ÒpÊtu]"] "."]]]]
      [:div
       (into [:ol.list.is-lower-alpha]
             (for [[image _text] items]
               [:li.list-item [:div.grid2.red
                               image
                               [:div.empty]]]))])]])

(defn a4
  "Activity 4"
  []
  [:section.a4
   [:li "Quais as diferenças entre as regras de distribuição dos seguintes fonemas para o Brasil e para Portugal. Dê exemplos."
    [shared/media-icon :r (rfe/href :fonetica-port.routes/c12-r) :same-session]
    (let [items [[[ipa "/a/"]
                  [:span.red "1) Há nasalização e alçamento da vogal tônica em sílabas abertas diante de um fonema nasal /m n ¦/ no Brasil " [ipa "[këmA]"] "; em Portugal há só alçamento sem nasalização " [ipa "[kA1mA]"] ". 2) No Brasil há alçamento só em posição átona final " [ipa "[akamÒdA]"] "; em Portugal o alçamento costuma ocorrer em toda posição átona " [ipa "[AkAmÒdA]"] "."]]

                 [[ipa "/i/"]
                  [:span.red "1) Quando átona diante de uma vogal tônica há hiato " [ipa "[pi.ë.nu]"] "; em Portugal costuma formar-se um ditongo crescente " [ipa "[pjA1.nu]"] ". 2) No Brasil a vogal átona não final é " [ipa "[i]"] " " [ipa "[aËivÔR]"] "; em Portugal, costuma ser " [ipa "[š]"] " " [ipa "[AdšvÔ³]"] ", porém há exceções " [ipa "[akamÒdA]"] "."]]

                 [[ipa "/o/"]
                  [:span.red "1) No Brasil a vogal átona não final é " [ipa "[o]"] " " [ipa "[on×stu]"] "; em Portugal costuma ser " [ipa "[u][un×Stu]"] ", porém, há exceções " [ipa "[vOsÓ]"] "."]]

                 [[ipa "/e/"]
                  [:span.red "1) No Brasil a vogal " [ipa "/e/"] " átona em posição inicial de palavra realiza-se como " [ipa "[e]"] " " [:span.ipa "[elemç" [:sup "ª"] "tu]"] " " [ipa "[eskØlA]"] "; em Portugal pode realizar-se como " [ipa "[i]"] " " [:span.ipa "[ilšmç" [:sup "ª"] "tu]"] " ou diante do arquifonema /S/ pode eliminar-se " [ipa "[SkØlA]"] ". 2) Em posição átona não final no Brasil ª realiza-se como " [ipa "[e]"] " " [:span.ipa "[elemç" [:sup "ª"] "tu]"] ", em Portugal, como " [ipa "[š]"] " " [:span.ipa "[ilšmç" [:sup "ª"] "tu]"] ". 3) No Brasil, quando átona diante de outra vogal tônica, forma-se uma sinérese " [ipa "[pele`ÒR]"] "; em Portugal, um ditongo crescente " [ipa "[pšljÒ³]"] ". 4) No Brasil, " [:span.ipa "/ÓN/"] " realiza-se como " [ipa "[çã]"] " " [:span.ipa "[tá" [:sup "m"] "bçã]"] "; em Portugal, como " [ipa "[ëã]"] " " [:span.ipa "[tá" [:sup "m"] "bëã]"] ". 5) No Brasil, realiza-se como " [ipa "[e]"] " diante de consoantes palatais " [ipa "[tÓYA]"] " " [ipa "[bÓIZu]"] "; em Portugal, como " [ipa "[A]"] " " [ipa "[tA1YA]"] " " [ipa "[bA1IZu]"] "."]]

                 [[ipa "/E/"]
                  [:span.red "1) No Brasil a vogal " [ipa "[E]"] " sempre se reduz à vogal " [ipa "[e]"] " em posição átona " [ipa "[h×KvA]"] " " [ipa "[heKvÒdu]"] "; em Portugal há casos de não redução " [ipa "[±×LvA]"] " " [ipa "[±ElvÒdu]"] ". 2) No Brasil, a forma plural de substantivos terminados em " [ipa "/×l/"] " formam-se com " [ipa "[×Is]"] " " [ipa "[pap×Is]"] "; em Portugal, com " [ipa "[A1IS]"] " " [ipa "[pApA1IS]"] "."]]]]
      [:div
       (into [:ol.list.is-lower-alpha]
             (for [[phon _text] items]
               [:li.list-item [:div.grid2
                               phon
                               [:div.empty]]]))])]])


(defn atividades
  "Chapter 12 atividades"
  []
  [:section.atividades.content
   [:h2 "ATIVIDADES"]
   [:ol.atividades
    [a1]
    [a2]
    [a3]
    [a4]]])

(defn recursos
  "Materials"
  []
  [:section.recursos
   [:h3 "RECURSOS ELETRÔNICOS"]
   
   (let [items [[:span "A oposição do fonema " [ipa "/i/"] " com as demais vogais."]
                [:span "Os alofones " [ipa "[j I]"] " do fonema " [ipa "/i/"] " no Brasil."]
                [:span "A nasalização do fonema " [ipa "/i/"] " no Brasil."]
                [:span "O alofone " [ipa "[i]"] " do fonema " [ipa "/i/"] " e a vogal epentética no Brasil."]
                [:span "O ensurdecimento do fonema " [ipa "/i/"] " no Brasil."]
                [:span "O ensurdecimento da vogal epentética [Ê] no Brasil."]
                [:span "Os alofones " [ipa "[j I]"] " do fonema " [ipa "/i/"] " em Portugal."]
                [:span "A centralização do fonema " [ipa "/i/"] " em Portugal e palavras anômalas."]
                [:span "A redução temporal da vogal epentética em Portugal."]
                [:span "A nasalização do fonema " [ipa "/i/"] " em Portugal."]
                [:span "O alofone " [ipa "[i]"] " do fonema " [ipa "/i/"] " em Portugal."]
                [:span "O ensurdecimento do fonema " [ipa "/i/"] " em Portugal."]
                [:span "Comparação entre os alofones do fonema " [ipa "/i/"] " no Brasil e em Portugal."]
                [:span "A oposição do fonema " [ipa "/u/"] " com as demais vogais."]
                [:span "A pronúncia de palavras grafadas com {gu} ou {qu}."]
                [:span "O fonema " [ipa "/u/"] " tônico e átono em posição não final."]
                [:span "O fonema " [ipa "/u/"] " tônico e átono em posição final."]
                [:span "Os alofones " [ipa "w U"] " do fonema " [ipa "/u/"] " no Brasil."]
                [:span "A nasalização do fonema " [ipa "/u/"] " no Brasil."]
                [:span "O alofone " [ipa "[u]"] " do fonema " [ipa "/u/"] "."]
                [:span "O ensurdecimento do fonema " [ipa "/u/"] " no Brasil."]
                [:span "Os alofones " [ipa "[w U]"] " do fonema " [ipa "/u/"] " em Portugal."]
                [:span "A nasalização do fonema " [ipa "/u/"] " em Portugal."]
                [:span "O alofone " [ipa "[u]"] " do fonema " [ipa "/u/"] " em Portugal."]
                [:span "O ensurdecimento do fonema " [ipa "/u/"] " em Portugal."]
                [:span "Comparação entre os alofones do fonema " [ipa "/u/"] " no Brasil e em Portugal."]
                [:span "O fonema " [ipa "/e/"] " em posição final de palavra no Brasil: alçamento em " [ipa "[i]"] " ou ditongação em " [ipa "[j]"] "."]
                [:span "A nasalização do fonema " [ipa "/e/"] " no Brasil."]
                [:span "O ensurdecimento do fonema " [ipa "/e/"] " no Brasil."]
                [:span "O fonema " [ipa "/e/"] " em posição final de palavra em Portugal: alçamento e centralização em " [ipa "[š]"] " e realização de zero fonético."]
                [:span "O alofone " [ipa "[š]"] " do fonema " [ipa "/e/"] " em posição interior de palavra em Portugal."]
                [:span "O alofone " [ipa "[i]"] " do fonema " [ipa "/e/"] " em posição inicial de palavra em Portugal."]
                [:span "O fonema " [ipa "/e/"] " em posição átona não final de palavra em Portugal: outras opções."]
                [:span "O alofone  " [ipa "[j]"] " do fonema " [ipa "/e/"] " em Portugal seguido de vogal."]
                [:span "A nasalização do fonema " [ipa "/e/"] " em Portugal."]
                [:span "O alofone " [ipa "[A]"] " do fonema " [ipa "/e/"] " em Portugal."]
                [:span "A realização do fonema " [ipa "/e/"] " em posição inicial de palavra seguido do arquifonema " [ipa "/"] [ipa-like "S"] [ipa "/"] " em Portugal."]
                [:span "O ensurdecimento do fonema " [ipa "/e/"] " em Portugal."]
                [:span "Comparação entre os alofones do fonema " [ipa "/e/"] " no Brasil e em Portugal."]
                [:span "O fonema " [ipa "/o/"] " em posição final de palavra no Brasil: alçamento em " [ipa "[u]"] " ou ditongação em " [ipa "[w]"] "."]
                [:span "A nasalização do fonema " [ipa "/o/"] " no Brasil."]
                [:span "Ensurdecimento do fonema " [ipa "/o/"] " no Brasil."]
                [:span "O fonema " [ipa "/o/"] " em posição final de palavra em Portugal: alçamento em " [ipa "[u]"] "."]
                [:span "O fonema " [ipa "/o/"] " em posição átona interior de palavra em Portugal: alçamento em " [ipa "[u]"] "."]
                [:span "O fonema " [ipa "/o/"] " em posição átona inicial de palavra em Portugal: alçamento em " [ipa "[u]"] "."]
                [:span "O fonema " [ipa "/o/"] " em posição átona não final de palavra em Portugal: outras opções."]
                [:span "O alofone " [ipa "[w]"] " do fonema " [ipa "/o/"] " em Portugal seguido de vogal."]
                [:span "A nasalização do fonema " [ipa "/o/"] " em Portugal."]
                [:span "O ensurdecimento do fonema " [ipa "/o/"] " em Portugal."]
                [:span "Comparação entre os alofones do fonema " [ipa "/o/"] " no Brasil e em Portugal."]
                [:span "O alofone " [ipa "[E]"] " do fonema " [ipa "/E/"] " no Brasil e em Portugal."]
                [:span "O alofone " [ipa "[A]"] " do fonema " [ipa "/E/"] " em Portugal."]
                [:span "Comparação entre os alofones do fonema " [ipa "/E/"] " no Brasil e em Portugal."]
                [:span "O alofone " [ipa "[O]"] " do fonema " [ipa "/O/"] " no Brasil e em Portugal."]
                [:span "Comparação entre os alofones do fonema " [ipa "/O/"] " no Brasil e em Portugal."]
                [:span "A oposição entre " [ipa "/O/"] " e " [ipa "/‚/"] " em inglês."]
                [:span "A pronúncia do som " [ipa "[O]"] " em posição não final."]
                [:span "A pronúncia do som " [ipa "[O]"] " em posição final."]
                [:span "Os alofones do fonema " [ipa "/a/"] " no Brasil."]
                [:span "O ensurdecimento do fonema " [ipa "/a/"] " no Brasil."]
                [:span "Os alofones do fonema " [ipa "/a/"] " em Portugal."]
                [:span "A manutenção do som " [ipa "[a]"] " por motivos fonológicos em Portugal."]
                [:span "A manutenção do som " [ipa "[a]"] " por motivos morfológicos em Portugal."]
                [:span "A manutenção do som " [ipa "[a]"] " por motivos lexicais/grafêmicos em Portugal."]
                [:span "O contraste verbal entre o presente " [ipa "[fAlA1mus]"] " e o pretérito " [ipa "[fAlÒmus]"] " em Portugal."]
                [:span "Diferenças no alçamento da vogal " [ipa "/a/"] " entre o Brasil e Portugal."]
                [:span "Comparação entre os alofones do fonema " [ipa "/a/"] " no Brasil e em Portugal."]
                [:span "Ensurdecimento em posição final de palavra."]
                [:span "Ensurdecimento em posição interior de palavra."]]
         ex-items [[:span "Exercícios de pronúncia: a vogal " [ipa "/i/"] "."]
                   [:span "Exercícios de pronúncia: a vogal " [ipa "/u/"] "."]
                   [:span "Exercícios de pronúncia: a vogal " [ipa "/e/"] "."]
                   [:span "Exercícios de pronúncia: a vogal " [ipa "/o/"] "."]
                   [:span "Exercícios de pronúncia: a vogal " [ipa "/E/"] "."]
                   [:span "Exercícios de pronúncia: a vogal " [ipa "/O/"] "."]
                   [:span "Exercícios de pronúncia: a vogal " [ipa "/a/"] "."]]
         
         fonetica-line-item (fn [n i]
                              [:li [shared/media-icon :audio
                                    (shared/file (str "C12-M" (inc n) ".mp3"))] i])
         fonetica-line-item2 (fn [n i]
                               [:li [shared/media-icon :ex
                                     (shared/file (str "C12-M" (+ 70 n) ".mp3"))] i])]
     (into
      [:ol]
      (conj 
       (map-indexed fonetica-line-item2 ex-items)
       (map-indexed fonetica-line-item items))))])

(defn render
  "Chapter twelve view"
  []
  [:main.chapter.container
   [shared/fonetica-title "Capítulo 12 — Os fonemas vocálicos"
    "eResource"]
   [:div.content.chapter-12
    [recursos]
    [atividades]]
   [shared/footer]])
