;; -*- eval: (rainbow-mode) -*-
(ns fonetica-port.views.styles
  "Instead of loading a .css file, use css-in-cljs to load these straight into the page.
  See https://tech.toryanderson.com/2020/03/14/my-garden-css-has-ascended/"
  (:require [garden.core :as garden :refer [css]]
            [garden.stylesheet :as gss]
            [garden.selectors :as s]
            [garden.units :as u :refer [in px em rem percent]]))

(defn url
  "Make a URL css function"
  [s]
  (str "url('" s "')"))

(defn file-url
  "get the filepath for `filename` and put it in a css url function `url()`"
  [file-name]
  (let [file (str "/Files/" file-name) ]
    (url file)))

(defn nav
  "Navbar style"
  []
  [:nav#tsa-nav
   [:.title {:margin-right (em 1)} ]
   [:.navbar-start
    [:.navbar-item {:font-size (rem 1.2)
                    :font-weight 900
                    :letter-spacing (em 0.1)}]]])

(defn footer
  []
  [:footer.footer
   [:.copyright-me {:text-align :center}]])

(defn accordions
  "Styles for the accordions "
  []
  [:div.zippy-container
   [:.goog-zippy-header {:margin-bottom 0}
    [:&:hover {:background "#Add8e6"
               :cursor :pointer}]
    [:&:after {:margin-left (em 1)
               :text-decoration :none} ]
    [:&.goog-zippy-collapsed:after {:content "\"↓\""}]
    [:&.goog-zippy-expanded:after {:content "\"↑\""}]]
   [:.goog-zippy-content {:transition-property :all
                          :transition-duration "0.5s"
                          :transition-timing-function :ease
                          :transition-delay "0.5s"}]])
(defn chapter-1 []
  [:.chapter-1 {:font-weight :bold
                        :font-family "'Humanst531 Blk BT', sans-serif"
                        :font-size (rem 1.5)
                        }
   
   [:p.left {:text-indent (in -0.25)
             :margin-left (in 0.25)}]
   [:.WPCharBoxWrapper {:width (px 156)
                        :vertical-align "text-top"}
    [:&.wide {:width (px 600)}]
    [:.WPCharBox {:border :none}]
    ]])

(defn fonetica-tory []
  "Styles added by Tory after Devin"
  [:.content ;
   ;[:a:hover {:border-color :blue}]
   [:li {:margin-top (em 1)}]
   [:p {:text-indent (em 3)}]
   [:h2 :h3 {:text-align :left
             :text-decoration :underline}]
   [:.subtext {:text-decoration :none
                 :text-align :center
                 :margin-top (em -0.5)}]
   [:section.e-resources
    [:h2 {:text-align :center
          :text-decoration :none}]]
   [:section {:margin-top (em 2)
              :margin-bottom 0}
    [:h4 {:margin-bottom (em 0.1)
          :text-decoration :underline
          :font-style :italic}]]

   [:img.ficon :img.fimage
    {:display :inline-block
     :border-style :solid
     :border-width (px 3)
     :border-color :transparent     
     :vertical-align :middle}
    [:&:hover {:border-color :blue}]]
   [:img.ficon {:width (em 1.5)}]

   #_["li.ficon::before"
    {"display" "block"
     "margin-right" "12px"
     "content" "\"\""
     "background-size" "cover"
     "width" "19px"
     "height" "15px"
     "float" "left"}]

   [:div.photogrid
    {:display :grid
     :grid-template-columns "repeat(3, 1fr)"
     :gap 0
     :grid-auto-rows "minmax(10px, auto)"
     :margin-top (em 2)}
    
    [:div.image-container
     {:border-style :solid
      :padding (em 1)
      :border-width (px 1)
      :border-color :black
      :display :flex ;; what does display flex do, that it makes this work?
      :justify-content :center
      :align-items :center
      :min-height (em 4)}
     [:a.ficon {:width (em 3)
                :display :block} 
      [:img {:width (percent 100)
             :height (percent 100)}
       [:&:hover {:border-color :blue}]]]
     ]
    
    ]])


(defn fonetica-devin
  "Styles drafted by Devin Asay"
  []
  (let [resources {:sound-icon (file-url "audio-icon.png")
	           :image-icon (file-url "image-icon.png")
	           :video-icon (file-url "video-icon.png")
	           :watermark-image (file-url "watermark-map.png")}]
    [["body" {"font-size" "16px"}] 
     ["h1, h2, h3" {"font-family" "\"Humanst531 Blk BT\", sans-serif", "text-align" "center"}] 
     ["h1, h2" {"font-size" "2em"}] 
     ["h3" {"font-size" "1.5em"}] 
     ["h1 + h2" {"margin-top" "-0.65em"}] 
     ["h2 + h3" {"margin-top" "-1em"}] 
     ["main.chapter" {:background-repeat :no-repeat
        	      :background-size :contain
	              :background-attachment :fixed
                      :background-position :center
                      :background-image (file-url "watermark-map.png")}
      [:.print-button  {:display :none}]] 
     ["span.ElCid" {"font-family" "\"Cid\", serif"
                    "font-size" "1em"}] 
     ["ol" {"padding-left" "24px"}]
     ["li.ficon::before" {"display" "block"
                          "margin-right" "12px"
                          "content" "\"\""
                          "background-size" "cover"
                          "width" "19px"
                          "height" "15px"
                          "float" "left"}]
     ["li.image-icon::before" {"background-image" (file-url "image-icon.png")}] 
     ["li.video-icon::before" {"background-image" (file-url "video-icon.png")}] 
     ["li.audio-icon::before" {"background-image" (file-url "audio-icon.png")}] 
     ["li.text-icon::before" {"background-image" (file-url "text-icon.png")}] 
     ["li.ej-icon::before" {"background-image" (file-url "EJ-icon.png")}] 
     ["li.r-icon::before" {"background-image" (file-url "R-icon.png")}] 
     ["li.ex-icon::before" {"background-image" (file-url "EX-icon.png")}] 
     ["li.text-ej-icon::before" {"background-image" (file-url "text-EJ-icon.png")}] 
     ["span.image-icon" {"display" "inline-block"
                         "margin-right" "12px"
                         "content" "\"\""
                         "background-image" (file-url "image-icon.png")
                         "background-size" "cover"
                         "width" "19px"
                         "height" "15px"}] 
     ["span.video-icon" {"display" "inline-block"
                         "margin-right" "12px"
                         "content" "\"\""
                         "background-image" (file-url "video-icon.png")
                         "background-size" "cover"
                         "width" "19px"
                         "height" "15px"}] 
     ["span.audio-icon" {"display" "inline-block"
                         "margin-right" "12px"
                         "content" "\"\""
                         "background-image" (file-url "audio-icon.png")
                         "background-size" "cover"
                         "width" "19px"
                         "height" "15px"}] 
     ["span.text-icon" {"display" "inline-block"
                        "margin-right" "12px"
                        "content" "\"\""
                        "background-image" (file-url "text-icon.png")
                        "background-size" "cover"
                        "width" "19px"
                        "height" "15px"}] 
     ["span.ej-icon" {"display" "inline-block"
                      "margin-right" "12px"
                      "content" "\"\""
                      "background-image" (file-url "EJ-icon.png")
                      "background-size" "cover"
                      "width" "19px"
                      "height" "15px"}] 
     ["span.r-icon" {"display" "inline-block"
                     "margin-right" "12px"
                     "content" "\"\""
                     "background-image" (file-url "R-icon.png")
                     "background-size" "cover"
                     "width" "19px"
                     "height" "15px"}] 
     ["span.ex-icon" {"display" "inline-block"
                      "margin-right" "12px"
                      "content" "\"\""
                      "background-image" (file-url "EX-icon.png")
                      "background-size" "cover"
                      "width" "19px"
                      "height" "15px"}] 
     ["span.text-ej-icon" {"display" "inline-block"
                           "margin-right" "12px"
                           "content" "\"\""
                           "background-image" (file-url "text-EJ-icon.png")
                           "background-size" "cover"
                           "width" "34px"
                           "height" "15px"}]]))
(defn table-of-contents []
  [:#toc
   ["a[href]" {:text-decoration :underline}]
   ["a:not([href])" {:color "#747474"
                     :text-decoration :none
                     :cursor :auto}]
   [:section {:margin-bottom (em 1)}
    [:h4 {:font-weight :bold
          :font-size (em 1.2)}]]
   [:li {:font-weight "bold"
         :margin-left (em 1)
         ;:text-decoration "underline"
         }]])

(defn at-font
  "add a garden @at-font-face item"
  [{:keys [font-family src-url font-weight]
    :as fm}]
  (let [font-map (cond-> (select-keys fm [:font-family :font-weight])
                  src-url (assoc :src (url src-url)))]
    ["@font-face" font-map]))

(defn ftable []
  "General table styles"
  [:table.ftable {:border-color "#000"
                  :background-color :white
                  :vertical-align :middle}
   [:p {:margin-top (em 1)
        :text-indent (em 1)}]   
   [:tr {:line-height (em 1.2)}
    [:th {:vertical-align :middle}]
    [:td {:border-color :black
          :vertical-align :middle
          :padding (px 3)
          :padding-left (em 0.5)}
     [:&.cid {:font-family "'Cid', serif"
             :border-right-style :solid
             :border-right-width (px 1)
              :border-right-color :black}]]]])

(defn comp-table 
  "Styles for comparison tables with lined-up text on the left and right"
  []
  [:table.comp {:border-style :solid
                :border-width (px 1)
                :border-color "#000"
                :background-color :white}
   [:p {:margin-top (em 1)
        :text-indent (em 1)}]
   [:.cid {:font-family "'Cid', serif"
           :border-right-style :solid
           :border-right-width (px 1)
           :border-right-color :black}]  
   [:tr {:line-height (em 1.2)}]
   [:td {:padding (em 1)
         :padding-left (em 0.5)
         :border :none}]
   ]
  
  )

(defn no-print
  "Styles to avoid printing things"
  []
  (gss/at-media {:print true}
                   [:.no-print ".no-print *"
                    {:display :none}])

  ;; @media print
;; {    
;;     .no-print, .no-print *
;;     {
;;         display: none !important;
;;     }
;; }

  )

(defn chapter-24
  "Special list styling for 24"
  []
  [:.chapter-24
   [:.list.ex-items
     {:margin-top (em -1)
      :list-style :none
      :padding-top 0
      :padding-left (em 2)}
    [:.list-item.exercises {:line-height 0
                            :margin 0}]]]
  )
(comment
  (css (chapter-24))
  )

(defn fonetica "Our stylesheet" []
  (css
   [(at-font {:font-family "Hum531" :src-url "/Fonts/HUM531K.TTF"}) ;; I don't know where this font file may be
    (at-font {:font-family "Cid" :src-url "/Fonts/CID.TTF"})
    (at-font {:font-family "IPA" :src-url "/Fonts/IPADSPV50.TTF"})
    (at-font {:font-family "IPA2" :src-url "/Fonts/IPADSPEC.TTF"})
    (at-font {:font-family "IPA3" :src-url "/Fonts/IPADG.TTF"})

    (let [bordered  {:border-style :solid
                     :border-width (px 1)
                     :border-color :black}
          grid2 {:display :grid
                 :grid-template-columns "repeat(2, 1fr)"
                 :gap (em 3)
                 :grid-auto-rows "minmax(10px, auto)"
                 :margin-top (em 1)}]
      [:body
       [:.list-item {:display :list-item}]
       [:em {:font-style :italic}]
       [:.bordered bordered
        ["> span" (assoc bordered :padding (em 1))]]
       [:.grid2 grid2
        [:div {:margin (em 0.5)}]
        [:&.no-gap (assoc grid2 :gap 0)]
        [:.1-3
         [:div.l {:width (em 10)}]]
        [:.b {:margin-bottom (em 0.75)}]
        [:.3-1 {:border-color "red"
                :border-style "solid"
                :border-width 1}
         [:div {:width (em 10)}]]]
       [:.grid3 {:display :grid
                  :grid-template-columns "repeat(3, 1fr)"
                  :gap (em 1)
                 :grid-auto-rows "minmax(1em, auto)"}]
       [:.grid4 {:display :grid
                  :grid-template-columns "repeat(4, 1fr)"
                  :gap (em 1)
                  :grid-auto-rows "minmax(1em, auto)"}]
       [:.boxed {:display :block}
        [:.fbox :.terminal
         {:padding-right (px 5)
          :padding-left (px 5)
          :border-style :solid
          :border-width (px 1)
          :text-align :center
          :display :inline-block
          :border-color :black}]
        [:.one-box :.terminal {:display :inline-block}]
        [:.terminal {:background-color :lightgray
                     :font-size (em 3)
                     :vertical-align :top}]

        ]
       [:img {:max-width (percent 88)}]
       [:.message.is-success
        [:.content {:color "green"
                    :font-weight 600}]]
       [:.content :table
        {:margin-left (em 2)}
        [:td {:vertical-align :middle}]
        [:.title {:white-space "nowrap"}]
        [:&.table
         [:thead {:background-color "#cacaca"}
          [:td {:font-weight :bold}]]
         [:&.is-bordered
          [:th :td {:border-color :black}]]]]
       [:.ipa {:font-family "'IPA', serif"}]
       [:.ipa2 {:font-family "'IPA2', serif"}
        [:&.red {:font-size (em 2)}]]
       [:.ipa3 {:font-family "'IPA3', serif"}]
       [:.cid {:font-family "'Cid', serif"}]
       [:.hebrew {:font-family "'Hebrew', serif"}]
       [:.normal {:font-family "'Hum531', sanserif"}]
       [:.ipa-like {:font-family "'Times New Roman', 'Times', serif"}]
       [:.grid-center {:display :grid
                       :justify-content :center}]
       [:.femp {:text-decoration :underline
                :font-weight :bold}]
       [:.red {:color :red}]
       [:.black {:color :black}]
       [:.blue {:color :blue}]
       [:.endnote {:margin-top (em 3)}]
       [:.list {:padding (em 0.5)
                :padding-left (em 2.5)                
                :background-color :white}]
       (table-of-contents)
       (chapter-1)
       (comp-table)
       (fonetica-tory)
       (ftable)
       (fonetica-devin)
       (nav)
       (accordions)
       (chapter-24)
       (no-print)
       (footer)
       ])]))

(defn clear-styles!
  "Remove existing style elements from the document <head>"
  []
  (let [styles (.getElementsByTagName js/document "style")
        style-count (.-length styles)]
    (doseq [n (range style-count 0 -1)]
      (.remove (aget styles (dec n))))))


(defn mount-style
  "Mount the style-element into the header with `style-text`, ensuring this is the only `<style>` in the doc"
  [style-text]
  (let [head (or (.-head js/document)
                 (aget (.getElementsByTagName js/document "head") 0))
        style-el (doto (.createElement js/document "style")
                   (-> .-type (set! "text/css"))
                   (.appendChild (.createTextNode js/document style-text)))]
    (clear-styles!)
    (.appendChild head style-el)))
